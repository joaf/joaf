package net.joaf.mainextensions.menu.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.mainextensions.menu.commands.handlers.CancelMenuGroupCommandHandler;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandDto(handlerClass = CancelMenuGroupCommandHandler.class)
public class CancelMenuGroupCommand extends AbstractUidCommand implements Command {
    public CancelMenuGroupCommand(String elementUid, String userUid) {
        super(elementUid, userUid);
    }
}
