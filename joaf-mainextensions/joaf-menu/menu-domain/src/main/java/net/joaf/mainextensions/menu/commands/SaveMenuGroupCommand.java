package net.joaf.mainextensions.menu.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractEntityCommand;
import net.joaf.mainextensions.menu.commands.handlers.SaveMenuGroupCommandHandler;
import net.joaf.mainextensions.menu.model.MenuGroup;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandDto(handlerClass = SaveMenuGroupCommandHandler.class)
public class SaveMenuGroupCommand extends AbstractEntityCommand<MenuGroup> implements Command {
    public SaveMenuGroupCommand(MenuGroup element, String userUid) {
        super(element, userUid);
    }
}
