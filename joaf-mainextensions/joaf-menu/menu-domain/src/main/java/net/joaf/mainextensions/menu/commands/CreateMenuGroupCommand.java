package net.joaf.mainextensions.menu.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.mainextensions.menu.commands.handlers.CreateMenuGroupCommandHandler;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandDto(handlerClass = CreateMenuGroupCommandHandler.class)
public class CreateMenuGroupCommand extends AbstractUidCommand implements Command {
    public CreateMenuGroupCommand(String userUid) {
        super(null, userUid);
    }
}
