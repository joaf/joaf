package net.joaf.mainextensions.menu.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.menu.commands.UndoTrashMenuGroupCommand;
import net.joaf.mainextensions.menu.repository.api.MenuGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class UndoTrashMenuGroupCommandHandler implements CommandHandler<UndoTrashMenuGroupCommand, Object> {
    @Autowired
    private MenuGroupRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, UndoTrashMenuGroupCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(UndoTrashMenuGroupCommand command) throws JoafException {
        repository.updateObjectState(command.getElementUid(), EObjectState.ACCEPTED);
        return null;
    }
}
