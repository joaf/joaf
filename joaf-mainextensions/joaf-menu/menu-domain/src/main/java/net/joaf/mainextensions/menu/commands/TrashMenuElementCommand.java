package net.joaf.mainextensions.menu.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.mainextensions.menu.commands.handlers.TrashMenuElementCommandHandler;

/**
 * Trash MenuElement command
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandDto(handlerClass = TrashMenuElementCommandHandler.class)
public class TrashMenuElementCommand extends AbstractUidCommand implements Command {

    public TrashMenuElementCommand(String elementUid, String userUid) {
        super(elementUid, userUid);
    }
}
