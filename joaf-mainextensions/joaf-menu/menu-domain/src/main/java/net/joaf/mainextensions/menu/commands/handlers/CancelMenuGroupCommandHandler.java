package net.joaf.mainextensions.menu.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.menu.commands.CancelMenuGroupCommand;
import net.joaf.mainextensions.menu.model.MenuGroup;
import net.joaf.mainextensions.menu.repository.api.MenuGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class CancelMenuGroupCommandHandler implements CommandHandler<CancelMenuGroupCommand, Object> {

    @Autowired
    private MenuGroupRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, CancelMenuGroupCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(CancelMenuGroupCommand command) throws JoafException {
        MenuGroup dbElement = repository.findOne(command.getElementUid());
        if (EObjectState.NEW.equals(dbElement.getObjectState())) {
            repository.remove(command.getElementUid());
        }
        return null;
    }
}
