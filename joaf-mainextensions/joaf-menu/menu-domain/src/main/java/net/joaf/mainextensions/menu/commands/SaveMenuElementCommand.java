package net.joaf.mainextensions.menu.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractEntityCommand;
import net.joaf.mainextensions.menu.commands.handlers.SaveMenuElementCommandHandler;
import net.joaf.mainextensions.menu.model.MenuElement;

/**
 * Save MenuElement command
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandDto(handlerClass = SaveMenuElementCommandHandler.class)
public class SaveMenuElementCommand extends AbstractEntityCommand<MenuElement> implements Command {
    public SaveMenuElementCommand(MenuElement element, String userUid) {
        super(element, userUid);
    }
}
