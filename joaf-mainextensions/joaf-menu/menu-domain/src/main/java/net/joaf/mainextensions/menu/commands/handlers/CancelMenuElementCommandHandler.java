package net.joaf.mainextensions.menu.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.menu.commands.CancelMenuElementCommand;
import net.joaf.mainextensions.menu.services.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Cancel edit MenuElement command handler
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class CancelMenuElementCommandHandler implements CommandHandler<CancelMenuElementCommand, Object> {

    @Autowired
    private MenuService service;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, CancelMenuElementCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(CancelMenuElementCommand command) throws JoafException {
        service.cancel(command.getElementUid());
        return null;
    }
}
