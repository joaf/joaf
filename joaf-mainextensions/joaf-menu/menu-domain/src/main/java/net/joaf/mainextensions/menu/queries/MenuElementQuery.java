package net.joaf.mainextensions.menu.queries;

import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.model.enums.EStandardAction;
import net.joaf.mainextensions.menu.model.MenuElement;
import net.joaf.mainextensions.menu.repository.api.MenuElementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@Service
public class MenuElementQuery {

    @Autowired
    private MenuElementRepository repository;

    public PageredResult<MenuElement> findAll(PageredRequest request) throws JoafDatabaseException {
        PageredResult<MenuElement> all = repository.findAll(request);
        all.getResult().forEach(this::appendActions);
        return all;
    }

    public MenuElement findOne(String uid) throws JoafDatabaseException {
        return repository.findOne(uid);
    }

    private void appendActions(MenuElement item) {
        item.getActions().add(EStandardAction.EDIT);
        item.getActions().add(EStandardAction.TRASH);
    }

    public List<MenuElement> findAllByGroup(String group) throws JoafDatabaseException {
        return new ArrayList<>(repository.findByMenuName(group).values()).stream().sorted((x, y) -> x.getOrdering().compareTo(y.getOrdering())).collect(
                Collectors.toList());
    }
}
