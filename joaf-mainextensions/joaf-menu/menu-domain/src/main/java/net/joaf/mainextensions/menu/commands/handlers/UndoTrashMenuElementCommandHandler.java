package net.joaf.mainextensions.menu.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.menu.commands.UndoTrashMenuElementCommand;
import net.joaf.mainextensions.menu.services.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Undo Trash MenuElement command handler
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class UndoTrashMenuElementCommandHandler implements CommandHandler<UndoTrashMenuElementCommand, Object> {

    @Autowired
    private MenuService service;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, UndoTrashMenuElementCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(UndoTrashMenuElementCommand command) throws JoafException {
        service.undotrash(command.getElementUid());
        return null;
    }
}
