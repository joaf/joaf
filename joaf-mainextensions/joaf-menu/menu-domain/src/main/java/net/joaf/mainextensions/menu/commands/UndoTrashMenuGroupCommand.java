package net.joaf.mainextensions.menu.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.mainextensions.menu.commands.handlers.UndoTrashMenuGroupCommandHandler;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandDto(handlerClass = UndoTrashMenuGroupCommandHandler.class)
public class UndoTrashMenuGroupCommand extends AbstractUidCommand implements Command {
    public UndoTrashMenuGroupCommand(String elementUid, String userUid) {
        super(elementUid, userUid);
    }
}
