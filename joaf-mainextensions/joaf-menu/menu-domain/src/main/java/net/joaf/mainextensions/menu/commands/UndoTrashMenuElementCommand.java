package net.joaf.mainextensions.menu.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.mainextensions.menu.commands.handlers.UndoTrashMenuElementCommandHandler;

/**
 * Undo Trash MenuElement command
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandDto(handlerClass = UndoTrashMenuElementCommandHandler.class)
public class UndoTrashMenuElementCommand extends AbstractUidCommand implements Command {
    public UndoTrashMenuElementCommand(String elementUid, String userUid) {
        super(elementUid, userUid);
    }
}
