/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.menu.services;

import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.menu.model.MenuElement;
import net.joaf.mainextensions.menu.repository.api.MenuElementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Service for menu management
 *
 * @author Cyprian Śniegota
 * @since 1.0
 */
@Service
public class MenuService {

    @Autowired
    private MenuElementRepository repository;

    public List<MenuElement> findAll() throws JoafDatabaseException {
        List<MenuElement> all = repository.findAll();
        all = all.stream().sorted((x, y) -> x.getOrdering().compareTo(y.getOrdering())).filter(x -> EObjectState.ACCEPTED.equals(x.getObjectState())).collect(Collectors.toList());
//        all.forEach(this::appendActions);
        return all;
    }

    public MenuElement findOne(String uid) throws JoafDatabaseException {
        return repository.findOne(uid);
    }

    public MenuElement create() throws JoafDatabaseException {
        MenuElement command = new MenuElement();
        command.setObjectState(EObjectState.NEW);
        command.setUid(UUID.randomUUID().toString());
        repository.insert(command);
        return command;
    }

    public void store(MenuElement command) throws JoafDatabaseException {
        MenuElement dbObject = repository.findOne(command.getUid());
        if (EObjectState.NEW.equals(dbObject.getObjectState())) {
            command.setObjectState(EObjectState.ACCEPTED);
        } else {
            command.setObjectState(dbObject.getObjectState());
        }
        repository.store(command);
    }

    public void trash(String uid) throws JoafDatabaseException {
        MenuElement one = repository.findOne(uid);
        one.setObjectState(EObjectState.TRASH);
        repository.store(one);
    }

    public MenuElement createObject() {
        return new MenuElement();
    }

    public void cancel(String uid) throws JoafDatabaseException {
        Optional<MenuElement> menuElement = repository.findOneOptional(uid);
        if (menuElement.isPresent() && EObjectState.NEW.equals(menuElement.get().getObjectState())) {
            repository.remove(uid);
        }
    }

    public void undotrash(String uid) throws JoafDatabaseException {
        MenuElement one = repository.findOne(uid);
        one.setObjectState(EObjectState.ACCEPTED);
        repository.store(one);
    }
}
