package net.joaf.mainextensions.menu.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.mainextensions.menu.commands.handlers.TrashMenuGroupCommandHandler;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandDto(handlerClass = TrashMenuGroupCommandHandler.class)
public class TrashMenuGroupCommand extends AbstractUidCommand implements Command {
    public TrashMenuGroupCommand(String elementUid, String userUid) {
        super(elementUid, userUid);
    }
}
