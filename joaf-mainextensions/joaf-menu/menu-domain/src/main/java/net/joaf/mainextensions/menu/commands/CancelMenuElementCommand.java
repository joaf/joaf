package net.joaf.mainextensions.menu.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.mainextensions.menu.commands.handlers.CancelMenuElementCommandHandler;

/**
 * Cancel edit MenuElement command
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandDto(handlerClass = CancelMenuElementCommandHandler.class)
public class CancelMenuElementCommand extends AbstractUidCommand implements Command {
    public CancelMenuElementCommand(String elementUid, String userUid) {
        super(elementUid, userUid);
    }
}
