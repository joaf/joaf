package net.joaf.mainextensions.menu;

import net.joaf.base.extension.AbstractExtension;
import net.joaf.base.extension.JoafExtension;
import org.springframework.stereotype.Component;

/**
 * Created by cyprian on 01.02.15.
 */
@Component
public class MenuExtension extends AbstractExtension implements JoafExtension {
    @Override
    public String getExtensionMetadataFile() {
        return "/extension-menu.xml";
    }
}
