package net.joaf.mainextensions.menu.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.menu.commands.TrashMenuGroupCommand;
import net.joaf.mainextensions.menu.repository.api.MenuGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class TrashMenuGroupCommandHandler implements CommandHandler<TrashMenuGroupCommand, Object> {
    @Autowired
    private MenuGroupRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, TrashMenuGroupCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(TrashMenuGroupCommand command) throws JoafException {
        repository.updateObjectState(command.getElementUid(), EObjectState.TRASH);
        return null;
    }
}
