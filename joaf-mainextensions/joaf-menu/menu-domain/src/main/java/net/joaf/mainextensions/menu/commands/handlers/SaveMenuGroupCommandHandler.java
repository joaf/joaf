package net.joaf.mainextensions.menu.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.menu.commands.SaveMenuGroupCommand;
import net.joaf.mainextensions.menu.model.MenuGroup;
import net.joaf.mainextensions.menu.repository.api.MenuGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class SaveMenuGroupCommandHandler implements CommandHandler<SaveMenuGroupCommand, Object> {

    @Autowired
    private MenuGroupRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, SaveMenuGroupCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(SaveMenuGroupCommand command) throws JoafException {
        MenuGroup dbObject = repository.findOne(command.getElement().getUid());
        if (EObjectState.NEW.equals(dbObject.getObjectState())) {
            repository.updateObjectState(command.getElement().getUid(), EObjectState.ACCEPTED);
        }
        repository.store(command.getElement());
        return null;
    }
}
