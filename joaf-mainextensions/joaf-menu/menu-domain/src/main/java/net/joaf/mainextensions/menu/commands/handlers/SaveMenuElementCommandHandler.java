package net.joaf.mainextensions.menu.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.menu.commands.SaveMenuElementCommand;
import net.joaf.mainextensions.menu.services.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Save MenuElement command handler
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class SaveMenuElementCommandHandler implements CommandHandler<SaveMenuElementCommand, Object> {

    @Autowired
    private MenuService service;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, SaveMenuElementCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(SaveMenuElementCommand command) throws JoafException {
        service.store(command.getElement());
        return null;
    }
}
