package net.joaf.mainextensions.menu.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.menu.commands.CreateMenuGroupCommand;
import net.joaf.mainextensions.menu.model.MenuGroup;
import net.joaf.mainextensions.menu.repository.api.MenuGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import java.time.LocalDateTime;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class CreateMenuGroupCommandHandler implements CommandHandler<CreateMenuGroupCommand, MenuGroup> {
    @Autowired
    private MenuGroupRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, CreateMenuGroupCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public MenuGroup execute(CreateMenuGroupCommand command) throws JoafException {
        MenuGroup menuGroup = new MenuGroup();
        menuGroup.setUid(repository.prepareId());
        menuGroup.setObjectState(EObjectState.NEW);
        LocalDateTime now = LocalDateTime.now();
        menuGroup.setCreated(now);
        menuGroup.setUpdated(now);
        menuGroup.setName("");
        menuGroup.setDescription("");
        repository.insert(menuGroup);
        return menuGroup;
    }
}
