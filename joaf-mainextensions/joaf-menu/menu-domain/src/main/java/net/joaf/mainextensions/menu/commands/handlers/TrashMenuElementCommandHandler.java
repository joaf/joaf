package net.joaf.mainextensions.menu.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.menu.commands.TrashMenuElementCommand;
import net.joaf.mainextensions.menu.services.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Trash MenuElement command handler
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class TrashMenuElementCommandHandler implements CommandHandler<TrashMenuElementCommand, Object> {

    @Autowired
    private MenuService service;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, TrashMenuElementCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(TrashMenuElementCommand command) throws JoafException {
        service.trash(command.getElementUid());
        return null;
    }
}
