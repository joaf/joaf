package net.joaf.mainextensions.menu.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.menu.commands.CreateMenuElementCommand;
import net.joaf.mainextensions.menu.model.MenuElement;
import net.joaf.mainextensions.menu.services.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Create MenuElement command handler
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class CreateMenuElementCommandHandler implements CommandHandler<CreateMenuElementCommand, MenuElement> {

    @Autowired
    private MenuService service;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, CreateMenuElementCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public MenuElement execute(CreateMenuElementCommand command) throws JoafException {
        return service.create();
    }
}
