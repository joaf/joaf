package net.joaf.mainextensions.menu.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.mainextensions.menu.commands.handlers.CreateMenuElementCommandHandler;

/**
 * Create MenuElement command
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandDto(handlerClass = CreateMenuElementCommandHandler.class)
public class CreateMenuElementCommand extends AbstractUidCommand implements Command {
    public CreateMenuElementCommand(String userUid) {
        super(null, userUid);
    }
}
