package net.joaf.mainextensions.menu.queries;

import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.model.enums.EStandardAction;
import net.joaf.mainextensions.menu.model.MenuGroup;
import net.joaf.mainextensions.menu.repository.api.MenuGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@Service
public class MenuGroupQuery {
    @Autowired
    private MenuGroupRepository repository;

    public PageredResult<MenuGroup> findAll(PageredRequest request) throws JoafDatabaseException {
        PageredResult<MenuGroup> all = repository.findAll(request);
        all.getResult().forEach(this::appendActions);
        return all;
    }

    public Map<String, String> findNameMap() throws JoafDatabaseException {
        PageredResult<MenuGroup> all = repository.findAll(new PageredRequest(EObjectState.ACCEPTED));
        return all.getResult().stream().collect(Collectors.toMap(MenuGroup::getName, MenuGroup::getDescription));
    }

    public MenuGroup findOne(String uid) throws JoafDatabaseException {
        return repository.findOne(uid);
    }

    private void appendActions(MenuGroup item) {
        item.getActions().add(EStandardAction.EDIT);
        item.getActions().add(EStandardAction.TRASH);
    }
}
