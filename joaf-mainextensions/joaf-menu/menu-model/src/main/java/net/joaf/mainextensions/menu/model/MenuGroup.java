package net.joaf.mainextensions.menu.model;

import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.db.model.StringUidEntity;
import net.joaf.base.core.model.enums.EStandardAction;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
public class MenuGroup implements StringUidEntity, Serializable {

    private String uid;
    private String name;
    private String description;
    private LocalDateTime created;
    private LocalDateTime updated;
    private EObjectState objectState;
    private List<EStandardAction> actions = new ArrayList<>();

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    public EObjectState getObjectState() {
        return objectState;
    }

    public void setObjectState(EObjectState objectState) {
        this.objectState = objectState;
    }

    public List<EStandardAction> getActions() {
        return actions;
    }
}
