/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.menu.repository.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.cassandra.AbstractCrudRepositoryCassandra;
import net.joaf.base.db.cassandra.RepositoryCassandra;
import net.joaf.mainextensions.menu.model.MenuElement;
import net.joaf.mainextensions.menu.repository.api.MenuElementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Cyprian.Sniegota on 24.01.14.
 */
@Repository
public class MenuElementRepositoryCassandra extends AbstractCrudRepositoryCassandra<MenuElement> implements MenuElementRepository, RepositoryCassandra {

    private static final Logger logger = LoggerFactory.getLogger(MenuElementRepositoryCassandra.class);

    @Override
    public String prepareId() {
        return RepositoryCassandra.super.prepareId();
    }

    protected MenuElement bindRow(Row row) {
        MenuElement menuElement = new MenuElement();
        menuElement.setUid(row.getUUID("uid").toString());
        menuElement.setMenuName(row.getString("menuName"));
        menuElement.setName(row.getString("name"));
        menuElement.setUrl(row.getString("url"));
        menuElement.setDisplayName(row.getString("displayName"));
        menuElement.setExtraStyleClass(row.getString("extraStyleClass"));
        menuElement.setIconName(row.getString("iconName"));
        menuElement.setAccessRole(row.getString("accessRole"));
        menuElement.setParentName(row.getString("parentName"));
        menuElement.setOrdering(row.getInt("ordering"));
        return menuElement;
    }

    @Override
    public String collectionName() {
        return "menuelement";
    }

    @Override
    public Class<MenuElement> getEntityClass() {
        return MenuElement.class;
    }

    public Map<String, MenuElement> findByMenuName(String menuName) throws JoafDatabaseException {
        try {
            Map<String, MenuElement> menuElementMap = new HashMap<>();
            Session session = sessionProvider.connect();
            ResultSet rs = session.execute("select * from menuelement where menuName = ?", menuName);
            List<Row> all = rs.all();
            for (Row row : all) {
                MenuElement menuElement = this.bindRow(row);
                menuElementMap.put(menuElement.getName(), menuElement);
            }
            return menuElementMap;
        } catch (Exception e) {
            throw new JoafDatabaseException("Database error", e);
        }
    }

    public void store(MenuElement obj) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        String updateQuery = "UPDATE menuelement SET menuName = ?, name = ?, url = ?, displayName = ?, extraStyleClass = ?, iconName = ?, accessRole = ?, parentName = ?, ordering = ? "
                        + "WHERE uid = ?;";
        ResultSet rs;
        rs = session.execute(updateQuery, obj.getMenuName(), obj.getName(), obj.getUrl(), obj.getDisplayName(), obj.getExtraStyleClass(),
                obj.getIconName(), obj.getAccessRole(), obj.getParentName(), obj.getOrdering(), UUID.fromString(obj.getUid()));
        logger.debug("stored: " + rs.all().size());
    }

    @Override
    public void insert(MenuElement obj) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        String insertQuery = "INSERT INTO menuelement (uid, menuName, name, url, displayName, extraStyleClass, iconName, accessRole, parentName, ordering) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        ResultSet rs;
        rs = session.execute(insertQuery, UUID.fromString(obj.getUid()), obj.getMenuName(), obj.getName(), obj.getUrl(), obj.getDisplayName(),
                obj.getExtraStyleClass(), obj.getIconName(), obj.getAccessRole(), obj.getParentName(), obj.getOrdering());
        logger.debug("stored: " + rs.all().size());
    }

}
