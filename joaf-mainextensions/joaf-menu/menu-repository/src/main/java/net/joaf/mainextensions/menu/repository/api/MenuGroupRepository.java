package net.joaf.mainextensions.menu.repository.api;

import net.joaf.base.core.db.CrudRepository;
import net.joaf.base.core.db.NoSqlRepository;
import net.joaf.mainextensions.menu.model.MenuGroup;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
public interface MenuGroupRepository extends NoSqlRepository, CrudRepository<MenuGroup> {

}
