package net.joaf.mainextensions.menu.repository.mysql;

import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.utils.DateUtils;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.mainextensions.menu.model.MenuGroup;
import net.joaf.mainextensions.menu.repository.api.MenuGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@Repository
public class MenuGroupRepositoryMysql extends AbstractRepositoryMysql<MenuGroup> implements MenuGroupRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    protected String getTableName() {
        return "menugroup";
    }

    @Override
    protected JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    protected MenuGroup mapRow(ResultSet rs, int rowNum) throws SQLException {
        MenuGroup menuGroup = new MenuGroup();
        menuGroup.setUid(rs.getString("uid"));
        menuGroup.setName(rs.getString("name"));
        menuGroup.setDescription(rs.getString("description"));
        menuGroup.setCreated(DateUtils.dateToLocalDateTime(rs.getDate("created")));
        menuGroup.setUpdated(DateUtils.dateToLocalDateTime(rs.getDate("updated")));
        menuGroup.setObjectState(EObjectState.valueOf(rs.getString("objectState")));
        return menuGroup;
    }

    @Override
    public void insert(MenuGroup element) throws JoafDatabaseException {
        try {
            String sql = "INSERT INTO menugroup (uid, name, description, created, updated, objectState) VALUES (?,?,?,?,?,?)";
            jdbcTemplate.update(sql, element.getUid(), element.getName(), element.getDescription(), DateUtils.localDateTimeToDate(element.getCreated()),
                    DateUtils.localDateTimeToDate(element.getUpdated()), element.getObjectState().toString());
        } catch (Exception e) {
            throw new JoafDatabaseException("Repository error", e);
        }
    }

    @Override
    public void store(MenuGroup element) throws JoafDatabaseException {
        try {
            String sql = "UPDATE menugroup SET name=?, description=?, updated=? where uid=?";
            jdbcTemplate.update(sql, element.getName(), element.getDescription(), DateUtils.localDateTimeToDate(element.getUpdated()), element.getUid());
        } catch (Exception e) {
            throw new JoafDatabaseException("Repository error", e);
        }
    }

    public void updateState(String uid, EObjectState newState) throws JoafDatabaseException {
        try {
            String sql = "UPDATE menugroup SET objectState=? where uid=?";
            jdbcTemplate.update(sql, newState.toString(), uid);
        } catch (Exception e) {
            throw new JoafDatabaseException("Repository error", e);
        }
    }

    @Override
    public String collectionName() {
        return getTableName();
    }

    @Override
    public Class<MenuGroup> getEntityClass() {
        return MenuGroup.class;
    }

    @Override
    public String prepareId() {
        return UUID.randomUUID().toString();
    }
}
