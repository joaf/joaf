package net.joaf.mainextensions.menu.web.controllers;

import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.cqrs.PageredResultExtended;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.base.core.web.model.WrapperWDTO;
import net.joaf.mainextensions.menu.commands.*;
import net.joaf.mainextensions.menu.commands.handlers.CancelMenuGroupCommandHandler;
import net.joaf.mainextensions.menu.commands.handlers.SaveMenuGroupCommandHandler;
import net.joaf.mainextensions.menu.commands.handlers.TrashMenuGroupCommandHandler;
import net.joaf.mainextensions.menu.commands.handlers.UndoTrashMenuGroupCommandHandler;
import net.joaf.mainextensions.menu.model.MenuGroup;
import net.joaf.mainextensions.menu.queries.MenuGroupQuery;
import net.joaf.mainextensions.menu.web.helpers.MenuGroupActionHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@RequestMapping("/administration/menu/group")
@Controller
public class MenuGroupController {
    private static final Logger log = LoggerFactory.getLogger(MenuGroupController.class);
    @Autowired
    private MenuGroupQuery query;

    //    @Autowired
    //    private ImportExportHelper importExportHelper;
    @Autowired
    private CommandGateway commandGateway;

    @RequestMapping
    public String list(HttpSession session, Model model) {
        try {
            PageredRequest pageredRequest = new PageredRequest();
            PageredResult<MenuGroup> all = query.findAll(pageredRequest);
            PageredResultExtended<MenuGroup> allExt = new PageredResultExtended<>(all, this::createActions);
            model.addAttribute("elements", allExt.getElements());
        } catch (Exception e) {
            model.addAttribute("elements", Collections.emptyList());
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            log.debug("error", e);
        }
        return getViewitem("/list");
    }

    private void createActions(WrapperWDTO<MenuGroup> x) {
        x.getData().getActions().forEach(action -> x.getWebActions().add(MenuGroupActionHelper.createActionDetails(action, x.getData().getUid())));
    }

    @RequestMapping("/create")
    public String create(HttpSession session, RedirectAttributes redirectAttributes, Principal principal, Model model) {
        try {
            MenuGroup command = (MenuGroup) commandGateway.send(new CreateMenuGroupCommand(principal.getName()), SaveMenuGroupCommandHandler.class);
            return getRedirect("/edit/" + command.getUid());
        } catch (Exception e) {
            log.debug("error", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return getRedirect("");
        }
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable("id") String id, HttpSession session, RedirectAttributes redirectAttributes, Model model) {
        try {
            MenuGroup command = query.findOne(id);
            model.addAttribute("command", command);
            return getViewitem("/edit");
        } catch (Exception e) {
            log.debug("error", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return getRedirect("");
        }
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String saveItem(@Valid @ModelAttribute("command") MenuGroup command, @PathVariable("id") String id, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) {
        try {
            commandGateway.send(new SaveMenuGroupCommand(command, principal.getName()), SaveMenuGroupCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successsave");
            return getRedirect("");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return getViewitem("/edit");
        }
    }

    @RequestMapping("/trash/{uid}")
    public String trash(@PathVariable("uid") String uid, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) {
        try {
            commandGateway.send(new TrashMenuGroupCommand(uid, principal.getName()), TrashMenuGroupCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successtrash", httpRequest.getContextPath() + "/administration/menu/group/undotrash/" + uid + ".html");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    @RequestMapping("/undotrash/{id}")
    public String undotrash(@PathVariable("id") String id, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) {
        try {
            commandGateway.send(new UndoTrashMenuGroupCommand(id, principal.getName()), UndoTrashMenuGroupCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successundotrash", "1");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    @RequestMapping(value = "/cancel/{uid}", method = RequestMethod.POST)
    public String cancel(@PathVariable("uid") String uid, HttpSession session, Principal principal, Model model) {
        try {
            commandGateway.send(new CancelMenuGroupCommand(uid, principal.getName()), CancelMenuGroupCommandHandler.class);
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    private String getViewitem(String name) {
        return "/freemarker/admin/menu/group" + name;
    }

    private String getRedirect(String name) {
        return "redirect:/administration/menu/group" + name + ".html";
    }
}
