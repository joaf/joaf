/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.menu.web.controllers;

import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.cqrs.PageredResultExtended;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.base.core.web.model.WrapperWDTO;
import net.joaf.mainextensions.menu.commands.*;
import net.joaf.mainextensions.menu.commands.handlers.CancelMenuElementCommandHandler;
import net.joaf.mainextensions.menu.commands.handlers.SaveMenuElementCommandHandler;
import net.joaf.mainextensions.menu.commands.handlers.TrashMenuElementCommandHandler;
import net.joaf.mainextensions.menu.commands.handlers.UndoTrashMenuElementCommandHandler;
import net.joaf.mainextensions.menu.model.MenuElement;
import net.joaf.mainextensions.menu.queries.MenuElementQuery;
import net.joaf.mainextensions.menu.queries.MenuGroupQuery;
import net.joaf.mainextensions.menu.web.helpers.MenuElementActionHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

//TODO: inspect net.joaf.base.core.lib
//import net.joaf.base.core.lib.helpers.ImportExportHelper;

/**
 * Menu management web controller
 *
 * @author Cyprian Śniegota
 * @since 1.0
 */
@Controller
@RequestMapping("/administration/menu/element")
public class MenuElementController {

    private static final Logger log = LoggerFactory.getLogger(MenuElementController.class);
    @Autowired
    private MenuElementQuery query;
    @Autowired
    private MenuGroupQuery menuGroupQuery;

    //    @Autowired
    //    private ImportExportHelper importExportHelper;
    @Autowired
    private CommandGateway commandGateway;

    @RequestMapping
    public String list(HttpSession session, Model model) {
        try {
            PageredRequest pageredRequest = new PageredRequest();
            PageredResult<MenuElement> all = query.findAll(pageredRequest);
            PageredResultExtended<MenuElement> allExt = new PageredResultExtended<>(all, this::createActions);
            model.addAttribute("elements", allExt.getElements());
        } catch (Exception e) {
            model.addAttribute("elements", Collections.emptyList());
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            log.debug("error", e);
        }
        return getViewitem("/list");
    }

    private void createActions(WrapperWDTO<MenuElement> x) {
        x.getData().getActions().forEach(action -> x.getWebActions().add(MenuElementActionHelper.createActionDetails(action, x.getData().getUid())));
    }

    @RequestMapping("/create")
    public String create(HttpSession session, RedirectAttributes redirectAttributes, Principal principal, Model model) {
        try {
            MenuElement command = (MenuElement) commandGateway.send(new CreateMenuElementCommand(principal.getName()), SaveMenuElementCommandHandler.class);
            return getRedirect("/edit/" + command.getUid());
        } catch (Exception e) {
            log.debug("error", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return getRedirect("");
        }
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable("id") String id, HttpSession session, RedirectAttributes redirectAttributes, Model model) {
        try {
            MenuElement command = query.findOne(id);
            Map<String, String> menuGroups = menuGroupQuery.findNameMap();
            model.addAttribute("command", command);
            model.addAttribute("menuGroups", menuGroups);
            return getViewitem("/edit");
        } catch (Exception e) {
            log.debug("error", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return getRedirect("");
        }
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String saveItem(@Valid @ModelAttribute("command") MenuElement command, @PathVariable("id") String id, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) {
        try {
            commandGateway.send(new SaveMenuElementCommand(command, principal.getName()), SaveMenuElementCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successsave");
            return getRedirect("");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return getViewitem("/edit");
        }
    }

    @RequestMapping("/trash/{uid}")
    public String trash(@PathVariable("uid") String uid, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) {
        try {
            commandGateway.send(new TrashMenuElementCommand(uid, principal.getName()), TrashMenuElementCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successtrash", httpRequest.getContextPath() + "/administration/menu/element/undotrash/" + uid + ".html");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    @RequestMapping("/undotrash/{id}")
    public String undotrash(@PathVariable("id") String id, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) {
        try {
            commandGateway.send(new UndoTrashMenuElementCommand(id, principal.getName()), UndoTrashMenuElementCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successundotrash", "1");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    @RequestMapping(value = "/cancel/{uid}", method = RequestMethod.POST)
    public String cancel(@PathVariable("uid") String uid, HttpSession session, Principal principal, Model model) {
        try {
            commandGateway.send(new CancelMenuElementCommand(uid, principal.getName()), CancelMenuElementCommandHandler.class);
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }


    @RequestMapping(value = "/export")
    public HttpEntity<String> export(HttpSession session) {
        //        try {
        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("application", "json"));
        header.set("Content-Disposition",
                "attachment; filename=menuelements.json");
        //            String elements = importExportHelper.exportData("menuelements");
        //            return new HttpEntity<>(elements, header);
        //        } catch (JoafDatabaseException e) {
        //            e.printStackTrace();
        //        }
        return null;
    }

    private String getViewitem(String name) {
        return "/freemarker/admin/menu/element" + name;
    }

    private String getRedirect(String name) {
        return "redirect:/administration/menu/element" + name + ".html";
    }
}
