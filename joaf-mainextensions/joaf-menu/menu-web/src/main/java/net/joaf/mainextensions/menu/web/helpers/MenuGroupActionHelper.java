package net.joaf.mainextensions.menu.web.helpers;

import net.joaf.base.core.model.enums.EStandardAction;
import net.joaf.base.core.web.model.WebActionDetails;

/**
 * Created by cyprian on 07.03.15.
 */
public class MenuGroupActionHelper {

    public static WebActionDetails createActionDetails(EStandardAction action, String uid) {
        WebActionDetails webActionDetails = new WebActionDetails();
        switch (action) {
            case EDIT:
                webActionDetails.setButtonStyle("btn-primary");
                webActionDetails.setActionUrl("/administration/menu/group/edit/" + uid + ".html");
                webActionDetails.setIconName("glyphicon-edit");
                webActionDetails.setTooltip("joaf.edit");
                break;
            case TRASH:
                webActionDetails.setButtonStyle("btn-danger");
                webActionDetails.setActionUrl("/administration/menu/group/trash/" + uid + ".html");
                webActionDetails.setIconName("glyphicon-trash");
                webActionDetails.setTooltip("joaf.trash");
                break;
            default:
                webActionDetails = null;
                break;
        }
        return webActionDetails;
    }
}
