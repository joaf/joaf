package net.joaf.mainextensions.menu.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@RequestMapping("/administration/menu")
@Controller
public class MenuController {

    @RequestMapping
    public String index() {
        return "redirect:/administration/menu/element.html";
    }
}
