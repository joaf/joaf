<div class="sidey">
    <ul class="nav">
        <!-- Main menu -->
    <#list joaf_main_menu as item>
        <li <#if item.active> class="current"</#if>><a href="${rc.contextPath}${item.url}">
            <#if item.iconName??>
                <i class="fa ${item.iconName}"></i>
            </#if>
        ${item.displayName}</a>
        </li>
    </#list>
    </ul>
</div>