package net.joaf.connection.smtp.model;

import net.joaf.mainextensions.connection.model.ConnectionMetadata;

import java.io.Serializable;

/**
 * Created by cyprian on 03.04.15.
 */
public class SmtpConnectionMetadata extends ConnectionMetadata implements Serializable {
    private String host;
    private Integer port;
    private String usernameandpassword;
    private transient String username;
    private transient String password;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUsernameandpassword() {
        return usernameandpassword;
    }

    public void setUsernameandpassword(String usernameandpassword) {
        this.usernameandpassword = usernameandpassword;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
