package net.joaf.connection.smtp.model.enums;

/**
 * Created by cyprian on 03.04.15.
 */
public enum ESmtpConnectionAction {
    EDIT, TRASH
}
