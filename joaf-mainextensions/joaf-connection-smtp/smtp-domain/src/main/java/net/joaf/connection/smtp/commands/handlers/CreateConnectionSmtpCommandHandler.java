package net.joaf.connection.smtp.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.connection.smtp.commands.CreateConnectionSmtpCommand;
import net.joaf.connection.smtp.model.SmtpConnectionMetadata;
import net.joaf.mainextensions.connection.model.ExternalConnection;
import net.joaf.mainextensions.connection.repository.api.ExternalConnectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import java.time.LocalDateTime;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandHandlerComponent
public class CreateConnectionSmtpCommandHandler implements CommandHandler<CreateConnectionSmtpCommand, Object> {

    @Autowired(required = false)
    private ExternalConnectionRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, CreateConnectionSmtpCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(CreateConnectionSmtpCommand command) throws JoafException {
        ExternalConnection element = new ExternalConnection();
        element.setUid(repository.prepareId());
        element.setObjectState(EObjectState.NEW);
        LocalDateTime now = LocalDateTime.now();
        element.setCreated(now);
        element.setUpdated(now);
        element.setType("smtp");
        element.setSubjectUid(command.getSubjectUid());
        element.setMetadataClass(SmtpConnectionMetadata.class);
        element.setConnectionMetadata(new SmtpConnectionMetadata());
        ((SmtpConnectionMetadata) element.getConnectionMetadata()).setUsernameandpassword(":");
        repository.insert(element);
        return element;
    }
}