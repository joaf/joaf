package net.joaf.connection.smtp.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidSuidCommand;
import net.joaf.connection.smtp.commands.handlers.CancelConnectionSmtpCommandHandler;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandDto(handlerClass = CancelConnectionSmtpCommandHandler.class)
public class CancelConnectionSmtpCommand extends AbstractUidSuidCommand implements Command {
    public CancelConnectionSmtpCommand(String elementUid, String userUid, String subjectUid) {
        super(elementUid, userUid, subjectUid);
    }
}