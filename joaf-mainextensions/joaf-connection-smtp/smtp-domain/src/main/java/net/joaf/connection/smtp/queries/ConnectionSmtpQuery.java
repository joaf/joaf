package net.joaf.connection.smtp.queries;

import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.connection.smtp.model.enums.ESmtpConnectionAction;
import net.joaf.mainextensions.connection.model.ExternalConnection;
import net.joaf.mainextensions.connection.repository.api.ExternalConnectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by cyprian on 03.04.15.
 */
@Component
public class ConnectionSmtpQuery {

    @Autowired(required = false)
    private ExternalConnectionRepository repository;

    public PageredResult<ExternalConnection> findAll(PageredRequest pageredRequest) throws JoafDatabaseException {
        PageredResult<ExternalConnection> smtp = repository.findAllByType(pageredRequest, "smtp");
        smtp.getResult().stream().forEach(x -> {
            x.getActions().add(ESmtpConnectionAction.EDIT.toString());
            x.getActions().add(ESmtpConnectionAction.TRASH.toString());
        });
        return smtp;
    }

    public ExternalConnection findOne(String id) throws JoafDatabaseException {
        return repository.findOne(id);
    }
}
