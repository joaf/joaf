package net.joaf.connection.smtp.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractEntityCommand;
import net.joaf.connection.smtp.commands.handlers.SaveConnectionSmtpCommandHandler;
import net.joaf.mainextensions.connection.model.ExternalConnection;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandDto(handlerClass = SaveConnectionSmtpCommandHandler.class)
public class SaveConnectionSmtpCommand extends AbstractEntityCommand<ExternalConnection> implements Command {
    public SaveConnectionSmtpCommand(ExternalConnection element, String userUid) {
        super(element, userUid);
    }
}