package net.joaf.connection.smtp;

import net.joaf.base.extension.AbstractExtension;
import net.joaf.base.extension.JoafExtension;
import org.springframework.stereotype.Component;

/**
 * Created by cyprian on 03.04.15.
 */
@Component
public class ConnectionSmtpExtension extends AbstractExtension implements JoafExtension {
    public static final String EXTENSION_BASE_PATH = "/connection/smtp";
    public static final String EXTENSION_ADMIN_BASE_PATH = "/administration/connection/smtp";

    @Override
    public String getExtensionMetadataFile() {
        return "/extension-connection-smtp.xml";
    }
}