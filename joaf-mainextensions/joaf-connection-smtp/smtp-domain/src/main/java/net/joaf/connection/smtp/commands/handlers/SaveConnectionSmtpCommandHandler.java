package net.joaf.connection.smtp.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.connection.smtp.commands.SaveConnectionSmtpCommand;
import net.joaf.connection.smtp.model.SmtpConnectionMetadata;
import net.joaf.mainextensions.connection.model.ExternalConnection;
import net.joaf.mainextensions.connection.repository.api.ExternalConnectionRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import java.time.LocalDateTime;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandHandlerComponent
public class SaveConnectionSmtpCommandHandler implements CommandHandler<SaveConnectionSmtpCommand, Object> {

    @Autowired(required = false)
    private ExternalConnectionRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, SaveConnectionSmtpCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(SaveConnectionSmtpCommand command) throws JoafException {
        ExternalConnection dbObject = repository.findOne(command.getElement().getUid());
        if (EObjectState.NEW.equals(dbObject.getObjectState())) {
            repository.updateObjectState(command.getElement().getUid(), EObjectState.ACCEPTED);
        }
        ExternalConnection element = command.getElement();
        SmtpConnectionMetadata smtpConnectionMetadata = (SmtpConnectionMetadata) element.getConnectionMetadata();
        SmtpConnectionMetadata smtpConnectionMetadataDb = (SmtpConnectionMetadata) dbObject.getConnectionMetadata();
        if (StringUtils.isEmpty(smtpConnectionMetadata.getUsername()) && StringUtils.isEmpty(smtpConnectionMetadata.getPassword())) {
            smtpConnectionMetadata.setUsernameandpassword(smtpConnectionMetadataDb.getUsernameandpassword());
        } else {
            smtpConnectionMetadata.setUsernameandpassword(smtpConnectionMetadata.getUsername() + ":" + smtpConnectionMetadata.getPassword());
        }
        element.setUpdated(LocalDateTime.now());
        repository.store(element);
        return null;
    }
}