package net.joaf.connection.smtp.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidSuidCommand;
import net.joaf.connection.smtp.commands.handlers.CreateConnectionSmtpCommandHandler;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandDto(handlerClass = CreateConnectionSmtpCommandHandler.class)
public class CreateConnectionSmtpCommand extends AbstractUidSuidCommand implements Command {
    public CreateConnectionSmtpCommand(String userUid, String subjectUid) {
        super(null, userUid, subjectUid);
    }
}