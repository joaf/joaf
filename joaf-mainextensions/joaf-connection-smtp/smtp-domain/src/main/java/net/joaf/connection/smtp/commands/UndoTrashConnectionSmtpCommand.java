package net.joaf.connection.smtp.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidSuidCommand;
import net.joaf.connection.smtp.commands.handlers.UndoTrashConnectionSmtpCommandHandler;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandDto(handlerClass = UndoTrashConnectionSmtpCommandHandler.class)
public class UndoTrashConnectionSmtpCommand extends AbstractUidSuidCommand implements Command {
    public UndoTrashConnectionSmtpCommand(String elementUid, String userUid, String subjectUid) {
        super(elementUid, userUid, subjectUid);
    }
}