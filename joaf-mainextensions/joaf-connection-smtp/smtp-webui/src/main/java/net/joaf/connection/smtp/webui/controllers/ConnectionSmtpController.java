package net.joaf.connection.smtp.webui.controllers;

import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.cqrs.PageredResultExtended;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafNoSubjectContextException;
import net.joaf.base.core.session.SessionContext;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.base.core.web.model.WrapperWDTO;
import net.joaf.connection.smtp.commands.*;
import net.joaf.connection.smtp.commands.handlers.*;
import net.joaf.connection.smtp.queries.ConnectionSmtpQuery;
import net.joaf.connection.smtp.webui.helpers.ConnectionSmtpActionHelper;
import net.joaf.connection.smtp.webui.model.ConnectionSmtpForm;
import net.joaf.mainextensions.connection.model.ExternalConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Collections;
import java.util.Optional;

/**
 * Created by cyprian on 03.04.15.
 */
@Controller
@RequestMapping("/connection/smtp")
public class ConnectionSmtpController {

    private static final Logger log = LoggerFactory.getLogger(ConnectionSmtpController.class);
    @Autowired
    private ConnectionSmtpQuery query;
    @Autowired
    private CommandGateway commandGateway;

    @RequestMapping
    public String list(HttpSession session, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            PageredRequest pageredRequest = new PageredRequest(null, null, EObjectState.ACCEPTED, getSubjectCardDataUid(sessionContext));
            PageredResult<ExternalConnection> all = query.findAll(pageredRequest);
            PageredResultExtended<ExternalConnection> allExt = new PageredResultExtended<>(all, this::createActions);
            model.addAttribute("elements", allExt.getElements());
        } catch (Exception e) {
            model.addAttribute("elements", Collections.emptyList());
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            log.debug("error", e);
        }
        updateModelWithPaths(model);
        return getViewitem("/list");
    }

    private void createActions(WrapperWDTO<ExternalConnection> x) {
        x.getData().getActions().forEach(action -> x.getWebActions().add(ConnectionSmtpActionHelper.createActionDetails(action, x.getData().getUid(), getControllerContextPath())));
    }

    @RequestMapping("/create")
    public String create(HttpSession session, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            ExternalConnection command = (ExternalConnection) commandGateway
                    .send(new CreateConnectionSmtpCommand(principal.getName(), getSubjectCardDataUid(sessionContext)), CreateConnectionSmtpCommandHandler.class);
            return getRedirect("/edit/" + command.getUid());
        } catch (Exception e) {
            log.debug("error", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return getRedirect("");
        }
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable("id") String id, HttpSession session, RedirectAttributes redirectAttributes, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            ExternalConnection dbobject = query.findOne(id);
            ConnectionSmtpForm command = ConnectionSmtpForm.createFromExternalConnection(dbobject);
            model.addAttribute("command", command);
            updateModelWithPaths(model);
            return getViewitem("/edit");
        } catch (Exception e) {
            log.debug("error", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return getRedirect("");
        }
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String saveItem(@Valid @ModelAttribute("command") ConnectionSmtpForm command, @PathVariable("id") String id, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            commandGateway.send(new SaveConnectionSmtpCommand(command.toExternalConnection(), principal.getName()), SaveConnectionSmtpCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successsave");
            return getRedirect("");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            updateModelWithPaths(model);
            return getViewitem("/edit");
        }
    }

    @RequestMapping("/trash/{uid}")
    public String trash(@PathVariable("uid") String uid, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            commandGateway.send(new TrashConnectionSmtpCommand(uid, principal.getName(), getSubjectCardDataUid(sessionContext)), TrashConnectionSmtpCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successtrash", httpRequest.getContextPath() + getUrl("/undotrash/" + uid));
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    @RequestMapping("/undotrash/{id}")
    public String undotrash(@PathVariable("id") String id, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            commandGateway.send(new UndoTrashConnectionSmtpCommand(id, principal.getName(), getSubjectCardDataUid(sessionContext)), UndoTrashConnectionSmtpCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successundotrash", "1");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    @RequestMapping(value = "/cancel/{uid}", method = RequestMethod.POST)
    public String cancel(@PathVariable("uid") String uid, HttpSession session, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            commandGateway.send(new CancelConnectionSmtpCommand(uid, principal.getName(), getSubjectCardDataUid(sessionContext)), CancelConnectionSmtpCommandHandler.class);
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    protected String getSubjectCardDataUid(SessionContext sessionContext) {
        return sessionContext.getSubjectCardDataId();
    }

    protected void updateModelWithPaths(Model model) {
        model.addAttribute("backPath", getBackPath());
        model.addAttribute("controllerContextPath", getControllerContextPath());
    }

    private void checkSubjectUid(String subjectUid) throws JoafNoSubjectContextException {
        if (subjectUid == null) {
            throw new JoafNoSubjectContextException("");
        }
    }

    protected String getBackPath() {
        return "/";
    }

    protected String getControllerContextPath() {
        return "/connection/smtp";
    }

    protected String getViewitem(String name) {
        return "/freemarker/connection/smtp" + name;
    }

    protected String getRedirect(String name) {
        return "redirect:" + getUrl(name);
    }

    protected String getUrl(String name) {
        return "/connection/smtp" + name + ".html";
    }
}
