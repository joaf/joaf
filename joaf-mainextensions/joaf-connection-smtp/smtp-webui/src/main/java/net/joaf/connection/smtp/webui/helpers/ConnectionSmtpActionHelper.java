package net.joaf.connection.smtp.webui.helpers;

import net.joaf.base.core.web.model.WebActionDetails;
import net.joaf.connection.smtp.model.enums.ESmtpConnectionAction;

/**
 * Created by cyprian on 03.04.15.
 */
public class ConnectionSmtpActionHelper {
    public static WebActionDetails createActionDetails(String saction, String uid, String controllerContextPath) {
        WebActionDetails webActionDetails = new WebActionDetails();
        ESmtpConnectionAction action = ESmtpConnectionAction.valueOf(saction);
        switch (action) {
            case EDIT:
                webActionDetails.setButtonStyle("btn-primary");
                webActionDetails.setActionUrl(controllerContextPath + "/edit/" + uid + ".html");
                webActionDetails.setIconName("glyphicon-edit");
                webActionDetails.setTooltip("joaf.edit");
                break;
            case TRASH:
                webActionDetails.setButtonStyle("btn-danger");
                webActionDetails.setActionUrl(controllerContextPath + "/trash/" + uid + ".html");
                webActionDetails.setIconName("glyphicon-trash");
                webActionDetails.setTooltip("joaf.trash");
                break;
            default:
                webActionDetails = null;
                break;
        }
        return webActionDetails;
    }
}
