package net.joaf.connection.smtp.webui.model;

import net.joaf.connection.smtp.model.SmtpConnectionMetadata;
import net.joaf.mainextensions.connection.model.ExternalConnection;

import java.io.Serializable;

/**
 * Created by cyprian on 03.04.15.
 */
public class ConnectionSmtpForm implements Serializable {
    private String uid;
    private String name;
    private String host;
    private Integer port;
    private String username;
    private String password;

    public static ConnectionSmtpForm createFromExternalConnection(ExternalConnection element) {
        ConnectionSmtpForm nelement = new ConnectionSmtpForm();
        nelement.setUid(element.getUid());
        nelement.setName(element.getName());
        SmtpConnectionMetadata smtpConnectionMetadata = (SmtpConnectionMetadata) element.getConnectionMetadata();
        nelement.setHost(smtpConnectionMetadata.getHost());
        nelement.setPort(smtpConnectionMetadata.getPort());
        return nelement;
    }

    public ExternalConnection toExternalConnection() {
        ExternalConnection element = new ExternalConnection();
        SmtpConnectionMetadata smtpConnectionMetadata = new SmtpConnectionMetadata();
        smtpConnectionMetadata.setHost(this.host);
        smtpConnectionMetadata.setPort(this.port);
        smtpConnectionMetadata.setUsername(this.username);
        smtpConnectionMetadata.setPassword(this.password);
        element.setConnectionMetadata(smtpConnectionMetadata);
        element.setName(this.name);
        element.setUid(this.uid);
        return element;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
