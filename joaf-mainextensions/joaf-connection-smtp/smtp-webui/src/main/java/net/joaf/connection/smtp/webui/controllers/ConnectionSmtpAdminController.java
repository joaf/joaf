package net.joaf.connection.smtp.webui.controllers;

import net.joaf.base.core.session.SessionContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by cyprian on 03.04.15.
 */
@Controller
@RequestMapping("/administration/connection/smtp")
public class ConnectionSmtpAdminController extends ConnectionSmtpController {

    protected String getSubjectCardDataUid(SessionContext sessionContext) {
        return "app";
    }

    protected String getBackPath() {
        return "/administration.html";
    }

    protected String getControllerContextPath() {
        return "/administration/connection/smtp";
    }

    protected String getUrl(String name) {
        return "/administration/connection/smtp" + name + ".html";
    }
}
