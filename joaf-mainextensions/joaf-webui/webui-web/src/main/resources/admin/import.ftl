<#import "/templates/base.ftl" as page />
<@page.layout "Invoices">
<div class="awidget full-width">
    <div class="awidget-head">
        <div class="btn-group">
        <#--<a href="/remoteconnections/edit/0.html" class="btn btn-primary">Create</a>-->
            <#--<a href="/invoice/syncInfakt.html" class="btn btn-info">Infakt sync</a>-->
        <#--<button class="btn btn-success">Center</button>-->
        <#--<button class="btn btn-primary">Right</button>-->
        </div>
    </div>
    <div class="awidget-body">
        <form class="form-horizontal" role="form" method="post" action="${rc.contextPath}/administration/restore.html"
              enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-lg-2 control-label">File</label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.fileData' 'placeholder="Select file" class="form-control"' 'file' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="button"
                    onclick="$(this.form).attr('action','${rc.contextPath}/administration/cancel.html');$(this.form).submit();"
                    class="btn btn-default">Cancel
            </button>
        </form>
    </div>

</div>
</@page.layout>