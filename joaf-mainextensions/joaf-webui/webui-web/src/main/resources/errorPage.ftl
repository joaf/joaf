<#import "/templates/login.ftl" as page />
<@page.layout "xxx">
<div class="awidget-head">

</div>
<div class="awidget-body">
    <!-- Page title -->
    <div class="page-title text-center">
        <h2>404<span class="color">!!!</span></h2>
        <hr/>
    </div>
    <!-- Page title -->
    <div class="text-center">
        <p>We are sorry, the page you requested cannot be found. </p>
        <br/>

        <form class="form-inline" role="form">
            <div class="form-group">
                <input type="email" class="form-control" id="search" placeholder="Type Something...">
            </div>
            <button type="submit" class="btn btn-info">Search</button>
        </form>
        <br/>
    </div>
</div>
</@page.layout>