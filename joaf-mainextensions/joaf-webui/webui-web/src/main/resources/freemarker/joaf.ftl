<#ftl strip_whitespace=true>
<#--
 * joaf.ftl: spring.ftl wrapper
 *
 -->

<#macro formInput path, attributes="", fieldType="text", label="", placeholder="", title="">
    <@spring.bind path/>
    <#if placeholder?? && placeholder != ''><#assign placeholder2 = 'placeholder="'+springMacroRequestContext.getMessage(placeholder, placeholder)+'" '/><#else><#assign placeholder2 = '' /></#if>
    <#if title?? && title != ''>
        <#assign msgTitle><@spring.messageText title title/></#assign>
        <#assign tooltip=' data-toggle="tooltip" data-placement="bottom" title="${msgTitle}" ' />
        <#assign infoIcon=' <i class="fa fa-info"></i>' />
    <#else>
        <#assign tooltip='' />
        <#assign infoIcon='' />
    </#if>
    <#assign attributes2 = tooltip+placeholder2+attributes />
<div class="row form-group">
    <label for="${spring.status.expression?replace('[','')?replace(']','')}"
           class="col-lg-3 control-label" ${tooltip}><@spring.messageText label label />${infoIcon}</label>

    <div class="col-lg-9">
        <@spring.formInput path attributes2 fieldType />
        <#if spring.status.error>
            <p><@spring.showErrors "<br>", "color:red" /></p>
        </#if>
    </div>
</div>
</#macro>

<#macro formInputDate path, attributes="", fieldType="text", label="", placeholder="", title="">
    <@spring.bind path/>
    <#if placeholder?? && placeholder != ''><#assign placeholder2 = 'placeholder="'+springMacroRequestContext.getMessage(placeholder, placeholder)+'" '/><#else><#assign placeholder2 = '' /></#if>
    <#if title?? && title != ''>
        <#assign msgTitle='spring.messageText title title' />
        <#assign tooltip=' data-toggle="tooltip" data-placement="bottom" title="${msgTitle}" ' />
        <#assign infoIcon=' <i class="fa fa-info"></i>' />
    <#else>
        <#assign tooltip='' />
        <#assign infoIcon='' />
    </#if>
    <#assign attributes2 = tooltip+placeholder2+attributes />
    <#assign attributes2 = 'data-format="yyyy-MM-dd" '+attributes2 />
<div class="row form-group">
    <label for="${spring.status.expression?replace('[','')?replace(']','')}"
           class="col-lg-3 control-label" ${tooltip}><@spring.messageText label label />${infoIcon}</label>

    <div class="col-lg-9">
        <div class="input-append input-group datepicker">
                                 <span class="add-on input-group-addon">
                                   &nbsp;<i data-date-icon="fa fa-calendar" data-time-icon="fa fa-clock-o" class="fa fa-calendar">
                                 </i>
                                 </span>
            <@spring.formInput path attributes2 fieldType />
        </div>
        <#if spring.status.error>
            <p><@spring.showErrors "<br>", "color:red" /></p>
        </#if>
    </div>
</div>
</#macro>


<#macro renderForm form, formModel, command="command">
    <#list form.fieldset as fieldset>
        <@renderFieldset fieldset=fieldset formModel=formModel command=command />
    </#list>
</#macro>

<#macro renderFieldset fieldset, formModel, command="command">
    <#list fieldset.field as field>
        <@renderField field=field formModel=formModel command=command />
    </#list>
</#macro>

<#macro renderField field, formModel, command="command">
    <#include '/freemarker/fields/'+field.type+".ftl" />
    <@.vars[field.type] field formModel command />
</#macro>

<#macro icon type, name>

</#macro>