<#ftl strip_whitespace=true>
<#macro selectModel field, formModel, command="command">
    <@spring.bind '${command}.${field.name}'/>
    <#assign placeholder = field.label />
<div class="row form-group">
    <label for="${spring.status.expression?replace('[','')?replace(']','')}"
            class="col-lg-3 control-label"><@spring.messageText field.label field.label /></label>

    <div class="col-lg-9">
        <@spring.formSingleSelect '${command}.${field.name}' formModel[field.modelOptions] 'class="form-control"'/>
        <#if spring.status.error>
            <p><@spring.showErrors "<br>", "color:red" /></p>
        </#if>
    </div>
</div>

</#macro>