<#ftl strip_whitespace=true>
<#import "/freemarker/joaf.ftl" as joaf />
<#macro date field, formModel, command="command">
    <#assign placeholder = field.label />
    <@joaf.formInputDate '${command}.${field.name}' 'class="form-control"' '${field.type}' '${field.label}' '${placeholder}' '${field.title!\'\'}'/>
</#macro>