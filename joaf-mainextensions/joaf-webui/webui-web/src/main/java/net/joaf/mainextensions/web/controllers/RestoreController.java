/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.web.controllers;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.services.FullJsonRestoreService;
import net.joaf.mainextensions.web.model.ImportFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by cyprian on 12.11.14.
 */
@Controller
public class RestoreController {

    @Autowired
    private FullJsonRestoreService restoreService;

    @RequestMapping(value = "/administration/restore", method = RequestMethod.GET)
    public String importForm(HttpSession session, Model model) {
        ImportFile command = new ImportFile();
        model.addAttribute("command", command);
        return "admin/import";
    }

    @RequestMapping(value = "/administration/restore", method = RequestMethod.POST)
    public String importFormSubmit(@ModelAttribute("command") ImportFile command, HttpSession session, Model model) throws IOException {
        try {
            restoreService.restoreAll(command.getFileData().getInputStream());
        } catch (JoafDatabaseException e) {
            e.printStackTrace();
        }
        return "redirect:/administration.html";
    }
}
