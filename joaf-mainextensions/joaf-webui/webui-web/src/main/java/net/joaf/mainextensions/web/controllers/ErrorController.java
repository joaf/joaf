/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.web.controllers;

import net.joaf.base.core.error.JoafNoSubjectContextException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

//TODO: remove subject company references
//import net.joaf.base.core.net.joaf.mainextension.subjectcompany.web.SubjectCardWeb;

/**
 * Created with IntelliJ IDEA.
 * User: cyprian
 * Date: 18.10.13
 * Time: 00:32
 * To change this template use File | Settings | File Templates.
 */
@ControllerAdvice
public class ErrorController {

    @ExceptionHandler(RuntimeException.class)
    public String pageError(Throwable ex, HttpServletRequest request) {
        return "errorPage";
    }

    @ExceptionHandler(JoafNoSubjectContextException.class)
    public String joafNoSubjectContextException(Exception ex, HttpServletRequest request) {
        return "redirect:/subjectcard/create.html?autoredirected=true";
    }

    @RequestMapping("/redirect404")
    public String page404redirect(HttpServletRequest request) {
        System.out.println("404: ");
        return "redirect:/404.html";
    }

    @RequestMapping("/404")
    public String page404() {
        return "errorPage";
    }

}
