package net.joaf.mainextensions.web;

import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.TemplateLoader;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: cyprian
 * Date: 17.10.13
 * Time: 01:10
 * To change this template use File | Settings | File Templates.
 */
@Component
public class ClasspathTemplateLoaderFactory {

    public TemplateLoader create() {
        ClassTemplateLoader classTemplateLoader = new ClassTemplateLoader(this.getClass(), "/");
        return classTemplateLoader;
    }
}
