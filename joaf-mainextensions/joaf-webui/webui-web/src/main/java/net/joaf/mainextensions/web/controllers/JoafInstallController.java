/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.web.controllers;

import net.joaf.mainextensions.web.model.InstallForm;
import net.joaf.mainextensions.web.services.JoafInstallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * Created by cyprian on 31.01.15.
 */
@Controller
@RequestMapping("/joafinstall")
public class JoafInstallController {

    @Autowired
    private JoafInstallService installService;

    @RequestMapping("/index")
    public String index(Model model) {
        try {
            Boolean isInstalled = installService.checkInstalled();
            model.addAttribute("installed", isInstalled.toString());
            InstallForm installForm = new InstallForm();
            model.addAttribute("command", installForm);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "install/index";
    }

    @RequestMapping(value = "/doInstall", method = RequestMethod.POST)
    public String doInstall(@Valid @ModelAttribute("command") InstallForm command, Model model) {
        try {
            Boolean isInstalled = installService.checkInstalled();
            if (!isInstalled) {
                installService.installWithCreditials(command);
            }
            model.addAttribute("installed", isInstalled.toString());
            return "redirect:/joafinstall/index.html";
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}

