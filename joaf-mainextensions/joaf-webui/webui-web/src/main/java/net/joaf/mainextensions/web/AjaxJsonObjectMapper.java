package net.joaf.mainextensions.web;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.stereotype.Component;

/**
 * Created by cyprian on 31.05.14.
 */
@Component
public class AjaxJsonObjectMapper extends ObjectMapper {
    public AjaxJsonObjectMapper() {
        super();
        this.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, false);
        this.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        this.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }
}
