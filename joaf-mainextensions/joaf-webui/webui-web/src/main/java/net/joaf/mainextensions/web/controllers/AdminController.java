/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.web.controllers;

//import org.springframework.beans.factory.annotation.Autowired;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.menu.model.MenuElement;
import net.joaf.mainextensions.menu.queries.MenuElementQuery;
import net.joaf.mainextensions.web.model.ImportFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

//import net.joaf.base.core.error.JoafDatabaseException;
//import net.joaf.base.core.error.JoafException;
//import net.joaf.base.core.lib.helpers.ImportExportHelper;
//TODO: import/export joaf-lib
//import java.util.Arrays;
//import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: cyprian
 * Date: 05.01.14
 * Time: 00:16
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class AdminController {

/*    @Autowired
    ImportExportHelper importExportHelper;*/

    @Autowired
    private MenuElementQuery menuElementQuery;

    @RequestMapping("/administration")
    public String list(HttpSession session, Model model) {
        try {
            List<MenuElement> elements = menuElementQuery.findAllByGroup("adminpanel");
            model.addAttribute("elements", elements);
        } catch (JoafDatabaseException e) {
            model.addAttribute("elements", Arrays.asList());
            e.printStackTrace();
        }
        return "admin/mainPage";
    }

    @RequestMapping(value = "/administration/import", method = RequestMethod.GET)
    public String importForm(HttpSession session, Model model) {
        ImportFile command = new ImportFile();
        model.addAttribute("command", command);
        return "admin/import";
    }

    @RequestMapping(value = "/administration/import", method = RequestMethod.POST)
    public String importFormSubmit(@ModelAttribute("command") ImportFile command, HttpSession session, Model model) throws IOException {
/*        try {
            List<String> availableCollections = Arrays.asList("configuration", "content");

            importExportHelper.importData(command.getFileData().getInputStream(), ImportExportHelper.EImportType.REPLACE, availableCollections, null);
        } catch (JoafDatabaseException e) {
            e.printStackTrace();
        } catch (JoafException e) {
            e.printStackTrace();
        }*/
        return "redirect:/administration.html";
    }

    @RequestMapping(value = "/administration/cancel", method = RequestMethod.POST)
    public String cancel(HttpSession session, Model model) {
        return "redirect:/administration.html";
    }
}
