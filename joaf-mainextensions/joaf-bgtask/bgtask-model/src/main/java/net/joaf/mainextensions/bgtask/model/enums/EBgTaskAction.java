package net.joaf.mainextensions.bgtask.model.enums;

/**
 * Created by cyprian on 08.03.15.
 */
public enum EBgTaskAction {
    EDIT, TRASH, SCHEDULE, UNSCHEDULE;
}
