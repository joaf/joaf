package net.joaf.mainextensions.bgtask.model;

import net.joaf.base.core.db.model.JoafSuidEntity;
import net.joaf.base.core.db.model.StringUidEntity;
import net.joaf.mainextensions.bgtask.model.enums.EAsyncTaskState;

import java.io.Serializable;

/**
 * Async task model
 *
 * @author Cyprian Śniegota
 * @since 1.2
 */
public class AsyncTask extends JoafSuidEntity implements Serializable, StringUidEntity {

    private String beanName;
    private EAsyncTaskState taskState;
    private long currentValue;
    private long maxValue;

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public EAsyncTaskState getTaskState() {
        return taskState;
    }

    public void setTaskState(EAsyncTaskState taskState) {
        this.taskState = taskState;
    }

    public long getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(long currentValue) {
        this.currentValue = currentValue;
    }

    public long getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(long maxValue) {
        this.maxValue = maxValue;
    }
}
