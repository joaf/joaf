/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.bgtask.model;

import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.db.model.StringUidEntity;
import net.joaf.mainextensions.bgtask.model.enums.EBgTaskAction;
import net.joaf.mainextensions.bgtask.model.enums.EBgtaskState;
import net.joaf.mainextensions.bgtask.model.enums.ETriggerType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Task prototype, used to create Quartz task and trigger
 * Created by cyprian on 09.08.14.
 */
public class Bgtask implements Serializable, StringUidEntity {

    private String uid;

    private EBgtaskState scheduleState;

    private String name = "";

    private String description = "";

    private String jobClassName = "";

    private String jobModule = "";

    private ETriggerType triggerType;

    private String cronExpression;

    private Map<String, String> params = new HashMap<>();

    private EObjectState objectState;

    private List<EBgTaskAction> actions = new ArrayList<>();

    //transient
    private Boolean active = Boolean.FALSE;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public ETriggerType getTriggerType() {
        return triggerType;
    }

    public void setTriggerType(ETriggerType triggerType) {
        this.triggerType = triggerType;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public String getJobClassName() {
        return jobClassName;
    }

    public void setJobClassName(String jobClassName) {
        this.jobClassName = jobClassName;
    }

    public String getJobModule() {
        return jobModule;
    }

    public void setJobModule(String jobModule) {
        this.jobModule = jobModule;
    }

    public EBgtaskState getScheduleState() {
        return scheduleState;
    }

    public void setScheduleState(EBgtaskState scheduleState) {
        this.scheduleState = scheduleState;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public EObjectState getObjectState() {
        return objectState;
    }

    public void setObjectState(EObjectState objectState) {
        this.objectState = objectState;
    }

    public List<EBgTaskAction> getActions() {
        return actions;
    }
}
