package net.joaf.mainextensions.bgtask.model.enums;

/**
 * Async task state
 *
 * @author Cyprian Śniegota
 * @since 1.2
 */
public enum EAsyncTaskState {
    CREATED, QUEUED, RUNNING, PAUSED, DONE
}
