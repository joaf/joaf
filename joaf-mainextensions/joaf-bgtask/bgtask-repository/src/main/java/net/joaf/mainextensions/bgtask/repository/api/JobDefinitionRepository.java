/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.bgtask.repository.api;

import net.joaf.base.core.db.CrudRepository;
import net.joaf.base.core.db.NoSqlRepository;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.bgtask.model.JobDefinition;

import java.util.List;
import java.util.Optional;

/**
 * Created by cyprian on 09.08.14.
 */
public interface JobDefinitionRepository extends NoSqlRepository, CrudRepository<JobDefinition> {

    Optional<JobDefinition> findByClassName(String className) throws JoafDatabaseException;

    List<JobDefinition> findAll() throws JoafDatabaseException;

    JobDefinition findOne(String id) throws JoafDatabaseException;

    void store(JobDefinition command) throws JoafDatabaseException;

    void insert(JobDefinition command) throws JoafDatabaseException;

    void remove(String id) throws JoafDatabaseException;
}
