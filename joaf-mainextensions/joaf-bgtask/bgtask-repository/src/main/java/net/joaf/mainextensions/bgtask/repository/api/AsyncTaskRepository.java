package net.joaf.mainextensions.bgtask.repository.api;

import net.joaf.base.core.db.CrudRepository;
import net.joaf.mainextensions.bgtask.model.AsyncTask;

/**
 * Async task repository
 *
 * @author Cyprian Śniegota
 * @since 1.2
 */
public interface AsyncTaskRepository extends CrudRepository<AsyncTask> {
}
