package net.joaf.mainextensions.bgtask;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java8.En;
import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.base.core.utils.JsonUtils;
import net.joaf.mainextensions.bgtask.commands.*;
import net.joaf.mainextensions.bgtask.commands.handlers.*;
import net.joaf.mainextensions.bgtask.model.Bgtask;
import net.joaf.mainextensions.bgtask.repository.api.BgtaskRepository;
import net.joaf.mainextensions.bgtask.services.BgtaskService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.quartz.Scheduler;

import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * Created by cyprian on 01.05.15.
 */
public class BgTaskStepdefs implements En {

    @InjectMocks
    private CreateBgTaskCommandHandler objectUnderTestCreate = new CreateBgTaskCommandHandler();
    @InjectMocks
    private CancelBgTaskCommandHandler objectUnderTestCancel = new CancelBgTaskCommandHandler();
    @InjectMocks
    private SaveBgTaskCommandHandler objectUnderTestSave = new SaveBgTaskCommandHandler();
    @InjectMocks
    private ScheduleBgTaskCommandHandler objectUnderTestSchedule = new ScheduleBgTaskCommandHandler();
    @InjectMocks
    private TrashBgTaskCommandHandler objectUnderTestTrash = new TrashBgTaskCommandHandler();
    @InjectMocks
    private UndoTrashBgTaskCommandHandler objectUnderTestUndoTrash = new UndoTrashBgTaskCommandHandler();
    @InjectMocks
    private UnscheduleBgTaskCommandHandler objectUnderTestUnschedule = new UnscheduleBgTaskCommandHandler();

    @Mock
    private BgtaskService bgtaskServiceMock;

    @Mock
    private BgtaskRepository bgtaskRepositoryMock;

    @Mock
    private CommandGateway commandGateway;

    @Mock
    private Scheduler quartzScheduler;

    private Command command;
    private String user;
    private String uid;
    private Bgtask bgtask;
    private Exception thenException;
    private Boolean thenExceptionVerified = false;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        given(bgtaskRepositoryMock.findOneOptional(anyString())).willReturn(Optional.empty());
    }

    @After
    public void setDown() throws Exception {
        if (!thenExceptionVerified && thenException != null) {
            throw new IllegalStateException("Exception not verified", thenException);
        }
    }

    @Given("^exists bgtask (.+)$")
    public void exists_bgtask_json(String json) throws Throwable {
        Bgtask bgtask = JsonUtils.getPojo(json, Bgtask.class);
        if (bgtask == null) {
            throw new IllegalArgumentException(String.format("Error parse json [%s]", json));
        }
        given(bgtaskRepositoryMock.findOne(bgtask.getUid())).willReturn(bgtask);
        given(bgtaskRepositoryMock.findOneOptional(bgtask.getUid())).willReturn(Optional.of(bgtask));
    }

    @Given("^user logged in as (\\w+)$")
    public void user_logged_in_as_ROLE(String role) throws Throwable {
        this.user = role;
    }

    @When("^execute (.+) command$")
    public void execute_command(String command) throws Throwable {
        try {
            switch (command) {
                case "create":
                    this.command = new CreateBgTaskCommand(user);
                    objectUnderTestCreate.execute((CreateBgTaskCommand) this.command);
                    break;
                case "cancel":
                    this.command = new CancelBgTaskCommand(this.uid, user);
                    objectUnderTestCancel.execute((CancelBgTaskCommand) this.command);
                    break;
                case "save":
                    this.command = new SaveBgTaskCommand(this.bgtask, user);
                    objectUnderTestSave.execute((SaveBgTaskCommand) this.command);
                    break;
                case "schedule":
//                    this.command = new ScheduleBgTaskCommand(this.user, this.uid);
//                    objectUnderTestSchedule.execute((ScheduleBgTaskCommand) this.command);
                    break;
                case "trash":
                    this.command = new TrashBgTaskCommand(this.uid, this.user);
                    objectUnderTestTrash.execute((TrashBgTaskCommand) this.command);
                    break;
                case "undotrash":
                    this.command = new UndoTrashBgTaskCommand(this.uid, this.user);
                    objectUnderTestUndoTrash.execute((UndoTrashBgTaskCommand) this.command);
                    break;
                case "unschedule":
//                    this.command = new UnscheduleBgTaskCommand(this.uid, this.user);
//                    objectUnderTestUnschedule.execute((UnscheduleBgTaskCommand) this.command);
                    break;
                default:
                    throw new PendingException();
            }
        } catch (IllegalArgumentException | JoafException e) {
            this.thenException = e;
        }
    }

    @Given("^command for uid (.+)$")
    public void command_for_uid(String uid) throws Throwable {
        this.uid = uid;
    }

    @Given("^command for object (.+)$")
    public void command_for_object(String json) throws Throwable {
        Bgtask bgtask = JsonUtils.getPojo(json, Bgtask.class);
        if (bgtask == null) {
            throw new IllegalArgumentException(String.format("Error parse json [%s]", json));
        }
        this.bgtask = bgtask;
    }

    @Then("^insert will execute$")
    public void insert_was_executed() throws Throwable {
        verify(bgtaskRepositoryMock, atLeastOnce()).insert(anyObject());
    }

    @Then("^remove will execute$")
    public void remove_was_executed() throws Throwable {
        verify(bgtaskRepositoryMock, atLeastOnce()).remove(this.uid);
    }

    @Then("^remove will not execute$")
    public void remove_was_not_executed() throws Throwable {
        verify(bgtaskRepositoryMock, never()).remove(this.uid);
    }

    @Then("^store will execute$")
    public void store_was_executed() throws Throwable {
        verify(bgtaskRepositoryMock, atLeastOnce()).store(this.bgtask);
    }

    @Then("^store will not execute$")
    public void store_will_not_execute() throws Throwable {
        verify(bgtaskRepositoryMock, never()).store(bgtask);
    }

    @And("^exception will throw$")
    public void exception_will_throw() throws Throwable {
        assertNotNull(this.thenException);
        thenExceptionVerified = true;
    }

    @And("^exception will not throw$")
    public void exception_will_not_throw() throws Throwable {
        assertNull(this.thenException);
        thenExceptionVerified = true;
    }

    @Then("^object state will change to (.*)$")
    public void object_state_will_change_to(EObjectState state) throws Throwable {
        verify(bgtaskRepositoryMock, atLeastOnce()).updateObjectState(uid, state);
    }

    @Then("^object state will not change to (.*)$")
    public void object_state_will_not_change_to(EObjectState state) throws Throwable {
        verify(bgtaskRepositoryMock, never()).updateObjectState(uid, state);
    }
}
