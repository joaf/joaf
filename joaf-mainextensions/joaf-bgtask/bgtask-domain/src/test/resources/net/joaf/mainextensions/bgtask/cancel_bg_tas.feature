Feature: cancel bg task
  Scenario: success cancel new bg task
    Given user logged in as ROLE_ADMIN
    Given exists bgtask {"uid":"1", "name":"bgtask", "objectState":"NEW"}
    Given command for uid 1
    When execute cancel command
    Then remove will execute

  Scenario: success cancel accepted bg task
    Given user logged in as ROLE_ADMIN
    Given exists bgtask {"uid":"1", "name":"bgtask", "objectState":"ACCEPTED"}
    Given command for uid 1
    When execute cancel command
    Then remove will not execute