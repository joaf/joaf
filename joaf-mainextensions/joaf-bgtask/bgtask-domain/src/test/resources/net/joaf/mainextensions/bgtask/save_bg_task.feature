Feature: save bg task
  Scenario: success save bg task
    Given exists bgtask {"uid":"1", "name":"bgtask", "objectState":"ACCEPTED"}
    Given command for object {"uid":"1", "name":"bgtask_renamed", "objectState":"ACCEPTED"}
    When execute save command
    Then store will execute
    And exception will not throw

  Scenario: error save bg task
    Given exists bgtask {"uid":"1", "name":"bgtask", "objectState":"ACCEPTED"}
    Given command for object {"uid":"2", "name":"bgtask_renamed", "objectState":"ACCEPTED"}
    When execute save command
    Then store will not execute
    And exception will throw