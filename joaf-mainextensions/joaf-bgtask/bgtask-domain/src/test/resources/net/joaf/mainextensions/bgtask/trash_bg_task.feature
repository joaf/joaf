Feature: trash bg task
  Scenario: should successfully trash
    Given exists bgtask {"uid":"1", "name":"bgtask", "objectState":"ACCEPTED"}
    Given command for uid 1
    When execute trash command
    Then object state will change to TRASH
    And exception will not throw

  Scenario: should unsuccessfully trash
    Given exists bgtask {"uid":"1", "name":"bgtask", "objectState":"ACCEPTED"}
    Given command for uid 2
    When execute trash command
    Then object state will not change to TRASH
    And exception will throw