Feature: undo trash feature
  Scenario: should successfully undo trash
    Given exists bgtask {"uid":"1", "name":"bgtask", "objectState":"ACCEPTED"}
    Given command for uid 1
    When execute undotrash command
    Then object state will change to ACCEPTED
    And exception will not throw

  Scenario: should unsuccessfully undo trash
    Given exists bgtask {"uid":"1", "name":"bgtask", "objectState":"ACCEPTED"}
    Given command for uid 2
    When execute undotrash command
    Then object state will not change to ACCEPTED
    And exception will throw