package net.joaf.mainextensions.bgtask.queries;

import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.bgtask.model.Bgtask;
import net.joaf.mainextensions.bgtask.model.enums.EBgTaskAction;
import net.joaf.mainextensions.bgtask.repository.api.BgtaskRepository;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * BgTask queries
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@Service
public class BgTaskQuery {

    public static final String BGTASK = "BGTASK";

    @Autowired
    private BgtaskRepository repository;

    @Autowired
    private Scheduler quartzScheduler;

    public PageredResult<Bgtask> findAll(PageredRequest request) throws JoafDatabaseException {
        PageredResult<Bgtask> all = repository.findAll(request);
        all.getResult().forEach(this::appendActions);
        try {
            final Set<JobKey> jobKeys = quartzScheduler.getJobKeys(GroupMatcher.jobGroupEquals(BGTASK));
            all.getResult().forEach(el -> {
                JobKey jobKey = new JobKey(el.getName(), BGTASK);
                if (jobKeys.contains(jobKey)) {
                    el.setActive(Boolean.TRUE);
                }
            });
        } catch (SchedulerException e) {
            throw new JoafDatabaseException("Error getting scheduler state");
        }
        return all;
    }

    public Bgtask findOne(String uid) throws JoafDatabaseException {
        return repository.findOne(uid);
    }

    private void appendActions(Bgtask item) {
        item.getActions().add(EBgTaskAction.EDIT);
        item.getActions().add(EBgTaskAction.TRASH);
        item.getActions().add(EBgTaskAction.SCHEDULE);
        item.getActions().add(EBgTaskAction.UNSCHEDULE);
    }
}
