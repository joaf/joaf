package net.joaf.mainextensions.bgtask.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.bgtask.commands.UndoTrashBgTaskCommand;
import net.joaf.mainextensions.bgtask.model.Bgtask;
import net.joaf.mainextensions.bgtask.repository.api.BgtaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import java.util.Optional;

/**
 * Remove BgTask command handler
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class UndoTrashBgTaskCommandHandler implements CommandHandler<UndoTrashBgTaskCommand, Object> {

    @Autowired
    private BgtaskRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, UndoTrashBgTaskCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(UndoTrashBgTaskCommand command) throws JoafException {
        Optional<Bgtask> bgtaskOptional = repository.findOneOptional(command.getElementUid());
        if (!bgtaskOptional.isPresent()) {
            throw new IllegalArgumentException("ELement not exists in repository");
        }
        repository.updateObjectState(command.getElementUid(), EObjectState.ACCEPTED);
        return null;
    }
}
