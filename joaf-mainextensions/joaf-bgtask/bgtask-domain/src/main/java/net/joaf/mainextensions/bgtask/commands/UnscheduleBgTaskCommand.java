package net.joaf.mainextensions.bgtask.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.mainextensions.bgtask.commands.handlers.UnscheduleBgTaskCommandHandler;

/**
 * Unschedule BgTask command
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandDto(handlerClass = UnscheduleBgTaskCommandHandler.class)
public class UnscheduleBgTaskCommand extends AbstractUidCommand implements Command {
    public UnscheduleBgTaskCommand(String elementUid, String userUid) {
        super(elementUid, userUid);
    }

    public UnscheduleBgTaskCommand(String elementUid, String userUid, String aggregateId) {
        super(elementUid, userUid, aggregateId);
    }
}
