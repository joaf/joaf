package net.joaf.mainextensions.bgtask.commands.handlers;

import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.bgtask.commands.TrashBgTaskCommand;
import net.joaf.mainextensions.bgtask.commands.UnscheduleBgTaskCommand;
import net.joaf.mainextensions.bgtask.model.Bgtask;
import net.joaf.mainextensions.bgtask.repository.api.BgtaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import java.util.Optional;

/**
 * Remove BgTask command handler
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class TrashBgTaskCommandHandler implements CommandHandler<TrashBgTaskCommand, Object> {

    @Autowired
    private BgtaskRepository repository;

    @Autowired
    private CommandGateway commandGateway;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, TrashBgTaskCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(TrashBgTaskCommand command) throws JoafException {
        Optional<Bgtask> bgtaskOptional = repository.findOneOptional(command.getElementUid());
        if (!bgtaskOptional.isPresent()) {
            throw new IllegalArgumentException("ELement not exists in repository");
        }
        commandGateway.send(new UnscheduleBgTaskCommand(command.getElementUid(), command.getUserUid(), command.getAggregateId()), UnscheduleBgTaskCommandHandler.class);
        repository.updateObjectState(command.getElementUid(), EObjectState.TRASH);
        return null;
    }
}
