package net.joaf.mainextensions.bgtask;

import net.joaf.base.extension.AbstractExtension;
import net.joaf.base.extension.JoafExtension;
import org.springframework.stereotype.Component;

/**
 * Created by cyprian on 01.02.15.
 */
@Component
public class BgtaskExtension extends AbstractExtension implements JoafExtension {
    public static final String EXTENSION_BASE_PATH = "/";
    public static final String EXTENSION_ADMIN_BASE_PATH = "/administration/bgtask";

    @Override
    public String getExtensionMetadataFile() {
        return "/extension-bgtask.xml";
    }
}
