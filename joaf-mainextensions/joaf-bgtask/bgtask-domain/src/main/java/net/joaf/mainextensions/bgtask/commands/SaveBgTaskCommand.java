package net.joaf.mainextensions.bgtask.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractEntityCommand;
import net.joaf.mainextensions.bgtask.commands.handlers.SaveBgTaskCommandHandler;
import net.joaf.mainextensions.bgtask.model.Bgtask;

/**
 * Save BgTask command
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandDto(handlerClass = SaveBgTaskCommandHandler.class)
public class SaveBgTaskCommand extends AbstractEntityCommand<Bgtask> implements Command {
    public SaveBgTaskCommand(Bgtask element, String userUid) {
        super(element, userUid);
    }
}
