package net.joaf.mainextensions.bgtask.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.bgtask.commands.CancelBgTaskCommand;
import net.joaf.mainextensions.bgtask.model.Bgtask;
import net.joaf.mainextensions.bgtask.repository.api.BgtaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Cancel edit BgTask command handler
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class CancelBgTaskCommandHandler implements CommandHandler<CancelBgTaskCommand, Object> {

    @Autowired
    private BgtaskRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, CancelBgTaskCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(CancelBgTaskCommand command) throws JoafException {
        Bgtask bgtask = repository.findOne(command.getElementUid());
        if (EObjectState.NEW.equals(bgtask.getObjectState())) {
            repository.remove(command.getElementUid());
        }
        return null;
    }
}
