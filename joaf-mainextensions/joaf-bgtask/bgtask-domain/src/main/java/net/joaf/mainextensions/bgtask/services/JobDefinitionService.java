/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.bgtask.services;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.job.JoafJob;
import net.joaf.mainextensions.bgtask.model.JobDefinition;
import net.joaf.mainextensions.bgtask.repository.api.JobDefinitionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Created by cyprian on 09.08.14.
 */
@Service
public class JobDefinitionService {

    private static final Logger logger = LoggerFactory.getLogger(JobDefinitionService.class);

    @Autowired
    private JobDefinitionRepository repository;

    @Autowired(required = false)
    private List<JoafJob> jobs = Arrays.asList();

    @PostConstruct
    public void init() {
        for (JoafJob job : jobs) {
            JobDefinition jobDefinition = new JobDefinition();
            jobDefinition.setModule(job.moduleName());
            jobDefinition.setClassName(job.getClass().getName());
            try {
                this.register(jobDefinition);
            } catch (Exception e) {
                logger.error("Error register job {}", jobDefinition, e);
            }
        }

    }

    public void register(JobDefinition command) throws JoafDatabaseException {
        Optional<JobDefinition> byClassName = repository.findByClassName(command.getClassName());
        if (!byClassName.isPresent()) {
            command.setUid(repository.prepareId());
            repository.insert(command);
        }
    }

    public List<JobDefinition> findAll() throws JoafDatabaseException {
        return repository.findAll();
    }

    public Map<String, String> findOptions() throws JoafDatabaseException {
        List<JobDefinition> all = this.findAll();
        Map<String, String> options = new HashMap<>();
        for (JobDefinition jd : all) {
            options.put(jd.getClassName(), jd.getModule() + " - " + jd.getClassName());
        }
        return options;
    }
}
