/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.bgtask.services;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.utils.DateUtils;
import net.joaf.mainextensions.bgtask.jobs.ScheduleAllActiveJob;
import net.joaf.mainextensions.bgtask.model.Bgtask;
import net.joaf.mainextensions.bgtask.model.enums.EBgtaskState;
import net.joaf.mainextensions.bgtask.repository.api.BgtaskRepository;
import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

/**
 * Created by cyprian on 09.08.14.
 */
@Service
public class BgtaskService implements ApplicationContextAware {

    public static final String BGTASK = "BGTASK";
    public static final String BGTASK_START = "BGTASK_START";
    public static final int FIVE_MINUTES = 5;
    @Autowired
    private BgtaskRepository repository;

    @Autowired
    private Scheduler quartzScheduler;

    private ApplicationContext applicationContext;

    @PostConstruct
    public void postConstruct() throws SchedulerException {
        String jobName = ScheduleAllActiveJob.JOB_NAME;
        String groupName = BGTASK_START; // Your Job Group

        JobDetailFactoryBean jobDetailFactoryBean = new JobDetailFactoryBean();
        jobDetailFactoryBean.setApplicationContext(applicationContext);
        jobDetailFactoryBean.setApplicationContextJobDataKey("applicationContext");
        jobDetailFactoryBean.setDurability(true);
        jobDetailFactoryBean.setGroup(groupName);
        jobDetailFactoryBean.setJobClass(ScheduleAllActiveJob.class);
        jobDetailFactoryBean.setName(jobName);

        jobDetailFactoryBean.afterPropertiesSet();
        JobDetail jobDetail = jobDetailFactoryBean.getObject();

        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity(jobName, groupName)
                .startAt(DateUtils.localDateTimeToDate(LocalDateTime.now().plusMinutes(FIVE_MINUTES)))
                .build();
        quartzScheduler.scheduleJob(jobDetail, trigger);
    }

    public List<Bgtask> findAll() throws JoafDatabaseException, SchedulerException {
        List<Bgtask> all = repository.findAll();
        Set<JobKey> jobKeys = quartzScheduler.getJobKeys(GroupMatcher.jobGroupEquals(BGTASK));
        for (Bgtask el : all) {
            JobKey jobKey = new JobKey(el.getName(), BGTASK);
            if (jobKeys.contains(jobKey)) {
                el.setActive(Boolean.TRUE);
            }
        }
        return all;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void unschedule(String id) throws JoafDatabaseException, SchedulerException {
        Bgtask dbBgtask = repository.findOne(id);
        dbBgtask.setScheduleState(EBgtaskState.INACTIVE);
        repository.updateScheduleState(id, EBgtaskState.INACTIVE);
        JobKey jobKey = new JobKey(dbBgtask.getName(), BGTASK);
        quartzScheduler.deleteJob(jobKey);
    }

}
