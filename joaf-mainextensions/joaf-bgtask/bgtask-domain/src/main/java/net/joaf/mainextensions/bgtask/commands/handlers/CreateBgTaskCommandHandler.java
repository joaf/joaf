package net.joaf.mainextensions.bgtask.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.bgtask.commands.CreateBgTaskCommand;
import net.joaf.mainextensions.bgtask.model.Bgtask;
import net.joaf.mainextensions.bgtask.model.enums.EBgtaskState;
import net.joaf.mainextensions.bgtask.model.enums.ETriggerType;
import net.joaf.mainextensions.bgtask.repository.api.BgtaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import java.util.UUID;

/**
 * Create BgTask command handler
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class CreateBgTaskCommandHandler implements CommandHandler<CreateBgTaskCommand, Object> {

    @Autowired
    private BgtaskRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, CreateBgTaskCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(CreateBgTaskCommand command) throws JoafException {
        Bgtask element = new Bgtask();
        element.setUid(UUID.randomUUID().toString());
        element.setScheduleState(EBgtaskState.INACTIVE);
        element.setTriggerType(ETriggerType.CRON);
        repository.insert(element);
        return element;
    }
}
