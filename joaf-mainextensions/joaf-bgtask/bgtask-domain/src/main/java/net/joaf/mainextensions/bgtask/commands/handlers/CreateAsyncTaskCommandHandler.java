package net.joaf.mainextensions.bgtask.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.error.JoafException;
import net.joaf.base.subjectcard.model.ApplicationSubjectCard;
import net.joaf.mainextensions.bgtask.commands.CreateAsyncTaskCommand;
import net.joaf.mainextensions.bgtask.model.AsyncTask;
import net.joaf.mainextensions.bgtask.repository.api.AsyncTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import java.util.UUID;

/**
 * Create AsyncTask command handler
 *
 * @author Cyprian Śniegota
 * @since 1.2
 */
@CommandHandlerComponent
public class CreateAsyncTaskCommandHandler implements CommandHandler<CreateAsyncTaskCommand, Object> {

    @Autowired
    private AsyncTaskRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, CreateAsyncTaskCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(CreateAsyncTaskCommand command) throws JoafException {
        AsyncTask element = new AsyncTask();
        element.setUid(UUID.randomUUID().toString());
        element.setSubjectUid(ApplicationSubjectCard.APP_UID);
        repository.insert(element);
        return element;
    }
}
