package net.joaf.mainextensions.bgtask.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.mainextensions.bgtask.commands.handlers.CreateAsyncTaskCommandHandler;

/**
 * Create AsyncTask command
 *
 * @author Cyprian Śniegota
 * @since 1.2
 */
@CommandDto(handlerClass = CreateAsyncTaskCommandHandler.class)
public class CreateAsyncTaskCommand extends AbstractUidCommand implements Command {
    public CreateAsyncTaskCommand(String userUid) {
        super(null, userUid);
    }
}
