package net.joaf.mainextensions.bgtask.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.bgtask.commands.UnscheduleBgTaskCommand;
import net.joaf.mainextensions.bgtask.model.Bgtask;
import net.joaf.mainextensions.bgtask.model.enums.EBgtaskState;
import net.joaf.mainextensions.bgtask.repository.api.BgtaskRepository;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Unschedule BgTask command handler
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class UnscheduleBgTaskCommandHandler implements CommandHandler<UnscheduleBgTaskCommand, Object> {

    public static final String BGTASK = "BGTASK";

    @Autowired
    private BgtaskRepository repository;

    @Autowired
    private Scheduler quartzScheduler;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, UnscheduleBgTaskCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(UnscheduleBgTaskCommand command) throws JoafException {
        try {
            Bgtask dbBgtask = repository.findOne(command.getElementUid());
            dbBgtask.setScheduleState(EBgtaskState.INACTIVE);
            repository.updateScheduleState(command.getElementUid(), EBgtaskState.INACTIVE);
            JobKey jobKey = new JobKey(dbBgtask.getName(), BGTASK);
            quartzScheduler.deleteJob(jobKey);
        } catch (SchedulerException e) {
            throw new JoafException("Command error", e);
        }
        return null;
    }
}
