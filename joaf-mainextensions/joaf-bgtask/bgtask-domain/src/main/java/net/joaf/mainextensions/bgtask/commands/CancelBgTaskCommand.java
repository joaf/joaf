package net.joaf.mainextensions.bgtask.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.mainextensions.bgtask.commands.handlers.CancelBgTaskCommandHandler;

/**
 * Cancel edit BgTask command
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandDto(handlerClass = CancelBgTaskCommandHandler.class)
public class CancelBgTaskCommand extends AbstractUidCommand implements Command {
    public CancelBgTaskCommand(String elementUid, String userUid) {
        super(elementUid, userUid);
    }
}
