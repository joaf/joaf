package net.joaf.mainextensions.bgtask.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.mainextensions.bgtask.commands.handlers.CreateBgTaskCommandHandler;

/**
 * Create BgTask command
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandDto(handlerClass = CreateBgTaskCommandHandler.class)
public class CreateBgTaskCommand extends AbstractUidCommand implements Command {
    public CreateBgTaskCommand(String userUid) {
        super(null, userUid);
    }
}
