package net.joaf.mainextensions.bgtask.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.bgtask.PersistableCronTriggerFactoryBean;
import net.joaf.mainextensions.bgtask.commands.ScheduleBgTaskCommand;
import net.joaf.mainextensions.bgtask.model.Bgtask;
import net.joaf.mainextensions.bgtask.model.enums.EBgtaskState;
import net.joaf.mainextensions.bgtask.repository.api.BgtaskRepository;
import org.apache.commons.lang3.StringUtils;
import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Set;

/**
 * Schedule BgTask command handler
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class ScheduleBgTaskCommandHandler implements CommandHandler<ScheduleBgTaskCommand, Object> {

    public static final String BGTASK = "BGTASK";

    @Autowired
    private BgtaskRepository repository;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private Scheduler quartzScheduler;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, ScheduleBgTaskCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(ScheduleBgTaskCommand command) throws JoafException {
        try {
            Bgtask dbBgtask = repository.findOne(command.getElementUid());
            if (StringUtils.isEmpty(dbBgtask.getParams().get("cronExpression"))) {
                throw new JoafException("Brak parametru cronExpression");
            }
            dbBgtask.setScheduleState(EBgtaskState.ACTIVE);
            repository.updateScheduleState(command.getElementUid(), EBgtaskState.ACTIVE);
            List<String> jobGroupNames = quartzScheduler.getJobGroupNames();
            List<JobExecutionContext> currentlyExecutingJobs = quartzScheduler.getCurrentlyExecutingJobs();
            Set<JobKey> jobKeys = quartzScheduler.getJobKeys(GroupMatcher.jobGroupEquals(BGTASK));
            JobDetailFactoryBean jobDetailFactoryBean = new JobDetailFactoryBean();
            jobDetailFactoryBean.setApplicationContext(applicationContext);
            jobDetailFactoryBean.setApplicationContextJobDataKey("applicationContext");
            jobDetailFactoryBean.setDurability(true);
            jobDetailFactoryBean.setGroup(BGTASK);
            jobDetailFactoryBean.setJobClass(Class.forName(dbBgtask.getJobClassName()));
            jobDetailFactoryBean.setName(dbBgtask.getName());

            jobDetailFactoryBean.afterPropertiesSet();
            JobDetail jobDetail = jobDetailFactoryBean.getObject();

            PersistableCronTriggerFactoryBean persistableCronTriggerFactoryBean = new PersistableCronTriggerFactoryBean();
            persistableCronTriggerFactoryBean.setName(dbBgtask.getName() + "Trigger");
            persistableCronTriggerFactoryBean.setJobDetail(jobDetail);
            persistableCronTriggerFactoryBean.setCronExpression(dbBgtask.getParams().get("cronExpression"));
            persistableCronTriggerFactoryBean.setGroup(BGTASK);
            persistableCronTriggerFactoryBean.afterPropertiesSet();
            CronTrigger trigger = persistableCronTriggerFactoryBean.getObject();
            quartzScheduler.scheduleJob(jobDetail, trigger);
/*
        JobDetailFactoryBean jobDetailFactoryBean = new JobDetailFactoryBean();
        jobDetailFactoryBean.setDurability(true);
        jobDetailFactoryBean.setJobClass(Class.forName(dbBgtask.getJobClassName()));
*/
        } catch (Exception e) {
            throw new JoafException("Command error", e);
        }
        return null;
    }
}
