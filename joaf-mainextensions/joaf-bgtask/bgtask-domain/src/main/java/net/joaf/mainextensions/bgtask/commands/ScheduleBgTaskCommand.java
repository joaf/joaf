package net.joaf.mainextensions.bgtask.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.mainextensions.bgtask.commands.handlers.ScheduleBgTaskCommandHandler;

/**
 * Schedule BgTask command
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandDto(handlerClass = ScheduleBgTaskCommandHandler.class)

public class ScheduleBgTaskCommand extends AbstractUidCommand implements Command {
    public ScheduleBgTaskCommand(String userUid, String elementUid) {
        super(userUid, elementUid);
    }
}
