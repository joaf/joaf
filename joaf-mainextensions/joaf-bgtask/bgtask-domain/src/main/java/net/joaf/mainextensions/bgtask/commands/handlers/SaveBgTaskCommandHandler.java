package net.joaf.mainextensions.bgtask.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.bgtask.commands.SaveBgTaskCommand;
import net.joaf.mainextensions.bgtask.model.Bgtask;
import net.joaf.mainextensions.bgtask.repository.api.BgtaskRepository;
import net.joaf.mainextensions.bgtask.services.BgtaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import java.util.Optional;

/**
 * Save BgTask command handler
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class SaveBgTaskCommandHandler implements CommandHandler<SaveBgTaskCommand, Object> {

    @Autowired
    private BgtaskService service;

    @Autowired
    private BgtaskRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, SaveBgTaskCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(SaveBgTaskCommand command) throws JoafException {
        Bgtask element = command.getElement();
        Optional<Bgtask> dbBgtaskOptional = repository.findOneOptional(element.getUid());
        if (!dbBgtaskOptional.isPresent()) {
            throw new IllegalArgumentException("No such element in repository");
        }
        element.setScheduleState(dbBgtaskOptional.get().getScheduleState());
        repository.store(element);
        return null;
    }
}
