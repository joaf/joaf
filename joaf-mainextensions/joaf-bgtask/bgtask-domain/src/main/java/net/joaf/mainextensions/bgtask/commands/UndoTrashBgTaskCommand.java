package net.joaf.mainextensions.bgtask.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.mainextensions.bgtask.commands.handlers.UndoTrashBgTaskCommandHandler;

/**
 * Remove BgTask command
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandDto(handlerClass = UndoTrashBgTaskCommandHandler.class)
public class UndoTrashBgTaskCommand extends AbstractUidCommand implements Command {
    public UndoTrashBgTaskCommand(String elementUid, String userUid) {
        super(elementUid, userUid);
    }
}
