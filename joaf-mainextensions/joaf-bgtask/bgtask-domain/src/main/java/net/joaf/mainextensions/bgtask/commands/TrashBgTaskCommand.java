package net.joaf.mainextensions.bgtask.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.mainextensions.bgtask.commands.handlers.TrashBgTaskCommandHandler;

/**
 * Remove BgTask command
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandDto(handlerClass = TrashBgTaskCommandHandler.class)
public class TrashBgTaskCommand extends AbstractUidCommand implements Command {
    public TrashBgTaskCommand(String elementUid, String userUid) {
        super(elementUid, userUid);
    }
}
