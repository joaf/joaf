<#import "/templates/base.ftl" as page />
<@page.layout "net.joaf.mainextensions.bgtask.modulename">


<div class="awidget full-width">
    <div class="panel">
        <div class="btn-group">
            <a href="${rc.contextPath}/administration/bgtask/create.html"
               class="btn btn-primary"><@spring.message 'joaf.create' /></a>
        </div>
        <div class="pull-right">
            <a href="${rc.contextPath}/administration.html" class="btn btn-primary"><i
                    class="fa fa-gear fa-fw"></i> <@spring.message 'joaf.back' /></a>
        </div>
        <table class="table table-bordered admin-media ">
            <thead>
            <tr>
                <th><@spring.message 'joaf.name' /></th>
                <th><@spring.message 'net.joaf.mainextensions.bgtask.jobclassname' /></th>
                <th><@spring.message 'joaf.state' /></th>
                <th><@spring.message 'joaf.uid' /></th>
                <th><@spring.message 'joaf.commands' /></th>
            </tr>
            </thead>
            <tbody>

                <#list elements as element>
                <tr>
                    <td>${element.data.name}</td>
                    <td>${element.data.jobClassName}</td>
                    <td>
                    ${element.data.scheduleState}
                        <#if element.data.active><#assign facolor='text-success' /><#else><#assign facolor='text-danger' /></#if>
                        <span aria-hidden="true" class="glyphicon glyphicon-flash ${facolor}"></span>
                        <i class="fa fa-lightbulb-o ${facolor}"></i>
                    </td>
                    <td>${element.data.uid}</td>
                    <td>
                        <#list element.webActions as action>
                            <a href="${rc.contextPath}${action.actionUrl}" class="btn btn-xs ${action.buttonStyle}"
                               data-toggle="tooltip" data-placement="bottom"
                               title="<@spring.message action.tooltip />"><span aria-hidden="true"
                                                                                class="glyphicon ${action.iconName}"></span></a>
                        </#list>
                    </td>
                </tr>
                </#list>
            </tbody>
        </table>
    </div>

</div>
</@page.layout>