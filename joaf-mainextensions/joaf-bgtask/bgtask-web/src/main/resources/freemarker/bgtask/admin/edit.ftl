<#import "/templates/base.ftl" as page />
<#import "/freemarker/joaf.ftl" as joaf />
<@page.layout "net.joaf.mainextensions.bgtask.editpage">
<div class="awidget full-width">
    <div class="awidget-head">
        <div class="btn-group">
        </div>
    </div>
    <div class="awidget-body">
        <form class="form-horizontal" role="form" method="post" action="">
            <@joaf.renderForm form=form formModel=formModel />
            <@spring.formHiddenInput 'command.uid' '' />
            <button type="submit" class="btn btn-primary"><@spring.message 'joaf.save' /></button>
            <button type="button"
                    onclick="$(this.form).attr('action','${rc.contextPath}/administration/bgtask/cancel/${command.uid}.html');$(this.form).submit();"
                    class="btn btn-default"><@spring.message 'joaf.cancel' /></button>
        </form>
    </div>
</div>
</@page.layout>