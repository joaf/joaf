package net.joaf.mainextensions.bgtask.admin.web.helpers;

import net.joaf.base.core.web.model.WebActionDetails;
import net.joaf.mainextensions.bgtask.model.enums.EBgTaskAction;

/**
 * Created by cyprian on 08.03.15.
 */
public class BgTaskActionHelper {

    public static WebActionDetails createActionDetails(EBgTaskAction action, String uid) {
        WebActionDetails webActionDetails = new WebActionDetails();
        switch (action) {
            case EDIT:
                webActionDetails.setButtonStyle("btn-primary");
                webActionDetails.setActionUrl("/administration/bgtask/edit/" + uid + ".html");
                webActionDetails.setIconName("glyphicon-edit");
                webActionDetails.setTooltip("joaf.edit");
                break;
            case TRASH:
                webActionDetails.setButtonStyle("btn-danger");
                webActionDetails.setActionUrl("/administration/bgtask/trash/" + uid + ".html");
                webActionDetails.setIconName("glyphicon-trash");
                webActionDetails.setTooltip("joaf.trash");
                break;
            case SCHEDULE:
                webActionDetails.setButtonStyle("btn-danger");
                webActionDetails.setActionUrl("/administration/bgtask/schedule/" + uid + ".html");
                webActionDetails.setIconName("glyphicon-flash");
                webActionDetails.setTooltip("joaf.trash");
                break;
            case UNSCHEDULE:
                webActionDetails.setButtonStyle("btn-danger");
                webActionDetails.setActionUrl("/administration/bgtask/unschedule/" + uid + ".html");
                webActionDetails.setIconName("glyphicon-ban-circle");
                webActionDetails.setTooltip("joaf.trash");
                break;
            default:
                webActionDetails = null;
                break;
        }
        return webActionDetails;
    }
}
