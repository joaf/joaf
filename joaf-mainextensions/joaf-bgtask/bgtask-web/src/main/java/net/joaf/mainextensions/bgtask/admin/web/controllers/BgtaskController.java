/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.bgtask.admin.web.controllers;

import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.cqrs.PageredResultExtended;
import net.joaf.base.core.error.JoafException;
import net.joaf.base.core.error.JoafNoSubjectContextException;
import net.joaf.base.core.formmodel.Form;
import net.joaf.base.core.formmodel.XmlFormHelper;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.base.core.web.model.WrapperWDTO;
import net.joaf.mainextensions.bgtask.admin.web.helpers.BgTaskActionHelper;
import net.joaf.mainextensions.bgtask.commands.*;
import net.joaf.mainextensions.bgtask.commands.handlers.*;
import net.joaf.mainextensions.bgtask.model.Bgtask;
import net.joaf.mainextensions.bgtask.model.enums.ETriggerType;
import net.joaf.mainextensions.bgtask.queries.BgTaskQuery;
import net.joaf.mainextensions.bgtask.services.JobDefinitionService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.*;

/**
 * BgTask management web controller
 *
 * @author Cyprian Śniegota
 * @since 1.0
 */
@Controller
@RequestMapping("/administration/bgtask")
public class BgtaskController {

    private static final String EXTENSION_BASE_PATH = "/administration/bgtask";

    private static final Logger log = LoggerFactory.getLogger(BgtaskController.class);

    @Autowired
    private CommandGateway commandGateway;

    @Autowired
    private BgTaskQuery query;

    @Autowired
    private JobDefinitionService jobDefinitionService;

    @RequestMapping
    public String list(HttpSession session, Model model) {
        try {
            PageredRequest pageredRequest = new PageredRequest();
            PageredResult<Bgtask> all = query.findAll(pageredRequest);
            PageredResultExtended<Bgtask> allExt = new PageredResultExtended<>(all, this::createActions);
            model.addAttribute("elements", allExt.getElements());
        } catch (Exception e) {
            model.addAttribute("elements", Collections.emptyList());
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            log.debug("error", e);
        }
        return "freemarker/bgtask/admin/list";
    }

    private void createActions(WrapperWDTO<Bgtask> bgtaskWdto) {
        bgtaskWdto.getData().getActions().forEach(action -> bgtaskWdto.getWebActions().add(BgTaskActionHelper.createActionDetails(action, bgtaskWdto.getData().getUid())));
    }

    @RequestMapping("/create")
    public String create(HttpSession session, RedirectAttributes redirectAttributes, Principal principal, Model model) {
        try {
            Bgtask command = (Bgtask) commandGateway.send(new CreateBgTaskCommand(principal.getName()), CreateBgTaskCommandHandler.class);
            return getRedirect("/edit/" + command.getUid() + ".html");
        } catch (Exception e) {
            log.debug("error", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return "redirect:/administration/bgtask.html";
        }
    }

    @RequestMapping("/edit/{id}")
    public String newItem(@PathVariable("id") String id, HttpSession session, RedirectAttributes redirectAttributes, Model model) {
        try {
            Bgtask command = query.findOne(id);
            model.addAttribute("command", command);
            addFormToModel(model);
            return "/freemarker/bgtask/admin/edit";
        } catch (Exception e) {
            log.debug("error", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return "redirect:/administration/bgtask.html";
        }
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String save(@PathVariable("id") String id, @Valid @ModelAttribute("command") Bgtask command, BindingResult result, Model model,
                       Principal principal, HttpSession session, final RedirectAttributes redirectAttributes) {
        try {
            SaveBgTaskCommand saveCommand = new SaveBgTaskCommand(command, principal.getName());
            commandGateway.validate(result, saveCommand);
            if (result.hasErrors()) {
                return getViewTemplate("/edit");
            }
            commandGateway.send(saveCommand, SaveBgTaskCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.formsavesuccess.title", "joaf.message.successsave");
            return getRedirect(".html");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addModelMessage(model, "joaf.formsaveerror.title", "joaf.error.body", e.getLocalizedMessage());
            return getViewTemplate("/edit");
        }
    }

    @RequestMapping("/cancel/{uid}")
    public String cancel(@PathVariable("uid") String uid, HttpSession session, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        try {
            commandGateway.send(new CancelBgTaskCommand(uid, principal.getName()), CancelBgTaskCommandHandler.class);
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addError(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect(".html");
    }

    @RequestMapping("/schedule/{uid}")
    public String schedule(@PathVariable("uid") String uid, HttpSession session, Principal principal, Model model, RedirectAttributes redirectAttributes)
            throws JoafNoSubjectContextException {
        try {
            commandGateway.send(new ScheduleBgTaskCommand(uid, principal.getName()), ScheduleBgTaskCommandHandler.class);
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addError(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect(".html");
    }

    @RequestMapping("/unschedule/{uid}")
    public String unschedule(@PathVariable("uid") String uid, HttpSession session, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        try {
            commandGateway.send(new UnscheduleBgTaskCommand(uid, principal.getName()), UnscheduleBgTaskCommandHandler.class);
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addError(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect(".html");
    }

    @RequestMapping("/trash/{uid}")
    public String trash(@PathVariable("uid") String uid, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        try {
            commandGateway.send(new TrashBgTaskCommand(uid, principal.getName()), TrashBgTaskCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successtrash", httpRequest.getContextPath() + "/administration/bgtask/undotrash/" + uid + ".html");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect(".html");
    }

    @RequestMapping("/undotrash/{id}")
    public String undotrash(@PathVariable("id") String id, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) {
        try {
            commandGateway.send(new UndoTrashBgTaskCommand(id, principal.getName()), UndoTrashBgTaskCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successundotrash", "1");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect(".html");
    }

    private void addFormToModel(Model model) throws JoafException {
        Form form = XmlFormHelper.readFromClasspath("/forms/bgtask.xml");
        model.addAttribute("form", form);
        HashMap<String, Object> formModel = new HashMap<>();
        Map<String, String> jobDefinitionList = jobDefinitionService.findOptions();
        formModel.put("jobDefinitionList", jobDefinitionList);
        formModel.put("triggerTypeList", Arrays.asList(ETriggerType.values()));
        model.addAttribute("formModel", formModel);

    }

    private String getRedirect(String subPath) {
        return "redirect:" + EXTENSION_BASE_PATH + "" + subPath;
    }

    private String getViewTemplate(String subPath) {
        return "freemarker" + EXTENSION_BASE_PATH + StringUtils.trimToEmpty(subPath);
    }

}
