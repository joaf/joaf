package net.joaf.mainextensions.bgtask.repository;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.mainextensions.bgtask.model.JobDefinition;
import net.joaf.mainextensions.bgtask.repository.api.JobDefinitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by cyprian on 01.02.15.
 */
@Repository
public class JobDefinitionRepositoryMysql extends AbstractRepositoryMysql<JobDefinition> implements JobDefinitionRepository {

    private static final JobDefinitionRowMapper ROW_MAPPER = new JobDefinitionRowMapper();

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    protected String getTableName() {
        return "jobDefinition";
    }

    @Override
    protected JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    protected JobDefinition mapRow(ResultSet rs, int rowNum) throws SQLException {
        return ROW_MAPPER.mapRow(rs, rowNum);
    }

    @Override
    public Optional<JobDefinition> findByClassName(String className) throws JoafDatabaseException {
        return findOneByFieldOptional("className", className);
    }

    @Override
    public void store(JobDefinition command) throws JoafDatabaseException {

    }

    @Override
    public String collectionName() {
        return getTableName();
    }

    @Override
    public Class<JobDefinition> getEntityClass() {
        return JobDefinition.class;
    }

    @Override
    public void insert(JobDefinition command) throws JoafDatabaseException {
        String query = "INSERT INTO " + getTableName() + " (uid, className, module) VALUES (?, ?, ?)";
        jdbcTemplate.update(query, new Object[]{command.getUid(), command.getClassName(), command.getModule()});
    }

    @Override
    public String prepareId() {
        return UUID.randomUUID().toString();
    }

    private static class JobDefinitionRowMapper implements RowMapper<JobDefinition> {

        @Override
        public JobDefinition mapRow(ResultSet resultSet, int number) throws SQLException {
            JobDefinition jobDefinition = new JobDefinition();
            jobDefinition.setUid(resultSet.getString("uid"));
            jobDefinition.setClassName(resultSet.getString("className"));
            jobDefinition.setModule(resultSet.getString("module"));
            return jobDefinition;
        }
    }
}
