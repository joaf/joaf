package net.joaf.mainextensions.bgtask.repository;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.mainextensions.bgtask.model.AsyncTask;
import net.joaf.mainextensions.bgtask.repository.api.AsyncTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Async task repository - mysql implementation
 *
 * @author Cyprian Śniegota
 * @since 1.2
 */
@Repository
public class AsyncTaskRepositoryMysql extends AbstractRepositoryMysql<AsyncTask> implements AsyncTaskRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    protected String getTableName() {
        return "asyncTask";
    }

    @Override
    protected JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    protected AsyncTask mapRow(ResultSet rs, int rowNum) throws SQLException {
        return getRowMapper().mapRow(rs, rowNum);
    }

    protected RowMapper<AsyncTask> getRowMapper() {
        return (rs, rowNum) -> {
            AsyncTask element = new AsyncTask();
            return element;
        };
    }

    @Override
    public void insert(AsyncTask element) throws JoafDatabaseException {

    }

    @Override
    public void store(AsyncTask element) throws JoafDatabaseException {

    }

    @Override
    public Class<AsyncTask> getEntityClass() {
        return AsyncTask.class;
    }
}
