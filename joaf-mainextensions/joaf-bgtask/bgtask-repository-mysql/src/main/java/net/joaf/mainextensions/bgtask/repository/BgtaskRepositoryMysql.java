package net.joaf.mainextensions.bgtask.repository;

import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.utils.JsonUtils;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.mainextensions.bgtask.model.Bgtask;
import net.joaf.mainextensions.bgtask.model.enums.EBgtaskState;
import net.joaf.mainextensions.bgtask.model.enums.ETriggerType;
import net.joaf.mainextensions.bgtask.repository.api.BgtaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by cyprian on 01.02.15.
 */
@Repository
public class BgtaskRepositoryMysql extends AbstractRepositoryMysql<Bgtask> implements BgtaskRepository {
    private static final BgtaskRowMapper ROW_MAPPER = new BgtaskRowMapper();
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static Map<String, String> unpackMap(String params) {
        return JsonUtils.getPojoStringMap(params, String.class);
    }

    @Override
    protected String getTableName() {
        return "bgTask";
    }

    @Override
    protected JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    protected Bgtask mapRow(ResultSet rs, int rowNum) throws SQLException {
        return ROW_MAPPER.mapRow(rs, rowNum);
    }

    @Override
    public void store(Bgtask command) throws JoafDatabaseException {
        try {
            command.setObjectState(EObjectState.ACCEPTED);
            command.setCronExpression(command.getParams().get("cronExpression"));
            String sql = "UPDATE bgTask set scheduleState=?, name=?, description=?, jobClassName=?, jobModule=?, triggerType=?, cronExpression=?, params=?, objectState = ? WHERE uid = ?";
            jdbcTemplate.update(sql, command.getScheduleState().toString(), command.getName(), command.getDescription(), command.getJobClassName(), command.getJobModule(),
                    command.getTriggerType().toString(), command.getCronExpression(), packMap(command.getParams()), command.getObjectState().toString(), command.getUid());
        } catch (Exception e) {
            throw new JoafDatabaseException("Error database", e);
        }
    }

    @Override
    public String collectionName() {
        return getTableName();
    }

    @Override
    public Class<Bgtask> getEntityClass() {
        return Bgtask.class;
    }

    @Override
    public void insert(Bgtask command) throws JoafDatabaseException {
        try {
            command.setObjectState(EObjectState.NEW);
            String sql = "insert into bgTask (uid, scheduleState, name, description, jobClassName, jobModule, triggerType, params, objectState) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            jdbcTemplate.update(sql, command.getUid(), command.getScheduleState().toString(), command.getName(), command.getDescription(), command.getJobClassName(),
                    command.getJobModule(), command.getTriggerType().toString(), packMap(command.getParams()), command.getObjectState().toString());
        } catch (Exception e) {
            throw new JoafDatabaseException("Error database", e);
        }
    }

    private String packMap(Map<String, String> params) throws IOException {
        return JsonUtils.stringValue(params);
    }

    @Override
    public void updateScheduleState(String id, EBgtaskState active) throws JoafDatabaseException {
        try {
            String sql = "UPDATE bgTask set scheduleState=? WHERE uid=?";
            jdbcTemplate.update(sql, active.toString(), id);
        } catch (Exception e) {
            throw new JoafDatabaseException("Database error", e);
        }
    }

    private static class BgtaskRowMapper implements RowMapper<Bgtask> {

        @Override
        public Bgtask mapRow(ResultSet resultSet, int number) throws SQLException {
            Bgtask bgtask = new Bgtask();
            bgtask.setUid(resultSet.getString("uid"));
            String scheduleState = resultSet.getString("scheduleState");
            if (scheduleState != null) {
                bgtask.setScheduleState(EBgtaskState.valueOf(scheduleState));
            }
            String triggerType = resultSet.getString("triggerType");
            if (triggerType != null) {
                bgtask.setTriggerType(ETriggerType.valueOf(triggerType));
            }
            String objectState = resultSet.getString("objectState");
            if (objectState != null) {
                bgtask.setObjectState(EObjectState.valueOf(objectState));
            }
            bgtask.setName(resultSet.getString("name"));
            bgtask.setCronExpression(resultSet.getString("cronExpression"));
            bgtask.setDescription(resultSet.getString("description"));
            bgtask.setJobClassName(resultSet.getString("jobClassName"));
            bgtask.setJobModule(resultSet.getString("jobModule"));
            bgtask.setParams(unpackMap(resultSet.getString("params")));
            bgtask.getParams().put("cronExpression", bgtask.getCronExpression());
            return bgtask;
        }
    }
}
