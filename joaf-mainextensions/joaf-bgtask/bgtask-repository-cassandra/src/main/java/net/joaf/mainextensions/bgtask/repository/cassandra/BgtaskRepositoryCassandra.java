/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.bgtask.repository.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.cassandra.AbstractCrudRepositoryCassandra;
import net.joaf.base.db.cassandra.CassandraSessionProvider;
import net.joaf.mainextensions.bgtask.model.Bgtask;
import net.joaf.mainextensions.bgtask.model.enums.EBgtaskState;
import net.joaf.mainextensions.bgtask.model.enums.ETriggerType;
import net.joaf.mainextensions.bgtask.repository.api.BgtaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by cyprian on 09.08.14.
 */
@Repository
public class BgtaskRepositoryCassandra extends AbstractCrudRepositoryCassandra<Bgtask> implements BgtaskRepository {

    @Autowired
    private CassandraSessionProvider sessionProvider;

    protected Bgtask bindRow(Row row) {
        Bgtask jobDefinition = new Bgtask();
        jobDefinition.setUid(row.getUUID("uid").toString());
        jobDefinition.setParams(row.getMap("params", String.class, String.class));
        String triggerType = row.getString("triggerType");
        jobDefinition.setTriggerType(ETriggerType.valueOf(triggerType));
        jobDefinition.setJobClassName(row.getString("jobClassName"));
        jobDefinition.setJobModule(row.getString("jobModule"));
        jobDefinition.setName(row.getString("name"));
        jobDefinition.setDescription(row.getString("description"));
        jobDefinition.setScheduleState(EBgtaskState.valueOf(row.getString("scheduleState")));
        return jobDefinition;
    }

    @Override
    public List<Bgtask> findAll() throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        ResultSet rs = session.execute("select * from bgtask");
        List<Row> all = rs.all();
        return all.stream().map(this::bindRow).collect(Collectors.toList());
    }

    @Override
    public Bgtask findOne(String id) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        ResultSet rs = session.execute("select * from bgtask where uid = ? ", UUID.fromString(id));
        List<Row> all = rs.all();
        if (all.size() > 0) {
            return this.bindRow(all.get(0));
        } else {
            return null;
        }
    }

    @Override
    public String collectionName() {
        return "bgtask";
    }

    @Override
    public Class<Bgtask> getEntityClass() {
        return Bgtask.class;
    }

    @Override
    public void store(Bgtask command) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        String sql = "UPDATE bgtask SET jobModule= ?, jobClassName = ?,  params = ?, triggerType = ?, name = ?, description = ?, scheduleState = ? WHERE uid = ?";
        session.execute(sql, command.getJobModule(), command.getJobClassName(), command.getParams(), command.getTriggerType().toString(),
                command.getName(), command.getDescription(), command.getScheduleState().toString(), UUID.fromString(command.getUid()));
    }

    @Override
    public void insert(Bgtask command) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        String sql = "INSERT INTO bgtask (uid) VALUES (?)";
        UUID uuid = UUID.fromString(command.getUid());
        session.execute(sql, uuid);
        this.store(command);
    }

    @Override
    public void remove(String id) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        String sql = "DELETE FROM bgtask WHERE uid = ?";
        session.execute(sql, UUID.fromString(id));
    }

    @Override
    public void updateScheduleState(String id, EBgtaskState state) {
        Session session = sessionProvider.connect();
        String sql = "UPDATE bgtask SET scheduleState = ? WHERE uid = ?";
        session.execute(sql, state.toString(), UUID.fromString(id));
    }

    public String prepareId() {
        return UUID.randomUUID().toString();
    }
}
