/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.oauth2.webui.conf;

import net.joaf.base.language.utils.I18nPropertyDefinition;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * Created by cyprian on 15.03.15.
 */
@Component
public class Oauth2I18nPropertyDefinition implements I18nPropertyDefinition {
    @Override
    public List<String> propertyFiles() {
        return Collections.singletonList("classpath:i18n/connection");
    }
}
