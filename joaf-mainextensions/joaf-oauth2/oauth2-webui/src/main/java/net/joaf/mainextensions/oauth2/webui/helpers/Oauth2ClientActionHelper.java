/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.oauth2.webui.helpers;

import net.joaf.base.core.model.enums.EStandardAction;
import net.joaf.base.core.web.model.WebActionDetails;

/**
 * Created by cyprian on 03.04.15.
 */
public class Oauth2ClientActionHelper {
    public static WebActionDetails createActionDetails(EStandardAction action, String uid, String controllerContextPath) {
        WebActionDetails webActionDetails = new WebActionDetails();
        switch (action) {
            case EDIT:
                webActionDetails.setButtonStyle("btn-primary");
                webActionDetails.setActionUrl(controllerContextPath + "/edit/" + uid + ".html");
                webActionDetails.setIconName("glyphicon-edit");
                webActionDetails.setTooltip("joaf.edit");
                break;
            case TRASH:
                webActionDetails.setButtonStyle("btn-danger");
                webActionDetails.setActionUrl(controllerContextPath + "/trash/" + uid + ".html");
                webActionDetails.setIconName("glyphicon-trash");
                webActionDetails.setTooltip("joaf.trash");
                break;
            default:
                webActionDetails = null;
                break;
        }
        return webActionDetails;
    }
}
