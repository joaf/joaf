/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.oauth2.webui.controllers;

import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.cqrs.PageredResultExtended;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.session.SessionContext;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.base.core.web.model.WebActionDetails;
import net.joaf.base.core.web.model.WrapperWDTO;
import net.joaf.mainextensions.oauth2.model.Oauth2Client;
import net.joaf.mainextensions.oauth2.queries.Oauth2ClientQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Collections;
import java.util.Optional;

/**
 * Created by cyprian on 03.04.15.
 */
@Controller
@RequestMapping("/oauth2client")
public class Oauth2ClientController {

    private static final Logger log = LoggerFactory.getLogger(Oauth2ClientAdminController.class);
    @Autowired
    private Oauth2ClientQuery connectionDefinitionQuery;
    @Autowired
    private CommandGateway commandGateway;

    @RequestMapping("")
    public String list(HttpSession session, Model model, Principal principal) {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        try {
            PageredRequest pageredRequest = new PageredRequest(EObjectState.ACCEPTED);
            PageredResult<Oauth2Client> all = connectionDefinitionQuery.findAll(pageredRequest);
            PageredResultExtended<Oauth2Client> allExt = new PageredResultExtended<>(all, this::createActions);
            model.addAttribute("elements", allExt.getElements());
        } catch (Exception e) {
            model.addAttribute("elements", Collections.emptyList());
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            log.debug("error", e);
        }
        updateModelWithPaths(model);
        return getViewitem("/list");
    }

    private void createActions(WrapperWDTO<Oauth2Client> x) {
        WebActionDetails webActionDetails = new WebActionDetails();
        webActionDetails.setButtonStyle("btn-primary");
        webActionDetails.setActionUrl(getControllerContextPath() + x.getData().getUid());
        webActionDetails.setIconName("glyphicon-list");
        webActionDetails.setTooltip("joaf.edit");
        x.getWebActions().add(webActionDetails);
    }

    protected void updateModelWithPaths(Model model) {
        model.addAttribute("backPath", getBackPath());
        model.addAttribute("controllerContextPath", getControllerContextPath());
    }

    protected String getBackPath() {
        return "/";
    }

    protected String getControllerContextPath() {
        return "/connection";
    }

    protected String getViewitem(String name) {
        return "/freemarker/connection/site" + name;
    }

    protected String getRedirect(String name) {
        return "redirect:" + getUrl(name);
    }

    protected String getUrl(String name) {
        return "/connection" + name + ".html";
    }

}
