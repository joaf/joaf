/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.oauth2.webui.controllers;

import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.cqrs.PageredResultExtended;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafNoSubjectContextException;
import net.joaf.base.core.model.enums.EStandardAction;
import net.joaf.base.core.session.SessionContext;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.base.core.web.model.WebActionDetails;
import net.joaf.base.core.web.model.WrapperWDTO;
import net.joaf.mainextensions.oauth2.commands.*;
import net.joaf.mainextensions.oauth2.commands.handlers.*;
import net.joaf.mainextensions.oauth2.model.Oauth2Client;
import net.joaf.mainextensions.oauth2.queries.Oauth2ClientQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Collections;
import java.util.Optional;

/**
 * Created by cyprian on 03.04.15.
 */
@Controller
@RequestMapping("/administration/oauth2client")
public class Oauth2ClientAdminController {

    private static final Logger log = LoggerFactory.getLogger(Oauth2ClientAdminController.class);
    @Autowired
    private Oauth2ClientQuery query;
    @Autowired
    private CommandGateway commandGateway;

    @RequestMapping("")
    public String list(HttpSession session, Model model, Principal principal) {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        try {
            PageredRequest pageredRequest = new PageredRequest(EObjectState.ACCEPTED);
            PageredResult<Oauth2Client> all = query.findAll(pageredRequest);
            PageredResultExtended<Oauth2Client> allExt = new PageredResultExtended<>(all, this::createActions);
            model.addAttribute("elements", allExt.getElements());
        } catch (Exception e) {
            model.addAttribute("elements", Collections.emptyList());
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            log.debug("error", e);
        }
        updateModelWithPaths(model);
        return getViewitem("/list");
    }

    private void createActions(WrapperWDTO<Oauth2Client> x) {
        for (EStandardAction action : x.getData().getActions()) {
            WebActionDetails webActionDetails = new WebActionDetails();
            webActionDetails.setButtonStyle("btn-primary");
            x.getWebActions().add(webActionDetails);
            if (EStandardAction.EDIT.equals(action)) {
                webActionDetails.setActionUrl(getControllerContextPath() + "/edit/" + x.getData().getUid());
                webActionDetails.setIconName("glyphicon-list");
                webActionDetails.setTooltip("joaf.edit");
            } else if (EStandardAction.TRASH.equals(action)) {
                webActionDetails.setActionUrl(getControllerContextPath() + "/trash/" + x.getData().getUid());
                webActionDetails.setIconName("glyphicon-trash");
                webActionDetails.setTooltip("joaf.remove");
            } else {
                x.getWebActions().remove(webActionDetails);
            }
        }
    }

    @RequestMapping("/create")
    public String create(HttpSession session, RedirectAttributes redirectAttributes, Principal principal, Model model) {
        try {
            Oauth2Client command = (Oauth2Client) commandGateway.send(new CreateOauth2ClientCommand(principal.getName()), CreateOauth2ClientCommandHandler.class);
            return getRedirect("/edit/" + command.getUid());
        } catch (Exception e) {
            log.debug("error", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return getRedirect("");
        }
    }

    @RequestMapping("/edit/{id}")
    public String newItem(@PathVariable("id") String id, HttpSession session, RedirectAttributes redirectAttributes, Model model) {
        try {
            Oauth2Client command = query.findOne(id);
            model.addAttribute("command", command);
            updateModelWithPaths(model);
            return getViewitem("/edit");
        } catch (Exception e) {
            log.debug("error", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return getRedirect("");
        }
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String save(@PathVariable("id") String id, @Valid @ModelAttribute("command") Oauth2Client command, BindingResult result, Model model,
                       Principal principal, HttpSession session, final RedirectAttributes redirectAttributes) {
        try {
            SaveOauth2ClientCommand saveCommand = new SaveOauth2ClientCommand(command, principal.getName());
            commandGateway.validate(result, saveCommand);
            if (result.hasErrors()) {
                updateModelWithPaths(model);
                return getViewitem("/edit");
            }
            commandGateway.send(saveCommand, SaveOauth2ClientCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.formsavesuccess.title", "joaf.message.successsave");
            return getRedirect("");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addModelMessage(model, "joaf.formsaveerror.title", "joaf.error.body", e.getLocalizedMessage());
            updateModelWithPaths(model);
            return getViewitem("/edit");
        }
    }

    @RequestMapping("/cancel/{uid}")
    public String cancel(@PathVariable("uid") String uid, HttpSession session, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        try {
            commandGateway.send(new CancelOauth2ClientCommand(uid, principal.getName()), CancelOauth2ClientCommandHandler.class);
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addError(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    @RequestMapping("/trash/{uid}")
    public String trash(@PathVariable("uid") String uid, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        try {
            commandGateway.send(new TrashOauth2ClientCommand(uid, principal.getName()), TrashOauth2ClientCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successtrash", httpRequest.getContextPath() + "/administration/oauth2client/undotrash/" + uid + ".html");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    @RequestMapping("/undotrash/{id}")
    public String undotrash(@PathVariable("id") String id, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) {
        try {
            commandGateway.send(new UndoTrashOauth2ClientCommand(id, principal.getName()), UndoTrashOauth2ClientCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successundotrash", "1");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    protected void updateModelWithPaths(Model model) {
        model.addAttribute("backPath", getBackPath());
        model.addAttribute("controllerContextPath", getControllerContextPath());
    }

    protected String getBackPath() {
        return "/";
    }

    protected String getControllerContextPath() {
        return "/administration/oauth2client";
    }

    protected String getViewitem(String name) {
        return "/freemarker/oauth2" + name;
    }

    protected String getRedirect(String name) {
        return "redirect:" + getUrl(name);
    }

    protected String getUrl(String name) {
        return "/administration/oauth2client" + name + ".html";
    }
}
