<#import "/templates/base.ftl" as page />
<@page.layout "net.joaf.connection.smtp.edit">
<div class="awidget full-width">
    <div class="awidget-head">
        <div class="btn-group">
        <#--<a href="/remoteconnections/edit/0.html" class="btn btn-primary">Create</a>-->
            <#--<a href="/invoice/syncInfakt.html" class="btn btn-info">Infakt sync</a>-->
        <#--<button class="btn btn-success">Center</button>-->
        <#--<button class="btn btn-primary">Right</button>-->
        </div>
    </div>
    <div class="awidget-body">
        <form class="form-horizontal" role="form" method="post"
              action="${rc.contextPath}${controllerContextPath}/edit/${command.uid}.html">

            <div class="form-group">
                <label class="col-lg-2 control-label"><@spring.message 'joaf.name' /></label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.client_id' 'placeholder="Alias" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label"><@spring.message 'joaf.host' /></label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.client_secret' 'placeholder="Host" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label"><@spring.message 'joaf.port' /></label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.redirectUrl' 'placeholder="0" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label"><@spring.message 'joaf.username' /></label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.accessTokenValidity' 'placeholder="username" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label"><@spring.message 'joaf.password' /></label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.refreshTokenValidity' 'placeholder="password" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>

            <@spring.formHiddenInput 'command.uid' '' />
            <button type="submit" class="btn btn-primary"><@spring.message 'joaf.save' /></button>
            <button type="button"
                    onclick="$(this.form).attr('action','${rc.contextPath}${controllerContextPath}/cancel/${command.uid}.html');$(this.form).submit();"
                    class="btn btn-default"><@spring.message 'joaf.cancel' />
            </button>
        </form>
    </div>

</div>
</@page.layout>