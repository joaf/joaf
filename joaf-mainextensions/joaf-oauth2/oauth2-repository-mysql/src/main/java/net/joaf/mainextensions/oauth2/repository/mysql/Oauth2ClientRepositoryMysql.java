/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.oauth2.repository.mysql;

import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.utils.DateUtils;
import net.joaf.base.core.utils.JsonUtils;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.mainextensions.oauth2.model.Oauth2Client;
import net.joaf.mainextensions.oauth2.repository.api.Oauth2ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

/**
 * @author Cyprian Śniegota
 * @since 1.2
 */
@Repository
public class Oauth2ClientRepositoryMysql extends AbstractRepositoryMysql<Oauth2Client> implements Oauth2ClientRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    protected String getTableName() {
        return "oauth2_client";
    }

    @Override
    protected JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    protected Oauth2Client mapRow(ResultSet rs, int rowNum) throws SQLException {
        try {
            Oauth2Client element = new Oauth2Client();
            element.setUid(rs.getString("uid"));
            element.setCreated(DateUtils.dateToLocalDateTime(rs.getDate("created")));
            element.setUpdated(DateUtils.dateToLocalDateTime(rs.getDate("updated")));
            element.setObjectState(EObjectState.valueOf(rs.getString("objectState")));
            element.setClient_id(rs.getString("client_id"));
            element.setClient_secret(rs.getString("client_secret"));
            element.setAccessTokenValidity(rs.getInt("accessTokenValidity"));
            element.setRefreshTokenValidity(rs.getInt("refreshTokenValidity"));
            element.getAllowedRedirectUrls().addAll(JsonUtils.getPojoSet(rs.getString("allowedRedirectUrls"), String.class));
            element.getAllowedIpAddresses().addAll(JsonUtils.getPojoSet(rs.getString("allowedIpAddresses"), String.class));
            element.getAuthorizationGrantTypes().addAll(JsonUtils.getPojoSet(rs.getString("authorizationGrantTypes"), String.class));
            element.getAuthorities().addAll(JsonUtils.getPojoSet(rs.getString("authorities"), String.class));
            element.getScope().addAll(JsonUtils.getPojoSet(rs.getString("scope"), String.class));
            return element;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void insert(Oauth2Client element) throws JoafDatabaseException {
        try {
            String sql = "INSERT INTO " + getTableName()
                    + " (uid, created, updated, objectState, client_id, client_secret, redirectUrl, accessTokenValidity, refreshTokenValidity"
                    + ", allowedRedirectUrls, allowedIpAddresses, authorizationGrantTypes, authorities, scope) VALUES (?,?,?,?,?,?,?,?,?, ?,?,?,?,?)";
            jdbcTemplate.update(sql, element.getUid(), DateUtils.localDateTimeToDate(element.getCreated()), DateUtils.localDateTimeToDate(element.getUpdated()), element.getObjectState().toString(),
                    element.getClient_id(), element.getClient_secret(), element.getRedirectUrl(), element.getAccessTokenValidity(), element.getRefreshTokenValidity(),
                    JsonUtils.stringValue(element.getAllowedRedirectUrls()), JsonUtils.stringValue(element.getAllowedIpAddresses()), JsonUtils.stringValue(element.getAuthorizationGrantTypes()),
                    JsonUtils.stringValue(element.getAuthorities()), JsonUtils.stringValue(element.getScope())
            );

        } catch (Exception e) {
            throw new JoafDatabaseException("Repository error", e);
        }
    }

    @Override
    public void store(Oauth2Client element) throws JoafDatabaseException {
        try {
            String sql = "UPDATE " + getTableName()
                    + " SET updated=?, client_id=?, client_secret=?, redirectUrl=?, accessTokenValidity=?, refreshTokenValidity=?"
                    + ", allowedRedirectUrls=?, allowedIpAddresses=?, authorizationGrantTypes=?, authorities=?, scope=? where uid=?";
            jdbcTemplate.update(sql, DateUtils.localDateTimeToDate(element.getUpdated()), element.getClient_id(), element.getClient_secret(), element.getRedirectUrl(),
                    element.getAccessTokenValidity(), element.getRefreshTokenValidity(), JsonUtils.stringValue(element.getAllowedRedirectUrls()),
                    JsonUtils.stringValue(element.getAllowedIpAddresses()), JsonUtils.stringValue(element.getAuthorizationGrantTypes()),
                    JsonUtils.stringValue(element.getAuthorities()), JsonUtils.stringValue(element.getScope()), element.getUid());
        } catch (Exception e) {
            throw new JoafDatabaseException("Repository error", e);
        }
    }

    @Override
    public Class<Oauth2Client> getEntityClass() {
        return Oauth2Client.class;
    }

    @Override
    public Oauth2Client findOneByClientId(String clientId) throws JoafDatabaseException {
        Optional<Oauth2Client> client_id = this.findOneByFieldOptional("client_id", clientId);
        return client_id.get();
    }
}
