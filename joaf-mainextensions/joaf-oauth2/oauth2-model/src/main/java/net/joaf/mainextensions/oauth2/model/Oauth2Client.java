/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.oauth2.model;

import net.joaf.base.core.db.model.JoafEntity;
import net.joaf.base.core.db.model.TransientMarker;
import net.joaf.base.core.model.enums.EStandardAction;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Cyprian Śniegota
 * @since 1.2
 */
public class Oauth2Client extends JoafEntity {

    private String client_id;
    private String client_secret;
    private String redirectUrl;
    private Integer accessTokenValidity;
    private Integer refreshTokenValidity;
    private Set<String> allowedRedirectUrls = new HashSet<>();
    private Set<String> allowedIpAddresses = new HashSet<>();
    private Set<String> authorizationGrantTypes = new HashSet<>();
    private Set<String> authorities = new HashSet<>();
    private Set<String> scope = new HashSet<>();

    @TransientMarker
    private List<EStandardAction> actions = new ArrayList<>();


    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getClient_secret() {
        return client_secret;
    }

    public void setClient_secret(String client_secret) {
        this.client_secret = client_secret;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public Set<String> getAllowedRedirectUrls() {
        return allowedRedirectUrls;
    }

    public Set<String> getAllowedIpAddresses() {
        return allowedIpAddresses;
    }

    public Integer getAccessTokenValidity() {
        return accessTokenValidity;
    }

    public void setAccessTokenValidity(Integer accessTokenValidity) {
        this.accessTokenValidity = accessTokenValidity;
    }

    public Integer getRefreshTokenValidity() {
        return refreshTokenValidity;
    }

    public void setRefreshTokenValidity(Integer refreshTokenValidity) {
        this.refreshTokenValidity = refreshTokenValidity;
    }

    public Set<String> getAuthorizationGrantTypes() {
        return authorizationGrantTypes;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }

    public Set<String> getScope() {
        return scope;
    }

    public List<EStandardAction> getActions() {
        return actions;
    }
}
