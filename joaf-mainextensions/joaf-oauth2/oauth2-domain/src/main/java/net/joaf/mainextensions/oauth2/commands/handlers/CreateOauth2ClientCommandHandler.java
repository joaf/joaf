/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.oauth2.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.base.core.utils.DateUtils;
import net.joaf.mainextensions.oauth2.commands.CreateOauth2ClientCommand;
import net.joaf.mainextensions.oauth2.model.Oauth2Client;
import net.joaf.mainextensions.oauth2.repository.api.Oauth2ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import java.time.LocalDateTime;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandHandlerComponent
public class CreateOauth2ClientCommandHandler implements CommandHandler<CreateOauth2ClientCommand, Object> {

    @Autowired
    private Oauth2ClientRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, CreateOauth2ClientCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(CreateOauth2ClientCommand command) throws JoafException {
        LocalDateTime time = DateUtils.operationDateTime();
        Oauth2Client element = new Oauth2Client();
        element.setObjectState(EObjectState.NEW);
        element.setUid(repository.prepareId());
        element.setCreated(time);
        element.setUpdated(time);
        repository.insert(element);
        return element;
    }
}
