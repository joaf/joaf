/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.oauth2.services;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.oauth2.model.Oauth2Client;
import net.joaf.mainextensions.oauth2.queries.Oauth2ClientQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Service;

/**
 * TODO: implement for OAuth2
 *
 * @author Cyprian Śniegota
 * @since 1.2
 */
@Service(value = "clientDetailsService")
public class SecurityOAuth2UserService implements ClientDetailsService {

    @Autowired
    private Oauth2ClientQuery query;

    @Override
    public ClientDetails loadClientByClientId(String s) throws ClientRegistrationException {
        try {
            Oauth2Client one = query.findOneByClientId(s);
            BaseClientDetails baseClientDetails = new BaseClientDetails(one.getClient_id(), "", "password,authorization_code", "password,authorization_code", "ROLE_USER");
            baseClientDetails.setClientSecret(one.getClient_secret());
            return baseClientDetails;
        } catch (JoafDatabaseException e) {
            throw new ClientRegistrationException("error", e);
        }
    }
}
