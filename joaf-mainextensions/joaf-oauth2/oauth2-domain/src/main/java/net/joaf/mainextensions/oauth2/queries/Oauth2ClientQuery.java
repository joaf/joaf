/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.oauth2.queries;

import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.model.enums.EStandardAction;
import net.joaf.mainextensions.oauth2.model.Oauth2Client;
import net.joaf.mainextensions.oauth2.repository.api.Oauth2ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by cyprian on 03.04.15.
 */
@Component
public class Oauth2ClientQuery {

    @Autowired
    private Oauth2ClientRepository repository;


    public PageredResult<Oauth2Client> findAll(PageredRequest pageredRequest) throws JoafDatabaseException {
        PageredResult<Oauth2Client> all = repository.findAll(pageredRequest);
        all.getResult().stream().forEach(x -> {
            x.getActions().add(EStandardAction.EDIT);
            x.getActions().add(EStandardAction.TRASH);
        });
        return all;
    }

    public Oauth2Client findOne(String id) throws JoafDatabaseException {
        return repository.findOne(id);
    }

    public Oauth2Client findOneByClientId(String clientId) throws JoafDatabaseException {
        return repository.findOneByClientId(clientId);
    }

}
