/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.content.model.converters.mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import net.joaf.base.core.utils.UniversalJsonConverter;
import net.joaf.mainextensions.content.model.Content;

/**
 * Created with IntelliJ IDEA.
 * User: cyprian
 * Date: 06.01.14
 * Time: 01:21
 * To change this template use File | Settings | File Templates.
 */
public class AdminContentConverter extends UniversalJsonConverter<Content> {
    @Override
    public DBObject dbObjectFromObject(Content obj) {
        DBObject dbObject = new BasicDBObject();
        dbObject.put("_id", obj.getUid());
        dbObject.put("body", obj.getBody());
        dbObject.put("category", obj.getCategory());
        dbObject.put("module", obj.getModule());
        dbObject.put("name", obj.getName());
        dbObject.put("title", obj.getTitle());
        return dbObject;
    }

    @Override
    public Content objectFromDBObject(DBObject object, Class<Content> adminContentClass) {
        Content adminContent = new Content();
        adminContent.setUid(String.valueOf(object.get("_id")));
        adminContent.setBody(String.valueOf(object.get("body")));
        adminContent.setCategory(String.valueOf(object.get("category")));
        adminContent.setModule(String.valueOf(object.get("module")));
        adminContent.setName(String.valueOf(object.get("name")));
        adminContent.setTitle(String.valueOf(object.get("title")));
        return adminContent;
    }
}
