/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.content.repository.mongodb;

import com.mongodb.*;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.mongo.MongoDBProvider;
import net.joaf.base.db.mongo.RepositoryMongo;
import net.joaf.mainextensions.content.model.Content;
import net.joaf.mainextensions.content.model.converters.mongo.AdminContentConverter;
import net.joaf.mainextensions.content.repository.api.ContentRepository;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by cyprian.sniegota on 28.02.14.
 */
//@Repository
public class ContentRepositoryMongo implements ContentRepository, RepositoryMongo {

    private final String collectionName = "content";
    //    @Autowired
    MongoDBProvider mongoDbProvider;

    @Override
    public Content getContentByBasicCriteria(String module, String name, String category) throws JoafDatabaseException {
        DB mongoDB = mongoDbProvider.getMongoDB();
        DBCollection configurationDbCollection = mongoDB.getCollection("content");
        BasicDBObject query = new BasicDBObject("module", module).append("name", name).append("category", category);
        DBObject one = configurationDbCollection.findOne(query);
        if (one != null) {
            Content content = new Content();
            content.setBody(String.valueOf(one.get("body")));
            content.setCategory(String.valueOf(one.get("category")));
            content.setModule(String.valueOf(one.get("module")));
            content.setName(String.valueOf(one.get("name")));
            content.setTitle(String.valueOf(one.get("title")));
            return content;
        } else {
            return null;
        }

    }

    public List<Content> findAll() throws JoafDatabaseException {
        DBCollection collection = mongoDbProvider.getMongoDB().getCollection(collectionName);
        DBCursor dbCursor = collection.find();
        AdminContentConverter adminContentConverter = new AdminContentConverter();
        List<Content> retList = new ArrayList<>();
        while (dbCursor.hasNext()) {
            DBObject next = dbCursor.next();
            Content element = adminContentConverter.objectFromDBObject(next, Content.class);
            retList.add(element);
        }
        dbCursor.close();
        return retList;
    }

    public Content findOne(String id) throws JoafDatabaseException {
        DBCollection collection = mongoDbProvider.getMongoDB().getCollection(collectionName);
        DBObject dbObject = collection.findOne(new BasicDBObject("_id", new ObjectId(id)));
        AdminContentConverter adminContentConverter = new AdminContentConverter();
        Content element = adminContentConverter.objectFromDBObject(dbObject, Content.class);
        element.setUid(dbObject.get("_id").toString());
        return element;
    }

    @Override
    public Optional<Content> findOneOptional(String uid) throws JoafDatabaseException {
        return null;
    }

    @Override
    public String collectionName() {
        return null;
    }

    @Override
    public Class<Content> getEntityClass() {
        return null;
    }

    public void store(Content command) throws JoafDatabaseException {
        DBCollection dbCollection = mongoDbProvider.getMongoDB().getCollection(collectionName);
        AdminContentConverter adminContentConverter = new AdminContentConverter();
        DBObject object = adminContentConverter.dbObjectFromObject(command);
        if (StringUtils.trimToNull(command.getUid()) != null) {
            object.put("_id", new ObjectId(command.getUid()));
        }
        object.removeField("id");
        dbCollection.save(object);
        Content retObject = adminContentConverter.objectFromDBObject(object, Content.class);
        retObject.setUid(object.get("_id").toString());
        //        return retObject;
    }

    @Override
    public void updateObjectState(String uid, EObjectState newState) throws JoafDatabaseException {

    }

    public void remove(String id) throws JoafDatabaseException {
        DBCollection dbCollection = mongoDbProvider.getMongoDB().getCollection(collectionName);
        DBObject object = new BasicDBObject("_id", new ObjectId(id));
        dbCollection.remove(object);
    }

    @Override
    public void insert(Content element) throws JoafDatabaseException {

    }

    public Content createObject() {
        return new Content();
    }

}
