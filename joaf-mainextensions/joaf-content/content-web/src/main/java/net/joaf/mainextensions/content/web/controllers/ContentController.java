/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.content.web.controllers;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.content.model.Content;
import net.joaf.mainextensions.content.services.ContentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

//import net.joaf.base.core.lib.helpers.ImportExportHelper;

/**
 * Created with IntelliJ IDEA.
 * User: cyprian
 * Date: 05.01.14
 * Time: 00:13
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class ContentController {

    private static final Logger log = LoggerFactory.getLogger(ContentController.class);

    //    @Autowired
    //    ImportExportHelper importExportHelper;
    @Autowired
    ContentService service;

    @RequestMapping("/administration/content")
    public String list(HttpSession session, Model model) {
        try {
            List<Content> all = service.findAll();
            model.addAttribute("command", all);
        } catch (JoafDatabaseException e) {
            e.printStackTrace();
        }
        return "freemarker/content/admin/list";
    }

    @RequestMapping(value = "/administration/content/export")
    public HttpEntity<String> export(HttpSession session) {
        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("application", "json"));
        header.set("Content-Disposition",
                "attachment; filename=content.json");
        String configurations = "";//importExportHelper.exportData("content");
        return new HttpEntity<>(configurations, header);
    }

    @RequestMapping("/administration/content/edit_{id}")
    public String newItem(@PathVariable("id") String id, HttpSession session, Model model) {
        Content command = null;
        try {
            if (id.equals("0")) {
                command = service.create(new Content());
            } else {
                command = service.findOne(id);
            }
        } catch (JoafDatabaseException e) {
            e.printStackTrace();
        }
        model.addAttribute("command", command);

        return "freemarker/content/admin/edit";
    }

    @RequestMapping(value = "/administration/content/save", method = RequestMethod.POST)
    public String saveItem(@Valid @ModelAttribute("command") Content command, HttpSession session, Model model) {
        try {
            service.save(command);
            return "redirect:/administration/content.html";
        } catch (JoafDatabaseException e) {
            e.printStackTrace();
        }
        return "freemarker/content/admin/edit";
    }

    @RequestMapping("/administration/content/remove_{id}")
    public String remove(@PathVariable("id") String id, HttpSession session, Model model) {
        if (!id.equals("0")) {
            try {
                service.remove(id);
            } catch (JoafDatabaseException e) {
                e.printStackTrace();
            }
        }
        return "redirect:/administration/content.html";
    }

    @RequestMapping(value = "/administration/content/cancel", method = RequestMethod.POST)
    public String cancel(HttpSession session, Model model) {
        return "redirect:/administration/content.html";
    }
}
