package net.joaf.connection.cjoaf.model;

import net.joaf.base.core.db.model.JoafEntity;

import java.io.Serializable;

/**
 * Created by cyprian on 12.05.15.
 */
public class CjoafConnection extends JoafEntity implements Serializable {

    private String applicationId;
    private String applicationSecret;

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationSecret() {
        return applicationSecret;
    }

    public void setApplicationSecret(String applicationSecret) {
        this.applicationSecret = applicationSecret;
    }
}
