<div class="awidget full-width">
    <div class="row">
        <div class="col-md-10">
            <div class="awidget-head">
                <h3><@spring.message 'joaf.subjectcard.details.main' /> <a class=""
                                                                           href="${rc.contextPath}/subjectcard/edit/${command.uid}.html"><@spring.message 'joaf.edit' /></a>
                </h3>
            </div>
            <div class="awidget-body">
                <div class="form-group row">
                    <label class="col-lg-3 control-label"><@spring.message 'joaf.subjectcard.companyName' /></label>

                    <div class="col-lg-8">
                        <p class="form-control-static">${command.name}</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 control-label"><@spring.message 'joaf.subjectcard.companyShortName' /></label>

                    <div class="col-lg-8">
                        <p class="form-control-static">${command.shortName}</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 control-label"><@spring.message 'joaf.subjectcard.taxNumber' /></label>

                    <div class="col-lg-8">
                        <p class="form-control-static">${command.vatNumber}</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 control-label"><@spring.message 'joaf.subjectcard.address' /></label>

                    <div class="col-lg-8">
                        <p class="form-control-static">${command.postCode} ${command.postCity}</p>

                        <p class="form-control-static">${command.street} ${command.houseNumber}</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-2">
            <i class="fa fa-info"></i>
        <@spring.message 'joaf.subjectcard.details.main.info' />
        </div>
    </div>
</div>
