/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.subjectcompany.repository.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.cassandra.AbstractCrudRepositoryCassandra;
import net.joaf.base.db.cassandra.CassandraSessionProvider;
import net.joaf.base.db.cassandra.RepositoryCassandra;
import net.joaf.mainextensions.subjectcompany.model.SubjectCardSettings;
import net.joaf.mainextensions.subjectcompany.repository.api.SubjectCardSettingsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by cyprian on 23.08.14.
 */

@Repository
public class SubjectCardSettingsRepositoryCassandra extends AbstractCrudRepositoryCassandra<SubjectCardSettings>
        implements SubjectCardSettingsRepository, RepositoryCassandra {

    private static final Logger logger = LoggerFactory.getLogger(SubjectCardSettingsRepositoryCassandra.class);

    @Autowired
    private CassandraSessionProvider sessionProvider;

    @Override
    public String prepareId() {
        return UUID.randomUUID().toString();
    }

    @Override
    public List<SubjectCardSettings> findAll() throws JoafDatabaseException {
        try {
            Session session = sessionProvider.connect();
            ResultSet rs = session.execute("select * from subjectCardSettings");
            List<Row> all = rs.all();
            return all.stream().map(this::bindRow).collect(Collectors.toList());
        } catch (Exception e) {
            throw new JoafDatabaseException("Database error", e);
        }

    }

    @Override
    public String collectionName() {
        return "subjectcardsettings";
    }

    @Override
    public Class<SubjectCardSettings> getEntityClass() {
        return SubjectCardSettings.class;
    }

    @Override
    public SubjectCardSettings findForModuleAndForm(String subjectUid, String module, String form) throws JoafDatabaseException {
        try {
            SubjectCardSettings command;
            String idx = subjectUid.concat(module).concat(form);
            Session session = sessionProvider.connect();
            ResultSet rs = session.execute("select * from subjectCardSettings where mainIdx = ?", idx);
            List<Row> all = rs.all();
            if (all.size() > 0) {
                return this.bindRow(all.iterator().next());
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new JoafDatabaseException("Database error", e);
        }
    }

    @Override
    public void insert(SubjectCardSettings command) throws JoafDatabaseException {
        try {
            Session session = sessionProvider.connect();
            command.setMainIdx(command.getSubjectUid().concat(command.getModule()).concat(command.getFormName()));
            String query = "INSERT INTO subjectCardSettings (uid, subjectUid, module, formName, params, mainIdx) "
                    + "VALUES (?, ?, ?, ?, ?, ?);";
            session.execute(query,
                    UUID.fromString(command.getUid()), UUID.fromString(command.getSubjectUid()), command.getModule(), command.getFormName(),
                    command.getParams(), command.getMainIdx());
        } catch (Exception e) {
            throw new JoafDatabaseException("Database error", e);
        }
    }

    @Override
    public void store(SubjectCardSettings command) throws JoafDatabaseException {
        try {
            Session session = sessionProvider.connect();
            command.setMainIdx(command.getSubjectUid().concat(command.getModule()).concat(command.getFormName()));
            String query = "UPDATE subjectCardSettings SET params = ? WHERE uid = ? ";
            session.execute(query, command.getParams(), UUID.fromString(command.getUid()));
        } catch (Exception e) {
            throw new JoafDatabaseException("Database error", e);
        }
    }

    protected SubjectCardSettings bindRow(Row row) {
        SubjectCardSettings command = new SubjectCardSettings();
        command.setSubjectUid(row.getUUID("subjectUid").toString());
        command.setUid(row.getUUID("uid").toString());
        command.setModule(row.getString("module"));
        command.setFormName(row.getString("formName"));
        command.setMainIdx(row.getString("mainIdx"));
        command.setParams(row.getMap("params", String.class, String.class));
        return command;
    }
}
