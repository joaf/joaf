/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.subjectcompany.repository.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.cassandra.AbstractCrudRepositoryCassandra;
import net.joaf.base.db.cassandra.CassandraSessionProvider;
import net.joaf.base.db.cassandra.RepositoryCassandra;
import net.joaf.mainextensions.subjectcompany.model.SubjectCard;
import net.joaf.mainextensions.subjectcompany.model.SubjectCardCompact;
import net.joaf.mainextensions.subjectcompany.repository.api.SubjectCardRepository;
import net.joaf.mainextensions.user.model.UserCard;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by cyprian on 14.03.14.
 */
@Repository
public class SubjectCardRepositoryCassandra extends AbstractCrudRepositoryCassandra<SubjectCard> implements SubjectCardRepository, RepositoryCassandra {

    private static final Logger logger = LoggerFactory.getLogger(SubjectCardRepositoryCassandra.class);

    @Autowired
    private CassandraSessionProvider sessionProvider;

    @Override
    public List<SubjectCard> findAll() throws JoafDatabaseException {
        try {
            List<SubjectCard> elementList = new ArrayList<>();
            Session session = sessionProvider.connect();
            ResultSet rs = session.execute("select * from subjectcard");
            List<Row> all = rs.all();
            elementList.addAll(all.stream().map(this::bindRow).collect(Collectors.toList()));
            return elementList;
        } catch (Exception e) {
            throw new JoafDatabaseException("Database error", e);
        }
    }

    @Override
    public String collectionName() {
        return "subjectcard";
    }

    @Override
    public Class<SubjectCard> getEntityClass() {
        return SubjectCard.class;
    }

    @Override
    public void store(SubjectCard element) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        String updateQuery = "UPDATE subjectcard SET name = ?, shortName = ?, taxNumber = ?, postCode = ?, postCity = ?, street = ?, houseNumber = ?, objectState = ? WHERE uid = ?;";
        UUID uuid = UUID.fromString(element.getUid());
        session.execute(updateQuery,
                element.getName(), element.getShortName(), element.getVatNumber(), element.getPostCode(), element.getPostCity(), element.getStreet()
                , element.getHouseNumber(), element.getObjectState().toString(), uuid);
    }

    @Override
    public void insert(SubjectCard element) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        String insertQuery = "INSERT INTO subjectcard (uid, name, shortName, taxNumber, postCode, postCity, street, houseNumber, objectState) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
        session.execute(insertQuery,
                UUID.fromString(element.getUid()), element.getName(), element.getShortName(), element.getVatNumber(), element.getPostCode(),
                element.getPostCity(), element.getStreet()
                , element.getHouseNumber(), element.getObjectState().toString());
    }

    @Override
    public List<SubjectCardCompact> findAllCompact() throws JoafDatabaseException {
        try {
            List<SubjectCardCompact> elementList = new ArrayList<>();
            Session session = sessionProvider.connect();
            ResultSet rs = session.execute("select * from usercard_subjectcard");
            List<Row> all = rs.all();
            elementList.addAll(all.stream().map(this::bindCompactRow).collect(Collectors.toList()));
            return elementList;
        } catch (Exception e) {
            throw new JoafDatabaseException("Database error", e);
        }
    }

    @Override
    public List<SubjectCardCompact> findForUser(String userCardId) throws JoafDatabaseException {
        try {
            List<SubjectCardCompact> elementList = new ArrayList<>();

            Session session = sessionProvider.connect();
            ResultSet rs = session.execute("select * from usercard_subjectcard where usercarduid = ?", UUID.fromString(userCardId));
            List<Row> all = rs.all();
            elementList.addAll(all.stream().map(this::bindCompactRow).collect(Collectors.toList()));
            return elementList;
        } catch (Exception e) {
            throw new JoafDatabaseException("Database error", e);
        }
    }

    @Override
    public SubjectCard findByUid(String uid) throws JoafDatabaseException {
        if (uid == null) {
            return null;
        }
        Session session = sessionProvider.connect();
        ResultSet rs = session.execute("select * from subjectcard where uid = ?", UUID.fromString(uid));
        List<Row> all = rs.all();
        if (all.size() > 0) {
            return this.bindRow(all.get(0));
        } else {
            return null;
        }
    }

    @Override
    public void save(SubjectCard command) throws JoafDatabaseException {
        Session session = sessionProvider.connect();

        String updateQuery = "UPDATE subjectcard SET name = ?, shortName = ?, taxNumber = ?, postCode = ?, postCity = ?, street = ?, houseNumber = ?, objectState = ? WHERE uid = ?;";
        UUID uuid = UUID.fromString(command.getUid());
        session.execute(updateQuery,
                command.getName(), command.getShortName(), command.getVatNumber(), command.getPostCode(), command.getPostCity(), command.getStreet()
                , command.getHouseNumber(), command.getObjectState().toString(), uuid);
        String mappedQuery = "SELECT subjectcarduid, usercarduid FROM usercard_subjectcard WHERE subjectcarduid = ? ALLOW FILTERING";
        ResultSet execute = session.execute(mappedQuery, UUID.fromString(command.getUid()));
        String mapInsertQuery = "UPDATE usercard_subjectcard SET subjectname = ?, visible = ? WHERE subjectcarduid = ? AND usercarduid = ?";
        List<Row> all = execute.all();
        Boolean visible = command.getObjectState().equals(EObjectState.ACCEPTED);
        for (Row row : all) {
            session.execute(mapInsertQuery, command.getShortName(), visible, row.getUUID("subjectcarduid"), row.getUUID("usercarduid"));
        }

    }

    @Override
    public void insert(SubjectCard command, UserCard userCard) throws JoafDatabaseException {
        try {
            Session session = sessionProvider.connect();
            String insertQuery = "INSERT INTO subjectcard (uid, name, shortName, taxNumber, postCode, postCity, street, houseNumber, objectState) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
            session.execute(insertQuery,
                    UUID.fromString(command.getUid()), command.getName(), command.getShortName(), command.getVatNumber(), command.getPostCode(),
                    command.getPostCity(), command.getStreet()
                    , command.getHouseNumber(), command.getObjectState().toString());

            String mapInsertQuery = "INSERT INTO usercard_subjectcard (usercarduid, subjectcarduid, userroles, username, subjectname, visible) VALUES (" +
                    "?, ?, ?, ?, ?, ?)";
            Set<String> roles = new HashSet<>();
            roles.addAll(Arrays.asList("ROLE_SUBJECT_USER", "ROLE_SUBJECT_ADMIN"));
            Boolean visible = command.getObjectState().equals(EObjectState.ACCEPTED);
            session.execute(mapInsertQuery,
                    UUID.fromString(userCard.getUid()), UUID.fromString(command.getUid()), roles, userCard.getUsername(), command.getShortName(), visible);
        } catch (Exception e) {
            throw new JoafDatabaseException("Insert error", e);
        }

    }

    @Override
    public SubjectCardCompact findCompactByUid(String uid, String userCardId) throws JoafDatabaseException {
        try {
            List<SubjectCardCompact> elementList = new ArrayList<>();

            Session session = sessionProvider.connect();
            ResultSet rs = session.execute("select * from usercard_subjectcard where usercarduid = ? AND subjectcarduid = ?", UUID.fromString(userCardId),
                    UUID.fromString(uid));
            List<Row> all = rs.all();
            return all.stream().map(this::bindCompactRow).reduce((x, y) -> x).orElse(null);
        } catch (Exception e) {
            throw new JoafDatabaseException("Database error", e);
        }
    }

    @Override
    public void remove(String id) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        String updateQuery = "DELETE FROM subjectcard WHERE uid = ?;";
        UUID uuid = UUID.fromString(id);
        session.execute(updateQuery, uuid);
    }

    @Override
    public void removeCompact(String id) {
        Session session = sessionProvider.connect();
        String mappedQuery = "SELECT subjectcarduid, usercarduid FROM usercard_subjectcard WHERE subjectcarduid = ?";
        ResultSet execute = session.execute(mappedQuery, UUID.fromString(id));
        String mapInsertQuery = "DELETE FROM usercard_subjectcard WHERE subjectcarduid = ? AND usercarduid = ?";
        List<Row> all = execute.all();
        for (Row row : all) {
            session.execute(mapInsertQuery, row.getUUID("subjectcarduid"), row.getUUID("usercarduid"));
        }
    }

    protected SubjectCard bindRow(Row row) {
        SubjectCard card = new SubjectCard();
        card.setName(row.getString("name"));
        card.setShortName(StringUtils.trimToEmpty(row.getString("shortName")));
        card.setUid(row.getUUID("uid").toString());
        //        card.setAccessUsers(row.getSet("accessUsers",String.class));
        card.setPostCity(row.getString("postCity"));
        card.setPostCode(row.getString("postCode"));
        card.setStreet(row.getString("street"));
        card.setHouseNumber(row.getString("houseNumber"));
        card.setVatNumber(row.getString("taxNumber"));
        card.setObjectState(EObjectState.valueOf(row.getString("objectState")));
        return card;
    }

    private SubjectCardCompact bindCompactRow(Row row) {
        SubjectCardCompact card = new SubjectCardCompact();
        card.setUsercardUid(row.getUUID("usercarduid").toString());
        card.setSubjectcardUid(row.getUUID("subjectcarduid").toString());
        card.setSubjectname(row.getString("subjectname"));
        card.setUsername(row.getString("username"));
        card.setUserRoles(row.getSet("userroles", String.class));
        card.setVisible(row.getBool("visible"));
        return card;
    }

    @Override
    public List<SubjectCardCompact> findCompactForSubject(String uid) throws JoafDatabaseException {
        try {
            List<SubjectCardCompact> elementList = new ArrayList<>();
            Session session = sessionProvider.connect();
            ResultSet rs = session.execute("select * from usercard_subjectcard where subjectcarduid = ?", UUID.fromString(uid));
            List<Row> all = rs.all();
            elementList.addAll(all.stream().map(this::bindCompactRow).collect(Collectors.toList()));
            return elementList;
        } catch (Exception e) {
            throw new JoafDatabaseException("Database error", e);
        }
    }

    @Override
    public String prepareId() {
        return UUID.randomUUID().toString();
    }
}
