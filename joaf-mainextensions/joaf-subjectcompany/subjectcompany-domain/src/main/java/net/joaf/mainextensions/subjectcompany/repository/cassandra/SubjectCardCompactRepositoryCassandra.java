/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.subjectcompany.repository.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.cassandra.AbstractCrudRepositoryCassandra;
import net.joaf.base.db.cassandra.RepositoryCassandra;
import net.joaf.mainextensions.subjectcompany.model.SubjectCardCompact;
import net.joaf.mainextensions.subjectcompany.repository.api.SubjectCardCompactRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by cyprian on 15.11.14.
 */
@Repository
public class SubjectCardCompactRepositoryCassandra extends AbstractCrudRepositoryCassandra<SubjectCardCompact>
        implements SubjectCardCompactRepository, RepositoryCassandra {

    protected static final String REMOVE_COMPACT_SQL = "delete from %s where usercarduid = ? AND subjectcarduid = ?";

    @Override
    public void store(SubjectCardCompact element) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        String mapInsertQuery = "UPDATE usercard_subjectcard SET userroles = ?, username = ?, subjectname = ?, visible = ? WHERE usercarduid = ? AND subjectcarduid = ?";
        session.execute(mapInsertQuery,
                element.getUserRoles(), element.getUsername(), element.getSubjectname(), element.getVisible(), UUID.fromString(element.getUsercardUid()),
                UUID.fromString(element.getSubjectcardUid()));
    }

    @Override
    public SubjectCardCompact findOne(String uid) throws JoafDatabaseException {
        try {
            List<SubjectCardCompact> elementList = new ArrayList<>();
            String[] uids = uid.split("_");
            Session session = sessionProvider.connect();
            ResultSet rs = session.execute("select * from usercard_subjectcard where usercarduid = ? AND subjectcarduid = ?", UUID.fromString(uids[0]),
                    UUID.fromString(uids[1]));
            List<Row> all = rs.all();
            return all.stream().map(this::bindRow).reduce((x, y) -> x).orElse(null);
        } catch (Exception e) {
            throw new JoafDatabaseException("Database error", e);
        }
    }

    @Override
    public String collectionName() {
        return "usercard_subjectcard";
    }

    @Override
    public Class<SubjectCardCompact> getEntityClass() {
        return SubjectCardCompact.class;
    }

    @Override
    public void insert(SubjectCardCompact element) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        String mapInsertQuery = "INSERT INTO usercard_subjectcard (usercarduid, subjectcarduid, userroles, username, subjectname, visible) VALUES ("
                + "?, ?, ?, ?, ?, ?)";
        session.execute(mapInsertQuery,
                UUID.fromString(element.getUsercardUid()), UUID.fromString(element.getSubjectcardUid()), element.getUserRoles(), element.getUsername(),
                element.getSubjectname(), element.getVisible());
    }

    @Override
    protected SubjectCardCompact bindRow(Row row) {
        SubjectCardCompact card = new SubjectCardCompact();
        card.setUsercardUid(row.getUUID("usercarduid").toString());
        card.setSubjectcardUid(row.getUUID("subjectcarduid").toString());
        card.setSubjectname(row.getString("subjectname"));
        card.setUsername(row.getString("username"));
        card.setUserRoles(row.getSet("userroles", String.class));
        card.setVisible(row.getBool("visible"));
        return card;
    }

    @Override
    public void remove(String uid) throws JoafDatabaseException {
        String[] uids = uid.split("_");
        Session session = sessionProvider.connect();
        session.execute(String.format(REMOVE_COMPACT_SQL, collectionName()), UUID.fromString(uids[0]), UUID.fromString(uids[1]));
    }
}
