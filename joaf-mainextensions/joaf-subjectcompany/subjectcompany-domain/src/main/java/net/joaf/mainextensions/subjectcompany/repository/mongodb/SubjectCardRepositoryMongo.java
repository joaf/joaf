/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.subjectcompany.repository.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.utils.UniversalJsonConverter;
import net.joaf.base.db.mongo.MongoDBProvider;
import net.joaf.base.db.mongo.RepositoryMongo;
import net.joaf.mainextensions.subjectcompany.model.SubjectCard;
import net.joaf.mainextensions.subjectcompany.model.SubjectCardCompact;
import net.joaf.mainextensions.subjectcompany.repository.api.SubjectCardRepository;
import net.joaf.mainextensions.user.model.UserCard;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by cyprian on 14.03.14.
 */
//@Repository
public class SubjectCardRepositoryMongo implements SubjectCardRepository, RepositoryMongo {

    @Autowired
    private MongoDBProvider mongoDBProvider;

    @Override
    public List<SubjectCardCompact> findAllCompact() {
        return null;
    }

    @Override
    public void removeCompact(String id) {

    }

    @Override
    public List<SubjectCardCompact> findCompactForSubject(String uid) throws JoafDatabaseException {
        return null;
    }

    @Override
    public List<SubjectCard> findAll() throws JoafDatabaseException {
        return null;
    }

    @Override
    public SubjectCard findOne(String uid) throws JoafDatabaseException {
        return null;
    }

    @Override
    public Optional<SubjectCard> findOneOptional(String uid) throws JoafDatabaseException {
        return null;
    }

    @Override
    public String collectionName() {
        return null;
    }

    @Override
    public Class<SubjectCard> getEntityClass() {
        return null;
    }

    @Override
    public List<SubjectCardCompact> findForUser(String username) throws JoafDatabaseException {
        DBCollection companies = mongoDBProvider.getMongoDB().getCollection("companies");
        BasicDBObject query = new BasicDBObject("accessUsers", new BasicDBObject("$in", new Object[]{username}));

        System.out.println(query);
        DBCursor dbCursor = companies.find(query);
        List<SubjectCard> retList = new ArrayList<>();
        UniversalJsonConverter<SubjectCard> universalJsonConverter = new UniversalJsonConverter<>();
        while (dbCursor.hasNext()) {
            DBObject next = dbCursor.next();
            next.put("uid", next.get("_id").toString());
            SubjectCard subjectCard = universalJsonConverter.objectFromString(next.toString(), SubjectCard.class);
            retList.add(subjectCard);
        }
        dbCursor.close();
        return null;
    }

    @Override
    public SubjectCard findByUid(String uid) throws JoafDatabaseException {
        if (uid == null) {
            return null;
        }
        UniversalJsonConverter<SubjectCard> universalJsonConverter = new UniversalJsonConverter<>();
        DBCollection companies = mongoDBProvider.getMongoDB().getCollection("companies");
        DBObject id = companies.findOne(new BasicDBObject("_id", new ObjectId(uid)));
        if (id == null) {
            return null;
        } else {
            id.put("uid", id.get("_id").toString());
            return universalJsonConverter.objectFromString(id.toString(), SubjectCard.class);
        }
    }

    @Override
    public void save(SubjectCard command) throws JoafDatabaseException {
        UniversalJsonConverter<SubjectCard> universalJsonConverter = new UniversalJsonConverter<>();
        DBCollection companies = mongoDBProvider.getMongoDB().getCollection("companies");
        DBObject dbObject = universalJsonConverter.dbObjectFromObject(command);
        if (command.getUid() != null) {
            dbObject.put("uid", new ObjectId(command.getUid()));
        }
        companies.save(dbObject);
        Object id = dbObject.get("_id");
        if (id != null) {
            dbObject.put("uid", id.toString());
        }
    }

    @Override
    public void remove(String id) throws JoafDatabaseException {

    }

    @Override
    public void insert(SubjectCard element) throws JoafDatabaseException {

    }

    @Override
    public void store(SubjectCard element) throws JoafDatabaseException {

    }

    @Override
    public void updateObjectState(String uid, EObjectState newState) throws JoafDatabaseException {

    }

    @Override
    public void insert(SubjectCard command, UserCard userCard) throws JoafDatabaseException {

    }

    @Override
    public SubjectCardCompact findCompactByUid(String uid, String userCardId) throws JoafDatabaseException {
        return null;
    }
}
