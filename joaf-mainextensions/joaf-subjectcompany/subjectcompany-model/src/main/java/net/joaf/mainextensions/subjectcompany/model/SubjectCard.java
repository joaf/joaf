/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.subjectcompany.model;

import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.db.model.StringUidEntity;
import net.joaf.base.subjectcard.model.SubjectCardData;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by cyprian on 14.03.14.
 */
public class SubjectCard implements Serializable, SubjectCardData, StringUidEntity {

    private static final long serialVersionUID = 7060819156396937407L;
    private String uid;
    @NotNull(message = "{joaf.subject.nameRequired}")
    @NotEmpty(message = "{joaf.subject.nameRequired}")
    private String name;
    private String shortName = "";
    @NotNull(message = "{joaf.subject.vatNumberRequired}")
    @NotEmpty(message = "{joaf.subject.vatNumberRequired}")
    private String vatNumber;
    private String postCode;
    private String postCity;
    private String street;
    private String houseNumber;
    private EObjectState objectState;
    private Set<String> accessUsers = new HashSet<>();

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public Set<String> getAccessUsers() {
        return accessUsers;
    }

    public void setAccessUsers(Set<String> accessUsers) {
        this.accessUsers = accessUsers;
    }

    public EObjectState getObjectState() {
        return objectState;
    }

    public void setObjectState(EObjectState objectState) {
        this.objectState = objectState;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getPostCity() {
        return postCity;
    }

    public void setPostCity(String postCity) {
        this.postCity = postCity;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}
