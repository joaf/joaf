/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.file.services;

import net.joaf.mainextensions.file.model.JoafFile;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by cyprian on 26.04.14.
 */
@Service
public class JoafFileService {

    // static 1
    public JoafFile findByOwnerUid(String uid) {
        JoafFile file = new JoafFile();
        file.setOwnerUid(uid);
        file.setUid("fake");
        file.setName("invoice_template.odt");
        try {
            InputStream resourceAsStream = this.getClass().getResourceAsStream("invoice_template.odt");
            file.setData(IOUtils.toByteArray(resourceAsStream));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public JoafFile findTripsByOwnerUid(String uid) {
        JoafFile file = new JoafFile();
        file.setOwnerUid(uid);
        file.setUid("fake");
        file.setName("trips_template.odt");
        try {
            InputStream resourceAsStream = this.getClass().getResourceAsStream("trips_template.odt");
            file.setData(IOUtils.toByteArray(resourceAsStream));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    // static file
    public JoafFile findByOwnerUid_FILE(String uid) {
        JoafFile file = new JoafFile();
        file.setOwnerUid(uid);
        file.setUid("fake");
        file.setName("invoice_template.odt");
        try {
            File hdfile = new File(
                    "/home/cyprian/projekty/java/joaf/joaf-main/joaf-file/file-domain/target/classes/net/joaf/core/file/services/invoice_template.odt");
            InputStream resourceAsStream = new FileInputStream(hdfile);
            file.setData(IOUtils.toByteArray(resourceAsStream));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

}
