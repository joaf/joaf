package net.joaf.connection.oauth2.model;

import net.joaf.mainextensions.connection.model.ConnectionMetadata;

import java.io.Serializable;

/**
 * Created by cyprian on 12.05.15.
 */
public class Oauth2ConnectionMetadata extends ConnectionMetadata implements Serializable {
    private String applicationId;
    private String applicationSecret;
    private String applicationName;
    private String applicationUrl;
    private String username;
    private String password;

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationSecret() {
        return applicationSecret;
    }

    public void setApplicationSecret(String applicationSecret) {
        this.applicationSecret = applicationSecret;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationUrl() {
        return applicationUrl;
    }

    public void setApplicationUrl(String applicationUrl) {
        this.applicationUrl = applicationUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
