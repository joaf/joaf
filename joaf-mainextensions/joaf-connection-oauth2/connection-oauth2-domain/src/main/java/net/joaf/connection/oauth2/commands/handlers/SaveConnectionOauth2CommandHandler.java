package net.joaf.connection.oauth2.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.connection.oauth2.commands.SaveConnectionOauth2Command;
import net.joaf.connection.oauth2.model.Oauth2ConnectionMetadata;
import net.joaf.mainextensions.connection.model.ExternalConnection;
import net.joaf.mainextensions.connection.repository.api.ExternalConnectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import java.time.LocalDateTime;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandHandlerComponent
public class SaveConnectionOauth2CommandHandler implements CommandHandler<SaveConnectionOauth2Command, Object> {

    @Autowired(required = false)
    private ExternalConnectionRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, SaveConnectionOauth2Command command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(SaveConnectionOauth2Command command) throws JoafException {
        ExternalConnection dbObject = repository.findOne(command.getElement().getUid());
        if (EObjectState.NEW.equals(dbObject.getObjectState())) {
            repository.updateObjectState(command.getElement().getUid(), EObjectState.ACCEPTED);
        }
        ExternalConnection element = command.getElement();
        Oauth2ConnectionMetadata connectionMetadata = (Oauth2ConnectionMetadata) element.getConnectionMetadata();
        Oauth2ConnectionMetadata connectionMetadataDb = (Oauth2ConnectionMetadata) dbObject.getConnectionMetadata();
        element.setUpdated(LocalDateTime.now());
        repository.store(element);
        return null;
    }
}