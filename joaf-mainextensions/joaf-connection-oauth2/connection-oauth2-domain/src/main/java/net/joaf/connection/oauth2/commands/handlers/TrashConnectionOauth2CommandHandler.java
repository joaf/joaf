package net.joaf.connection.oauth2.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.connection.oauth2.commands.TrashConnectionOauth2Command;
import net.joaf.mainextensions.connection.repository.api.ExternalConnectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandHandlerComponent
public class TrashConnectionOauth2CommandHandler implements CommandHandler<TrashConnectionOauth2Command, Object> {

    @Autowired(required = false)
    private ExternalConnectionRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, TrashConnectionOauth2Command command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(TrashConnectionOauth2Command command) throws JoafException {
        repository.updateObjectState(command.getElementUid(), EObjectState.TRASH);
        return null;
    }
}