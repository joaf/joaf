package net.joaf.connection.oauth2.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.error.JoafException;
import net.joaf.connection.oauth2.commands.RequestAuthorizationOauth2Command;
import net.joaf.connection.oauth2.model.Oauth2ConnectionMetadata;
import net.joaf.mainextensions.connection.model.ExternalConnection;
import net.joaf.mainextensions.connection.repository.api.ExternalConnectionRepository;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.ResponseType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Created by cyprian on 13.05.15.
 */
@CommandHandlerComponent
public class RequestAuthorizationOauth2CommandHandler implements CommandHandler<RequestAuthorizationOauth2Command, Object> {

    @Autowired(required = false)
    private ExternalConnectionRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, RequestAuthorizationOauth2Command command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(RequestAuthorizationOauth2Command command) throws JoafException {
        try {
            ExternalConnection one = repository.findOne(command.getElementUid());
            Oauth2ConnectionMetadata connectionMetadata = (Oauth2ConnectionMetadata) one.getConnectionMetadata();
            OAuthClientRequest request = OAuthClientRequest
                    .authorizationLocation(connectionMetadata.getApplicationUrl()) //authorize
                    .setClientId(connectionMetadata.getApplicationId())
                    .setRedirectURI("http://localhost:8080/mailapp/administration/connection/oauth2/responseauth/" + command.getElementUid() + ".html")
                    .setResponseType(ResponseType.CODE.toString())
                    .buildQueryMessage();
            return request.getLocationUri();
        } catch (OAuthSystemException e) {
            throw new JoafException("error executing request connect", e);
        }
    }
}