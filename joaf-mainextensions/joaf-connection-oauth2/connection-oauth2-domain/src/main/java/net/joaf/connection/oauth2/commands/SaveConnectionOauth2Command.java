package net.joaf.connection.oauth2.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractEntityCommand;
import net.joaf.connection.oauth2.commands.handlers.SaveConnectionOauth2CommandHandler;
import net.joaf.mainextensions.connection.model.ExternalConnection;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandDto(handlerClass = SaveConnectionOauth2CommandHandler.class)
public class SaveConnectionOauth2Command extends AbstractEntityCommand<ExternalConnection> implements Command {
    public SaveConnectionOauth2Command(ExternalConnection element, String userUid) {
        super(element, userUid);
    }
}