package net.joaf.connection.oauth2.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidSuidCommand;
import net.joaf.connection.oauth2.commands.handlers.CreateConnectionOauth2CommandHandler;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandDto(handlerClass = CreateConnectionOauth2CommandHandler.class)
public class CreateConnectionOauth2Command extends AbstractUidSuidCommand implements Command {
    public CreateConnectionOauth2Command(String userUid, String subjectUid) {
        super(null, userUid, subjectUid);
    }
}