package net.joaf.connection.oauth2;

import net.joaf.base.extension.AbstractExtension;
import net.joaf.base.extension.JoafExtension;
import org.springframework.stereotype.Component;

/**
 * Created by cyprian on 12.05.15.
 */
@Component
public class ConnectionOauth2Extension extends AbstractExtension implements JoafExtension {
    public static final String EXTENSION_BASE_PATH = "/connection/oauth2";
    public static final String EXTENSION_ADMIN_BASE_PATH = "/administration/connection/oauth2";

    @Override
    public String getExtensionMetadataFile() {
        return "/extension-connection-oauth2.xml";
    }
}