package net.joaf.connection.oauth2.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidSuidCommand;
import net.joaf.connection.oauth2.commands.handlers.RequestAuthorizationOauth2CommandHandler;

/**
 * Created by cyprian on 13.05.15.
 */
@CommandDto(handlerClass = RequestAuthorizationOauth2CommandHandler.class)
public class RequestAuthorizationOauth2Command extends AbstractUidSuidCommand implements Command {
    public RequestAuthorizationOauth2Command(String elementUid, String userUid, String subjectUid) {
        super(elementUid, userUid, subjectUid);
    }
}