package net.joaf.connection.oauth2.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidSuidCommand;
import net.joaf.connection.oauth2.commands.handlers.RequestTokenOauth2CommandHandler;

/**
 * Created by cyprian on 13.05.15.
 */
@CommandDto(handlerClass = RequestTokenOauth2CommandHandler.class)
public class RequestTokenOauth2Command extends AbstractUidSuidCommand implements Command {

    private String code;

    public RequestTokenOauth2Command(String elementUid, String userUid, String subjectUid, String code) {
        super(elementUid, userUid, subjectUid);
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}