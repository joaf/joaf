package net.joaf.connection.oauth2.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidSuidCommand;
import net.joaf.connection.oauth2.commands.handlers.TrashConnectionOauth2CommandHandler;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandDto(handlerClass = TrashConnectionOauth2CommandHandler.class)
public class TrashConnectionOauth2Command extends AbstractUidSuidCommand implements Command {
    public TrashConnectionOauth2Command(String elementUid, String userUid, String subjectUid) {
        super(elementUid, userUid, subjectUid);
    }
}