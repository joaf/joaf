package net.joaf.connection.oauth2.queries;

import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.model.enums.EStandardAction;
import net.joaf.mainextensions.connection.model.ExternalConnection;
import net.joaf.mainextensions.connection.repository.api.ExternalConnectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by cyprian on 03.04.15.
 */
@Component
public class ConnectionOauth2Query {

    @Autowired(required = false)
    private ExternalConnectionRepository repository;

    public PageredResult<ExternalConnection> findAll(PageredRequest pageredRequest) throws JoafDatabaseException {
        PageredResult<ExternalConnection> result = repository.findAllByType(pageredRequest, "oauth2");
        result.getResult().stream().forEach(x -> {
            x.getActions().add(EStandardAction.EDIT.name());
            x.getActions().add(EStandardAction.TRASH.name());
            x.getActions().add("CONNECT");
        });
        return result;
    }

    public ExternalConnection findOne(String id) throws JoafDatabaseException {
        return repository.findOne(id);
    }
}
