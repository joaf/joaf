package net.joaf.connection.oauth2.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.connection.oauth2.commands.UndoTrashConnectionOauth2Command;
import net.joaf.mainextensions.connection.repository.api.ExternalConnectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandHandlerComponent
public class UndoTrashConnectionOauth2CommandHandler implements CommandHandler<UndoTrashConnectionOauth2Command, Object> {

    @Autowired(required = false)
    private ExternalConnectionRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, UndoTrashConnectionOauth2Command command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(UndoTrashConnectionOauth2Command command) throws JoafException {
        repository.updateObjectState(command.getElementUid(), EObjectState.ACCEPTED);
        return null;
    }
}