package net.joaf.connection.oauth2.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.error.JoafException;
import net.joaf.connection.oauth2.commands.RequestTokenOauth2Command;
import net.joaf.connection.oauth2.model.Oauth2ConnectionMetadata;
import net.joaf.mainextensions.connection.model.ExternalConnection;
import net.joaf.mainextensions.connection.repository.api.ExternalConnectionRepository;
import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthBearerClientRequest;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse;
import org.apache.oltu.oauth2.client.response.OAuthResourceResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Created by cyprian on 13.05.15.
 */
@CommandHandlerComponent
public class RequestTokenOauth2CommandHandler implements CommandHandler<RequestTokenOauth2Command, Object> {

    @Autowired(required = false)
    private ExternalConnectionRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, RequestTokenOauth2Command command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(RequestTokenOauth2Command command) throws JoafException {
        try {
            ExternalConnection one = repository.findOne(command.getElementUid());
            Oauth2ConnectionMetadata connectionMetadata = (Oauth2ConnectionMetadata) one.getConnectionMetadata();
            OAuthClientRequest request = OAuthClientRequest
                    .tokenLocation("http://localhost:8080/mailapp/oauth/token") //token
                    .setGrantType(GrantType.AUTHORIZATION_CODE)
                    .setClientId(connectionMetadata.getApplicationId())
                    .setClientSecret(connectionMetadata.getApplicationSecret())
                    .setRedirectURI("http://localhost:8080/mailapp/administration/connection/oauth2/responseauth/" + command.getElementUid() + ".html")
                    .setCode(command.getCode())
                    .buildBodyMessage();
            OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());
            OAuthJSONAccessTokenResponse oAuthJSONAccessTokenResponse = oAuthClient.accessToken(request);
            String accessToken = oAuthJSONAccessTokenResponse.getAccessToken();
            Long expiresIn = oAuthJSONAccessTokenResponse.getExpiresIn();

            OAuthClientRequest bearerClientRequest = new OAuthBearerClientRequest("http://localhost:8080/mailapp/api/1/user/info")
                    .setAccessToken(accessToken)
                    .buildQueryMessage();
            OAuthResourceResponse resourceResponse = oAuthClient.resource(bearerClientRequest, OAuth.HttpMethod.GET, OAuthResourceResponse.class);
            return request.getLocationUri();
        } catch (OAuthSystemException | OAuthProblemException e) {
            throw new JoafException("error executing request connect \"" + e.getLocalizedMessage() + "\"", e);
        }
    }
}