package net.joaf.connection.oauth2.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.connection.oauth2.commands.CancelConnectionOauth2Command;
import net.joaf.mainextensions.connection.model.ExternalConnection;
import net.joaf.mainextensions.connection.repository.api.ExternalConnectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandHandlerComponent
public class CancelConnectionOauth2CommandHandler implements CommandHandler<CancelConnectionOauth2Command, Object> {

    @Autowired(required = false)
    private ExternalConnectionRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, CancelConnectionOauth2Command command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(CancelConnectionOauth2Command command) throws JoafException {
        ExternalConnection element = repository.findOne(command.getElementUid());
        if (EObjectState.NEW.equals(element.getObjectState())) {
            repository.remove(command.getElementUid());
        }
        return null;
    }
}