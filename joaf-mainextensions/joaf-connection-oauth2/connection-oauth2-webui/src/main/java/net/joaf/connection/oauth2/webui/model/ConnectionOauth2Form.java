package net.joaf.connection.oauth2.webui.model;

import net.joaf.connection.oauth2.model.Oauth2ConnectionMetadata;
import net.joaf.mainextensions.connection.model.ExternalConnection;

import java.io.Serializable;

/**
 * Created by cyprian on 03.04.15.
 */
public class ConnectionOauth2Form implements Serializable {
    private String uid;
    private String applicationId;
    private String applicationSecret;
    private String applicationUrl;
    private String applicationName;
    private String username;
    private String password;

    public static ConnectionOauth2Form createFromExternalConnection(ExternalConnection element) {
        ConnectionOauth2Form nelement = new ConnectionOauth2Form();
        nelement.setUid(element.getUid());
        Oauth2ConnectionMetadata connectionMetadata = (Oauth2ConnectionMetadata) element.getConnectionMetadata();
        nelement.setApplicationId(connectionMetadata.getApplicationId());
        nelement.setApplicationName(connectionMetadata.getApplicationName());
        nelement.setApplicationSecret(connectionMetadata.getApplicationSecret());
        nelement.setApplicationUrl(connectionMetadata.getApplicationUrl());
        nelement.setUsername(connectionMetadata.getUsername());
        nelement.setPassword(connectionMetadata.getPassword());
        return nelement;
    }

    public ExternalConnection toExternalConnection() {
        ExternalConnection element = new ExternalConnection();
        Oauth2ConnectionMetadata connectionMetadata = new Oauth2ConnectionMetadata();
        element.setConnectionMetadata(connectionMetadata);
        element.setName(this.applicationName);
        element.setUid(this.uid);
        connectionMetadata.setApplicationId(this.applicationId);
        connectionMetadata.setApplicationName(this.applicationName);
        connectionMetadata.setApplicationSecret(this.applicationSecret);
        connectionMetadata.setUsername(this.username);
        connectionMetadata.setPassword(this.password);
        connectionMetadata.setApplicationUrl(this.applicationUrl);
        return element;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationSecret() {
        return applicationSecret;
    }

    public void setApplicationSecret(String applicationSecret) {
        this.applicationSecret = applicationSecret;
    }

    public String getApplicationUrl() {
        return applicationUrl;
    }

    public void setApplicationUrl(String applicationUrl) {
        this.applicationUrl = applicationUrl;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
