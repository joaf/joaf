package net.joaf.connection.oauth2.webui.controllers;

import net.joaf.base.core.session.SessionContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by cyprian on 03.04.15.
 */
@Controller
@RequestMapping("/administration/connection/oauth2")
public class ConnectionOauth2AdminController extends ConnectionOauth2Controller {

    protected String getSubjectCardDataUid(SessionContext sessionContext) {
        return "app";
    }

    protected String getBackPath() {
        return "/administration.html";
    }

    protected String getControllerContextPath() {
        return "/administration/connection/oauth2";
    }

    protected String getUrl(String name) {
        return "/administration/connection/oauth2" + name + ".html";
    }
}
