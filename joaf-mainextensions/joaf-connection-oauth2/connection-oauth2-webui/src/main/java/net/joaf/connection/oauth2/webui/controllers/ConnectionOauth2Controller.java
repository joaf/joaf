package net.joaf.connection.oauth2.webui.controllers;

import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.cqrs.PageredResultExtended;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafNoSubjectContextException;
import net.joaf.base.core.session.SessionContext;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.base.core.web.model.WrapperWDTO;
import net.joaf.connection.oauth2.commands.*;
import net.joaf.connection.oauth2.commands.handlers.*;
import net.joaf.connection.oauth2.queries.ConnectionOauth2Query;
import net.joaf.connection.oauth2.webui.helpers.ConnectionOauth2ActionHelper;
import net.joaf.connection.oauth2.webui.model.ConnectionOauth2Form;
import net.joaf.mainextensions.connection.model.ExternalConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.ws.rs.QueryParam;
import java.security.Principal;
import java.util.Collections;
import java.util.Optional;

/**
 * Created by cyprian on 03.04.15.
 */
@Controller
@RequestMapping("/connection/oauth2")
public class ConnectionOauth2Controller {

    private static final Logger log = LoggerFactory.getLogger(ConnectionOauth2Controller.class);
    @Autowired
    private ConnectionOauth2Query query;
    @Autowired
    private CommandGateway commandGateway;

    @RequestMapping
    public String list(HttpSession session, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            PageredRequest pageredRequest = new PageredRequest(null, null, EObjectState.ACCEPTED, getSubjectCardDataUid(sessionContext));
            PageredResult<ExternalConnection> all = query.findAll(pageredRequest);
            PageredResultExtended<ExternalConnection> allExt = new PageredResultExtended<>(all, this::createActions);
            model.addAttribute("elements", allExt.getElements());
        } catch (Exception e) {
            model.addAttribute("elements", Collections.emptyList());
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            log.debug("error", e);
        }
        updateModelWithPaths(model);
        return getViewitem("/list");
    }

    private void createActions(WrapperWDTO<ExternalConnection> x) {
        x.getData().getActions().forEach(action -> x.getWebActions().add(ConnectionOauth2ActionHelper.createActionDetails(action, x.getData().getUid(), getControllerContextPath())));
    }

    @RequestMapping("/create")
    public String create(HttpSession session, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            ExternalConnection command = (ExternalConnection) commandGateway
                    .send(new CreateConnectionOauth2Command(principal.getName(), getSubjectCardDataUid(sessionContext)), CreateConnectionOauth2CommandHandler.class);
            return getRedirect("/edit/" + command.getUid());
        } catch (Exception e) {
            log.debug("error", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return getRedirect("");
        }
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable("id") String id, HttpSession session, RedirectAttributes redirectAttributes, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            ExternalConnection dbobject = query.findOne(id);
            ConnectionOauth2Form command = ConnectionOauth2Form.createFromExternalConnection(dbobject);
            model.addAttribute("command", command);
            updateModelWithPaths(model);
            return getViewitem("/edit");
        } catch (Exception e) {
            log.debug("error", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return getRedirect("");
        }
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String saveItem(@Valid @ModelAttribute("command") ConnectionOauth2Form command, @PathVariable("id") String id, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            commandGateway.send(new SaveConnectionOauth2Command(command.toExternalConnection(), principal.getName()), SaveConnectionOauth2CommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successsave");
            return getRedirect("");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            updateModelWithPaths(model);
            return getViewitem("/edit");
        }
    }

    @RequestMapping("/trash/{uid}")
    public String trash(@PathVariable("uid") String uid, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            commandGateway.send(new TrashConnectionOauth2Command(uid, principal.getName(), getSubjectCardDataUid(sessionContext)), TrashConnectionOauth2CommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successtrash", httpRequest.getContextPath() + getUrl("/undotrash/" + uid));
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    @RequestMapping("/undotrash/{id}")
    public String undotrash(@PathVariable("id") String id, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            commandGateway.send(new UndoTrashConnectionOauth2Command(id, principal.getName(), getSubjectCardDataUid(sessionContext)), UndoTrashConnectionOauth2CommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successundotrash", "1");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    @RequestMapping(value = "/cancel/{uid}", method = RequestMethod.POST)
    public String cancel(@PathVariable("uid") String uid, HttpSession session, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            commandGateway.send(new CancelConnectionOauth2Command(uid, principal.getName(), getSubjectCardDataUid(sessionContext)), CancelConnectionOauth2CommandHandler.class);
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    @RequestMapping("/requestauth/{uid}")
    public String requestauth(@PathVariable("uid") String uid, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            String redirUrl = (String) commandGateway.send(new RequestAuthorizationOauth2Command(uid, principal.getName(), getSubjectCardDataUid(sessionContext)), RequestAuthorizationOauth2CommandHandler.class);
            return "redirect:" + redirUrl;
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    @RequestMapping("/responseauth/{uid}")
    public String responseauth(@PathVariable("uid") String uid, @QueryParam("code") String code, @QueryParam("error") String error, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            commandGateway.send(new RequestTokenOauth2Command(uid, principal.getName(), getSubjectCardDataUid(sessionContext), code), RequestTokenOauth2CommandHandler.class);
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    protected String getSubjectCardDataUid(SessionContext sessionContext) {
        return sessionContext.getSubjectCardDataId();
    }

    protected void updateModelWithPaths(Model model) {
        model.addAttribute("backPath", getBackPath());
        model.addAttribute("controllerContextPath", getControllerContextPath());
    }

    private void checkSubjectUid(String subjectUid) throws JoafNoSubjectContextException {
        if (subjectUid == null) {
            throw new JoafNoSubjectContextException("");
        }
    }

    protected String getBackPath() {
        return "/";
    }

    protected String getControllerContextPath() {
        return "/connection/oauth2";
    }

    protected String getViewitem(String name) {
        return "/freemarker/connection/oauth2" + name;
    }

    protected String getRedirect(String name) {
        return "redirect:" + getUrl(name);
    }

    protected String getUrl(String name) {
        return "/connection/oauth2" + name + ".html";
    }
}
