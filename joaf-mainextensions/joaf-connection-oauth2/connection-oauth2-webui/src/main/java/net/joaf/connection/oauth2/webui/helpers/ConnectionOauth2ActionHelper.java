package net.joaf.connection.oauth2.webui.helpers;

import net.joaf.base.core.web.model.WebActionDetails;

/**
 * Created by cyprian on 03.04.15.
 */
public class ConnectionOauth2ActionHelper {
    public static WebActionDetails createActionDetails(String saction, String uid, String controllerContextPath) {
        WebActionDetails webActionDetails = new WebActionDetails();
        switch (saction) {
            case "EDIT":
                webActionDetails.setButtonStyle("btn-primary");
                webActionDetails.setActionUrl(controllerContextPath + "/edit/" + uid + ".html");
                webActionDetails.setIconName("glyphicon-edit");
                webActionDetails.setTooltip("joaf.edit");
                break;
            case "TRASH":
                webActionDetails.setButtonStyle("btn-danger");
                webActionDetails.setActionUrl(controllerContextPath + "/trash/" + uid + ".html");
                webActionDetails.setIconName("glyphicon-trash");
                webActionDetails.setTooltip("joaf.trash");
                break;
            case "CONNECT":
                webActionDetails.setButtonStyle("btn-danger");
                webActionDetails.setActionUrl(controllerContextPath + "/requestauth/" + uid + ".html");
                webActionDetails.setIconName("glyphicon-sun");
                webActionDetails.setTooltip("joaf.trash");
                break;
            default:
                webActionDetails = null;
                break;
        }
        return webActionDetails;
    }
}
