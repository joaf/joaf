/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.confirmation.repository.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.utils.DateUtils;
import net.joaf.base.db.cassandra.AbstractCrudRepositoryCassandra;
import net.joaf.base.db.cassandra.CassandraSessionProvider;
import net.joaf.mainextensions.confirmation.model.EEmailActivationStatus;
import net.joaf.mainextensions.confirmation.model.EmailActivation;
import net.joaf.mainextensions.confirmation.repository.api.ConfirmationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by cyprian on 14.05.14.
 */
@Repository
public class ConfirmationRepositoryCassandra extends AbstractCrudRepositoryCassandra<EmailActivation> implements ConfirmationRepository {

    @Autowired
    private CassandraSessionProvider sessionProvider;

    @Override
    public String prepareId() {
        return String.valueOf(UUID.randomUUID());
    }

    @Override
    public void insert(EmailActivation obj) {
        Session session = sessionProvider.connect();
        String insertQuery = "INSERT INTO emailActivation (uid, confirmationKey, email, confirmationCode, originalAccountEmail, status, registerDate, confirmationDate, callbackName, metadata) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        session.execute(insertQuery, UUID.fromString(obj.getUid()), obj.getKey(), obj.getEmail(), obj.getCode(), obj.getOriginalAccountEmail(), obj.getStatus().toString(),
                DateUtils.localDateTimeToDate(obj.getRegisterDate()), DateUtils.localDateTimeToDate(obj.getConfirmationDate()), obj.getCallbackName(), obj.getMetadata());
    }

    @Override
    public EmailActivation findByKeyAndCode(String key, String code) {
        Session session = sessionProvider.connect();
        ResultSet execute = session.execute("select * from emailActivation where confirmationKey = ? AND confirmationCode = ? ALLOW FILTERING", key, code);
        List<Row> all = execute.all();
        if (all.size() == 1) {
            return this.bindRow(all.get(0));
        } else {
            return null;
        }
    }

    @Override
    public List<EmailActivation> findByKey(String key) {
        Session session = sessionProvider.connect();
        ResultSet execute = session.execute("select * from emailActivation where confirmationKey = ?", key);
        List<Row> all = execute.all();
        List<EmailActivation> retList = new ArrayList<>();
        for (Row row : all) {
            retList.add(bindRow(row));
        }
        return retList.stream().filter((x) -> EEmailActivationStatus.READY.equals(x.getStatus())).collect(Collectors.toList());
    }

    @Override
    public void updateMailData(String referenceUid, String json) {
        Session session = sessionProvider.connect();
        session.execute("update emailActivation set mailData = ? where uid = ?", json, UUID.fromString(referenceUid));
    }

    @Override
    public List<EmailActivation> findAllActive() {
        Session session = sessionProvider.connect();
        ResultSet execute = session.execute("select * from emailActivation where status = ?", EEmailActivationStatus.READY.toString());
        List<Row> all = execute.all();
        List<EmailActivation> retList = new ArrayList<>();
        for (Row row : all) {
            retList.add(bindRow(row));
        }
        return retList;
    }

    @Override
    public void deprecate(EmailActivation element) {
        Session session = sessionProvider.connect();
        session.execute("update emailActivation set status = ? where uid = ?", EEmailActivationStatus.DEPRECATED.toString(), UUID.fromString(element.getUid()));
    }

    @Override
    public List<EmailActivation> findAll() {
        Session session = sessionProvider.connect();
        ResultSet execute = session.execute("select * from emailActivation");
        List<Row> all = execute.all();
        return all.stream().map(this::bindRow).collect(Collectors.toList());
    }

    @Override
    public String collectionName() {
        return "emailactivation";
    }

    @Override
    public Class<EmailActivation> getEntityClass() {
        return EmailActivation.class;
    }

    @Override
    public void store(EmailActivation element) throws JoafDatabaseException {
    }

    public void updateStatusAndConfirmationDate(String uid, EEmailActivationStatus status, Date confirmationDate) {
        Session session = sessionProvider.connect();
        String insertQuery = "UPDATE emailActivation SET status = ?, confirmationDate = ? WHERE uid = ?";
        session.execute(insertQuery, status.toString(), confirmationDate, UUID.fromString(uid));
    }

    public void updateStatus(String uid, EEmailActivationStatus status) {
        Session session = sessionProvider.connect();
        String insertQuery = "UPDATE emailActivation SET status = ? WHERE uid = ?";
        session.execute(insertQuery, status.toString(), UUID.fromString(uid));
    }

    protected EmailActivation bindRow(Row row) {
        EmailActivation obj = new EmailActivation();
        obj.setUid(row.getUUID("uid").toString());
        obj.setEmail(row.getString("email"));
        obj.setKey(row.getString("confirmationKey"));
        obj.setCode(row.getString("confirmationCode"));
        obj.setStatus(EEmailActivationStatus.valueOf(row.getString("status")));
        obj.setRegisterDate(DateUtils.dateToLocalDateTime(row.getTimestamp("registerDate")));
        obj.setConfirmationDate(DateUtils.dateToLocalDateTime(row.getTimestamp("confirmationDate")));
        obj.setCallbackName(row.getString("callbackName"));
        obj.setMetadata(row.getString("metadata"));
        obj.setOriginalAccountEmail(row.getString("originalAccountEmail"));
        obj.setMailData(row.getString("mailData"));
        return obj;
    }

}
