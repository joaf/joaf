package net.joaf.mainextensions.confirmation.repository.mysql;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.utils.DateUtils;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.mainextensions.confirmation.model.EEmailActivationStatus;
import net.joaf.mainextensions.confirmation.model.EmailActivation;
import net.joaf.mainextensions.confirmation.repository.api.ConfirmationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by cyprian on 08.03.15.
 */
@Repository
public class ConfirmationRepositoryMysql extends AbstractRepositoryMysql<EmailActivation> implements ConfirmationRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    protected String getTableName() {
        return "emailActivation";
    }

    @Override
    protected JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    protected EmailActivation mapRow(ResultSet rs, int rowNum) throws SQLException {
        return getRowMapper().mapRow(rs, rowNum);
    }

    protected RowMapper<EmailActivation> getRowMapper() {
        return (row, i) -> {
            EmailActivation obj = new EmailActivation();
            obj.setUid(row.getString("uid"));
            obj.setEmail(row.getString("email"));
            obj.setKey(row.getString("confirmationKey"));
            obj.setCode(row.getString("confirmationCode"));
            obj.setStatus(EEmailActivationStatus.valueOf(row.getString("status")));
            obj.setRegisterDate(DateUtils.dateToLocalDateTime(row.getDate("registerDate")));
            obj.setConfirmationDate(DateUtils.dateToLocalDateTime(row.getDate("confirmationDate")));
            obj.setCallbackName(row.getString("callbackName"));
            obj.setMetadata(row.getString("metadata"));
            obj.setOriginalAccountEmail(row.getString("originalAccountEmail"));
            obj.setMailData(row.getString("mailData"));
            return obj;
        };
    }

    @Override
    public EmailActivation findByKeyAndCode(String key, String code) {
        String sql = "SELECT * FROM " + getTableName() + " WHERE confirmationKey=? AND confirmationCode=? LIMIT 1";
        return jdbcTemplate.query(sql, new Object[]{key, code}, this.getRowMapper()).stream().findFirst().get();
    }

    @Override
    public void updateStatusAndConfirmationDate(String uid, EEmailActivationStatus status, Date confirmationDate) {
        String insertQuery = "UPDATE emailActivation SET status = ?, confirmationDate = ? WHERE uid = ?";
        jdbcTemplate.update(insertQuery, status.toString(), confirmationDate, uid);
    }

    @Override
    public void updateStatus(String uid, EEmailActivationStatus status) {
        String insertQuery = "UPDATE emailActivation SET status = ? WHERE uid = ?";
        jdbcTemplate.update(insertQuery, status.toString(), uid);
    }

    @Override
    public List<EmailActivation> findByKey(String key) {
        String sql = "SELECT * FROM " + getTableName() + " WHERE confirmationKey=? ";
        return jdbcTemplate.query(sql, new Object[]{key}, this.getRowMapper());
    }

    @Override
    public void updateMailData(String referenceUid, String json) {
        jdbcTemplate.update("update " + getTableName() + " set mailData = ? where uid = ?", json, referenceUid);
    }

    @Override
    public List<EmailActivation> findAllActive() {
        String sql = "SELECT * FROM " + getTableName() + " WHERE status=? ";
        return jdbcTemplate.query(sql, new Object[]{EEmailActivationStatus.READY.toString()}, this.getRowMapper());
    }

    @Override
    public void deprecate(EmailActivation element) {
        jdbcTemplate.update("update " + getTableName() + " set status = ? where uid = ?", EEmailActivationStatus.DEPRECATED.toString(), element.getUid());
    }

    @Override
    public void insert(EmailActivation obj) throws JoafDatabaseException {
        String insertQuery =
                "INSERT INTO " + getTableName() + " (uid, confirmationKey, email, confirmationCode, originalAccountEmail, status, registerDate, confirmationDate, callbackName, metadata)"
                        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        jdbcTemplate.update(insertQuery,
                obj.getUid(), obj.getKey(), obj.getEmail(), obj.getCode(), obj.getOriginalAccountEmail()
                , obj.getStatus().toString(), DateUtils.localDateTimeToDate(obj.getRegisterDate()),
                DateUtils.localDateTimeToDate(obj.getConfirmationDate()), obj.getCallbackName(), obj.getMetadata());
    }

    @Override
    public void store(EmailActivation element) throws JoafDatabaseException {

    }

    @Override
    public String collectionName() {
        return getTableName();
    }

    @Override
    public Class<EmailActivation> getEntityClass() {
        return EmailActivation.class;
    }

    @Override
    public String prepareId() {
        return UUID.randomUUID().toString();
    }
}
