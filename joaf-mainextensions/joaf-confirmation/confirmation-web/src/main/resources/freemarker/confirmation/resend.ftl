<#import "/templates/login.ftl" as page />
<@page.layout "xxx">
<#--<@spring.bind "registerForm" />-->
<div class="awidget-head">
<#--${registerForm.email}-->
    <#--<@spring.showErrors "*", "errors" />-->
</div>
<div class="awidget-body">
    <!-- Page title -->
    <div class="page-title text-center">
        <h2><@spring.message 'joaf.confirmation.resend.form.title' /></h2>
        <hr/>
    </div>
    <!-- Page title -->
    <form class="form-horizontal" role="form" action="${rc.contextPath}/confirmation/resend.html" method="post">
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-3 control-label"><@spring.message 'joaf.email.label' /></label>

            <div class="col-lg-8">
                <#assign placeholder = springMacroRequestContext.getMessage('joaf.email.label') />
                <@spring.formInput 'command.email' 'placeholder="${placeholder}" class="form-control"' />
                <#if spring.status.error>
                    <p><@spring.showErrors "<br>", "color:red" /></p>
                </#if>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-3 col-lg-10">
                <button type="submit" class="btn btn-info"><@spring.message 'joaf.send' /></button>
                <a href="${rc.contextPath}/loginPage.html"
                   class="btn btn-success"><@spring.message 'joaf.user.signin' /></a>
            </div>
        </div>
    </form>
</div>
</@page.layout>