/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.confirmation.web.controllers;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.mainextensions.confirmation.model.dto.ConfirmationResultDto;
import net.joaf.mainextensions.confirmation.services.ConfirmationService;
import net.joaf.mainextensions.confirmation.web.dto.ResendForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.ws.rs.QueryParam;
import java.security.Principal;

/**
 * Created by cyprian on 14.05.14.
 */
@Controller
@RequestMapping("/confirmation")
public class ConfirmationController {

    public static final String SPRING_SECURITY_CONTEXT = "SPRING_SECURITY_CONTEXT";
    @Autowired
    private ConfirmationService confirmationService;
    @Autowired
    private MessageSource messageSource;

    @RequestMapping(method = RequestMethod.GET)
    public String autoConfirmation(@QueryParam("key") String key, @QueryParam("code") String code, HttpServletRequest request, HttpSession session,
                                   RedirectAttributes redirectAttributes) {
        // no spring sercurity context in Controller Principal parameter
        ConfirmationResultDto confirm = null;
        try {
            confirm = confirmationService.confirm(key, code, request.getRemoteAddr());
        } catch (JoafDatabaseException e) {
            e.printStackTrace();
        }
        if (confirm == null) {
            WebFlashUtil.addError(redirectAttributes, "joaf.confirmation.message.error.title", "joaf.confirmation.message.error.body");
            return "redirect:/loginPage.html";
        }
        if (confirm.getMessageBody() != null && confirm.getMessageTitle() != null) {
            if (confirm.getConfirmed()) {
                WebFlashUtil.addMessage(redirectAttributes, confirm.getMessageTitle(), confirm.getMessageBody());
            } else {
                WebFlashUtil.addError(redirectAttributes, confirm.getMessageTitle(), confirm.getMessageBody());
            }
        }
        if (confirm.getInvalidateSession()) {
            cleanSecuritySessionAttibutes(session);
        }
        for (String sessionKey : confirm.getSessionAttributes().keySet()) {
            session.setAttribute(sessionKey, confirm.getSessionAttributes().get(sessionKey));
        }
        Principal principal = getPrincipal(session);
        if (principal == null && confirm.getRedirectUrl().equals("/")) {
            confirm.setRedirectUrl("/loginPage.html");
        }
        return "redirect:" + confirm.getRedirectUrl();
    }

    @RequestMapping(value = "/resend", method = RequestMethod.GET)
    public String resendForm(Model model) {
        ResendForm command = new ResendForm();
        model.addAttribute("command", command);
        return "freemarker/confirmation/resend";
    }

    @RequestMapping(value = "/resend", method = RequestMethod.POST)
    public String resendPost(@Valid @ModelAttribute("command") ResendForm resendForm, BindingResult result, HttpServletRequest request, HttpSession session,
                             RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "freemarker/confirmation/resend";
        }
        try {
            confirmationService.resend(resendForm.getEmail());
        } catch (JoafDatabaseException e) {
            e.printStackTrace();
        }
        WebFlashUtil.addMessage(redirectAttributes, "joaf.confirmation.resend.title", "joaf.confirmation.resend.body");
        return "redirect:/loginPage.html";
    }

    private void cleanSecuritySessionAttibutes(HttpSession session) {
        Object springSecurityContext = session.getAttribute(SPRING_SECURITY_CONTEXT);
        if (springSecurityContext != null) {
            SecurityContext securityContext = (SecurityContext) springSecurityContext;
            session.removeAttribute(SPRING_SECURITY_CONTEXT);
        }
    }

    private Principal getPrincipal(HttpSession session) {
        Object springSecurityContext = session.getAttribute(SPRING_SECURITY_CONTEXT);
        if (springSecurityContext != null) {
            SecurityContext securityContext = (SecurityContext) springSecurityContext;
            return securityContext.getAuthentication();
        } else {
            return null;
        }
    }

}

