/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.confirmation.model.dto;

import net.joaf.mainextensions.confirmation.model.EmailActivation;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cyprian on 15.05.14.
 */
public class ConfirmationResultDto implements Serializable {

    private String redirectUrl = "/";
    private Boolean confirmed = Boolean.FALSE;
    private EmailActivation emailActivation;
    private String messageTitle;
    private String messageBody;
    private Map<String, Object> sessionAttributes = new HashMap<>();
    private Boolean invalidateSession = Boolean.FALSE;
    private String remoteAddr;

    public ConfirmationResultDto(boolean confirmed, EmailActivation emailActivation, String remoteAddr) {
        this.setConfirmed(confirmed);
        this.setEmailActivation(emailActivation);
        this.setRemoteAddr(remoteAddr);
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public EmailActivation getEmailActivation() {
        return emailActivation;
    }

    public void setEmailActivation(EmailActivation emailActivation) {
        this.emailActivation = emailActivation;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public Map<String, Object> getSessionAttributes() {
        return sessionAttributes;
    }

    public Boolean getInvalidateSession() {
        return invalidateSession;
    }

    public void setInvalidateSession(Boolean invalidateSession) {
        this.invalidateSession = invalidateSession;
    }

    public String getRemoteAddr() {
        return remoteAddr;
    }

    public void setRemoteAddr(String remoteAddr) {
        this.remoteAddr = remoteAddr;
    }
}
