/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.confirmation.repository.api;

import net.joaf.base.core.db.CrudRepository;
import net.joaf.base.core.db.NoSqlRepository;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.confirmation.model.EEmailActivationStatus;
import net.joaf.mainextensions.confirmation.model.EmailActivation;

import java.util.Date;
import java.util.List;

/**
 * Confirmation repository.
 *
 * @author Cyprian Śniegota
 * @since 1.0
 */
public interface ConfirmationRepository extends NoSqlRepository, CrudRepository<EmailActivation> {

    void insert(EmailActivation emailActivation) throws JoafDatabaseException;

    EmailActivation findByKeyAndCode(String key, String code) throws JoafDatabaseException;

    void updateStatusAndConfirmationDate(String uid, EEmailActivationStatus status, Date confirmationDate) throws JoafDatabaseException;

    void updateStatus(String uid, EEmailActivationStatus status) throws JoafDatabaseException;

    List<EmailActivation> findByKey(String key) throws JoafDatabaseException;

    void updateMailData(String referenceUid, String json) throws JoafDatabaseException;

    List<EmailActivation> findAllActive() throws JoafDatabaseException;

    void deprecate(EmailActivation element) throws JoafDatabaseException;

    List<EmailActivation> findAll() throws JoafDatabaseException;
}
