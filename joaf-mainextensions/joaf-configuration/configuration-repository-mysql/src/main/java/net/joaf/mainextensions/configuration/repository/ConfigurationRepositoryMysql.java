/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.configuration.repository;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.mainextensions.configuration.model.Configuration;
import net.joaf.mainextensions.configuration.repository.api.ConfigurationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * Created by cyprian on 01.02.15.
 */
@Repository
public class ConfigurationRepositoryMysql extends AbstractRepositoryMysql<Configuration> implements ConfigurationRepository {
    private static final ConfigurationRowMapper ROW_MAPPER = new ConfigurationRowMapper();
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    protected String getTableName() {
        return "configuration";
    }

    @Override
    protected JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    protected Configuration mapRow(ResultSet rs, int rowNum) throws SQLException {
        return ROW_MAPPER.mapRow(rs, rowNum);
    }

    @Override
    public Configuration findByNameAndModule(String name, String module) throws JoafDatabaseException {
        String sql = "SELECT * FROM configuration WHERE name = ? AND module = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{name, module}, this::mapRow);
    }

    @Override
    public void store(Configuration command) throws JoafDatabaseException {

    }

    @Override
    public String collectionName() {
        return getTableName();
    }

    @Override
    public Class<Configuration> getEntityClass() {
        return Configuration.class;
    }

    @Override
    public void insert(Configuration command) throws JoafDatabaseException {

    }

    @Override
    public String prepareId() {
        return UUID.randomUUID().toString();
    }

    private static class ConfigurationRowMapper implements RowMapper<Configuration> {

        @Override
        public Configuration mapRow(ResultSet resultSet, int number) throws SQLException {
            Configuration configuration = new Configuration();
            configuration.setUid(resultSet.getString("uid"));
            configuration.setModule(resultSet.getString("module"));
            configuration.setName(resultSet.getString("name"));
            configuration.setValue(resultSet.getString("value"));
            return configuration;
        }
    }
}
