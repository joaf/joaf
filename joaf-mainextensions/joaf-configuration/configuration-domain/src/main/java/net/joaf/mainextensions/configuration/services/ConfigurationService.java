/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.configuration.services;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.configuration.model.Configuration;
import net.joaf.mainextensions.configuration.repository.api.ConfigurationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: cyprian
 * Date: 06.01.14
 * Time: 02:40
 * To change this template use File | Settings | File Templates.
 */

@Service
public class ConfigurationService {

    @Autowired
    private ConfigurationRepository repository;

    public List<Configuration> findAll() throws JoafDatabaseException {
        return repository.findAll();
    }

    public Configuration findOne(String id) throws JoafDatabaseException {
        return repository.findOne(id);
    }

    public void store(Configuration command) throws JoafDatabaseException {
        repository.store(command);
    }

    public void insert(Configuration command) throws JoafDatabaseException {
        repository.insert(command);
    }

    public void remove(String id) throws JoafDatabaseException {
        repository.remove(id);
    }

    public Configuration createObject() {
        Configuration configuration = new Configuration();
        configuration.setUid(repository.prepareId());
        return configuration;
    }
}
