/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.configuration.services;

import net.joaf.base.core.db.CrudRepository;
import net.joaf.base.core.services.AbstractJsonRestoreService;
import net.joaf.base.core.services.JsonRestoreService;
import net.joaf.mainextensions.configuration.model.Configuration;
import net.joaf.mainextensions.configuration.repository.api.ConfigurationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by cyprian on 13.11.14.
 */
@Service
public class ConfigurationRestoreService extends AbstractJsonRestoreService<Configuration> implements JsonRestoreService {
    @Autowired
    private ConfigurationRepository repository;

    @Override
    protected CrudRepository<Configuration> getRepository() {
        return repository;
    }

    @Override
    public String getModuleName() {
        return "configuration";
    }
}
