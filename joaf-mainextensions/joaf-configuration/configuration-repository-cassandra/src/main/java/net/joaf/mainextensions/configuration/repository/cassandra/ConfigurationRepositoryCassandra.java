/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.configuration.repository.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.cassandra.AbstractCrudRepositoryCassandra;
import net.joaf.base.db.cassandra.CassandraSessionProvider;
import net.joaf.base.db.cassandra.RepositoryCassandra;
import net.joaf.mainextensions.configuration.model.Configuration;
import net.joaf.mainextensions.configuration.repository.api.ConfigurationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by cyprian.sniegota on 03.03.14.
 */
@Repository
public class ConfigurationRepositoryCassandra extends AbstractCrudRepositoryCassandra<Configuration> implements ConfigurationRepository, RepositoryCassandra {

    @Autowired
    private CassandraSessionProvider sessionProvider;

    protected Configuration bindRow(Row row) {
        Configuration configuration = new Configuration();
        configuration.setUid(row.getUUID("uid").toString());
        configuration.setName(row.getString("name"));
        configuration.setValue(row.getString("value"));
        configuration.setModule(row.getString("module"));
        return configuration;
    }

    @Override
    public Configuration findByNameAndModule(String name, String module) {
        Session session = sessionProvider.connect();
        ResultSet rs = session.execute("select * from configuration where name = ? AND module = ? ALLOW FILTERING", name, module);
        List<Row> all = rs.all();
        if (all.size() > 0) {
            return this.bindRow(all.get(0));
        } else {
            return null;
        }
    }

    @Override
    public List<Configuration> findAll() throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        ResultSet rs = session.execute("select * from configuration");
        List<Row> all = rs.all();
        return all.stream().map(this::bindRow).collect(Collectors.toList());
    }

    @Override
    public Configuration findOne(String id) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        ResultSet rs = session.execute("select * from configuration where uid = ? ", UUID.fromString(id));
        List<Row> all = rs.all();
        if (all.size() > 0) {
            return this.bindRow(all.get(0));
        } else {
            return null;
        }
    }

    @Override
    public String collectionName() {
        return "configuration";
    }

    @Override
    public Class<Configuration> getEntityClass() {
        return Configuration.class;
    }

    @Override
    public void store(Configuration command) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        String sql = "UPDATE configuration SET module= ?, name = ?, value = ? WHERE uid = ?";
        session.execute(sql, command.getModule(), command.getName(), command.getValue(), UUID.fromString(command.getUid()));
    }

    @Override
    public void insert(Configuration command) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        String sql = "INSERT INTO configuration (uid, module, name, value) VALUES"
                + " (?,?,?,?)";
        UUID uuid = UUID.fromString(command.getUid());
        session.execute(sql, uuid, command.getModule(), command.getName(), command.getValue());
    }

    @Override
    public void remove(String id) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        String sql = "DELETE FROM configuration WHERE uid = ?";
        session.execute(sql, UUID.fromString(id));
    }
}
