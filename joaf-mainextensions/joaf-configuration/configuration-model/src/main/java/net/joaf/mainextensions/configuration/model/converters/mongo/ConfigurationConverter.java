/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.configuration.model.converters.mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import net.joaf.base.core.utils.UniversalJsonConverter;
import net.joaf.mainextensions.configuration.model.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

/**
 * Created with IntelliJ IDEA.
 * User: cyprian
 * Date: 06.01.14
 * Time: 02:36
 * To change this template use File | Settings | File Templates.
 */
public class ConfigurationConverter extends UniversalJsonConverter<Configuration> {

    @Override
    public Configuration objectFromDBObject(DBObject dbObject, Class<Configuration> configurationClass) {
        Configuration object = new Configuration();
        object.setUid(String.valueOf(dbObject.get("_id")));
        object.setName(String.valueOf(dbObject.get("name")));
        object.setModule(String.valueOf(dbObject.get("module")));
        object.setValue(String.valueOf(dbObject.get("value")));
        return object;
    }

    @Override
    public DBObject dbObjectFromObject(Configuration obj) {
        DBObject dbObject = new BasicDBObject();
        if (StringUtils.trimToNull(obj.getUid()) != null) {
            dbObject.put("_id", new ObjectId(obj.getUid()));
        }
        dbObject.put("module", obj.getModule());
        dbObject.put("name", obj.getName());
        dbObject.put("value", obj.getValue());
        return dbObject;
    }
}
