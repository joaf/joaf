/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.configuration.dictionary;

/**
 * Created by cyprian on 21.12.14.
 */
public enum CountryEnum {

    PL_PL("PL_pl", "country.poland"),
    EN_GB("EN_gb", "country.greatbritan");
    private String code;
    private String localizedName;

    CountryEnum(String code, String localizedName) {
        this.code = code;
        this.localizedName = localizedName;
    }

    public String getCode() {
        return code;
    }

    public String getLocalizedName() {
        return localizedName;
    }
}
