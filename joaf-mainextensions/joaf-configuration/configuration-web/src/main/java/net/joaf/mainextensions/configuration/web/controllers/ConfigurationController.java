/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.configuration.web.controllers;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.configuration.model.Configuration;
import net.joaf.mainextensions.configuration.services.ConfigurationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

//import net.joaf.base.core.configuration.helpers.ImportExportHelper;

/**
 * Created with IntelliJ IDEA.
 * User: cyprian
 * Date: 06.01.14
 * Time: 02:45
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class ConfigurationController {

    private static final Logger log = LoggerFactory.getLogger(ConfigurationController.class);

    //    @Autowired
    //    ImportExportHelper importExportHelper;
    @Autowired
    ConfigurationService service;

    @RequestMapping("/administration/configuration")
    public String list(HttpSession session, Model model) {
        try {
            List<Configuration> all = service.findAll();
            model.addAttribute("command", all);
        } catch (JoafDatabaseException e) {
            e.printStackTrace();
        }
        return "freemarker/configuration/admin/list";
    }

    @RequestMapping(value = "/administration/configuration/export")
    public HttpEntity<String> export(HttpSession session) {
        //        try {
        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("application", "json"));
        header.set("Content-Disposition",
                "attachment; filename=configuration.json");
        //            String configurations = importExportHelper.exportData("configuration");
        //            return new HttpEntity<>(configurations, header);
        //        } catch (JoafDatabaseException e) {
        //            e.printStackTrace();
        //        }
        return null;
    }

    @RequestMapping("/administration/configuration/edit_{id}")
    public String newItem(@PathVariable("id") String id, HttpSession session, Model model) {
        Configuration command = null;
        if (id.equals("0")) {
            command = service.createObject();
        } else {
            try {
                command = service.findOne(id);
            } catch (JoafDatabaseException e) {
                e.printStackTrace();
            }
        }
        model.addAttribute("command", command);

        return "freemarker/configuration/admin/edit";
    }

    @RequestMapping(value = "/administration/configuration/save", method = RequestMethod.POST)
    public String saveItem(@Valid @ModelAttribute("command") Configuration command, HttpSession session, Model model) {
        try {
            service.store(command);
            return "redirect:/administration/configuration.html";
        } catch (JoafDatabaseException e) {
            e.printStackTrace();
        }
        return "freemarker/configuration/admin/edit";
    }

    @RequestMapping("/administration/configuration/remove_{id}")
    public String remove(@PathVariable("id") String id, HttpSession session, Model model) {
        if (!id.equals("0")) {
            try {
                service.remove(id);
            } catch (JoafDatabaseException e) {
                e.printStackTrace();
            }
        }
        return "redirect:/administration/configuration.html";
    }

    @RequestMapping(value = "/administration/configuration/cancel", method = RequestMethod.POST)
    public String cancel(HttpSession session, Model model) {
        return "redirect:/administration/configuration.html";
    }
}
