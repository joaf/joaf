<#import "/templates/base.ftl" as page />
<@page.layout "Invoices">
<div class="awidget full-width">
    <div class="awidget-head">
        <div class="btn-group">
        <#--<a href="/remoteconnections/edit/0.html" class="btn btn-primary">Create</a>-->
            <#--<a href="/invoice/syncInfakt.html" class="btn btn-info">Infakt sync</a>-->
        <#--<button class="btn btn-success">Center</button>-->
        <#--<button class="btn btn-primary">Right</button>-->
        </div>
    </div>
    <div class="awidget-body">
        <form class="form-horizontal" role="form" method="post"
              action="${rc.contextPath}/administration/configuration/save.html">
            <div class="form-group">
                <label class="col-lg-2 control-label">Module</label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.module' 'placeholder="Module" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Name</label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.name' 'placeholder="Name" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Value</label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.value' 'placeholder="Value" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>

            <@spring.formHiddenInput 'command.uid' '' />
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="button"
                    onclick="$(this.form).attr('action','${rc.contextPath}/administration/configuration/cancel.html');$(this.form).submit();"
                    class="btn btn-default">Cancel
            </button>
        </form>
    </div>

</div>
</@page.layout>