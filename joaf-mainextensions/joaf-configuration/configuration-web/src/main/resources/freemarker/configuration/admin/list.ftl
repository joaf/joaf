<#import "/templates/base.ftl" as page />
<@page.layout "xxx">


<div class="awidget full-width">
    <div class="awidget-head">
        <div class="btn-group">
            <a href="${rc.contextPath}/administration/configuration/edit_0.html" class="btn btn-primary">Create</a>
            <a href="${rc.contextPath}/administration/configuration/export.html" class="btn btn-default">Export</a>
        </div>
    </div>
    <div class="awidget-body">
        <table class="table table-bordered admin-media ">
            <thead>
            <tr>
                <th>
                    <input type='checkbox' value='check1'/>
                </th>
                <th>Name</th>
                <th>Module</th>
                <th>Value</th>
                <th>Uid</th>
            </tr>
            </thead>
            <tbody>

                <#list command as element>
                <tr>
                    <td>
                        <input type='checkbox' value='check1'/>
                    </td>
                    <td>${element.name}</td>
                    <td>${element.module}</td>
                    <td>${element.value}</td>
                    <td>${element.uid}</td>
                    <td>

                        <a href="${rc.contextPath}/administration/configuration/edit_${element.uid}.html"
                           class="btn btn-xs btn-success"><i class="fa fa-pencil"></i> </a>
                        <a href="${rc.contextPath}/administration/configuration/remove_${element.uid}.html"
                           class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> </a>

                    </td>
                </tr>
                </#list>
            </tbody>
        </table>
    </div>

</div>
</@page.layout>