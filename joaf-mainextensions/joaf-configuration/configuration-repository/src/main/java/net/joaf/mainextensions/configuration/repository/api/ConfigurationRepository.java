/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.configuration.repository.api;

import net.joaf.base.core.db.CrudRepository;
import net.joaf.base.core.db.NoSqlRepository;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.configuration.model.Configuration;

import java.util.List;

/**
 * Created by cyprian.sniegota on 03.03.14.
 */
public interface ConfigurationRepository extends NoSqlRepository, CrudRepository<Configuration> {

    public Configuration findByNameAndModule(String name, String module) throws JoafDatabaseException;

    public List<Configuration> findAll() throws JoafDatabaseException;

    public Configuration findOne(String id) throws JoafDatabaseException;

    public void store(Configuration command) throws JoafDatabaseException;

    public void insert(Configuration command) throws JoafDatabaseException;

    public void remove(String id) throws JoafDatabaseException;
}
