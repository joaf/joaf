/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.user.callbacks;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.confirmation.interfaces.ConfirmationCallback;
import net.joaf.mainextensions.confirmation.model.EmailActivation;
import net.joaf.mainextensions.confirmation.model.dto.ConfirmationResultDto;
import net.joaf.mainextensions.user.activity.UserActivityEntryProducer;
import net.joaf.mainextensions.user.model.UserCard;
import net.joaf.mainextensions.user.repository.api.UserCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by cyprian on 15.05.14.
 */
@Component
@Qualifier("confirmationCallback")
public class UserRegistrationConfirmationCallback implements ConfirmationCallback {

    public static final String CALLBACK_NAME = "userRegistration";

    @Autowired
    private UserCardRepository userCardRepository;

    @Autowired
    private UserActivityEntryProducer userActivityEntryProducer;

    @Override
    public ConfirmationResultDto callback(EmailActivation emailActivation, ConfirmationResultDto confirmationResultDto) {
        try {
            Optional<UserCard> oneByUsernameOptional = userCardRepository.findByUsername(emailActivation.getKey());
            UserCard oneByUsername = oneByUsernameOptional.get();
            oneByUsername.setEmail(emailActivation.getKey());
            oneByUsername.setUsername(emailActivation.getKey());
            oneByUsername.setActive(true);

            userCardRepository.store(oneByUsername);
            confirmationResultDto.setMessageTitle("joaf.user.emailactivation.message.title");
            confirmationResultDto.setMessageBody("joaf.user.emailactivation.success.body");
            userActivityEntryProducer.createRegisterConfirm(confirmationResultDto.getRemoteAddr(), oneByUsername);
        } catch (JoafDatabaseException e) {
            e.printStackTrace();
        }
        return confirmationResultDto;
    }

    @Override
    public String getName() {
        return CALLBACK_NAME;
    }
}
