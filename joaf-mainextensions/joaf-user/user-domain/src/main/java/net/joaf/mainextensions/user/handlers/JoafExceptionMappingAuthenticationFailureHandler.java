/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.user.handlers;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.user.activity.UserActivityEntryProducer;
import net.joaf.mainextensions.user.model.UserCard;
import net.joaf.mainextensions.user.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/**
 * Created by cyprian on 20.05.14.
 */
public class JoafExceptionMappingAuthenticationFailureHandler extends ExceptionMappingAuthenticationFailureHandler {
    @Autowired
    private UserActivityEntryProducer userActivityEntryProducer;

    @Autowired
    private UserService userService;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
            throws IOException, ServletException {
        String username = request.getParameter("username");
        if (username != null) {
            try {
                Optional<UserCard> oneUserDetailsOptional = userService.findOneUserDetails(username);
                if (oneUserDetailsOptional.isPresent()) {
                    userActivityEntryProducer.createBadLogin(request, oneUserDetailsOptional.get());
                }
            } catch (JoafDatabaseException e) {
                e.printStackTrace();
            }
        }
        super.onAuthenticationFailure(request, response, exception);
    }

    public void setUserActivityEntryProducer(UserActivityEntryProducer userActivityEntryProducer) {
        this.userActivityEntryProducer = userActivityEntryProducer;
    }
}
