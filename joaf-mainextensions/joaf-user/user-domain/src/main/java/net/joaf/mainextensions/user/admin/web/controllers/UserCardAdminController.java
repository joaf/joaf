/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.user.admin.web.controllers;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.error.JoafNoSubjectContextException;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.base.core.web.model.WebActionDetails;
import net.joaf.mainextensions.user.admin.web.helpers.UserCardAdminActionHelper;
import net.joaf.mainextensions.user.admin.web.model.UserCardAdminWDTO;
import net.joaf.mainextensions.user.model.UserCard;
import net.joaf.mainextensions.user.model.dto.EUserCardAdminAction;
import net.joaf.mainextensions.user.services.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by cyprian on 26.04.14.
 */
@Controller
@RequestMapping(UserCardAdminController.BASE_PATH)
public class UserCardAdminController {
    protected static final String BASE_PATH = "/administration/user";
    private static final Logger logger = LoggerFactory.getLogger(UserCardAdminController.class);
    @Autowired
    private UserService service;

    @RequestMapping
    public String list(HttpSession session, Model model) throws JoafNoSubjectContextException {
        try {
            List<UserCard> retList = service.findAll();
            List<UserCardAdminWDTO> displayList = retList.stream().map(UserCardAdminWDTO::new).collect(Collectors.toList());
            displayList.forEach((x) -> {
                List<EUserCardAdminAction> actions = x.getData().getAdminActions();
                for (EUserCardAdminAction action : actions) {
                    WebActionDetails actionDetails = UserCardAdminActionHelper.createActionDetails(action, x.getData().getUid());
                    if (actionDetails != null) {
                        x.getWebActions().add(actionDetails);
                    }
                }
            });
            model.addAttribute("elements", displayList);
        } catch (JoafDatabaseException e) {
            e.printStackTrace();
        }
        return getViewTemplate("/list");
    }

    @RequestMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, HttpSession session, Model model) throws JoafNoSubjectContextException {
        UserCard command;
        try {
            command = service.findByUid(id);
            model.addAttribute("command", command);
            HashMap<String, String> roleOptions = new LinkedHashMap<String, String>();
            roleOptions.put("ROLE_USER", "ROLE_USER");
            roleOptions.put("ROLE_SYSTEMUSER", "ROLE_SYSTEMUSER");
            roleOptions.put("ROLE_TESTUSER", "ROLE_TESTUSER");
            roleOptions.put("ROLE_ADMIN", "ROLE_ADMIN");
            model.addAttribute("roleOptions", roleOptions);
        } catch (JoafDatabaseException e) {
            logger.error("Error in controller", e);
        }
        return getViewTemplate("/edit");
    }

    @RequestMapping("/activate/{id}")
    public String activateUser(@PathVariable("id") String uid, HttpSession session, Model model, final RedirectAttributes redirectAttributes)
            throws JoafNoSubjectContextException {
        UserCard command;
        try {
            service.activateUser(uid);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.user.admin.activated.title", "joaf.user.admin.activated.body");
        } catch (JoafDatabaseException e) {
            logger.error("Error in controller", e);
        }
        return getRedirect("");
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute("command") UserCard command, @PathVariable("id") String id, BindingResult result, Model model,
                       Principal principal, HttpSession session, final RedirectAttributes redirectAttributes) throws JoafNoSubjectContextException {
        if (result.hasErrors()) {
            return getViewTemplate("/edit");
        }
        try {
            UserCard userCard = service.findByUid(command.getUid());
            userCard.setRoles(command.getRoles());
            userCard.setEffectiveRoles(command.getRoles());
            service.store(userCard);
        } catch (JoafDatabaseException e) {
            //            result.addError(new FieldError("command","id","Error saving"));
            //            redirectAttributes.addFlashAttribute("error","basiccustomer.error.1");
            model.addAttribute("error", "basiccustomer.error.savingCustomerForm");
            return getViewTemplate("/edit");
        }
        //        redirectAttributes.addFlashAttribute("message", "Item saved successful");
        WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successsave", "1");
        return getRedirect("");
    }

    @RequestMapping("/fixnullvalues")
    public String fixnullvalues(final RedirectAttributes redirectAttributes) throws JoafNoSubjectContextException {
        try {
            service.fixNullValues();
            WebFlashUtil.addMessage(redirectAttributes, "joaf.user.admin.activated.title", "Finished");
        } catch (JoafDatabaseException e) {
            logger.error("Error in controller", e);
        }
        return getRedirect("");
    }

    @RequestMapping("/cancel/{id}")
    public String cancel(@PathVariable("id") String id) {
        return this.getRedirect("");
    }

    private String getRedirect(String s) {
        String suffix = s;
        if (StringUtils.isEmpty(s)) {
            suffix = ".html";
        }
        return "redirect:" + BASE_PATH + "" + suffix;
    }

    private String getViewTemplate(String subPath) {
        return "freemarker" + "/user/admin" + StringUtils.trimToEmpty(subPath);
    }
}
