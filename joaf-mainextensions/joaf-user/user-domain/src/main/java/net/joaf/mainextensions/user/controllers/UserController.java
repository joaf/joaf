/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.user.controllers;

import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.error.JoafException;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.base.core.web.template.TemplateLocator;
import net.joaf.base.core.web.template.TemplateResources;
import net.joaf.mainextensions.user.activity.UserActivityEntryProducer;
import net.joaf.mainextensions.user.commands.RegisterUserCommand;
import net.joaf.mainextensions.user.commands.handlers.RegisterUserCommandHandler;
import net.joaf.mainextensions.user.exceptions.NoUserFoundException;
import net.joaf.mainextensions.user.model.RequestResetPassword;
import net.joaf.mainextensions.user.model.UserCard;
import net.joaf.mainextensions.user.model.dto.ActivateForm;
import net.joaf.mainextensions.user.model.dto.PasswordChangeForm;
import net.joaf.mainextensions.user.model.dto.RegisterForm;
import net.joaf.mainextensions.user.services.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.ws.rs.QueryParam;
import java.security.Principal;
import java.util.Optional;

/**
 * Created with IntelliJ IDEA.
 * User: cyprian
 * Date: 17.10.13
 * Time: 23:24
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private UserActivityEntryProducer userActivityEntryProducer;

    @Autowired
    private CommandGateway commandGateway;

    @Autowired
    private TemplateLocator templateLocator;

    @RequestMapping("/loginPage")
    public String loginPage(@RequestParam(value = "auth", required = false) String value, @RequestParam(value = "reason", required = false) String reason,
                            Model model) {
        if (reason != null && "removed".equals(reason)) {
            WebFlashUtil.addModelMessage(model, "joaf.error.title", "joaf.user.accountremoved.body");
        } else if (value != null) {
            WebFlashUtil.addModelMessage(model, "joaf.error.title", "joaf.user.unsuccessfullogin.body");
        }
        return getView("login");
    }

    @RequestMapping("/activatePage")
    public String activatePage(@RequestParam(value = "auth", required = false) String value, Model model, final RedirectAttributes redirectAttributes,
                               HttpServletRequest request) {
        if (value != null) {
            WebFlashUtil
                    .addMessage(redirectAttributes, "joaf.user.loginerror", "joaf.user.pleaseactivate", request.getContextPath() + "/confirmation/resend.html");
            return "redirect:/loginPage.html";
            //            model.addAttribute("error","Please activate your email");
        }
        ActivateForm command = new ActivateForm();
        model.addAttribute("command", command);
        return getView("activate");
    }

    @RequestMapping("/logout")
    public String logout(HttpServletRequest request, Authentication authentication) {
        userActivityEntryProducer.createLogout(request, authentication);
        return "redirect:/logoutTask.html";
    }

    @RequestMapping("/logoutPage")
    public String logoutPage(@RequestParam(value = "auth", required = false) String value, Model model, final RedirectAttributes redirectAttributes) {

        if (value != null) {
            model.addAttribute("error", "Login unsuccessful");
        }
        WebFlashUtil.addMessage(redirectAttributes, "joaf.user.successlogout", "joaf.user.successlogout.thankyou");
        return "redirect:/loginPage.html";
    }

    @RequestMapping("/register")
    public String registerPage(Model model) {
        model.addAttribute("registerForm", new RegisterForm());
        return getView("register");
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerTask(@Valid @ModelAttribute("registerForm") RegisterForm form, BindingResult result, HttpServletRequest request, Model model,
                               final RedirectAttributes redirectAttributes) {
        try {
            RegisterUserCommand registerUserCommand = new RegisterUserCommand(form);
            commandGateway.validate(result, registerUserCommand);
            if (result.hasErrors()) {
                //            model.addAllAttributes(result.getModel());
                return getView("register");
            }
            commandGateway.send(registerUserCommand, RegisterUserCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.user.registered.message.title", "joaf.user.registered.message.body");
            return "redirect:/loginPage.html";
        } catch (JoafException e) {
            WebFlashUtil.addMessage(redirectAttributes, "joaf.error.title", "joaf.error.unknown.body");
        }
        return getView("register");
    }

    @RequestMapping(value = "/requestResetPassword", method = {RequestMethod.GET})
    public String requestResetPassword(Model model, Principal principal) {
        RequestResetPassword command = new RequestResetPassword();
        command.setEmail("");
        model.addAttribute("command", command);
        return getView("resetPasswordRequest");
    }

    @RequestMapping(value = "/requestResetPassword", method = {RequestMethod.POST})
    public String requestResetPasswordPost(@Valid @ModelAttribute("command") RequestResetPassword command, HttpServletRequest request
            , BindingResult result, Model model, Authentication authentication, final RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return getView("resetPasswordRequest");
        } else {
            try {
                userService.sendResetPassword(command.getEmail());
                Optional<UserCard> userCard = userService.findOneUserDetails(command.getEmail());
                if (userCard.isPresent()) {
                    userActivityEntryProducer.createRegisterRequest(request, userCard.get());
                }
                WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.user.requestresetpassword.body");
            } catch (JoafDatabaseException | NoUserFoundException e) {
                e.printStackTrace();
            }
            return "redirect:/loginPage.html";
        }
    }

    @RequestMapping(value = "/resetPasswordCode", method = {RequestMethod.GET})
    public String resetPassword(@QueryParam("code") String code, @QueryParam("email") String email, Model model, Principal principal, HttpSession session) {
        RequestResetPassword command = new RequestResetPassword();
        command.setEmail("");
        model.addAttribute("command", command);
        if (StringUtils.trimToNull(code) != null) {
            try {
                if (userService.validateResetPasswordCode(email, code)) {
                    session.setAttribute("resetCode", code);
                    session.setAttribute("resetEmail", email);
                    return "redirect:/changePassword.html";
                }
            } catch (JoafDatabaseException e) {
                e.printStackTrace();
            }
        }
        return getView("resetPasswordCode");
    }

    @RequestMapping(value = "/resetPasswordCode", method = {RequestMethod.POST})
    public String resetPasswordPost(@ModelAttribute("command") RequestResetPassword command, BindingResult result, Model model, Principal principal,
                                    HttpSession session) {
        if (StringUtils.trimToNull(command.getCode()) == null) {
            result.addError(new FieldError("command", "code", "May not be empty"));
        }
        if (result.hasErrors()) {
            return getView("resetPasswordCode");
        } else {
            session.setAttribute("resetCode", command.getCode());
            session.setAttribute("resetEmail", command.getEmail());
            return "redirect:/changePassword.html";
        }
    }

    @RequestMapping(value = "/changePassword", method = {RequestMethod.GET})
    public String changePassword(Model model, Principal principal, HttpSession session) {
        String code = (String) session.getAttribute("resetCode");
        String email = (String) session.getAttribute("resetEmail");
        if (StringUtils.trimToNull(code) == null) {
            return "redirect:/500.html";
        }
        PasswordChangeForm command = new PasswordChangeForm();
        model.addAttribute("command", command);
        return getView("changePassword");
    }

    @RequestMapping(value = "/changePassword", method = {RequestMethod.POST})
    public String changePasswordPost(@Valid @ModelAttribute("command") PasswordChangeForm command
            , BindingResult result, Model model, Principal principal, HttpSession session, final RedirectAttributes redirectAttributes) {
        String code = (String) session.getAttribute("resetCode");
        String email = (String) session.getAttribute("resetEmail");
        if (result.hasErrors()) {
            return getView("changePassword");
        }
        try {
            command.setEmail(email);
            if (userService.resetChangePassword(command, email, code)) {
                session.removeAttribute("resetCode");
                session.removeAttribute("resetEmail");
                redirectAttributes.addFlashAttribute("joaf.message.notification.title", "joaf.user.resetpassword.success.body");
                return "redirect:/loginPage.html";
            } else {
                redirectAttributes.addFlashAttribute("joaf.message.notification.title", "joaf.error.unknown.body");
                return "redirect:/changePassword.html";
            }
        } catch (JoafException e) {
            e.printStackTrace();
        }
        return getView("changePassword");
    }

    private String getView(String name) {
        return templateLocator.locate(TemplateResources.builderForExtension("user").forView(name).build());
    }

}
