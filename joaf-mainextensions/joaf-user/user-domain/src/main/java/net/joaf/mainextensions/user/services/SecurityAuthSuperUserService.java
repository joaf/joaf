package net.joaf.mainextensions.user.services;

import net.joaf.mainextensions.user.exceptions.AccountRemovedException;
import net.joaf.mainextensions.user.exceptions.EmailNotActiveException;
import net.joaf.mainextensions.user.model.BasicUserDetails;
import net.joaf.mainextensions.user.model.UserCard;
import net.joaf.mainextensions.user.model.enums.EUserCardStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by cyprian on 11.01.15.
 */
@Service
public class SecurityAuthSuperUserService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserCard user = null;
        try {
            user = userService.findOneUserDetailsForSuperuser(username);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        //if removed
        if (user != null && EUserCardStatus.DELETED.equals(user.getStatus())) {
            throw new AccountRemovedException("Konto zostało usunięte");
        }
        //if not activated
        if (user != null && !user.getActive()) {
            throw new EmailNotActiveException("Email not active");
            //            return null;
        }
        if (user != null) {
            return new BasicUserDetails(user);
        } else {
            return null;
        }
    }
}