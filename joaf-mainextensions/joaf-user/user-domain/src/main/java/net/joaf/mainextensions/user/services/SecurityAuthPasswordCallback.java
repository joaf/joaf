/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.user.services;

import org.apache.ws.security.WSPasswordCallback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import java.io.IOException;

/**
 * Created by cyprian.sniegota on 17.02.14.
 */
@Component
public class SecurityAuthPasswordCallback implements CallbackHandler {

    @Autowired
    private SecurityAuthUserService securityAuthUserService;

    @Autowired
    @Qualifier("authenticationManager")
    private AuthenticationManager providerManager;

    @Override
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
        //        WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
        //
        //        if ("joe".equals(pc.getIdentifier())) {
        //            pc.setPassword("joespassword");
        //        }
        for (Callback callback : callbacks) {
            WSPasswordCallback wsPasswordCallback = (WSPasswordCallback) callback;
            UserDetails userDetails = securityAuthUserService.loadUserByUsername(wsPasswordCallback.getIdentifier());
            if (userDetails != null && userDetails.getPassword() != null) {
                String pass = userDetails.getPassword();
                if (pass != null) {
                    wsPasswordCallback.setPassword("qq");
                    //                    Authentication authentication  = providerManager.authenticate(new UsernamePasswordAuthenticationToken(wsPasswordCallback.getIdentifier(), wsPasswordCallback.getPassword()));
                    //                    SecurityContextHolder.getContext().setAuthentication(authentication);

                    return;
                }
            }
        }
    }
}
