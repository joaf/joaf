package net.joaf.mainextensions.user;

import net.joaf.base.extension.AbstractExtension;
import net.joaf.base.extension.JoafExtension;
import org.springframework.stereotype.Component;

/**
 * Created by cyprian on 01.02.15.
 */
@Component
public class UserExtension extends AbstractExtension implements JoafExtension {
    @Override
    public String getExtensionMetadataFile() {
        return "/extension-user.xml";
    }
}
