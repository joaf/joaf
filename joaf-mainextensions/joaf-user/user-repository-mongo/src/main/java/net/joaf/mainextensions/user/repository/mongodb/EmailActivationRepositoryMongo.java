/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.user.repository.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.mongo.MongoDBProvider;
import net.joaf.mainextensions.confirmation.model.EmailActivation;
import net.joaf.mainextensions.user.converters.EmailActivationConverter;
import net.joaf.mainextensions.user.repository.api.EmailActivationRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

/**
 * Created by cyprian on 01.01.14.
 */
//@Component
public class EmailActivationRepositoryMongo implements EmailActivationRepository {

    @Autowired
    private MongoDBProvider mongoDBProvider;

    private String collectionName = "emailActivation";

    protected DBCollection getCollection() throws JoafDatabaseException {
        return mongoDBProvider.getMongoDB().getCollection(this.collectionName);
    }

    public void store(EmailActivation obj) throws JoafDatabaseException {
        EmailActivationConverter emailActivationConverter = new EmailActivationConverter();
        getCollection().save(emailActivationConverter.dbObjectFromObject(obj));
    }

    @Override
    public void updateObjectState(String uid, EObjectState newState) throws JoafDatabaseException {

    }

    @Override
    public List<EmailActivation> findAll() throws JoafDatabaseException {
        return null;
    }

    @Override
    public EmailActivation findOne(String uid) throws JoafDatabaseException {
        return null;
    }

    @Override
    public Optional<EmailActivation> findOneOptional(String uid) throws JoafDatabaseException {
        return null;
    }

    @Override
    public String collectionName() {
        return null;
    }

    @Override
    public Class<EmailActivation> getEntityClass() {
        return null;
    }

    @Override
    public void remove(String uid) throws JoafDatabaseException {

    }

    @Override
    public void insert(EmailActivation obj) throws JoafDatabaseException {
        this.store(obj);
    }

    public EmailActivation findByEmailAndCode(String email, String code) throws JoafDatabaseException {
        DBCollection collection = this.getCollection();
        DBObject one = collection.findOne(new BasicDBObject("email", email).append("activationCode", code));
        if (one != null) {
            EmailActivationConverter emailActivationConverter = new EmailActivationConverter();
            return emailActivationConverter.objectFromDBObject(one, EmailActivation.class);
        } else {
            return null;
        }
    }

    @Override
    public String prepareId() {
        return null;
    }
}
