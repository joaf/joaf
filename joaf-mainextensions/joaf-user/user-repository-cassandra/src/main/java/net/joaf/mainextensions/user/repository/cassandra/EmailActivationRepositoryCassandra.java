/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.user.repository.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.cassandra.AbstractCrudRepositoryCassandra;
import net.joaf.base.db.cassandra.CassandraSessionProvider;
import net.joaf.mainextensions.confirmation.model.EmailActivation;
import net.joaf.mainextensions.user.repository.api.EmailActivationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public class EmailActivationRepositoryCassandra extends AbstractCrudRepositoryCassandra<EmailActivation> implements EmailActivationRepository {

    @Autowired
    private CassandraSessionProvider sessionProvider;

    @Override
    public void store(EmailActivation obj) throws JoafDatabaseException {
        Session session = sessionProvider.connect();

        String updateQuery = "UPDATE emailActivation SET email = ?, code = ?, originalAccountEmail = ? WHERE uid = ?;";
        UUID uuid = UUID.fromString(obj.getUid());
        session.execute(updateQuery, obj.getEmail(), obj.getCode(), obj.getOriginalAccountEmail(), uuid);
    }

    @Override
    public String collectionName() {
        return "emailactivation";
    }

    @Override
    public Class<EmailActivation> getEntityClass() {
        return EmailActivation.class;
    }

    @Override
    public void insert(EmailActivation obj) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        String insertQuery = "INSERT INTO emailActivation (uid, email, code, originalAccountEmail) "
                + "VALUES (?, ?, ?, ?);";
        session.execute(insertQuery,
                UUID.fromString(obj.getUid()), obj.getEmail(), obj.getCode(), obj.getOriginalAccountEmail());
    }

    @Override
    public EmailActivation findByEmailAndCode(String email, String code) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        ResultSet execute = session.execute("select * from emailActivation where email = ? AND code = ? ALLOW FILTERING", email, code);
        List<Row> all = execute.all();
        if (all.size() == 1) {
            return this.bindRow(all.get(0));
        } else {
            return null;
        }
    }

    protected EmailActivation bindRow(Row row) {
        EmailActivation obj = new EmailActivation();
        obj.setEmail(row.getString("email"));
        obj.setUid(row.getUUID("uid").toString());
        obj.setCode(row.getString("code"));
        obj.setOriginalAccountEmail(row.getString("originalAccountEmail"));
        return obj;
    }

    @Override
    public String prepareId() {
        return UUID.randomUUID().toString();
    }
}
