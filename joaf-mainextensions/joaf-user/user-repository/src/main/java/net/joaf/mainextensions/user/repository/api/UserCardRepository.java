/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.user.repository.api;

import net.joaf.base.core.db.CrudRepository;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.user.model.ERequestResetPasswordState;
import net.joaf.mainextensions.user.model.RequestResetPassword;
import net.joaf.mainextensions.user.model.UserCard;

import java.util.List;
import java.util.Optional;

/**
 * Created by cyprian.sniegota on 28.02.14.
 */
public interface UserCardRepository extends CrudRepository<UserCard> {

    Optional<UserCard> findByUsername(String username) throws JoafDatabaseException;

    UserCard findByUid(String id) throws JoafDatabaseException;

    void store(UserCard obj) throws JoafDatabaseException;

    void updatePassword(String uid, String password) throws JoafDatabaseException;

    void storeRequestResetPassword(RequestResetPassword requestResetPassword) throws JoafDatabaseException;

    RequestResetPassword getRequestResetPassword(String uid, ERequestResetPasswordState state) throws JoafDatabaseException;

    List<UserCard> findAll() throws JoafDatabaseException;

    void updateActivation(String uid, boolean active) throws JoafDatabaseException;

    List<UserCard> findInactive();

    void deprecate(UserCard userCard);

    void remove(String uid);
}
