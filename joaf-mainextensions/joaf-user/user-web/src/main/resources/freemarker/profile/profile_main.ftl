<div class="awidget full-width">
    <div class="row">
        <div class="col-md-10">
            <div class="awidget-head">
                <h3><@spring.message 'joaf.user.profile.main' /> <a class=""
                                                                    href="${rc.contextPath}/profile/edit.html"><@spring.message 'joaf.edit' /></a>
                </h3>
            </div>
            <div class="awidget-body">
                <div class="form-group row">
                    <label class="col-lg-3 control-label"><@spring.message 'joaf.user.firstname' /></label>

                    <div class="col-lg-8">
                        <p class="form-control-static">${mainprofile.firstName}</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 control-label"><@spring.message 'joaf.user.lastname' /></label>

                    <div class="col-lg-8">
                        <p class="form-control-static">${mainprofile.lastName}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <i class="fa fa-info"></i>
        <@spring.message 'joaf.user.profile.main.info' />
        </div>
    </div>
</div>

<div class="awidget full-width">
    <div class="row">
        <div class="col-md-10">
            <div class="awidget-head">
                <h3><@spring.message 'joaf.email.label' /> <a class=""
                                                              href="${rc.contextPath}/profile/changeEmail.html"><@spring.message 'joaf.edit' /></a>
                </h3>
            </div>
            <div class="awidget-body">
                <div class="form-group row">
                    <label class="col-lg-3 control-label"><@spring.message 'joaf.email.label' /></label>

                    <div class="col-lg-8">
                        <p class="form-control-static">${mainprofile.email}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <i class="fa fa-info"></i>
        <@spring.message 'joaf.user.profile.main.email.info' />
        </div>
    </div>
</div>

<div class="awidget full-width">
    <div class="row">
        <div class="col-md-10">
            <div class="awidget-head">
                <h3><@spring.message 'joaf.password.label' /> <a class=""
                                                                 href="${rc.contextPath}/profile/changePassword.html"><@spring.message 'joaf.edit' /></a>
                </h3>
            </div>
            <div class="awidget-body">
                <div class="form-group row">
                    <label class="col-lg-3 control-label"><@spring.message 'joaf.password.label' /></label>

                    <div class="col-lg-8">
                        <p class="form-control-static">* * * * * *</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <i class="fa fa-info"></i>
        <@spring.message 'joaf.user.profile.main.password.info' />
        </div>
    </div>
</div>