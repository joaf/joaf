<div class="awidget full-width">
    <div class="row">
        <div class="col-md-10">
            <div class="awidget-head">
                <h3><@spring.message 'joaf.user.profile.account' /></h3>
            </div>
            <div class="awidget-body">
                <div class="form-group row">
                    <label class="col-lg-3 control-label"><@spring.message 'joaf.user.profile.account.type.label' /></label>

                    <div class="col-lg-8">
                        <p class="form-control-static"><@spring.message 'joaf.user.profile.account.type.basic' /></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <i class="fa fa-info"></i>
        <@spring.message 'joaf.user.profile.account.info' />
        </div>
    </div>
</div>

<div class="awidget full-width">
    <div class="row">
        <div class="col-md-10">
            <div class="awidget-head">
                <h3><@spring.message 'joaf.user.profile.account.remove.header' /></h3>
            </div>
            <div class="awidget-body">
                <div class="form-group row">
                    <div class="col-lg-8">
                        <a class="btn btn-warning" data-toggle="modal"
                           href="#removeAccountModal"><@spring.message 'joaf.user.profile.removeMyAccount' /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <i class="fa fa-info"></i>
        <@spring.message 'joaf.user.profile.account.remove.info' />
        </div>
    </div>
</div>

<#--modal usuwania konta-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal fade"
     id="removeAccountModal" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                <h4 class="modal-title"><@spring.message 'joaf.user.profile.account.remove.header' /></h4>
            </div>
            <div class="modal-body">
                <p><@spring.message 'joaf.user.profile.removeMyAccount.question' /></p>
            </div>
            <div class="modal-footer">
                <button aria-hidden="true" data-dismiss="modal" class="btn btn-default"
                        type="button"><@spring.message 'joaf.close' /></button>
                <a href="${rc.contextPath}/profile/removeAccount.html" class="btn btn-danger"
                   type="button"><@spring.message 'joaf.user.profile.removeMyAccount' /></a>
            </div>
        </div>
    </div>
</div>