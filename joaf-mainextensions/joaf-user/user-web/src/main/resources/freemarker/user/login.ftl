<#import "/templates/login.ftl" as page />
<@page.layout "xxx">
<div class="awidget-head">

</div>
<div class="awidget-body">
    <!-- Page title -->
    <div class="page-title text-center">
        <h2><@spring.message 'joaf.user.login.header' /></h2>
        <hr/>
    </div>
    <!-- Page title -->
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" action="${rc.contextPath}/login" method="post">
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="username"><@spring.message 'joaf.email.label' /></label>

                    <div class="col-lg-9">
                        <input name="username" id="username" type="text" class="form-control"
                               placeholder="<@spring.message 'joaf.email.label' />"> <!-- type="email" -->
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label"
                           for="password"><@spring.message 'joaf.password.label' /></label>

                    <div class="col-lg-9">
                        <input name="password" id="password" type="password" class="form-control"
                               placeholder="<@spring.message 'joaf.password.label' />">
                    </div>
                </div>
                <hr/>
                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-10">
                        <button type="submit" class="btn btn-primary"><@spring.message 'joaf.user.signin' /></button>
                    <#--<button type="reset" class="btn btn-default">Reset</button>-->
                        <a href="${rc.contextPath}/register.html"
                           class="btn btn-success"><@spring.message 'joaf.user.register' /></a>
                        <a href="${rc.contextPath}/requestResetPassword.html"
                           class="btn btn-default"><@spring.message 'joaf.user.resetpassword' /></a>
                    </div>
                </div>
            </form>
        </div>
    </div>
<#--
<br />
<form class="form-horizontal" role="form" action="${rc.contextPath}/openidlogin" method="post">
    <div class="form-group">
        <label for="openid_identifier" class="col-lg-2 control-label">Openid</label>
        <div class="col-lg-10">
            <input name="openid_identifier" type="text" class="form-control" id="inputEmail1" placeholder="Email"> <!-- type="email" -- >
            </div>
        </div>
        <hr />
        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <button type="submit" class="btn btn-info">Sign in</button>
                <button type="reset" class="btn btn-default">Reset</button>
                <a href="${rc.contextPath}/register.html" class="btn btn-success">Register</a>
            </div>
        </div>
    </form>
    -->
</div>
</@page.layout>