<#import "/templates/base.ftl" as page />
<#import "/freemarker/joaf.ftl" as joaf />
<@page.layout 'net.joaf.mainextensions.menu.modulename'>
<div class="awidget full-width">
<#--    <div class="awidget-head">
    </div> -->
    <div class="awidget-body">
        <form id="invoice-form" class="form-compact" role="form" method="post"
              action="${rc.contextPath}/administration/user/edit/${command.uid}.html">
            <@joaf.formInput 'command.username' 'class="form-control"' 'text' 'username' 'username'/>

            <div class="row form-group">
                <label class="col-lg-2 control-label">Role</label>

                <div class="col-lg-10">
                    <@spring.formMultiSelect 'command.roles' roleOptions '' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>
        <#-- commands -->
            <div class="row form-group">
                <hr/>
                <div class="col-lg-offset-2 col-lg-10">

                    <@spring.formHiddenInput 'command.uid' '' />
                    <button type="submit"
                            class="btn btn-primary"><@spring.messageText 'joaf.save' 'joaf.save'/></button>
                    <button type="button"
                            onclick="this.form.action = '${rc.contextPath}/administration/user/cancel/${command.uid}';this.form.submit();return false;"
                            class="btn btn-default"><@spring.messageText 'joaf.cancel' 'joaf.cancel'/></button>
                </div>
            </div>
        </form>
    </div>
</div>
</@page.layout>