<ul class="nav navbar-nav navbar-right">
    <li>
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                class="fa fa-user"></i> ${principal.username}<#--<@spring.message 'joaf.main.setting' />--> <b
                class="caret"></b></a>
        <ul class="dropdown-menu animated fadeInUp">
        <#--<li><a href="${rc.contextPath}/settings.html"><@spring.message 'joaf.main.configuration' /></a></li>-->
            <li><a href="${rc.contextPath}/profile.html"><@spring.message 'joaf.user.profile' /></a></li>
            <li><a href="${rc.contextPath}/logout.html"><@spring.message 'joaf.user.logout' /></a></li>
        </ul>
    </li>
</ul>

