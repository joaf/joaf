<#import "/templates/base.ftl" as page />
<@page.layout "net.joaf.mainextensions.user.modulename">
<div class="awidget full-width">
    <div class="awidget-head">
        <div class="btn-group">
        </div>
        <div class="btn-group">
            <a href="${rc.contextPath}/administration/user/fixnullvalues" class="btn btn-xs"
               data-toggle="tooltip" data-placement="bottom"
               title=""><i class="fa fa-play"></i></a>
        ${action_buttons!}
        </div>
    </div>
    <div class="awidget-body">
        <table class="table admin-media table-hover">
            <thead>
            <tr>
                <th style="width: 10px;">
                    <input type='checkbox' value='check1'/>
                </th>
                <th><@spring.message 'joaf.user.myaccount' /></th>
                <th></th>
            </tr>
            </thead>
            <tbody>

                <#list elements as element>
                <tr>
                    <td>
                        <input type='checkbox' value='check1'/>
                    </td>
                    <td>${element.data.username}</td>
                    <td>
                        <div class="hidden-table-cell">
                            <#list element.webActions as action>
                                <a href="${rc.contextPath}${action.actionUrl}" class="btn btn-xs ${action.buttonStyle}"
                                   data-toggle="tooltip" data-placement="bottom"
                                   title="<@spring.message action.tooltip />"><i class="fa ${action.iconName}"></i></a>
                            </#list>
                        </div>
                    </td>
                </tr>
                </#list>
            </tbody>
        </table>
    </div>

</div>
</@page.layout>