<#import "/templates/login.ftl" as page />
<@page.layout "xxx">
<#--<@spring.bind "registerForm" />-->
<div class="awidget-head">
<#--${registerForm.email}-->
    <#--<@spring.showErrors "*", "errors" />-->
</div>
<div class="awidget-body">
    <!-- Page title -->
    <div class="page-title text-center">
        <h2><@spring.message 'joaf.user.changemypassword' /></h2>
        <hr/>
    </div>
    <!-- Page title -->
    <br/>

    <form class="form-horizontal" role="form" action="${rc.contextPath}/changePassword.html" method="post">
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-4 control-label"><@spring.message 'joaf.password.label' /></label>

            <div class="col-lg-8">
                <#assign placeholder = springMacroRequestContext.getMessage('joaf.password.label') />
                <@spring.formPasswordInput 'command.password[0]' 'placeholder="${placeholder}" class="form-control"' />
                <@spring.bind 'command.password'/>
                <#if spring.status.error>
                    <p><@spring.showErrors "<br>", "color:red" /></p>
                </#if>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-4 control-label"><@spring.message 'joaf.retype.password.label' /></label>

            <div class="col-lg-8">
                <#assign placeholder = springMacroRequestContext.getMessage('joaf.retype.password.label') />
                <@spring.formPasswordInput 'command.password[1]' 'placeholder="${placeholder}" class="form-control"' />
                <#if spring.status.error>
                    <p><@spring.showErrors "<br>", "color:red" /></p>
                </#if>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <button type="submit" class="btn btn-info"><@spring.message 'joaf.user.changemypassword' /></button>
                <a href="${rc.contextPath}/loginPage.html"
                   class="btn btn-success"><@spring.message 'joaf.user.signin' /></a>
            </div>
        </div>
    </form>
</div>
</@page.layout>