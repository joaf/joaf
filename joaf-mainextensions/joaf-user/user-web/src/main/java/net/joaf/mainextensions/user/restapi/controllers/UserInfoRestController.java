package net.joaf.mainextensions.user.restapi.controllers;

import net.joaf.mainextensions.user.model.BasicUserDetails;
import net.joaf.mainextensions.user.model.UserCard;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;

/**
 * Created by cyprian on 14.05.15.
 */
@Controller
@RequestMapping("/api/1/user")
public class UserInfoRestController {

    @RequestMapping("/info")
    public
    @ResponseBody
    UserCard info(Principal principal) {
        System.out.println(principal);
        BasicUserDetails principal1 = (BasicUserDetails) ((OAuth2Authentication) principal).getPrincipal();
        return principal1.getUser();
    }
}
