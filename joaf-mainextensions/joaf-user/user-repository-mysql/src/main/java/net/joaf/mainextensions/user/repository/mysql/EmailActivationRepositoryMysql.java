package net.joaf.mainextensions.user.repository.mysql;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.mainextensions.confirmation.model.EmailActivation;
import net.joaf.mainextensions.user.repository.api.EmailActivationRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by cyprian on 31.01.15.
 */
@Repository
public class EmailActivationRepositoryMysql extends AbstractRepositoryMysql<EmailActivation> implements EmailActivationRepository {
    @Override
    public void store(EmailActivation obj) throws JoafDatabaseException {

    }

    @Override
    protected String getTableName() {
        return null;
    }

    @Override
    protected JdbcTemplate getJdbcTemplate() {
        return null;
    }

    @Override
    protected EmailActivation mapRow(ResultSet rs, int rowNum) throws SQLException {
        return null;
    }

    @Override
    public List<EmailActivation> findAll() throws JoafDatabaseException {
        return null;
    }

    @Override
    public EmailActivation findOne(String uid) throws JoafDatabaseException {
        return null;
    }

    @Override
    public String collectionName() {
        return null;
    }

    @Override
    public Class<EmailActivation> getEntityClass() {
        return null;
    }

    @Override
    public void remove(String uid) throws JoafDatabaseException {

    }

    @Override
    public void insert(EmailActivation obj) throws JoafDatabaseException {

    }

    @Override
    public EmailActivation findByEmailAndCode(String email, String code) throws JoafDatabaseException {
        return null;
    }

    @Override
    public String prepareId() {
        return null;
    }
}
