/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.user.model.dto;

import net.joaf.mainextensions.user.validators.PasswordsMatch;
import net.joaf.mainextensions.user.validators.PasswordsNotEmpty;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created with IntelliJ IDEA.
 * User: Cyprian.Sniegota
 * Date: 18.10.13
 * Time: 10:58
 * To change this template use File | Settings | File Templates.
 */

public class RegisterForm {

    @NotEmpty
    @Email
    private String email = "";

    @PasswordsMatch
    @PasswordsNotEmpty
    private String[] password = {"", ""};

    @NotEmpty(message = "{joaf.user.error.regulationscheck}")
    private String acceptRegulations;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String[] getPassword() {
        return password;
    }

    public void setPassword(String[] password) {
        this.password = password;
    }

    public String getAcceptRegulations() {
        return acceptRegulations;
    }

    public void setAcceptRegulations(String acceptRegulations) {
        this.acceptRegulations = acceptRegulations;
    }
}
