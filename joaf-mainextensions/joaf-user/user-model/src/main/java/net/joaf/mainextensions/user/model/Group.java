/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.user.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by cyprian.sniegota on 03.03.14.
 */
public class Group implements Serializable {
    private static final long serialVersionUID = -927905026876474874L;

    private String id;
    private String name;
    private String description;
    private Set<Role> roles = new HashSet<Role>();

    public String getId() {
        // Automatically generated method. Please do not modify this code.
        return this.id;
    }

    public void setId(String value) {
        // Automatically generated method. Please do not modify this code.
        this.id = value;
    }

    public String getName() {
        // Automatically generated method. Please do not modify this code.
        return this.name;
    }

    public void setName(String value) {
        // Automatically generated method. Please do not modify this code.
        this.name = value;
    }

    public String getDescription() {
        // Automatically generated method. Please do not modify this code.
        return this.description;
    }

    public void setDescription(String value) {
        // Automatically generated method. Please do not modify this code.
        this.description = value;
    }

    public Set<Role> getRoles() {
        // Automatically generated method. Please do not modify this code.
        return this.roles;
    }

    public void setRoles(Set<Role> value) {
        // Automatically generated method. Please do not modify this code.
        this.roles = value;
    }
}
