/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.user.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import net.joaf.base.core.db.model.StringUidEntity;
import net.joaf.mainextensions.user.model.dto.EUserCardAdminAction;
import net.joaf.mainextensions.user.model.enums.EUserCardStatus;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by cyprian.sniegota on 03.03.14.
 */
public class UserCard implements Serializable, StringUidEntity {

    private static final long serialVersionUID = 2640120586603955616L;
    private String uid;

    private String username;

    private String email;

    private String lastName;

    private String firstName;

    private EUserCardStatus status;

    //        private EmailActivation emailActivation = new EmailActivation();
    //
    //        public EmailActivation getEmailActivation() {
    //            return emailActivation;
    //        }
    //
    //        public void setEmailActivation(EmailActivation emailActivation) {
    //            this.emailActivation = emailActivation;
    private Boolean active = true;

    private Set<String> effectiveRoles = new HashSet<>();

    private Set<String> roles = new HashSet<>();

    private Set<Group> groups = new HashSet<>();

    private String password;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime registered;

    private Boolean deleted = false;
    //transient
    private List<EUserCardAdminAction> adminActions = new LinkedList<>();

    public String getUid() {

        return this.uid;
    }

    public void setUid(String value) {

        this.uid = value;
    }

    public String getUsername() {

        return this.username;
    }

    public void setUsername(String value) {

        this.username = value;
    }

    public String getPassword() {

        return this.password;
    }

    public void setPassword(String value) {

        this.password = value;
    }

    public String getEmail() {

        return this.email;
    }

    public void setEmail(String value) {

        this.email = value;
    }

    public String getFirstName() {

        return this.firstName;
    }

    public void setFirstName(String value) {

        this.firstName = value;
    }

    public String getLastName() {

        return this.lastName;
    }

    public void setLastName(String value) {

        this.lastName = value;
    }

    public Set<String> getRoles() {

        return this.roles;
    }

    public void setRoles(Set<String> value) {

        this.roles = value;
    }

    public Set<String> getEffectiveRoles() {

        return this.effectiveRoles;
    }

    public void setEffectiveRoles(Set<String> value) {

        this.effectiveRoles = value;
    }

    public Set<Group> getGroups() {

        return this.groups;
    }

    public void setGroups(Set<Group> value) {

        this.groups = value;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<EUserCardAdminAction> getAdminActions() {
        return adminActions;
    }

    public void setAdminActions(List<EUserCardAdminAction> adminActions) {
        this.adminActions = adminActions;
    }

    public LocalDateTime getRegistered() {
        return registered;
    }

    public void setRegistered(LocalDateTime registered) {
        this.registered = registered;
    }

    public EUserCardStatus getStatus() {
        return status;
    }

    public void setStatus(EUserCardStatus status) {
        this.status = status;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
