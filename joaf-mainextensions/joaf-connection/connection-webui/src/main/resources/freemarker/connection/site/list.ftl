<#import "/templates/base.ftl" as page />
<@page.layout "net.joaf.mainextensions.connection.modulename">


<div class="awidget full-width">
    <div class="panel">
        <div class="btn-group">
        ${action_buttons!}
        </div>
        <div class="pull-right">
            <a href="${rc.contextPath}${backPath}" class="btn btn-primary"><i
                    class="fa fa-gear fa-fw"></i> <@spring.message 'joaf.back' /></a>
        </div>
        <#list elements as element>
            <#list element.webActions as action>
                <a href="${rc.contextPath}${action.actionUrl}" class="btn btn-xs ${action.buttonStyle}"
                   data-toggle="tooltip" data-placement="bottom"
                   title="<@spring.message action.tooltip />"><span aria-hidden="true"
                                                                    class="glyphicon ${action.iconName}"></span>
                ${element.data.name}
                </a><br/>
            </#list>
        </#list>
    </div>

</div>
</@page.layout>