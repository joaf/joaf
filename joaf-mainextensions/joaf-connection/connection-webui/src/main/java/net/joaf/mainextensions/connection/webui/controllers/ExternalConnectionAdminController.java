package net.joaf.mainextensions.connection.webui.controllers;

import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.cqrs.PageredResultExtended;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.session.SessionContext;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.base.core.web.model.WebActionDetails;
import net.joaf.base.core.web.model.WrapperWDTO;
import net.joaf.mainextensions.connection.model.ConnectionDefinition;
import net.joaf.mainextensions.connection.queries.ConnectionDefinitionQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Collections;
import java.util.Optional;

/**
 * Created by cyprian on 03.04.15.
 */
@Controller
@RequestMapping("/administration/connection")
public class ExternalConnectionAdminController {

    private static final Logger log = LoggerFactory.getLogger(ExternalConnectionAdminController.class);
    @Autowired
    private ConnectionDefinitionQuery connectionDefinitionQuery;
    @Autowired
    private CommandGateway commandGateway;

    @RequestMapping("")
    public String list(HttpSession session, Model model, Principal principal) {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        try {
            PageredRequest pageredRequest = new PageredRequest( EObjectState.ACCEPTED);
            PageredResult<ConnectionDefinition> all = connectionDefinitionQuery.findAll(pageredRequest);
            PageredResultExtended<ConnectionDefinition> allExt = new PageredResultExtended<>(all, this::createActions);
            model.addAttribute("elements", allExt.getElements());
        } catch (Exception e) {
            model.addAttribute("elements", Collections.emptyList());
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            log.debug("error", e);
        }
        updateModelWithPaths(model);
        return getViewitem("/list");
    }

    private void createActions(WrapperWDTO<ConnectionDefinition> element) {
        WebActionDetails webActionDetails = new WebActionDetails();
        webActionDetails.setButtonStyle("btn-primary");
        webActionDetails.setActionUrl(getControllerContextPath() + element.getData().getUrl());
        webActionDetails.setIconName("glyphicon-list");
        webActionDetails.setTooltip("joaf.edit");
        element.getWebActions().add(webActionDetails);
    }

    protected void updateModelWithPaths(Model model) {
        model.addAttribute("backPath", getBackPath());
        model.addAttribute("controllerContextPath", getControllerContextPath());
    }

    protected String getBackPath() {
        return "/";
    }

    protected String getControllerContextPath() {
        return "/administration/connection";
    }

    protected String getViewitem(String name) {
        return "/freemarker/connection/admin" + name;
    }

    protected String getRedirect(String name) {
        return "redirect:" + getUrl(name);
    }

    protected String getUrl(String name) {
        return "/administration/connection" + name + ".html";
    }
}
