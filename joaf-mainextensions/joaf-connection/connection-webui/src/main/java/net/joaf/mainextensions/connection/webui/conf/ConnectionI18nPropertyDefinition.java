package net.joaf.mainextensions.connection.webui.conf;

import net.joaf.base.language.utils.I18nPropertyDefinition;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * Created by cyprian on 15.03.15.
 */
@Component
public class ConnectionI18nPropertyDefinition implements I18nPropertyDefinition {
    @Override
    public List<String> propertyFiles() {
        return Collections.singletonList("classpath:i18n/connection");
    }
}
