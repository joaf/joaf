package net.joaf.mainextensions.connection.webui.controllers;

import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.cqrs.PageredResultExtended;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafNoSubjectContextException;
import net.joaf.base.core.session.SessionContext;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.base.core.web.model.WrapperWDTO;
import net.joaf.mainextensions.connection.commands.*;
import net.joaf.mainextensions.connection.commands.handlers.*;
import net.joaf.mainextensions.connection.model.ConnectionDefinition;
import net.joaf.mainextensions.connection.queries.ConnectionDefinitionQuery;
import net.joaf.mainextensions.connection.webui.helpers.ConnectionDefinitionActionHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Collections;
import java.util.Optional;

/**
 * Created by cyprian on 03.04.15.
 */
@Controller
@RequestMapping("/administration/connection/definition")
public class ConnectionDefinitionAdminController {


    private static final Logger log = LoggerFactory.getLogger(ConnectionDefinitionAdminController.class);
    @Autowired
    private ConnectionDefinitionQuery query;
    @Autowired
    private CommandGateway commandGateway;

    @RequestMapping
    public String list(HttpSession session, Model model, Principal principal) {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        try {
            PageredRequest pageredRequest = new PageredRequest( EObjectState.ACCEPTED);
            PageredResult<ConnectionDefinition> all = query.findAll(pageredRequest);
            PageredResultExtended<ConnectionDefinition> allExt = new PageredResultExtended<>(all, this::createActions);
            model.addAttribute("elements", allExt.getElements());
        } catch (Exception e) {
            model.addAttribute("elements", Collections.emptyList());
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            log.debug("error", e);
        }
        updateModelWithPaths(model);
        return getViewitem("/list");
    }

    private void createActions(WrapperWDTO<ConnectionDefinition> element) {
        element.getData().getActions().forEach(action -> element.getWebActions().add(ConnectionDefinitionActionHelper.createActionDetails(action, element.getData().getUid(), getControllerContextPath())));
    }

    @RequestMapping("/create")
    public String create(HttpSession session, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        try {
            ConnectionDefinition command = (ConnectionDefinition) commandGateway
                    .send(new CreateConnectionDefinitionCommand(principal.getName()), CreateConnectionDefinitionCommandHandler.class);
            return getRedirect("/edit/" + command.getUid());
        } catch (Exception e) {
            log.debug("error", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return getRedirect("");
        }
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable("id") String id, HttpSession session, RedirectAttributes redirectAttributes, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        try {
            ConnectionDefinition command = query.findOne(id);
            model.addAttribute("command", command);
            updateModelWithPaths(model);
            return getViewitem("/edit");
        } catch (Exception e) {
            log.debug("error", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return getRedirect("");
        }
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String saveItem(@Valid @ModelAttribute("command") ConnectionDefinition command, @PathVariable("id") String id, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        try {
            commandGateway.send(new SaveConnectionDefinitionCommand(command, principal.getName()), SaveConnectionDefinitionCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successsave");
            return getRedirect("");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            updateModelWithPaths(model);
            return getViewitem("/edit");
        }
    }

    @RequestMapping("/trash/{uid}")
    public String trash(@PathVariable("uid") String uid, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        try {
            commandGateway.send(new TrashConnectionDefinitionCommand(uid, principal.getName()), TrashConnectionDefinitionCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successtrash", httpRequest.getContextPath() + getUrl("/undotrash/" + uid));
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    @RequestMapping("/undotrash/{id}")
    public String undotrash(@PathVariable("id") String id, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        try {
            commandGateway.send(new UndoTrashConnectionDefinitionCommand(id, principal.getName()), UndoTrashConnectionDefinitionCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successundotrash", "1");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    @RequestMapping(value = "/cancel/{uid}", method = RequestMethod.POST)
    public String cancel(@PathVariable("uid") String uid, HttpSession session, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        try {
            commandGateway.send(new CancelConnectionDefinitionCommand(uid, principal.getName()), CancelConnectionDefinitionCommandHandler.class);
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    protected String getSubjectCardDataUid(SessionContext sessionContext) {
        return sessionContext.getSubjectCardDataId();
    }

    protected void updateModelWithPaths(Model model) {
        model.addAttribute("backPath", getBackPath());
        model.addAttribute("controllerContextPath", getControllerContextPath());
    }

    protected String getBackPath() {
        return "/";
    }

    protected String getControllerContextPath() {
        return "/administration/connection/definition";
    }

    protected String getViewitem(String name) {
        return "/freemarker/connection/definition" + name;
    }

    protected String getRedirect(String name) {
        return "redirect:" + getUrl(name);
    }

    protected String getUrl(String name) {
        return "/administration/connection/definition" + name + ".html";
    }
}
