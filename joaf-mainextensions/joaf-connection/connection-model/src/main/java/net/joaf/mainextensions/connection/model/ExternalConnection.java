package net.joaf.mainextensions.connection.model;

import net.joaf.base.core.db.model.JoafSuidEntity;
import net.joaf.base.core.db.model.StringUidEntity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by cyprian on 03.04.15.
 */
public class ExternalConnection extends JoafSuidEntity implements Serializable, StringUidEntity {

    private String name;
    private String metadata;
    private String type;
    private Class<? extends ConnectionMetadata> metadataClass;
    //transient
    private ConnectionMetadata connectionMetadata;
    private Set<String> actions = new HashSet<>();

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public ConnectionMetadata getConnectionMetadata() {
        return connectionMetadata;
    }

    public void setConnectionMetadata(ConnectionMetadata connectionMetadata) {
        this.connectionMetadata = connectionMetadata;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class<? extends ConnectionMetadata> getMetadataClass() {
        return metadataClass;
    }

    public void setMetadataClass(Class<? extends ConnectionMetadata> metadataClass) {
        this.metadataClass = metadataClass;
    }

    public Set<String> getActions() {
        return actions;
    }
}
