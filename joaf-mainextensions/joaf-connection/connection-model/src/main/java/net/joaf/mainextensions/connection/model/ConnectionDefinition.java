package net.joaf.mainextensions.connection.model;

import net.joaf.base.core.db.model.JoafEntity;
import net.joaf.base.core.db.model.StringUidEntity;
import net.joaf.base.core.model.enums.EStandardAction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by cyprian on 03.04.15.
 */
public class ConnectionDefinition extends JoafEntity implements Serializable, StringUidEntity {
    private String name;
    private String type;
    private String beanName;
    private String url;
    private Set<String> viewRoles = new HashSet<>();
    private Set<String> editRoles = new HashSet<>();
    private List<EStandardAction> actions = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Set<String> getViewRoles() {
        return viewRoles;
    }

    public Set<String> getEditRoles() {
        return editRoles;
    }

    public List<EStandardAction> getActions() {
        return actions;
    }
}
