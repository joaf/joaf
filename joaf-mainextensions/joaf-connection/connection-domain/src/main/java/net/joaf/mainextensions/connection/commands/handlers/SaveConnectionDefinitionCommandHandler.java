package net.joaf.mainextensions.connection.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.connection.commands.SaveConnectionDefinitionCommand;
import net.joaf.mainextensions.connection.model.ConnectionDefinition;
import net.joaf.mainextensions.connection.repository.api.ConnectionDefinitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandHandlerComponent
public class SaveConnectionDefinitionCommandHandler implements CommandHandler<SaveConnectionDefinitionCommand, Object> {

    @Autowired
    private ConnectionDefinitionRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, SaveConnectionDefinitionCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(SaveConnectionDefinitionCommand command) throws JoafException {
        ConnectionDefinition dbObject = repository.findOne(command.getElement().getUid());
//        command.getElement().setUpdated(LocalDateTime.now());
        repository.store(command.getElement());
        if (EObjectState.NEW.equals(dbObject.getObjectState())) {
            repository.updateObjectState(command.getElement().getUid(), EObjectState.ACCEPTED);
        }
        return null;
    }
}
