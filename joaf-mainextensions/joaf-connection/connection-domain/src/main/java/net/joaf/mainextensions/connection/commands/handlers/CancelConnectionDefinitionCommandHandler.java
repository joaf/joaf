package net.joaf.mainextensions.connection.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.connection.commands.CancelConnectionDefinitionCommand;
import net.joaf.mainextensions.connection.model.ConnectionDefinition;
import net.joaf.mainextensions.connection.repository.api.ConnectionDefinitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandHandlerComponent
public class CancelConnectionDefinitionCommandHandler implements CommandHandler<CancelConnectionDefinitionCommand, Object> {

    @Autowired
    private ConnectionDefinitionRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, CancelConnectionDefinitionCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(CancelConnectionDefinitionCommand command) throws JoafException {
        ConnectionDefinition element = repository.findOne(command.getElementUid());
        if (EObjectState.NEW.equals(element.getObjectState())) {
            repository.remove(command.getElementUid());
        }
        return null;
    }
}
