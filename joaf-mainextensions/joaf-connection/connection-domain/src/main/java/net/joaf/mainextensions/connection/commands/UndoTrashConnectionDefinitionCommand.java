package net.joaf.mainextensions.connection.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.mainextensions.connection.commands.handlers.UndoTrashConnectionDefinitionCommandHandler;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandDto(handlerClass = UndoTrashConnectionDefinitionCommandHandler.class)
public class UndoTrashConnectionDefinitionCommand extends AbstractUidCommand implements Command {
    public UndoTrashConnectionDefinitionCommand(String elementUid, String userUid) {
        super(elementUid, userUid);
    }
}
