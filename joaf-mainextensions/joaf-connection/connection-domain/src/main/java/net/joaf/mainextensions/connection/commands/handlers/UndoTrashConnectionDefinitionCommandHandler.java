package net.joaf.mainextensions.connection.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.connection.commands.UndoTrashConnectionDefinitionCommand;
import net.joaf.mainextensions.connection.repository.api.ConnectionDefinitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandHandlerComponent
public class UndoTrashConnectionDefinitionCommandHandler implements CommandHandler<UndoTrashConnectionDefinitionCommand, Object> {

    @Autowired
    private ConnectionDefinitionRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, UndoTrashConnectionDefinitionCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(UndoTrashConnectionDefinitionCommand command) throws JoafException {
        repository.updateObjectState(command.getElementUid(), EObjectState.ACCEPTED);
        return null;
    }
}
