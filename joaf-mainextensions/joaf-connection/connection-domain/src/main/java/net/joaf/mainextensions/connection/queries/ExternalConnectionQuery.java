package net.joaf.mainextensions.connection.queries;

import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.connection.model.ExternalConnection;
import net.joaf.mainextensions.connection.repository.api.ExternalConnectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by cyprian on 03.04.15.
 */
@Component
public class ExternalConnectionQuery {

    @Autowired
    private ExternalConnectionRepository repository;

    public PageredResult<ExternalConnection> findForType(PageredRequest pageredRequest, String type) throws JoafDatabaseException {
        return repository.findAllByType(pageredRequest, type);
    }
}
