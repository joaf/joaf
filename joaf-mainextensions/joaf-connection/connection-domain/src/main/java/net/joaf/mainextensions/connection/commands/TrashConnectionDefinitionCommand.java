package net.joaf.mainextensions.connection.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.mainextensions.connection.commands.handlers.TrashConnectionDefinitionCommandHandler;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandDto(handlerClass = TrashConnectionDefinitionCommandHandler.class)
public class TrashConnectionDefinitionCommand extends AbstractUidCommand implements Command {
    public TrashConnectionDefinitionCommand(String elementUid, String userUid) {
        super(elementUid, userUid);
    }
}
