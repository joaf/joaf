package net.joaf.mainextensions.connection.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractEntityCommand;
import net.joaf.mainextensions.connection.commands.handlers.SaveConnectionDefinitionCommandHandler;
import net.joaf.mainextensions.connection.model.ConnectionDefinition;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandDto(handlerClass = SaveConnectionDefinitionCommandHandler.class)
public class SaveConnectionDefinitionCommand extends AbstractEntityCommand<ConnectionDefinition> implements Command {
    public SaveConnectionDefinitionCommand(ConnectionDefinition element, String userUid) {
        super(element, userUid);
    }
}
