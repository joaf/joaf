package net.joaf.mainextensions.connection.queries;

import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.connection.model.ConnectionDefinition;
import net.joaf.mainextensions.connection.repository.api.ConnectionDefinitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by cyprian on 03.04.15.
 */
@Component
public class ConnectionDefinitionQuery {

    @Autowired
    private ConnectionDefinitionRepository repository;


    public PageredResult<ConnectionDefinition> findAll(PageredRequest pageredRequest) throws JoafDatabaseException {
        return repository.findAll(pageredRequest);
    }

    public ConnectionDefinition findOne(String id) throws JoafDatabaseException {
        return repository.findOne(id);
    }
}
