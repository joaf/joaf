package net.joaf.mainextensions.connection.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.connection.commands.CreateConnectionDefinitionCommand;
import net.joaf.mainextensions.connection.model.ConnectionDefinition;
import net.joaf.mainextensions.connection.repository.api.ConnectionDefinitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandHandlerComponent
public class CreateConnectionDefinitionCommandHandler implements CommandHandler<CreateConnectionDefinitionCommand, Object> {

    @Autowired
    private ConnectionDefinitionRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, CreateConnectionDefinitionCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(CreateConnectionDefinitionCommand command) throws JoafException {
        ConnectionDefinition element = new ConnectionDefinition();
        element.setObjectState(EObjectState.NEW);
        element.setName("");
        element.setUid(repository.prepareId());
        repository.insert(element);
        return element;
    }
}
