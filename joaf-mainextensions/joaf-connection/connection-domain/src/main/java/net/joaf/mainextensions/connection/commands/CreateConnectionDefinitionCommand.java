package net.joaf.mainextensions.connection.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.mainextensions.connection.commands.handlers.CreateConnectionDefinitionCommandHandler;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandDto(handlerClass = CreateConnectionDefinitionCommandHandler.class)
public class CreateConnectionDefinitionCommand extends AbstractUidCommand implements Command {
    public CreateConnectionDefinitionCommand(String userUid) {
        super(null, userUid);
    }
}
