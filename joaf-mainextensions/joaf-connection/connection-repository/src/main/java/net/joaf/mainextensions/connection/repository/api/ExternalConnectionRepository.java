package net.joaf.mainextensions.connection.repository.api;

import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.db.CrudRepository;
import net.joaf.base.core.db.JoafRepository;
import net.joaf.base.core.db.NoSqlRepository;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.connection.model.ExternalConnection;

/**
 * Created by cyprian on 03.04.15.
 */
public interface ExternalConnectionRepository extends NoSqlRepository, JoafRepository, CrudRepository<ExternalConnection> {
    PageredResult<ExternalConnection> findAllByType(PageredRequest pageredRequest, String smtp) throws JoafDatabaseException;
}
