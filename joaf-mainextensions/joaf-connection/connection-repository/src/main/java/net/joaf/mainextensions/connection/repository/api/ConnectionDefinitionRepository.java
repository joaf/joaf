package net.joaf.mainextensions.connection.repository.api;

import net.joaf.base.core.db.CrudRepository;
import net.joaf.base.core.db.JoafRepository;
import net.joaf.base.core.db.NoSqlRepository;
import net.joaf.mainextensions.connection.model.ConnectionDefinition;

/**
 * Created by cyprian on 03.04.15.
 */
public interface ConnectionDefinitionRepository extends NoSqlRepository, JoafRepository, CrudRepository<ConnectionDefinition> {
}
