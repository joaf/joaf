package net.joaf.mainextensions.connection.repository.mysql;

import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.utils.DateUtils;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.mainextensions.connection.model.ConnectionDefinition;
import net.joaf.mainextensions.connection.repository.api.ConnectionDefinitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by cyprian on 03.04.15.
 */
@Repository
public class ConnectionDefinitionRepositoryMysql extends AbstractRepositoryMysql<ConnectionDefinition> implements ConnectionDefinitionRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    protected String getTableName() {
        return "connection_definition";
    }

    @Override
    protected JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    protected ConnectionDefinition mapRow(ResultSet rs, int rowNum) throws SQLException {
        return getRowMapper().mapRow(rs, rowNum);
    }

    protected RowMapper<ConnectionDefinition> getRowMapper() {
        return (rs, rowNum) -> {
            ConnectionDefinition element = new ConnectionDefinition();
            element.setUid(rs.getString("uid"));
            element.setCreated(DateUtils.dateToLocalDateTime(rs.getDate("created")));
            element.setUpdated(DateUtils.dateToLocalDateTime(rs.getDate("updated")));
            element.setObjectState(EObjectState.valueOf(rs.getString("objectState")));
            element.setName(rs.getString("name"));
            element.setType(rs.getString("type"));
            element.setUrl(rs.getString("url"));
            element.setBeanName(rs.getString("beanName"));
            return element;
        };
    }

    @Override
    public void insert(ConnectionDefinition element) throws JoafDatabaseException {

    }

    @Override
    public void store(ConnectionDefinition element) throws JoafDatabaseException {

    }

    @Override
    public Class<ConnectionDefinition> getEntityClass() {
        return ConnectionDefinition.class;
    }
}
