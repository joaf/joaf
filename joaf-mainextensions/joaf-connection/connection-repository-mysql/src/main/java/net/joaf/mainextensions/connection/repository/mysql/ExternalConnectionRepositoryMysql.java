package net.joaf.mainextensions.connection.repository.mysql;

import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.utils.DateUtils;
import net.joaf.base.core.utils.JsonUtils;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.mainextensions.connection.model.ConnectionMetadata;
import net.joaf.mainextensions.connection.model.ExternalConnection;
import net.joaf.mainextensions.connection.repository.api.ExternalConnectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by cyprian on 03.04.15.
 */
@Repository
public class ExternalConnectionRepositoryMysql extends AbstractRepositoryMysql<ExternalConnection> implements ExternalConnectionRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    protected String getTableName() {
        return "connection";
    }

    @Override
    protected JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    protected ExternalConnection mapRow(ResultSet rs, int rowNum) throws SQLException {
        return getRowMapper().mapRow(rs, rowNum);
    }

    protected RowMapper<ExternalConnection> getRowMapper() {
        return (rs, rowNum) -> {
            try {
                ExternalConnection element = new ExternalConnection();
                element.setUid(rs.getString("uid"));
                element.setCreated(DateUtils.dateToLocalDateTime(rs.getDate("created")));
                element.setUpdated(DateUtils.dateToLocalDateTime(rs.getDate("updated")));
                element.setObjectState(EObjectState.valueOf(rs.getString("objectState")));
                element.setName(rs.getString("name"));
                element.setType(rs.getString("type"));
                element.setSubjectUid(rs.getString("subjectUid"));
                element.setMetadataClass(Class.forName(rs.getString("metadataClass")).asSubclass(ConnectionMetadata.class));
                String metadata = rs.getString("metadata");
                element.setMetadata(metadata);
                Class<ConnectionMetadata> connectionMetadataClass = (Class<ConnectionMetadata>) element.getMetadataClass();
                element.setConnectionMetadata(JsonUtils.getPojo(metadata, connectionMetadataClass));
                return element;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        };
    }

    @Override
    public void insert(ExternalConnection element) throws JoafDatabaseException {
        try {
            String sql = "INSERT INTO " + getTableName()
                    + " (uid, created, updated, objectState, subjectUid, name, type, metadataClass, metadata) VALUES (?,?,?,?,?,?,?,?,?)";
            jdbcTemplate.update(sql, element.getUid(), DateUtils.localDateTimeToDate(element.getCreated()), DateUtils.localDateTimeToDate(element.getUpdated()), element.getObjectState().toString(),
                    element.getSubjectUid(), element.getName(), element.getType(), element.getMetadataClass().getName(), JsonUtils.stringValue(element.getConnectionMetadata())
            );

        } catch (Exception e) {
            throw new JoafDatabaseException("Repository error", e);
        }
    }

    @Override
    public void store(ExternalConnection element) throws JoafDatabaseException {
        try {
            String sql = "UPDATE " + getTableName()
                    + " SET updated=?, name=?, metadata=? where uid=?";
            jdbcTemplate.update(sql, DateUtils.localDateTimeToDate(element.getUpdated()), element.getName(), JsonUtils.stringValue(element.getConnectionMetadata()), element.getUid());
        } catch (Exception e) {
            throw new JoafDatabaseException("Repository error", e);
        }
    }

    @Override
    public Class<ExternalConnection> getEntityClass() {
        return ExternalConnection.class;
    }

    @Override
    public PageredResult<ExternalConnection> findAllByType(PageredRequest pageredRequest, String type) throws JoafDatabaseException {
        PageredResult<ExternalConnection> all = findAll(pageredRequest);
        List<ExternalConnection> collect = all.getResult().stream().filter(x -> x.getType().equals(type)).collect(Collectors.toList());
        all.getResult().clear();
        all.getResult().addAll(collect);
        return all;
    }
}
