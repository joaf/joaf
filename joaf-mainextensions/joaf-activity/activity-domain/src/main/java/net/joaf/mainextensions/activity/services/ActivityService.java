/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.activity.services;

import net.joaf.mainextensions.activity.model.ActivityEntry;
import net.joaf.mainextensions.activity.repository.api.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by cyprian on 19.05.14.
 */
@Service
public class ActivityService {

    @Autowired
    private ActivityRepository activityRepository;

    public void insert(ActivityEntry activityEntry) {
        if (activityEntry.getUid() == null) {
            activityEntry.setUid(activityRepository.prepareId());
        }
        activityRepository.insert(activityEntry);
    }

    public List<ActivityEntry> findForUser(String userUid, Date date) {
        return activityRepository.findForUser(userUid, date);
    }

    public List<ActivityEntry> findForSubject(String subjectUid, Date date) {
        return activityRepository.findForUser(subjectUid, date);
    }

    public List<ActivityEntry> findForObject(String objectUid, Date date) {
        return activityRepository.findForUser(objectUid, date);
    }

}
