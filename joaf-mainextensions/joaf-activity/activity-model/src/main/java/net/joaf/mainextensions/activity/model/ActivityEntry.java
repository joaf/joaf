/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.joaf.mainextensions.activity.model;

import net.joaf.base.core.db.model.StringUidEntity;
import net.joaf.mainextensions.activity.model.enums.ActivityType;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Activity entry.
 *
 * @author Cyprian Sniegota
 * @since 1.0
 */
public class ActivityEntry implements Serializable, StringUidEntity {

    private String uid;
    private String userUid;
    private String username;
    private String subjectUid;
    private String subjectShortName;
    private String objectUid;
    private String objectName;
    /**
     * i18n string.
     */
    private String bodyKey;
    /**
     * i18n string format params
     */
    private List<String> bodyParams;
    /**
     * ref to historyObject
     */
    private String historyObjectUid;
    private Date created;
    private ActivityType type = ActivityType.INFO;
    private String metadata;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUserUid() {
        return userUid;
    }

    public void setUserUid(String userUid) {
        this.userUid = userUid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSubjectUid() {
        return subjectUid;
    }

    public void setSubjectUid(String subjectUid) {
        this.subjectUid = subjectUid;
    }

    public String getSubjectShortName() {
        return subjectShortName;
    }

    public void setSubjectShortName(String subjectShortName) {
        this.subjectShortName = subjectShortName;
    }

    public String getObjectUid() {
        return objectUid;
    }

    public void setObjectUid(String objectUid) {
        this.objectUid = objectUid;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getBodyKey() {
        return bodyKey;
    }

    public void setBodyKey(String bodyKey) {
        this.bodyKey = bodyKey;
    }

    public List<String> getBodyParams() {
        return bodyParams;
    }

    public void setBodyParams(List<String> bodyParams) {
        this.bodyParams = bodyParams;
    }

    public String getHistoryObjectUid() {
        return historyObjectUid;
    }

    public void setHistoryObjectUid(String historyObjectUid) {
        this.historyObjectUid = historyObjectUid;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public ActivityType getType() {
        return type;
    }

    public void setType(ActivityType type) {
        this.type = type;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }
}
