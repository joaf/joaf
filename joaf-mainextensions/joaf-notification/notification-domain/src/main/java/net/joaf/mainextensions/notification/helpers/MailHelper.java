/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.mainextensions.notification.helpers;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.configuration.helpers.ConfigurationHelper;
import net.joaf.mainextensions.content.helpers.ContentHelper;
import net.joaf.mainextensions.content.model.Content;
import org.apache.camel.Exchange;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cyprian on 01.01.14.
 */
@Component
public class MailHelper {

    private static final Logger log = LoggerFactory.getLogger(MailHelper.class);

    @Produce //(uri = "direct:sendEmailMessage")
    private ProducerTemplate template;

    @Autowired
    private ConfigurationHelper configurationHelper;

    @Autowired
    private ContentHelper contentHelper;

    /**
     * Send email with template content from db
     *
     * @param model
     * @param contentAlias
     * @param destinationEmail
     * @return true if success, false if error
     */
    public boolean sendContentEmail(Map<String, String> model, String contentAlias, String destinationEmail) {
        log.debug("sending email [" + model + "]");

        //get configuration
        String emailFrom = "";
        String connectionUrl = "";
        String emailSendDisabled = "";
        String app_base_url = "";
        String admin_email = "";
        try {
            emailFrom = configurationHelper.getValueNotEmpty("core-lib", "email_from");
            connectionUrl = configurationHelper.getValueNotEmpty("core-lib", "email_connection_url");
            emailSendDisabled = configurationHelper.getValueNotEmpty("core-lib", "email_send_disabled");
            app_base_url = configurationHelper.getValueNotEmpty("core-lib", "app_url");
            admin_email = configurationHelper.getValueNotEmpty("core-lib", "admin_email");
        } catch (JoafException e) {
            log.error("Configuration for email empty", e);
            return false;
        }

        // get content
        Content emailContent = null;
        try {
            // copy to content model (do not change model object)
            Map<String, String> contentModel = new HashMap<>(model);
            contentModel.put("app_base_url", app_base_url);
            contentModel.put("admin_email", admin_email);
            emailContent = contentHelper.getContentReplaced(contentModel, "core-lib", contentAlias, "main");
        } catch (JoafDatabaseException e) {
            log.error("Content error", e);
            return false;
        }
        if (emailContent == null) {
            log.error("emailContent is null");
            return false;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("To", destinationEmail);
        map.put("From", emailFrom);
        map.put("Subject", emailContent.getTitle());
        map.put(Exchange.CONTENT_TYPE, "text/plain; charset=UTF-8");
        map.put(Exchange.CHARSET_NAME, "UTF-8");
        String body = emailContent.getBody();

        try {
            if (!emailSendDisabled.equals("1")) {
                this.sendBodyAndHeaders(connectionUrl, body, map);
            } else {
                log.info("Disabled sending: " + body);
            }
        } catch (Exception e) {
            log.error("Sending error", e);
            return false;
        }
        log.debug("email send " + destinationEmail);
        return true;
    }

    /**
     * send via Camel
     *
     * @param connectionUrl
     * @param body
     * @param headers
     */
    protected void sendBodyAndHeaders(String connectionUrl, String body, Map<String, Object> headers) {
        template.sendBodyAndHeaders(connectionUrl, body, headers);
    }

}
