# README #

Java Online Application Framework. Build to improve speed of developing new SaaS system.

* Version 1.0.0 (it means very first, but used at production already)
* Vagrant and Docker scripts will be here soon (for development and production). Need review before publishing. 
* Deployment schema (haproxy, tomcat, cassandra) in progress. Need production test.

### How do I get set up? ###

* Build with maven
* create joaf.properties in Your tomcat/conf directory
* copy server.xml to Your tomcat/conf directory
* run vagrant (with docker inside) - soon

### Who do I talk to? ###
Cyprian Śniegota 
joaf@hmail.pl
