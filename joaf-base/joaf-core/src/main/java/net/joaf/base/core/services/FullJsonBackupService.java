/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.core.services;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.model.ModuleBackupContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
//TODO: should be moved to backup-restore module

/**
 * Provides full backup.
 *
 * @author Cyprian.Sniegota
 * @since 1.0
 */
@Service
public class FullJsonBackupService {

    @Autowired
    private List<JsonBackupService> backupServices = Arrays.asList();

    public ByteArrayOutputStream backupAll() throws JoafDatabaseException {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ZipOutputStream zos = new ZipOutputStream(outputStream);
            for (JsonBackupService backupService : backupServices) {
                ModuleBackupContainer moduleBackupContainer = backupService.backupAll();
                for (String key : moduleBackupContainer.getData().keySet()) {
                    writeToZipOS(zos, moduleBackupContainer.getModuleName() + "/" + key + "_data.json", moduleBackupContainer.getData().get(key));
                }
            }
            zos.close();
            outputStream.close();
            return outputStream;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void writeToZipOS(ZipOutputStream outputStream, String filename, String data) throws IOException {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(data.getBytes("UTF-8"));
        ZipEntry zipEntry = new ZipEntry(filename);
        outputStream.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;
        while ((length = inputStream.read(bytes)) >= 0) {
            outputStream.write(bytes, 0, length);
        }
        outputStream.closeEntry();
        inputStream.close();
    }
}
