package net.joaf.base.core.web.template;

/**
 * Created by cyprian on 23.08.15.
 */
public interface TemplateLocatorStrategy {

    String locate(TemplateResource templateResource);

}
