/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.core.session;

import net.joaf.base.core.error.JoafNoSubjectContextException;
import net.joaf.base.subjectcard.model.SubjectCardData;

import javax.servlet.http.HttpSession;
import java.util.Objects;

/**
 * Context stored in web session.
 *
 * @author Cyprian.Sniegota
 * @since 1.0
 */
public class SessionContext {
    private static final String JOAF_SESSION_CONTEXT = "net.joaf.base.SessionContext";

    /**
     * keep in session.
     */
    private String subjectCardDataId;
    private String templateName = "template";
    /**
     * load every request.
     */
    private SubjectCardData subjectCardData;

    /**
     * Loads SessionContext from http session.
     *
     * @param session Http session.
     * @return session context instance if exists.
     */
    public static SessionContext loadFromSession(HttpSession session) {
        return (SessionContext) session.getAttribute(SessionContext.JOAF_SESSION_CONTEXT);
    }

    /**
     * Validates session context against subject card id existence.
     *
     * @return this object.
     * @throws JoafNoSubjectContextException if not found.
     */
    public SessionContext check() throws JoafNoSubjectContextException {
        if (this.getSubjectCardDataId() == null) {
            throw new JoafNoSubjectContextException("");
        }
        return this;
    }

    /**
     * Update session with current object.
     *
     * @param session Http session.
     */
    public void saveToSession(HttpSession session) {
        session.setAttribute(SessionContext.JOAF_SESSION_CONTEXT, this);
    }

    /**
     * SubjectCardDataId getter.
     *
     * @return internal value.
     */
    public String getSubjectCardDataId() {
        return subjectCardDataId;
    }

    /**
     * SubjectCardDataId setter.
     *
     * @param subjectCardDataId id to set.
     */
    public void setSubjectCardDataId(String subjectCardDataId) {
        this.subjectCardDataId = subjectCardDataId;
    }

    /**
     * SubjectCardData getter.
     *
     * @return internal value.
     */
    public SubjectCardData getSubjectCardData() {
        return subjectCardData;
    }

    /**
     * SubjectCardData setter.
     *
     * @param subjectCardData data to set.
     */
    public void setSubjectCardData(SubjectCardData subjectCardData) {
        this.subjectCardData = subjectCardData;
    }

    /**
     * User's selected template.
     *
     * @return name of the template.
     */
    public String getTemplateName() {
        return templateName;
    }

    /**
     * User's selected template setter.
     *
     * @param templateName name to set.
     */
    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    @Override
    public String toString() {
        return "SessionContext{" +
                "subjectCardDataId='" + subjectCardDataId + '\'' +
                ", templateName='" + templateName + '\'' +
                ", subjectCardData=" + subjectCardData +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SessionContext)) return false;
        SessionContext that = (SessionContext) o;
        return Objects.equals(subjectCardDataId, that.subjectCardDataId) &&
                Objects.equals(templateName, that.templateName) &&
                Objects.equals(subjectCardData, that.subjectCardData);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subjectCardDataId, templateName, subjectCardData);
    }
}
