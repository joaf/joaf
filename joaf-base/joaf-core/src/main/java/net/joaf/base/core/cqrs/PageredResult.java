/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.core.cqrs;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * Pagered result - wrapper for query result list
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
public class PageredResult<T extends Serializable> {
    private List<T> result;
    private Optional<Integer> total;
    private Optional<Integer> pageSize;
    private Optional<Integer> offset;

    public PageredResult(List<T> result, Optional<Integer> total, Optional<Integer> pageSize, Optional<Integer> offset) {
        this.result = result;
        this.total = total;
        this.pageSize = pageSize;
        this.offset = offset;
    }

    public Optional<Integer> getTotal() {
        return total;
    }

    public Optional<Integer> getPageSize() {
        return pageSize;
    }

    public Optional<Integer> getOffset() {
        return offset;
    }

    public Integer getTotalValue() {
        return total.isPresent() ? total.get() : null;
    }

    public Integer getPageSizeValue() {
        return pageSize.isPresent() ? pageSize.get() : null;
    }

    public Integer getOffsetValue() {
        return offset.isPresent() ? offset.get() : null;
    }

    public List<T> getResult() {
        return result;
    }
}
