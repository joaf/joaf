package net.joaf.base.core.web.template;

import org.springframework.stereotype.Component;

/**
 * Created by cyprian on 23.08.15.
 */
@Component
public class BasicTemplateLocatorStrategy implements TemplateLocatorStrategy {

    @Override
    public String locate(TemplateResource templateResource) {
        StringBuilder location = new StringBuilder("freemarker/" + templateResource.getExtensionName());
        if (TemplateResourceType.WIDGET.equals(templateResource.getTemplateType())) {
            location.append("/widget");
        }
        if (templateResource.getSubpackageName() != null) {
            location.append("/").append(templateResource.getSubpackageName());
        }
        if (templateResource.isAdmin()) {
            location.append("/admin");
        }
        location.append("/").append(templateResource.getViewName()).append(".ftl");
        return location.toString();
    }
}
