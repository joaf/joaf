/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.core.utils;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Generates random token.
 *
 * @author Cyprian.Sniegota
 * @since 1.0
 */
public class TokenUtil {

    private static final int DEFAULT_LENGTH = 8;

    public static String generateToken() {
        return generateToken(DEFAULT_LENGTH);
    }

    public static String generateToken(int length) {
        return RandomStringUtils.randomAlphanumeric(length);
    }
}
