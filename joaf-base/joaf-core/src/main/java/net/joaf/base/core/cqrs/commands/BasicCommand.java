/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.core.cqrs.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.handlers.BasicCommandHandler;

import java.util.UUID;

/**
 * Sample Command DTO class. Could be extended to provide aggregateId property automatically.
 *
 * @author Cyprian.Sniegota
 * @since 1.0
 */
@CommandDto(handlerClass = BasicCommandHandler.class)
public class BasicCommand implements Command {

    private String aggregateId;

    public BasicCommand(String aggregateId) {
        this.aggregateId = aggregateId;
    }

    public BasicCommand() {
        this(UUID.randomUUID().toString());
    }

    @Override
    public String getAggregateId() {
        return aggregateId;
    }

    @Override
    public void setAggregateId(String aggregateId) {
        this.aggregateId = aggregateId;
    }
}
