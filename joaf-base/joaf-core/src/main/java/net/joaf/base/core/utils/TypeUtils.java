/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.core.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Util type converter.
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
public class TypeUtils {

    /**
     * Converts map to it's string representation.
     *
     * @param map input map.
     * @return string representation.
     */
    public static String mapToString(Map<String, String> map) {
        return StringUtils.join(map.entrySet().stream().map(x -> x.getKey() + ":" + x.getValue()).collect(Collectors.toList()), "\n");
    }

    /**
     * Converts set to string.
     *
     * @param set input set.
     * @return String representation of the input set.
     */
    public static String setToString(Set<String> set) {
        return StringUtils.join(set, "\n");
    }

    /**
     * Converts string with ":" delimiter for key/value and new line for next entry.
     *
     * @param string input data
     * @return String map.
     * @throws IllegalArgumentException if input data has invalid format.
     */
    public static Map<String, String> stringToMap(String string) {
        return Arrays.asList(StringUtils.split(string, "\n")).stream().map(x ->
        {
            String[] entry = x.trim().split(":");
            if (entry.length != 2) {
                throw new IllegalArgumentException("IllegalArgument " + x);
            }
            return new HashMap.SimpleEntry<>(entry[0], entry[1]);
        }).collect(Collectors.toMap(HashMap.SimpleEntry::getKey, HashMap.SimpleEntry::getValue));
    }

    /**
     * Converts string with new line delimiter to set of strings.
     *
     * @param string input string.
     * @return set representation.
     */
    public static Set<String> stringToSet(String string) {
        return new HashSet<>(Arrays.asList(StringUtils.split(string, "\n")).stream().map(String::trim).collect(Collectors.toList()));
    }

}
