package net.joaf.base.core.web.template;

import java.io.Serializable;

/**
 * Template resource object.
 *
 * @author Cyprian Sniegota
 * @since 1.4
 */
public final class TemplateResource implements Serializable {
    private String extensionName;
    private String subpackageName = null;
    private String viewName;
    private boolean admin = false;
    private TemplateResourceType templateType = TemplateResourceType.PAGE;
    /**
     * skipped in equals, only for warnings.
     */
    private int version = 1;

    public static Builder builder() {
        return new Builder();
    }

    public String getExtensionName() {
        return extensionName;
    }

    public String getSubpackageName() {
        return subpackageName;
    }

    public String getViewName() {
        return viewName;
    }

    public boolean isAdmin() {
        return admin;
    }

    public int getVersion() {
        return version;
    }

    public TemplateResourceType getTemplateType() {
        return templateType;
    }

    @Override
    public String toString() {
        return "TemplateResource{"
                + "extensionName='" + extensionName + '\''
                + ", subpackageName='" + subpackageName + '\''
                + ", viewName='" + viewName + '\''
                + ", admin=" + admin
                + ", templateType=" + templateType
                + ", version=" + version
                + '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        TemplateResource that = (TemplateResource) object;

        if (admin != that.admin) {
            return false;
        }
        if (!extensionName.equals(that.extensionName)) {
            return false;
        }
        if (subpackageName != null ? !subpackageName.equals(that.subpackageName) : that.subpackageName != null) {
            return false;
        }
        return viewName.equals(that.viewName) && templateType == that.templateType;

    }

    @Override
    public int hashCode() {
        int result = extensionName.hashCode();
        result = 31 * result + (subpackageName != null ? subpackageName.hashCode() : 0);
        result = 31 * result + viewName.hashCode();
        result = 31 * result + (admin ? 1 : 0);
        result = 31 * result + templateType.hashCode();
        return result;
    }

    public static class Builder {
        TemplateResource data = new TemplateResource();

        public Builder forExtension(String name) {
            data.extensionName = name;
            return this;
        }

        public Builder forSubpackage(String name) {
            data.subpackageName = name;
            return this;
        }

        public Builder forAdmin(boolean isAdmin) {
            data.admin = isAdmin;
            return this;
        }

        public Builder forView(String name) {
            data.viewName = name;
            return this;
        }

        public Builder forVersion(int number) {
            data.version = number;
            return this;
        }

        public Builder forType(TemplateResourceType type) {
            data.templateType = type;
            return this;
        }

        public TemplateResource build() {
            return this.data;
        }

    }
}
