package net.joaf.base.core.web.template;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by cyprian on 23.08.15.
 */
@Component
public class TemplateLocator {

    @Autowired
    @Qualifier("basicTemplateLocatorStrategy")
    private TemplateLocatorStrategy templateLocatorStrategy;

    public String locate(TemplateResource templateResource) {
        return templateLocatorStrategy.locate(templateResource);
    }

    public synchronized void replaceStrategy(TemplateLocatorStrategy strategy) {
        this.templateLocatorStrategy = strategy;
    }
}
