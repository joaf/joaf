/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.core.cqrs;

import net.joaf.base.core.error.JoafException;
import org.springframework.validation.BindingResult;

/**
 * Main interface for all Command Handlers.
 *
 * @author Cyprian.Sniegota
 * @since 1.0
 */
public interface CommandHandler<T extends Command, R> {

    /**
     * Validate command against proper values and other conditions (ex: check duplicates in database).
     *
     * @param originalBindingResult original binding result, for example from MVC
     * @param command               command object
     * @return binding result; could differ from original binding result.
     * @throws JoafException in case of error
     */
    BindingResult validate(BindingResult originalBindingResult, T command) throws JoafException;

    /**
     * Execute the command.
     *
     * @param command the command to execute
     * @return null or specific value, depends on implementation
     * @throws JoafException in case of error
     */
    R execute(T command) throws JoafException;
}
