/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.core.utils;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

/**
 * Utils for JSON conversions
 *
 * @author Cyprian.Sniegota
 * @since 1.0
 */
public class JsonUtils {

    private static final Logger log = LoggerFactory.getLogger(JsonUtils.class);

    private static final ObjectMapper objectMapper = new ObjectMapper();

    static {
        objectMapper.setVisibilityChecker(VisibilityChecker.Std.defaultInstance().withFieldVisibility(
                JsonAutoDetect.Visibility.ANY));
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.findAndRegisterModules();
    }

    /**
     * Converts object to String representation.
     *
     * @param obj object to convert
     * @return Json string representation or empty string
     */
    public static String stringValue(Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.error("Error converting object to String [obj={}]", obj, e);
        }
        return "";
    }

    /**
     * Converts json to object.
     *
     * @param json  json string
     * @param clazz target class
     * @param <T>   target class type
     * @return object of clazz or null
     */
    public static <T> T getPojo(String json, Class<T> clazz) {
        try {
            return objectMapper.readValue(json, clazz);
        } catch (IOException e) {
            log.error("Error converting json to object [class={},json={}]", clazz, json, e);
        }
        return null;
    }

    /**
     * Converts json to list of objects.
     *
     * @param json  json string
     * @param clazz target class
     * @param <T>   target class type
     * @return object of clazz or null
     */
    public static <T> List<T> getPojoList(String json, Class<T> clazz) {
        CollectionType collectionType = objectMapper.getTypeFactory().constructCollectionType(List.class, clazz);
        try {
            return objectMapper.readValue(json, collectionType);
        } catch (IOException e) {
            log.error("Error converting json to object [class={},json={}]", clazz, json, e);
        }
        return new ArrayList<>();
    }

    /**
     * Converts json to set of objects.
     *
     * @param json  json string
     * @param clazz target class
     * @param <T>   target class type
     * @return object of clazz or null
     */

    public static <T> Set<T> getPojoSet(String json, Class<T> clazz) {
        CollectionType collectionType = objectMapper.getTypeFactory().constructCollectionType(Set.class, clazz);
        try {
            return objectMapper.readValue(json, collectionType);
        } catch (IOException e) {
            log.error("Error converting json to object [class={},json={}]", clazz, json, e);
        }
        return new HashSet<>();
    }

    /**
     * Converts json map of objects identified by Integer.
     *
     * @param json  json string
     * @param clazz target object class
     * @param <E>   key type (Integer)
     * @param <T>   target class type
     * @return object of clazz or null
     */
    public static <E, T> Map<E, T> getPojoIntegerMap(String json, Class<T> clazz) {
        MapType collectionType = objectMapper.getTypeFactory().constructMapType(Map.class, Integer.class, clazz);
        try {
            return objectMapper.readValue(json, collectionType);
        } catch (IOException e) {
            log.error("Error converting json to object [class={},json={}]", clazz, json, e);
        }
        return new HashMap<>();
    }

    /**
     * Converts json map of objects identified by String.
     *
     * @param json  json string
     * @param clazz target object class
     * @param <E>   key type (String)
     * @param <T>   target class type
     * @return object of clazz or null
     */
    public static <E, T> Map<E, T> getPojoStringMap(String json, Class<T> clazz) {
        MapType collectionType = objectMapper.getTypeFactory().constructMapType(Map.class, String.class, clazz);
        try {
            return objectMapper.readValue(json, collectionType);
        } catch (IOException e) {
            log.error("Error converting json to object [class={},json={}]", clazz, json, e);
        }
        return new HashMap<>();
    }

}
