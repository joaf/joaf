/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.core.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Util for manipulating Date formats.
 *
 * @author Cyprian.Sniegota
 * @since 1.0
 */
public class DateUtils {

    public static Map<Integer, Integer> generateWeekdaysForMonth(LocalDate localDate) {
        Map<Integer, Integer> retMap = new LinkedHashMap<>();
        LocalDate currentDate = localDate.withDayOfMonth(1);
        Month month = currentDate.getMonth();
        while (currentDate.getMonth().equals(month)) {
            Integer dayOfMonth = currentDate.getDayOfMonth();
            if (!retMap.containsKey(dayOfMonth)) {
                retMap.put(dayOfMonth, currentDate.getDayOfWeek().getValue());
            }
            currentDate = currentDate.plusDays(1L);
        }
        return retMap;
    }

    public static Map<String, Integer> generateWeekdaysForMonthStringKey(LocalDate localDate) {
        Map<String, Integer> retMap = new LinkedHashMap<>();
        Map<Integer, Integer> integerIntegerMap = generateWeekdaysForMonth(localDate);
        for (Integer key : integerIntegerMap.keySet()) {
            retMap.put(String.valueOf(key), integerIntegerMap.get(key));
        }
        return retMap;
    }

    public static LocalDateTime dateToLocalDateTime(Date date) {
        if (date == null) {
            return null;
        }
        Date utilDate = date instanceof java.sql.Date ? new Date(date.getTime()) : date;
        return LocalDateTime.ofInstant(utilDate.toInstant(), ZoneId.systemDefault());
    }

    public static Date localDateTimeToDate(LocalDateTime dateTime) {
        if (dateTime == null) {
            return null;
        }
        return Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date localDateToDate(LocalDate dateTime) {
        if (dateTime == null) {
            return null;
        }
        return Date.from(dateTime.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDate dateToLocalDate(Date date) {
        if (date == null) {
            return null;
        }
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDateTime operationDateTime() {
        return LocalDateTime.now();
    }

    public static LocalDate operationDate() {
        return LocalDate.now();
    }

}
