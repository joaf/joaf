package net.joaf.base.core.cqrs;

import net.joaf.base.core.web.model.WrapperWDTO;

import java.io.Serializable;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * PageredResult extended - results wrapped by WrapperWDTO
 *
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class PageredResultExtended<T extends Serializable> {

    private final PageredResult<T> pageredResult;
    private final List<WrapperWDTO<T>> elements;

    public PageredResultExtended(PageredResult<T> pageredResult, Consumer<WrapperWDTO<T>> wrapConsumer) {
        this.pageredResult = pageredResult;
        this.elements = this.wrap(pageredResult.getResult(), wrapConsumer);
    }

    private List<WrapperWDTO<T>> wrap(List<T> result, Consumer<WrapperWDTO<T>> wrapConsumer) {
        List<WrapperWDTO<T>> collect = result.stream().map(WrapperWDTO<T>::new).collect(Collectors.toList());
        collect.forEach(wrapConsumer);
        return collect;
    }

    public PageredResult<T> getPageredResult() {
        return pageredResult;
    }

    public List<WrapperWDTO<T>> getElements() {
        return elements;
    }
}
