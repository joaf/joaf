package net.joaf.base.core.web.template;

/**
 * Created by cyprian on 23.08.15.
 */
public final class TemplateResources {

    public static TemplateResource list(String extensionName) {
        return builderForExtension(extensionName).forView("list").build();
    }

    public static TemplateResource listAdmin(String extensionName) {
        return builderForExtension(extensionName).forView("list").forAdmin(true).build();
    }

    public static TemplateResource edit(String extensionName) {
        return builderForExtension(extensionName).forView("edit").build();
    }

    public static TemplateResource editAdmin(String extensionName) {
        return builderForExtension(extensionName).forView("edit").forAdmin(true).build();
    }

    public static TemplateResource customView(String extensionName, String viewName) {
        return builderForExtension(extensionName).forView(viewName).build();
    }

    public static TemplateResource.Builder builderForExtension(String extensionName) {
        return TemplateResource.builder().forExtension(extensionName);
    }
}
