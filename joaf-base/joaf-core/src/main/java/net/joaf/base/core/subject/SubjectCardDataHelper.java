/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.core.subject;

import net.joaf.base.core.error.JoafNoSubjectContextException;

/**
 * Checks if subjectUid is set.
 *
 * @author Cyprian.Sniegota
 * @since 1.2
 */
public class SubjectCardDataHelper {

    /**
     * Checks if subject UID is proper id.
     * @param subjectUid uid to test.
     * @throws JoafNoSubjectContextException
     */
    public static void checkSubjectUid(String subjectUid) throws JoafNoSubjectContextException {
        if (subjectUid == null || subjectUid.trim().isEmpty()) {
            throw new JoafNoSubjectContextException("");
        }
    }
}
