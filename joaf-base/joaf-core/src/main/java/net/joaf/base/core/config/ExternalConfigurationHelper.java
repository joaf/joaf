/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.core.config;

import net.joaf.base.core.error.JoafException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * Helper to get main configuration variables (stored in configuration file)
 *
 * @author Cyprian.Sniegota
 * @since 1.0
 */
@Service
public class ExternalConfigurationHelper {

    private static final String JNDI_NAME = "java:comp/env/joafConfigFileLocation";
    private final Logger logger = LoggerFactory.getLogger(ExternalConfigurationHelper.class);
    private Properties properties;

    @PostConstruct
    public void postConstruct() throws JoafException {
        try {
            logger.info("Lookup configuration at [" + JNDI_NAME + "]");
            Object lookup = new InitialContext().lookup(JNDI_NAME);
            if (lookup != null) {
                String fileName = (String) lookup;
                this.loadProperties(fileName);
            } else {
                throw new JoafException("Error initializing External Configuration for lookup (lookup is null): [" + JNDI_NAME + "]");
            }
        } catch (NamingException e) {
            throw new JoafException("Error initializing External Configuration for lookup: [" + JNDI_NAME + "]", e);
        }
    }

    private void loadProperties(String fileName) throws JoafException {
        Properties localProperties = new Properties();
        try {
            localProperties.load(new FileReader(fileName));
        } catch (IOException e) {
            throw new JoafException("Error loading file [" + fileName + "]", e);
        }
        this.properties = localProperties;
    }

    public String getProperty(String name) throws JoafException {
        if (properties == null) {
            throw new JoafException("Properties not loaded");
        }
        String property = properties.getProperty(name);
        logger.info("Get property [" + name + ":" + property + "]");
        return property;
    }

}
