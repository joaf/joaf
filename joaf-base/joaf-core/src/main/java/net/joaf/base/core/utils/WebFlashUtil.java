/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.core.utils;

import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Util for managin web flash messages.
 *
 * @author Cyprian.Sniegota
 * @since 1.0
 */
public class WebFlashUtil {

    @SuppressWarnings(value = "unchecked")
    public static void addMessage(RedirectAttributes redirectAttributes, String title, String message, String... args) {
        FlashMessage flashMessage = new FlashMessage(title, message, args);
        flashMessage.setGritter("gritter-info");
        Map<String, ?> flashAttributes = redirectAttributes.getFlashAttributes();
        List<FlashMessage> messages;
        if (!flashAttributes.containsKey("messages")) {
            messages = new ArrayList<>();
            redirectAttributes.addFlashAttribute("messages", messages);
        } else {
            messages = (List<FlashMessage>) flashAttributes.get("messages");
        }
        messages.add(flashMessage);
    }

    @SuppressWarnings(value = "unchecked")
    public static void addError(RedirectAttributes redirectAttributes, String title, String message, String... args) {
        FlashMessage flashMessage = new FlashMessage(title, message, args);
        flashMessage.setGritter("gritter-error");
        Map<String, ?> flashAttributes = redirectAttributes.getFlashAttributes();
        List<FlashMessage> messages;
        if (!flashAttributes.containsKey("messages")) {
            messages = new ArrayList<>();
            redirectAttributes.addFlashAttribute("messages", messages);
        } else {
            messages = (List<FlashMessage>) flashAttributes.get("messages");
        }
        messages.add(flashMessage);
    }

    @SuppressWarnings(value = "unchecked")
    public static void addModelMessage(Model redirectAttributes, String title, String message, String... args) {
        FlashMessage flashMessage = new FlashMessage(title, message, args);
        flashMessage.setGritter("gritter-info");
        Map<String, ?> flashAttributes = redirectAttributes.asMap();
        List<FlashMessage> messages;
        if (!flashAttributes.containsKey("messages")) {
            messages = new ArrayList<>();
            redirectAttributes.addAttribute("messages", messages);
        } else {
            messages = (List<FlashMessage>) flashAttributes.get("messages");
        }
        messages.add(flashMessage);
    }

    public static class FlashMessage implements Serializable {
        private String title;
        private String body;
        private String[] args;
        private String gritter = "gritter-custom";

        public FlashMessage(String title, String body, String[] args) {
            this.title = title;
            this.body = body;
            this.args = args;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public String[] getArgs() {
            return args;
        }

        public void setArgs(String[] args) {
            this.args = args;
        }

        public String getGritter() {
            return gritter;
        }

        public void setGritter(String gritter) {
            this.gritter = gritter;
        }
    }
}
