/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.core.formmodel;

import com.sun.org.apache.xerces.internal.impl.PropertyManager;
import com.sun.org.apache.xerces.internal.impl.XMLStreamReaderImpl;
import net.joaf.base.core.error.JoafException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * Helper to read xml forms
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
public class XmlFormHelper {
    public static Form readFromClasspath(String path) throws JoafException {
        XMLStreamReader xmlStreamReader = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Form.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            PropertyManager propertyManager = new PropertyManager(PropertyManager.CONTEXT_READER);
            xmlStreamReader = new XMLStreamReaderImpl(XmlFormHelper.class.getResourceAsStream(path), propertyManager);
            return jaxbUnmarshaller.unmarshal(xmlStreamReader, Form.class).getValue();
        } catch (JAXBException | XMLStreamException e) {
            throw new JoafException("Error form parsing [" + path + "]", e);
        } finally {
            if (xmlStreamReader != null) {
                try {
                    xmlStreamReader.close();
                } catch (XMLStreamException ignored) {}
            }
        }
    }
}
