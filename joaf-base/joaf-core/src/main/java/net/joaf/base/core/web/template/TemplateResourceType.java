package net.joaf.base.core.web.template;

/**
 * Created by cyprian on 24.08.15.
 */
public enum TemplateResourceType {
    WIDGET, PAGE
}
