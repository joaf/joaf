/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.core.db;

import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.db.model.StringUidEntity;
import net.joaf.base.core.error.JoafDatabaseException;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * Default CRUD repository interface
 *
 * @author Cyprian.Sniegota
 * @since 1.0
 */
public interface CrudRepository<T extends StringUidEntity & Serializable> {

    void remove(String uid) throws JoafDatabaseException;

    void insert(T element) throws JoafDatabaseException;

    void store(T element) throws JoafDatabaseException;

    void updateObjectState(String uid, EObjectState newState) throws JoafDatabaseException;

    List<T> findAll() throws JoafDatabaseException;

    default PageredResult<T> findAll(PageredRequest request) throws JoafDatabaseException {
        return new PageredResult<>(findAll(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    T findOne(String uid) throws JoafDatabaseException;

    Optional<T> findOneOptional(String uid) throws JoafDatabaseException;

    String collectionName();

    Class<T> getEntityClass();
}
