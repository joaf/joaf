/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.core.services;

import com.google.common.collect.Maps;
import net.joaf.base.core.error.JoafDatabaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
//TODO: should be moved to backup-restore module

/**
 * Provides full restore.
 *
 * @author Cyprian.Sniegota
 * @since 1.0
 */
@Service
public class FullJsonRestoreService {

    @Autowired
    private List<JsonRestoreService> restoreServices = Arrays.asList();

    private Map<String, JsonRestoreService> restoreServiceMap = Maps.newHashMap();

    @PostConstruct
    public void setUp() {
        for (JsonRestoreService jsonRestoreService : restoreServices) {
            restoreServiceMap.put(jsonRestoreService.getModuleName() + "-" + jsonRestoreService.getCollectionName(), jsonRestoreService);
        }
    }

    public void restoreAll(InputStream inputStream) throws JoafDatabaseException {
/*        restoreServiceMap.clear();
        setUp();*/
        ZipInputStream zipInputStream = new ZipInputStream(inputStream);
        ZipEntry zipEntry;
        byte[] buffer = new byte[2048];
        try {
            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                System.out.println("zipEntry: " + zipEntry.getName());
                String[] path = zipEntry.getName().split("/");
                String key = path[0];
                String fname = path[1];
                String collectionName = fname.replace("_data.json", "");
                key = key + "-" + collectionName;
                if (restoreServiceMap.containsKey(key)) {
                    JsonRestoreService jsonRestoreService = restoreServiceMap.get(key);
                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    int len;
                    try {
                        while ((len = zipInputStream.read(buffer)) > 0) {
                            outputStream.write(buffer, 0, len);
                        }
                    } finally {
                        outputStream.close();
                    }
                    jsonRestoreService.restoreAll(outputStream.toString("UTF-8"), collectionName, true);
                } else {
                    System.out.println("not found: " + key);
                }

            }
            zipInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void writeToZipOS(ZipOutputStream outputStream, String filename, String data) throws IOException {

        ByteArrayInputStream inputStream = new ByteArrayInputStream(data.getBytes());
        ZipEntry zipEntry = new ZipEntry(filename);
        outputStream.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;
        while ((length = inputStream.read(bytes)) >= 0) {
            outputStream.write(bytes, 0, length);
        }
        outputStream.closeEntry();
        inputStream.close();
    }
}
