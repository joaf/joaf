/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.core.cqrs;

import net.joaf.base.core.db.model.EObjectState;

import java.util.Optional;

/**
 * Pagered request metadata.
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
public class PageredRequest {
    private Optional<Integer> limit;
    private Optional<Integer> offset;
    private Optional<EObjectState> state;
    private Optional<String> subjectUid;

    public PageredRequest() {
        this(null, null);
    }

    public PageredRequest(EObjectState state) {
        this(null, null, state);
    }

    public PageredRequest(Integer limit, Integer offset) {
        this(limit, offset, EObjectState.ACCEPTED, null);
    }

    public PageredRequest(Integer limit, Integer offset, EObjectState state) {
        this(limit, offset, state, null);
    }

    public PageredRequest(Integer limit, Integer offset, EObjectState state, String subjectUid) {
        this.limit = Optional.ofNullable(limit);
        this.offset = Optional.ofNullable(offset);
        this.state = Optional.ofNullable(state);
        this.subjectUid = Optional.ofNullable(subjectUid);
    }

    public Optional<Integer> getLimit() {
        return limit;
    }

    public Optional<Integer> getOffset() {
        return offset;
    }

    public Optional<EObjectState> getState() {
        return state;
    }

    public Optional<String> getSubjectUid() {
        return subjectUid;
    }
}
