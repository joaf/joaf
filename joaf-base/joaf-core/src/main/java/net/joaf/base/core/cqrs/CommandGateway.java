/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.core.cqrs;

import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.exceptions.JoafCQRSExecutionException;
import net.joaf.base.core.error.JoafException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Command Gateway. Dispatcher for all commands.
 *
 * @author Cyprian.Sniegota
 * @since 1.0
 */
@Component
public class CommandGateway {

    private static final Logger logger = LoggerFactory.getLogger(CommandGateway.class);

    @Autowired
    private List<CommandHandler> handlers = Collections.emptyList();

    private Map<Class<?>, CommandHandler> handlerMap = new HashMap<>();

    @PostConstruct
    public void setUp() {
        handlerMap.clear();
        for (CommandHandler handler : handlers) {
            handlerMap.put(handler.getClass(), handler);
        }
    }

    @SuppressWarnings(value = "unchecked")
    public <T extends Command> Object send(T command, Class<? extends CommandHandler> handlerClass) throws JoafException {
        logger.trace("sending command: [{}], expected handler:", command, handlerClass);
        checkInitialConditions(command);
        CommandHandler commandHandler = findHandler(command);
        logger.trace("sending command: [{}], found handler:", command, commandHandler);
        return commandHandler.execute(command);
    }

    @SuppressWarnings(value = "unchecked")
    public <T extends Command> void validate(BindingResult originalBindingResult, T command) throws JoafException {
        logger.trace("validating command: [{}], original hasErrors: [{}]", command, originalBindingResult.hasErrors());
        checkInitialConditions(command);
        CommandHandler commandHandler = findHandler(command);
        BindingResult validateResult = commandHandler.validate(originalBindingResult, command);
        if (isReturnedObjectNotTheOriginal(originalBindingResult, validateResult)) {
            mergeValidateResult(originalBindingResult, validateResult);
        }
        logger.trace("validated command: [{}], hasErrors: [{}], result: [{}]", command, originalBindingResult.hasErrors(), originalBindingResult);
    }

    public <T extends Command> Object validateAndSend(BindingResult originalBindingResult, T command, Class<? extends CommandHandler> handlerClass)
            throws JoafException {
        this.validate(originalBindingResult, command);
        if (!originalBindingResult.hasErrors()) {
            return this.send(command, handlerClass);
        }
        return null;
    }

    private void mergeValidateResult(BindingResult originalBindingResult, BindingResult validateResult) {
        logger.trace("Merging BindingResult: [{}] with [{}]", originalBindingResult, validateResult);
        if (validateResult == null) {
            return;
        }
        for (ObjectError objectError : validateResult.getAllErrors()) {
            if (!originalBindingResult.getAllErrors().contains(objectError)) {
                originalBindingResult.addError(objectError);
            }
        }
        logger.trace("Merged BindingResult: [{}]", originalBindingResult);
    }

    private boolean isReturnedObjectNotTheOriginal(BindingResult originalBindingResult, BindingResult validateResult) {
        return validateResult != originalBindingResult;
    }

    private <T extends Command> void checkInitialConditions(T command) throws JoafCQRSExecutionException {
        if (command.getAggregateId() == null) {
            command.setAggregateId(UUID.randomUUID().toString());
        }
        if (!command.getClass().isAnnotationPresent(CommandDto.class)) {
            throw new JoafCQRSExecutionException("Command class not annotated");
        }
    }

    private <T extends Command> CommandHandler findHandler(T command) throws JoafCQRSExecutionException {
        CommandDto commandDtoAnnotation = command.getClass().getAnnotation(CommandDto.class);
        if (!handlerMap.containsKey(commandDtoAnnotation.handlerClass())) {
            throw new JoafCQRSExecutionException(String.format("No handler found for class '%s'", commandDtoAnnotation.handlerClass()));
        }
        return handlerMap.get(commandDtoAnnotation.handlerClass());
    }
}
