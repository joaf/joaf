package net.joaf.base.core.cqrs;

import org.junit.Ignore;
import org.junit.Test;

import java.util.Collections;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class PageredResultTest {

    @Test
    public void testConstructor() throws Exception {
        //given
        //when
        PageredResult<Integer> result = new PageredResult<>(Collections.singletonList(1), Optional.<Integer>empty(), Optional.<Integer>empty(), Optional.<Integer>empty());
        //then
        assertFalse(result.getOffset().isPresent());
        assertFalse(result.getPageSize().isPresent());
        assertFalse(result.getTotal().isPresent());
        assertNull(result.getOffsetValue());
        assertNull(result.getPageSizeValue());
        assertNull(result.getTotalValue());
        assertEquals(1, result.getResult().size());
    }

    @Test
    public void testConstructorWithData() throws Exception {
        //given
        //when
        PageredResult<Integer> result = new PageredResult<>(Collections.singletonList(1), Optional.of(1), Optional.of(1), Optional.of(1));
        //then
        assertTrue(result.getOffset().isPresent());
        assertTrue(result.getPageSize().isPresent());
        assertTrue(result.getTotal().isPresent());
        Integer one = 1;
        assertEquals(one, result.getOffsetValue());
        assertEquals(one, result.getPageSizeValue());
        assertEquals(one, result.getTotalValue());
        assertEquals(1, result.getResult().size());
    }

}