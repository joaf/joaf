package net.joaf.base.core.cqrs.commands;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class AbstractUidCommandTest {

    private static final String TEST_USER_UID = "12345";
    private static final String TEST_ELEMENT_UID = "6789";

    @Test
    public void testGetElementUid() throws Exception {
        //given
        AbstractUidCommand objectUnderTest = new TestAbstractUidCommand(TEST_ELEMENT_UID, TEST_USER_UID);
        //when
        String result = objectUnderTest.getElementUid();
        //then
        assertEquals(TEST_ELEMENT_UID, result);
    }

    private static class TestAbstractUidCommand extends AbstractUidCommand {

        public TestAbstractUidCommand(String elementUid, String userUid) {
            super(elementUid, userUid);
        }
    }
}