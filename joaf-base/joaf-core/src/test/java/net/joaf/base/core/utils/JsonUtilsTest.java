/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.core.utils;

import org.junit.Test;
import org.mockito.InjectMocks;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * @author Cyprian.Sniegota
 * @since 1.1
 */
public class JsonUtilsTest {

    @InjectMocks
    private JsonUtils objectUnderTest = new JsonUtils();

    @Test
    public void powinienWywolacStringValue() throws Exception {
        //given
        CheckPojo checkPojo = new CheckPojo();
        checkPojo.setBool(false);
        //when
        String s = JsonUtils.stringValue(checkPojo);
        //then
        System.out.println(s);
        assertEquals(s, "{\"localDate\":null,\"localDateTime\":null,\"name\":null,\"bool\":false}");
    }

    @Test
    public void powinienWywolacGetPojo() throws Exception {
        //given
        CheckPojo checkPojo = createCheckPojo();
        String s = JsonUtils.stringValue(checkPojo);
        //when
        CheckPojo pojo = JsonUtils.getPojo(s, CheckPojo.class);
        //then
        assertEquals(pojo, checkPojo);
    }

    private CheckPojo createCheckPojo() {
        CheckPojo checkPojo = new CheckPojo();
        checkPojo.setBool(false);
        checkPojo.setLocalDate(LocalDate.now());
        checkPojo.setLocalDateTime(LocalDateTime.MAX);
        return checkPojo;
    }

    @Test
    public void powinienWywolacGetPojoSet() throws Exception {
        //given
        Set<CheckPojo> set = new HashSet<>();
        set.add(createCheckPojo());
        String s = JsonUtils.stringValue(set);
        //when
        Set<CheckPojo> pojoSet = JsonUtils.getPojoSet(s, CheckPojo.class);
        //then
        assertEquals(pojoSet, set);
    }

    @Test
    public void powinienWywolacGetPojoStringMap() throws Exception {
        //given
        Map<String, CheckPojo> map = new HashMap<>();
        map.put("x", createCheckPojo());
        String s = JsonUtils.stringValue(map);
        //when
        Map<String, CheckPojo> pojoStringMap = JsonUtils.getPojoStringMap(s, CheckPojo.class);
        //then
        assertEquals(pojoStringMap, map);
    }

    private static class CheckPojo implements Serializable {
        private LocalDate localDate;
        private LocalDateTime localDateTime;
        private String name;
        private Boolean bool;

        public LocalDate getLocalDate() {
            return localDate;
        }

        public void setLocalDate(LocalDate localDate) {
            this.localDate = localDate;
        }

        public LocalDateTime getLocalDateTime() {
            return localDateTime;
        }

        public void setLocalDateTime(LocalDateTime localDateTime) {
            this.localDateTime = localDateTime;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Boolean getBool() {
            return bool;
        }

        public void setBool(Boolean bool) {
            this.bool = bool;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (o == null || getClass() != o.getClass())
                return false;

            CheckPojo checkPojo = (CheckPojo) o;

            if (bool != null ? !bool.equals(checkPojo.bool) : checkPojo.bool != null)
                return false;
            if (localDate != null ? !localDate.equals(checkPojo.localDate) : checkPojo.localDate != null)
                return false;
            if (localDateTime != null ? !localDateTime.equals(checkPojo.localDateTime) : checkPojo.localDateTime != null)
                return false;
            if (name != null ? !name.equals(checkPojo.name) : checkPojo.name != null)
                return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = localDate != null ? localDate.hashCode() : 0;
            result = 31 * result + (localDateTime != null ? localDateTime.hashCode() : 0);
            result = 31 * result + (name != null ? name.hashCode() : 0);
            result = 31 * result + (bool != null ? bool.hashCode() : 0);
            return result;
        }
    }
}