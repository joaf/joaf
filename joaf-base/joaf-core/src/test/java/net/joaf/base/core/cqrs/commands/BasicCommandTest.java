package net.joaf.base.core.cqrs.commands;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class BasicCommandTest {

    @Test
    public void testGetAggregateId() throws Exception {
        //given
        BasicCommand objectUnderTest = new BasicCommand();
        //when
        String result = objectUnderTest.getAggregateId();
        //then
        assertNotNull(result);
    }

    @Test
    public void testSetAggregateId() throws Exception {
        //given
        BasicCommand objectUnderTest = new BasicCommand();
        String aid = "123";
        //when
        objectUnderTest.setAggregateId(aid);
        //then
        assertEquals(aid, objectUnderTest.getAggregateId());
    }
}