package net.joaf.base.core.error;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class JoafNoSubjectContextExceptionTest {
    @Test
    public void testConstructorMsg() {
        //given
        //when
        JoafNoSubjectContextException result = new JoafNoSubjectContextException("msg");
        //then
        assertEquals("msg", result.getLocalizedMessage());
        assertNull(result.getCause());
    }
}