package net.joaf.base.core.session;

import net.joaf.base.core.error.JoafNoSubjectContextException;
import net.joaf.base.subjectcard.model.SubjectCardData;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class SessionContextTest {

    @Mock
    private HttpSession httpSessionMock;

    @Mock
    private SubjectCardData subjectCardDataMock;

    @InjectMocks
    private SessionContext objectUnderTest;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldLoadFromSession() throws Exception {
        //given
        SessionContext sessionContext = new SessionContext();
        given(httpSessionMock.getAttribute("net.joaf.base.SessionContext")).willReturn(sessionContext);
        //when
        SessionContext result = SessionContext.loadFromSession(httpSessionMock);
        //then
        assertEquals(sessionContext, result);
    }

    @Test(expected = JoafNoSubjectContextException.class)
    public void shouldCheckNoSubjectContext() throws Exception {
        //given
        objectUnderTest.setSubjectCardDataId(null);
        //when
        objectUnderTest.check();
        //then
    }

    @Test
    public void shouldCheckValid() throws Exception {
        //given
        objectUnderTest.setSubjectCardDataId("123");
        //when
        SessionContext result = objectUnderTest.check();
        //then
        assertEquals(objectUnderTest, result);
    }


    @Test
    public void shouldSaveToSession() throws Exception {
        //given
        //when
        objectUnderTest.saveToSession(httpSessionMock);
        //then
        verify(httpSessionMock).setAttribute("net.joaf.base.SessionContext", objectUnderTest);
    }

    @Test
    public void shouldGetSubjectCardDataId() throws Exception {
        //given
        objectUnderTest.setSubjectCardDataId("123");
        //when
        String result = objectUnderTest.getSubjectCardDataId();
        //then
        assertEquals("123", result);
    }

    @Test
    public void shouldSetSubjectCardDataId() throws Exception {
        //given
        //when
        objectUnderTest.setSubjectCardDataId("4321");
        //then
        assertEquals("4321", objectUnderTest.getSubjectCardDataId());
    }

    @Test
    public void testGetSubjectCardData() throws Exception {
        //given
        objectUnderTest.setSubjectCardData(subjectCardDataMock);
        //when
        SubjectCardData result = objectUnderTest.getSubjectCardData();
        //then
        assertEquals(subjectCardDataMock, result);
    }

    @Test
    public void testSetSubjectCardData() throws Exception {
        //given
        //when
        objectUnderTest.setSubjectCardData(subjectCardDataMock);
        //then
        assertEquals(subjectCardDataMock, objectUnderTest.getSubjectCardData());
    }

    @Test
    public void testGetTemplateName() throws Exception {
        //given
        objectUnderTest.setTemplateName("base");
        //when
        String result = objectUnderTest.getTemplateName();
        //then
        assertEquals("base", result);
    }

    @Test
    public void testSetTemplateName() throws Exception {
        //given
        //when
        objectUnderTest.setTemplateName("xyz");
        //then
        assertEquals("xyz", objectUnderTest.getTemplateName());
    }
}