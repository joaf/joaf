package net.joaf.base.core.config;

import net.joaf.base.core.error.JoafException;
import net.joaf.base.testutils.InitialContextTestUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.naming.Context;
import javax.naming.NamingException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class ExternalConfigurationHelperTest {

    @InjectMocks
    private ExternalConfigurationHelper objectUnderTest;

    @Mock
    @SuppressWarnings("ALL")
    private Properties properties;

    @Mock
    private Context context;

    @Before
    public void setUp() throws Exception {
        this.objectUnderTest = null;
        MockitoAnnotations.initMocks(this);
    }

    /*
    // v1
    LocalContext ctx = LocalContextFactory.createLocalContext();
    ctx.addDataSource("jdbc/testdb", driverName, url, usr, pwd);

    // v2
    // select the registry context factory for creating a context
env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.rmi.registry.RegistryContextFactory");

// specify where that factory is running.
env.put(Context.PROVIDER_URL, "rmi://server:1099");

// create an initial context that accesses the registry
Context ctx = new InitialContext(env);
     */

    @Test
    public void testGetProperty() throws Exception {
        //given
        String testKey = "abc.def";
        given(properties.getProperty(testKey)).willReturn("ABC");
        //when
        String result = objectUnderTest.getProperty(testKey);
        //then
        assertEquals("ABC", result);
    }

    @Test(expected = JoafException.class)
    public void testGetPropertyNotLoaded() throws Exception {
        //given
        String testKey = "abc.def";
        this.objectUnderTest = new ExternalConfigurationHelper();
        //when
        String result = objectUnderTest.getProperty(testKey);
        //then exception
    }

    @Test(expected = JoafException.class)
    public void testPostConstructThrowNamingException() throws Exception {
        //given
        InitialContextTestUtil.getInstance().getInitialContext().setMockContext(context);
        given(context.lookup(anyString())).willThrow(new NamingException("test naming exception"));
        //when
        objectUnderTest.postConstruct();
        //then exception
    }

    @Test(expected = JoafException.class)
    public void testPostConstructFileNotFound() throws Exception {
        //given
        InitialContextTestUtil.getInstance().getInitialContext().setMockContext(context);
        given(context.lookup("java:comp/env/joafConfigFileLocation")).willReturn("/tmp/FileDoesNotExists");
        //when
        objectUnderTest.postConstruct();
        //then exception
    }

    @Test(expected = JoafException.class)
    public void testPostConstructLookupIsNull() throws Exception {
        //given
        InitialContextTestUtil.getInstance().getInitialContext().setMockContext(context);
        given(context.lookup("java:comp/env/joafConfigFileLocation")).willReturn(null);
        //when
        objectUnderTest.postConstruct();
        //then exception
    }

    @Test
    public void testPostConstruct() throws Exception {
        //given
        InitialContextTestUtil.getInstance().getInitialContext().setMockContext(context);
        Path path = Paths.get(getClass().getResource("/testJoafConfiguration.properties").toURI());
        given(context.lookup("java:comp/env/joafConfigFileLocation")).willReturn(path.toString());
        //when
        objectUnderTest.postConstruct();
        //then
        assertEquals("tcp://localhost:61616", objectUnderTest.getProperty("activemq.brokerUrl"));
    }


}