package net.joaf.base.core.cqrs;

import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractCommand;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.base.core.cqrs.exceptions.JoafCQRSExecutionException;
import net.joaf.base.core.error.JoafException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.MapBindingResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class CommandGatewayTest {

    @InjectMocks
    private CommandGateway objectUnderTest;

    @Mock
    @SuppressWarnings("ALL")
    private List<CommandHandler> handlersMock = new ArrayList<>();

    @Mock
    private CommandHandler commandHandlerMock;

    @Mock
    private BindingResult bindingResultMock;

    private TestCommandHandler testCommandHandler;

    private Command command;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        command = new TestCommand("123");
        testCommandHandler = new TestCommandHandler();
    }

    @Test
    public void testSetUp() throws Exception {
        //given
        List<CommandHandler> testList = Collections.singletonList(commandHandlerMock);
        given(handlersMock.iterator()).willReturn(testList.iterator());
        //when
        objectUnderTest.setUp();
        //then no exception
    }

    @Test
    @SuppressWarnings("ALL")
    public void testSend() throws Exception {
        //given
        List<CommandHandler> testList = Collections.singletonList(testCommandHandler);
        given(handlersMock.iterator()).willReturn(testList.iterator());
        objectUnderTest.setUp();
        //when
        objectUnderTest.send(command, TestCommandHandler.class);
        //then
        verify(commandHandlerMock).execute(command);
    }

    @Test
    @SuppressWarnings("ALL")
    public void testSendNoAggregateUid() throws Exception {
        //given
        List<CommandHandler> testList = Collections.singletonList(testCommandHandler);
        given(handlersMock.iterator()).willReturn(testList.iterator());
        objectUnderTest.setUp();
        command.setAggregateId(null);
        //when
        objectUnderTest.send(command, TestCommandHandler.class);
        //then
        verify(commandHandlerMock).execute(command);
        assertNotNull(command.getAggregateId());
    }

    @Test(expected = JoafCQRSExecutionException.class)
    public void testSendNoSetup() throws Exception {
        //given
        List<CommandHandler> testList = Collections.singletonList(testCommandHandler);
        given(handlersMock.iterator()).willReturn(testList.iterator());
        //when
        objectUnderTest.send(command, TestCommandHandler.class);
        //then exception expected
    }

    @Test(expected = JoafCQRSExecutionException.class)
    public void testSendNotAnnotated() throws Exception {
        //given
        List<CommandHandler> testList = Collections.singletonList(testCommandHandler);
        given(handlersMock.iterator()).willReturn(testList.iterator());
        objectUnderTest.setUp();
        //when
        objectUnderTest.send(new AbstractUidCommand("123", "456", null) {}, TestCommandHandler.class);
        //then exception expected
    }

    @Test
    @SuppressWarnings("ALL")
    public void testValidate() throws Exception {
        //given
        List<CommandHandler> testList = Collections.singletonList(testCommandHandler);
        given(handlersMock.iterator()).willReturn(testList.iterator());
        objectUnderTest.setUp();
        //when
        objectUnderTest.validate(bindingResultMock, command);
        //then
        verify(commandHandlerMock).validate(bindingResultMock, command);
    }

    @Test
    @SuppressWarnings("ALL")
    public void testValidateDifferentResult() throws Exception {
        //given
        List<CommandHandler> testList = Collections.singletonList(testCommandHandler);
        given(handlersMock.iterator()).willReturn(testList.iterator());
        MapBindingResult mapBindingResult = new MapBindingResult(new HashMap<Object, Object>(), "command");
        FieldError errorTest = new FieldError("testname", "testfield", "testerror message");
        mapBindingResult.addError(errorTest);
        FieldError errorAlreadyCovered = new FieldError("testexists", "testfieldexists", "testerror message exists");
        mapBindingResult.addError(errorAlreadyCovered);
        given(bindingResultMock.getAllErrors()).willReturn(Collections.singletonList(errorAlreadyCovered));
        given(commandHandlerMock.validate(anyObject(), anyObject())).willReturn(mapBindingResult);
        objectUnderTest.setUp();
        //when
        objectUnderTest.validate(bindingResultMock, this.command);
        //then
        verify(commandHandlerMock).validate(bindingResultMock, this.command);
    }

    @Test
    @SuppressWarnings("ALL")
    public void testValidateSameResult() throws Exception {
        //given
        List<CommandHandler> testList = Collections.singletonList(testCommandHandler);
        given(handlersMock.iterator()).willReturn(testList.iterator());
        given(commandHandlerMock.validate(anyObject(), anyObject())).willReturn(bindingResultMock);
        objectUnderTest.setUp();
        //when
        objectUnderTest.validate(bindingResultMock, command);
        //then
        verify(commandHandlerMock).validate(bindingResultMock, command);
    }

    @Test
    @SuppressWarnings("ALL")
    public void testValidateAndSend() throws Exception {
        //given
        List<CommandHandler> testList = Collections.singletonList(testCommandHandler);
        given(handlersMock.iterator()).willReturn(testList.iterator());
        objectUnderTest.setUp();
        //when
        objectUnderTest.validateAndSend(bindingResultMock, command, TestCommandHandler.class);
        //then
        verify(commandHandlerMock).validate(bindingResultMock, command);
        verify(commandHandlerMock).execute(command);
    }

    @Test
    @SuppressWarnings("ALL")
    public void testValidateAndSendHasErrors() throws Exception {
        //given
        List<CommandHandler> testList = Collections.singletonList(testCommandHandler);
        given(handlersMock.iterator()).willReturn(testList.iterator());
        given(commandHandlerMock.validate(anyObject(), anyObject())).willReturn(bindingResultMock);
        given(bindingResultMock.hasErrors()).willReturn(true);
        objectUnderTest.setUp();
        //when
        objectUnderTest.validateAndSend(bindingResultMock, command, TestCommandHandler.class);
        //then
        verify(commandHandlerMock).validate(bindingResultMock, command);
        verify(commandHandlerMock, never()).execute(command);
    }

    @CommandDto(handlerClass = TestCommandHandler.class)
    public static class TestCommand extends AbstractCommand {

        public TestCommand(String userUid) {
            super(userUid);
        }
    }

    public class TestCommandHandler implements CommandHandler<AbstractCommand, Object> {

        @Override
        @SuppressWarnings("ALL")
        public BindingResult validate(BindingResult originalBindingResult, AbstractCommand command) throws JoafException {
            return commandHandlerMock.validate(originalBindingResult, command);
        }

        @Override
        @SuppressWarnings("ALL")
        public Object execute(AbstractCommand command) throws JoafException {
            return commandHandlerMock.execute(command);
        }
    }
}