package net.joaf.base.core.cqrs.events;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class SampleEventTest {

    @Test
    public void testConstructor() {
        //given
        //when
        SampleEvent result = new SampleEvent();
        //then
        assertNotNull(result);
    }
}