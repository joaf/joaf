package net.joaf.base.core.cqrs.commands.handlers;

import org.junit.Test;
import org.springframework.validation.BindingResult;

import static org.junit.Assert.assertNull;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class BasicCommandHandlerTest {

    @Test
    public void testValidate() throws Exception {
        //given
        BasicCommandHandler objectUnderTest = new BasicCommandHandler();
        //when
        BindingResult result = objectUnderTest.validate(null, null);
        //then
        assertNull(result);
    }

    @Test
    public void testExecute() throws Exception {
        //given
        BasicCommandHandler objectUnderTest = new BasicCommandHandler();
        //when
        Object result = objectUnderTest.execute(null);
        //then
        assertNull(result);
    }
}