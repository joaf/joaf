package net.joaf.base.core.config;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class ExternalConfigurationPropertyFactoryTest {

    @InjectMocks
    private ExternalConfigurationPropertyFactory objectUnderTest;
    @Mock
    private ExternalConfigurationHelper externalConfigurationHelper;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testActiveMqBrokerUrl() throws Exception {
        //given
        given(externalConfigurationHelper.getProperty("activemq.brokerUrl")).willReturn("testurl");
        //when
        String result = objectUnderTest.activeMqBrokerUrl();
        //then
        assertEquals("testurl", result);
    }


}