package net.joaf.base.core.error;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class JoafExceptionTest {
    @Test
    public void testConstructorMsg() {
        //given
        //when
        JoafException result = new JoafException("msg");
        //then
        assertEquals("msg", result.getLocalizedMessage());
        assertNull(result.getCause());
    }

    @Test
    public void testConstructorCause() {
        //given
        //when
        JoafException result = new JoafException("msg", new RuntimeException("msgre"));
        //then
        assertEquals("msg", result.getLocalizedMessage());
        assertNotNull(result.getCause());
    }

    @Test
    public void testConstructorErrorCode() {
        //given
        //when
        JoafException result = new JoafException("err-01","msg", new RuntimeException("msgre"));
        //then
        assertEquals("msg", result.getLocalizedMessage());
        assertEquals("err-01", result.getExceptionNumber());
        assertNotNull(result.getCause());
    }
}