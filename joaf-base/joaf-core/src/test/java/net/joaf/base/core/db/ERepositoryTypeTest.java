package net.joaf.base.core.db;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class ERepositoryTypeTest {

    @Test
    public void checkAllExists() {
        //given
        //when
        List<ERepositoryType> result = Arrays.asList(ERepositoryType.values());
        //then
        assertTrue(result.contains(ERepositoryType.CASSANDRA));
        assertTrue(result.contains(ERepositoryType.MONGO));
        assertTrue(result.contains(ERepositoryType.MYSQL));
    }
}