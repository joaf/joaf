package net.joaf.base.core.utils;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class TokenUtilTest {

    @Test
    public void shouldGenerateToken() throws Exception {
        //given
        //when
        String result = TokenUtil.generateToken();
        //then
        assertNotNull(result);
        assertEquals(8, result.length());
    }

    @Test
    public void shouldGenerateToken1() throws Exception {
        //given
        //when
        String result = TokenUtil.generateToken(2);
        //then
        assertNotNull(result);
        assertEquals(2, result.length());
    }
}