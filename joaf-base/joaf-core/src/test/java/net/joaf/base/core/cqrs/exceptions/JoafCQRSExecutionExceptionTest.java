package net.joaf.base.core.cqrs.exceptions;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class JoafCQRSExecutionExceptionTest {

    @Test
    @SuppressWarnings("ALL")
    public void testConstructor() {
        //given
        //when
        JoafCQRSExecutionException result = new JoafCQRSExecutionException("test message");
        //then
        assertEquals("test message", result.getLocalizedMessage());
        assertNull("test message", result.getCause());
    }

    @Test
    @SuppressWarnings("ALL")
    public void testConstructorCaused() {
        //given
        //when
        JoafCQRSExecutionException result = new JoafCQRSExecutionException("test message", new RuntimeException("another test message"));
        //then
        assertEquals("test message", result.getLocalizedMessage());
        assertNotNull("test message", result.getCause());
    }

}