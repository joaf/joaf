package net.joaf.base.core.cqrs.commands;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class AbstractUidSuidCommandTest {
    private static final String TEST_USER_UID = "12345";
    private static final String TEST_ELEMENT_UID = "6789";
    private static final String TEST_SUBJECT_UID = "012789";

    @Test
    public void testGetSubjectUid() throws Exception {
        //given
        AbstractUidSuidCommand objectUnderTest = new TestAbstractUidSuidCommand(TEST_ELEMENT_UID, TEST_USER_UID, TEST_SUBJECT_UID);
        //when
        String result = objectUnderTest.getSubjectUid();
        //then
        assertEquals(TEST_SUBJECT_UID, result);
    }

    private static class TestAbstractUidSuidCommand extends AbstractUidSuidCommand {

        public TestAbstractUidSuidCommand(String elementUid, String userUid, String subjectUid) {
            super(elementUid, userUid, subjectUid);
        }
    }
}