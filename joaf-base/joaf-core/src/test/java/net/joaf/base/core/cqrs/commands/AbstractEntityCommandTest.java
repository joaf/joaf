package net.joaf.base.core.cqrs.commands;

import net.joaf.base.core.db.model.StringUidEntity;
import org.junit.Test;

import java.io.Serializable;

import static org.junit.Assert.assertEquals;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class AbstractEntityCommandTest {

    private static final String TEST_USER_UID = "12345";

    @Test
    public void testGetElement() throws Exception {
        //given
        TestEntity element = new TestEntity();
        TestAbstractEntityCommand objectUnderTest = new TestAbstractEntityCommand(element, TEST_USER_UID);
        //when
        TestEntity result = objectUnderTest.getElement();
        //then
        assertEquals(element, result);
    }

    private static class TestAbstractEntityCommand extends AbstractEntityCommand<TestEntity> {
        public TestAbstractEntityCommand(TestEntity element, String userUid) {
            super(element, userUid);
        }
    }

    private static class TestEntity implements StringUidEntity, Serializable {

        @Override
        public String getUid() {
            return null;
        }
    }
}