package net.joaf.base.core.cqrs;

import net.joaf.base.core.cqrs.events.SampleEvent;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class EventPublisherTest {

    @InjectMocks
    private EventPublisher objectUnderTest;

    @Mock
    private EventHandler eventHandlerMock;

    @Mock
    private ConfigurableListableBeanFactory beanFactoryMock;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testPost() throws Exception {
        //given
        given(beanFactoryMock.getBean(Matchers.<Class>anyObject())).willReturn(eventHandlerMock);
        objectUnderTest.register(EventHandler.class, SampleEvent.class);
        //when
        objectUnderTest.post(new SampleEvent());
        //then
        verify(eventHandlerMock).execute(anyObject());
    }

    @Test
    public void testPostWithException() throws Exception {
        //given
        given(beanFactoryMock.getBean(Matchers.<Class>anyObject())).willReturn(eventHandlerMock);
        objectUnderTest.register(EventHandler.class, SampleEvent.class);
        doThrow(new RuntimeException("test exception")).when(eventHandlerMock).execute(anyObject());
        //when
        objectUnderTest.post(new SampleEvent());
        //then
        verify(eventHandlerMock).execute(anyObject());
    }

    @Test
    public void testRegister() throws Exception {
        //given
        //when
        objectUnderTest.register(EventHandler.class, Event.class);
        //then
    }

    @Test
    public void testRegisterDouble() throws Exception {
        //given
        objectUnderTest.register(EventHandler.class, Event.class);
        //when
        objectUnderTest.register(EventHandler.class, Event.class);
        //then
    }
}