package net.joaf.base.core.cqrs;

import net.joaf.base.core.db.model.EObjectState;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class PageredRequestTest {

    @Test
    public void testConstructorDefault() {
        //given
        //when
        PageredRequest result = new PageredRequest();
        //then
        assertFalse(result.getLimit().isPresent());
        assertFalse(result.getOffset().isPresent());
        assertFalse(result.getSubjectUid().isPresent());
        assertEquals(EObjectState.ACCEPTED, result.getState().get());
    }

    @Test
    public void testConstructorState() {
        //given
        //when
        PageredRequest result = new PageredRequest(EObjectState.TRASH);
        //then
        assertFalse(result.getLimit().isPresent());
        assertFalse(result.getOffset().isPresent());
        assertFalse(result.getSubjectUid().isPresent());
        assertEquals(EObjectState.TRASH, result.getState().get());
    }

    @Test
    public void testConstructorLimitOffset() {
        //given
        //when
        PageredRequest result = new PageredRequest(1,0);
        //then
        assertTrue(result.getLimit().isPresent());
        assertTrue(result.getOffset().isPresent());
        assertFalse(result.getSubjectUid().isPresent());
        assertEquals(EObjectState.ACCEPTED, result.getState().get());
    }

    @Test
    public void testConstructorLimitOffsetState() {
        //given
        //when
        PageredRequest result = new PageredRequest(1,0, EObjectState.ARCHIVED);
        //then
        assertTrue(result.getLimit().isPresent());
        assertTrue(result.getOffset().isPresent());
        assertFalse(result.getSubjectUid().isPresent());
        assertEquals(EObjectState.ARCHIVED, result.getState().get());
    }

    @Test
    public void testConstructorLimitOffsetNoStateSubject() {
        //given
        //when
        PageredRequest result = new PageredRequest(1,0, null, "123");
        //then
        assertTrue(result.getLimit().isPresent());
        assertTrue(result.getOffset().isPresent());
        assertTrue(result.getSubjectUid().isPresent());
        assertEquals(result.getSubjectUid().get(), "123");
        assertFalse(result.getState().isPresent());
    }

}