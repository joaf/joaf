package net.joaf.base.core.subject;

import net.joaf.base.core.error.JoafNoSubjectContextException;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class SubjectCardDataHelperTest {

    @Test
    public void shouldCheckSubjectUidValid() throws Exception {
        //given
        //when
        SubjectCardDataHelper.checkSubjectUid("123");
        //then ok
    }

    @Test(expected = JoafNoSubjectContextException.class)
    public void shouldCheckSubjectUidInvalid() throws Exception {
        //given
        //when
        SubjectCardDataHelper.checkSubjectUid(null);
        //then ok
    }

    @Test(expected = JoafNoSubjectContextException.class)
    public void shouldCheckSubjectUidEmpty() throws Exception {
        //given
        //when
        SubjectCardDataHelper.checkSubjectUid("");
        //then ok
    }

}