package net.joaf.base.core.formmodel;

import net.joaf.base.core.error.JoafException;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class XmlFormHelperTest {

    @Test
    public void testReadFromClasspath() throws Exception {
        //given
        //when
        Form result = XmlFormHelper.readFromClasspath("/xmlformtest.xml");
        //then
        assertNotNull(result);
        assertEquals("testform", result.getId());
    }

    @Test(expected = JoafException.class)
    public void testReadFromClasspathNotExists() throws Exception {
        //given
        //when
        Form result = XmlFormHelper.readFromClasspath("/xmlformtest_not_exists.xml");
        //then exception
    }
}