package net.joaf.base.core.cqrs.commands;

import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class AbstractCommandTest {

    private static final String TEST_USER_UID = "12345";
    private TestAbstractCommand objectUnderTest;

    @Before
    public void setUp() throws Exception {
        objectUnderTest = new TestAbstractCommand(TEST_USER_UID);

    }

    @Test
    public void testGetAggregateIdGenerated() throws Exception {
        //given
        //when
        String result = objectUnderTest.getAggregateId();
        //then
        assertNotNull(result);
        assertNotNull(UUID.fromString(result));
    }

    @Test
    public void testGetAggregateId() throws Exception {
        //given
        String testuid = "123";
        objectUnderTest = new TestAbstractCommand(TEST_USER_UID, testuid);
        //when
        String result = objectUnderTest.getAggregateId();
        //then
        assertEquals(testuid, result);
    }

    @Test
    public void testSetAggregateId() throws Exception {
        //given
        String aggregateId = "123";
        //when
        objectUnderTest.setAggregateId(aggregateId);
        //then
        assertEquals(aggregateId, objectUnderTest.getAggregateId());
    }

    @Test
    public void testGetUserUid() throws Exception {
        //given
        //when
        String result = objectUnderTest.getUserUid();
        //then
        assertEquals(TEST_USER_UID, result);
    }

    private static class TestAbstractCommand extends AbstractCommand {

        public TestAbstractCommand(String userUid) {
            super(userUid);
        }

        public TestAbstractCommand(String userUid, String aggregateId) {
            super(userUid, aggregateId);
        }
    }
}