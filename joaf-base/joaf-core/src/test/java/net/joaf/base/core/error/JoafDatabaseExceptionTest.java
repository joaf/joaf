package net.joaf.base.core.error;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class JoafDatabaseExceptionTest {

    @Test
    public void testConstructorMsg() {
        //given
        //when
        JoafDatabaseException result = new JoafDatabaseException("msg");
        //then
        assertEquals("msg", result.getLocalizedMessage());
        assertNull(result.getCause());
    }

    @Test
    public void testConstructorCause() {
        //given
        //when
        JoafDatabaseException result = new JoafDatabaseException("msg", new RuntimeException("msgre"));
        //then
        assertEquals("msg", result.getLocalizedMessage());
        assertNotNull(result.getCause());
    }

}