package net.joaf.base.core.utils;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class TypeUtilsTest {

    @Test
    public void testMapToString() throws Exception {
        //given
        Map<String, String> testMap = new TreeMap<>();
        testMap.put("keyone", "valueOne");
        testMap.put("keytwo", "valueTwo");
        //when
        String result = TypeUtils.mapToString(testMap);
        //then
        assertEquals("keyone:valueOne\nkeytwo:valueTwo", result);
    }

    @Test
    public void testSetToString() throws Exception {
        //given
        Set<String> testSet = new HashSet<>();
        testSet.add("valOne");
        testSet.add("valTwo");
        //when
        String result = TypeUtils.setToString(testSet);
        //then
        assertEquals("valOne\nvalTwo", result);
    }

    @Test
    public void testStringToMap() throws Exception {
        //given
        String input = "keyOne:valOne\r\nkeytwo:ValTwo\r\n";
        Map<String, String> expected = new HashMap<>();
        expected.put("keyOne", "valOne");
        expected.put("keytwo", "ValTwo");
        //when
        Map<String, String> result = TypeUtils.stringToMap(input);
        //then
        assertEquals(expected, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testStringToMapInvalid() throws Exception {
        //given
        String input = "keyOne\nkeytwo:ValTwo\r\n";
        Map<String, String> expected = new HashMap<>();
        expected.put("keyOne", "ValOne");
        expected.put("keytwo", "ValTwo");
        //when
        Map<String, String> result = TypeUtils.stringToMap(input);
        //then
        assertEquals(expected, result);
    }

    @Test
    public void testStringToSet() throws Exception {
        //given
        String input = "valOne\r\nValTwo\r\n";
        Set<String> expected = new HashSet<>();
        expected.add("valOne");
        expected.add("ValTwo");
        //when
        Set<String> result = TypeUtils.stringToSet(input);
        //then
        assertEquals(expected, result);
    }
}