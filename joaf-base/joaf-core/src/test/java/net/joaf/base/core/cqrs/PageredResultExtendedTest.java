package net.joaf.base.core.cqrs;

import net.joaf.base.core.web.model.WrapperWDTO;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Collections;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class PageredResultExtendedTest {


    @Test
    public void testGetPageredResult() throws Exception {
        //given
        PageredResult<Integer> pageredResult = new PageredResult<>(Collections.singletonList(1), Optional.<Integer>empty(), Optional.<Integer>empty(), Optional.<Integer>empty());
        //when
        PageredResultExtended<Integer> result = new PageredResultExtended<>(pageredResult, WrapperWDTO::new);
        //then
        assertEquals(pageredResult, result.getPageredResult());
    }

    @Test
    public void testGetElements() throws Exception {
        //given
        PageredResult<Integer> pageredResult = new PageredResult<>(Collections.singletonList(1), Optional.<Integer>empty(), Optional.<Integer>empty(), Optional.<Integer>empty());
        //when
        PageredResultExtended<Integer> result = new PageredResultExtended<>(pageredResult, WrapperWDTO::new);
        //then
        assertEquals(1, result.getElements().size());
    }
}