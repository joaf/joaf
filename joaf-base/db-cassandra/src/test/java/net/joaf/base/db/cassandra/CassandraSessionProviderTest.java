package net.joaf.base.db.cassandra;

import net.joaf.base.core.config.ExternalConfigurationHelper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.BDDMockito.given;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class CassandraSessionProviderTest {

    @Mock
    private ExternalConfigurationHelper externalConfigurationHelperMock;


    @InjectMocks
    private CassandraSessionProvider objectUnderTest;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = java.lang.NullPointerException.class)
    public void testConnect() throws Exception {
        //given
        given(externalConfigurationHelperMock.getProperty("db.cassandra.node")).willReturn(null);
        given(externalConfigurationHelperMock.getProperty("db.cassandra.schema")).willReturn("schema");
        //when
        objectUnderTest.connect();
        //then
    }

    @Test
    public void testDestroy() throws Exception {
        //given
        //when
        objectUnderTest.destroy();
        //then
    }
}