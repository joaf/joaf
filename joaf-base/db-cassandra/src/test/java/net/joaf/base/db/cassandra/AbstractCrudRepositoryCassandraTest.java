package net.joaf.base.db.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.db.ERepositoryType;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.db.model.StringUidEntity;
import net.joaf.base.core.error.JoafDatabaseException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class AbstractCrudRepositoryCassandraTest {

    @Mock
    private CassandraSessionProvider sessionProvider;
    @Mock
    private Session sessionMock;
    @Mock
    private ResultSet resultSetMock;
    @Mock
    private Row rowMock;
    @InjectMocks
    private TestCrudRepositoryCassandra objectUnderTest = new TestCrudRepositoryCassandra();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindOne() throws Exception {
        //given
        UUID uuid = UUID.randomUUID();
        String uid = uuid.toString();
        given(sessionProvider.connect()).willReturn(sessionMock);
        given(sessionMock.execute("select * from testcollection where uid = ?", uuid)).willReturn(resultSetMock);
        given(resultSetMock.all()).willReturn(Collections.singletonList(rowMock));
        //when
        TestStringUidEntity one = objectUnderTest.findOne(uid);
        //then
        verify(sessionMock).execute("select * from testcollection where uid = ?", uuid);
        assertNotNull(one);
    }

    @Test
    public void testFindOneOptional() throws Exception {
        //given
        UUID uuid = UUID.randomUUID();
        String uid = uuid.toString();
        given(sessionProvider.connect()).willReturn(sessionMock);
        given(sessionMock.execute("select * from testcollection where uid = ?", uuid)).willReturn(resultSetMock);
        given(resultSetMock.all()).willReturn(Collections.singletonList(rowMock));
        //when
        Optional<TestStringUidEntity> one = objectUnderTest.findOneOptional(uid);
        //then
        verify(sessionMock).execute("select * from testcollection where uid = ?", uuid);
        assertNotNull(one);
        assertTrue(one.isPresent());
    }

    @Test
    public void testFindAll() throws Exception {
        //given
        given(sessionProvider.connect()).willReturn(sessionMock);
        given(sessionMock.execute("select * from testcollection")).willReturn(resultSetMock);
        given(resultSetMock.all()).willReturn(Collections.singletonList(rowMock));
        //when
        List<TestStringUidEntity> all = objectUnderTest.findAll();
        //then
        verify(sessionMock).execute("select * from testcollection");
        assertEquals(1, all.size());
    }

    @Test
    public void testFindAllPageredRequest() throws Exception {
        //given
        given(sessionProvider.connect()).willReturn(sessionMock);
        given(sessionMock.execute("select * from testcollection")).willReturn(resultSetMock);
        given(resultSetMock.all()).willReturn(Collections.singletonList(rowMock));
        //when
        PageredResult<TestStringUidEntity> all = objectUnderTest.findAll(new PageredRequest(1, 0));
        //then
        verify(sessionMock).execute("select * from testcollection");
        assertEquals(1, all.getResult().size());
    }

    @Test
    public void testRemove() throws Exception {
        //given
        UUID uuid = UUID.randomUUID();
        String uid = uuid.toString();
        given(sessionProvider.connect()).willReturn(sessionMock);
        //when
        objectUnderTest.remove(uid);
        //then
        verify(sessionMock).execute("delete from testcollection where uid = ?", uuid);
    }

    @Test
    public void testPrepareId() throws Exception {
        //given
        //when
        String result = objectUnderTest.prepareId();
        //then
        assertNotNull(result);
        UUID uuid = UUID.fromString(result);
        assertNotNull(uuid);
    }

    @Test
    public void testGetType() throws Exception {
        //given
        //when
        ERepositoryType result = objectUnderTest.getType();
        //then
        assertEquals(ERepositoryType.CASSANDRA, result);
    }

    @Test
    public void testUpdateObjectState() throws Exception {
        //given
        UUID uuid = UUID.randomUUID();
        String uid = uuid.toString();
        given(sessionProvider.connect()).willReturn(sessionMock);
        //when
        objectUnderTest.updateObjectState(uid, EObjectState.ACCEPTED);
        //then
        verify(sessionMock).execute("update testcollection set objectState = ? where uid = ?", EObjectState.ACCEPTED.toString(), uuid);
    }

    private static class TestStringUidEntity implements StringUidEntity, Serializable {

        private String uid;

        public TestStringUidEntity() {
            this.uid = UUID.randomUUID().toString();
        }

        @Override
        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }
    }

    private static class TestCrudRepositoryCassandra extends AbstractCrudRepositoryCassandra<TestStringUidEntity> {

        @Override
        public void store(TestStringUidEntity element) throws JoafDatabaseException {

        }

        @Override
        public String collectionName() {
            return "testcollection";
        }

        @Override
        public Class<TestStringUidEntity> getEntityClass() {
            return TestStringUidEntity.class;
        }

        @Override
        public void insert(TestStringUidEntity element) throws JoafDatabaseException {
            //
        }

        @Override
        protected TestStringUidEntity bindRow(Row row) {
            return new TestStringUidEntity();
        }
    }
}