/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.db.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.db.CrudRepository;
import net.joaf.base.core.db.ERepositoryType;
import net.joaf.base.core.db.JoafRepository;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.db.model.StringUidEntity;
import net.joaf.base.core.error.JoafDatabaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Abstract generic repository for CRUD operations on Cassandra database.
 *
 * @author Cyprian.Sniegota
 * @since 1.0
 */
@Repository
public abstract class AbstractCrudRepositoryCassandra<T extends StringUidEntity & Serializable> implements CrudRepository<T>, JoafRepository {

    protected static final String FIND_ONE_SQL = "select * from %s where uid = ?";
    protected static final String FIND_ALL_SQL = "select * from %s";
    protected static final String REMOVE_SQL = "delete from %s where uid = ?";
    protected static final String UPDATE_STATE_SQL = "update %s set objectState = ? where uid = ?";

    @Autowired(required = false)
    protected CassandraSessionProvider sessionProvider;

    @Override
    public T findOne(String uid) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        ResultSet rs = session.execute(String.format(FIND_ONE_SQL, collectionName()), UUID.fromString(uid));
        List<Row> all = rs.all();
        return all.stream().map(this::bindRow).reduce((vala, valb) -> vala).orElse(null);
    }

    @Override
    public Optional<T> findOneOptional(String uid) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        ResultSet rs = session.execute(String.format(FIND_ONE_SQL, collectionName()), UUID.fromString(uid));
        List<Row> all = rs.all();
        return all.stream().map(this::bindRow).findFirst();
    }

    @Override
    public List<T> findAll() throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        ResultSet rs = session.execute(String.format(FIND_ALL_SQL, collectionName()));
        List<Row> all = rs.all();
        return all.stream().map(this::bindRow).collect(Collectors.toList());
    }

    @Override
    public PageredResult<T> findAll(PageredRequest request) throws JoafDatabaseException {
        //TODO: pagered result
        return new PageredResult<>(findAll(), Optional.empty(), Optional.empty(), Optional.empty());
    }

    @Override
    public abstract void store(T element) throws JoafDatabaseException;

    @Override
    public abstract void insert(T element) throws JoafDatabaseException;

    @Override
    public void remove(String uid) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        session.execute(String.format(REMOVE_SQL, collectionName()), UUID.fromString(uid));
    }

    protected abstract T bindRow(Row row);

    public String prepareId() {
        return UUID.randomUUID().toString();
    }

    @Override
    public ERepositoryType getType() {
        return ERepositoryType.CASSANDRA;
    }

    @Override
    public void updateObjectState(String uid, EObjectState newState) {
        Session session = sessionProvider.connect();
        session.execute(String.format(UPDATE_STATE_SQL, collectionName()), newState.toString(), UUID.fromString(uid));
    }
}
