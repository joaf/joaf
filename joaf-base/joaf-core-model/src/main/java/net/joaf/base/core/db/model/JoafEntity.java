/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.core.db.model;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Base abstract entity
 *
 * @author Cyprian Śniegota
 * @since 1.1
 */
public abstract class JoafEntity implements Serializable, StringUidEntity {
    private String uid;

    private EObjectState objectState;
    private LocalDateTime created;
    private LocalDateTime updated;

    public EObjectState getObjectState() {
        return objectState;
    }

    public void setObjectState(EObjectState objectState) {
        this.objectState = objectState;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    @Override
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
