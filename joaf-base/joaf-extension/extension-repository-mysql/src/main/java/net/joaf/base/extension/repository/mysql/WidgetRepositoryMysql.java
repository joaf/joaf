/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.extension.repository.mysql;

import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.utils.DateUtils;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.base.extension.model.WidgetConfiguration;
import net.joaf.base.extension.repository.api.WidgetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@Repository
public class WidgetRepositoryMysql extends AbstractRepositoryMysql<WidgetConfiguration> implements WidgetRepository {

    private static final RowMapper<WidgetConfiguration> ROW_MAPPER = new WidgetRowMapper();
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    protected String getTableName() {
        return "widgetconfiguration";
    }

    @Override
    protected JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    protected WidgetConfiguration mapRow(ResultSet rs, int rowNum) throws SQLException {
        return ROW_MAPPER.mapRow(rs, rowNum);
    }

    @Deprecated
    public List<WidgetConfiguration> findAll_Old() {
        WidgetConfiguration widgetSpec;
/*
        widgetSpec = new WidgetSpecyfication();
        widgetSpec.setOrdering(0);
        widgetSpec.setScope(WidgetSpecyfication.EWidgetScope.TEMPLATE);
        widgetSpec.setUid("11");
        widgetSpec.setWidgetBeanName("subjectCardSelectorWidget");
        widgetSpec.setWidgetTargetFile("/freemarker/");
        widgetSpec.setTemplatePosition("widgets_top");
        retList.add(widgetSpec);

        widgetSpec = new WidgetSpecyfication();
        widgetSpec.setOrdering(1);
        widgetSpec.setScope(WidgetSpecyfication.EWidgetScope.TEMPLATE);
        widgetSpec.setUid("12");
        widgetSpec.setWidgetBeanName("userProfileWidget");
        widgetSpec.setWidgetTargetFile("/freemarker/");
        widgetSpec.setTemplatePosition("widgets_top");
        retList.add(widgetSpec);
*/
        widgetSpec = new WidgetConfiguration();
        widgetSpec.setOrdering(0);
        widgetSpec.setScope(WidgetConfiguration.EWidgetScope.TEMPLATE);
        widgetSpec.setUid("21");
        widgetSpec.setWidgetBeanName("mainMenuWidget");
        widgetSpec.setWidgetTargetFile("/freemarker/");
        widgetSpec.setTemplatePosition("widgets_left");
        List<WidgetConfiguration> retList = new ArrayList<>();
        retList.add(widgetSpec);

        return retList;
    }

    @Override
    public String collectionName() {
        return getTableName();
    }

    @Override
    public Class<WidgetConfiguration> getEntityClass() {
        return WidgetConfiguration.class;
    }

    @Override
    public void store(WidgetConfiguration element) throws JoafDatabaseException {
        String sql = "UPDATE " + getTableName() + " SET widgetBeanName=?, templatePosition=?, ordering=?, updated=? WHERE uid=?";
        jdbcTemplate.update(sql, element.getWidgetBeanName(), element.getTemplatePosition(), element.getOrdering(),
                DateUtils.localDateTimeToDate(element.getUpdated()), element.getUid());
    }

    @Override
    public void insert(WidgetConfiguration element) throws JoafDatabaseException {
        String sql = "INSERT INTO " + getTableName()
                + " (uid, widgetBeanName, scope, objectState, widgetTargetFile,  templatePosition, ordering, created, updated) VALUES (?,?,?,?,?,?,?,?,?)";
        Date created = DateUtils.localDateTimeToDate(element.getCreated());
        Date updated = DateUtils.localDateTimeToDate(element.getUpdated());
        jdbcTemplate.update(sql, element.getUid(), element.getWidgetBeanName(), element.getScope().toString(), element.getObjectState().toString(), element.getWidgetTargetFile(),
                element.getTemplatePosition(), element.getOrdering(), created, updated);
    }

    @Override
    public String prepareId() {
        return UUID.randomUUID().toString();
    }

    private static class WidgetRowMapper implements RowMapper<WidgetConfiguration> {

        @Override
        public WidgetConfiguration mapRow(ResultSet resultSet, int number) throws SQLException {
            WidgetConfiguration widgetConfiguration = new WidgetConfiguration();
            widgetConfiguration.setUid(resultSet.getString("uid"));
            widgetConfiguration.setOrdering(resultSet.getInt("ordering"));
            widgetConfiguration.setScope(WidgetConfiguration.EWidgetScope.valueOf(resultSet.getString("scope")));
            widgetConfiguration.setTemplatePosition(resultSet.getString("templatePosition"));
            widgetConfiguration.setWidgetBeanName(resultSet.getString("widgetBeanName"));
            widgetConfiguration.setWidgetTargetFile(resultSet.getString("widgetTargetFile"));
            widgetConfiguration.setWidgetTargetUrlRegexp(resultSet.getString("widgetTargetUrlRegexp"));
            widgetConfiguration.setObjectState(EObjectState.valueOf(resultSet.getString("objectState")));
            return widgetConfiguration;
        }
    }

}
