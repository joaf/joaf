/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.extension.web.helpers;

import net.joaf.base.core.web.model.WebActionDetails;
import net.joaf.base.extension.model.enums.EExtensionAction;
import net.joaf.base.extension.model.extensioninfo.Action;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
public class ExtensionActionHelper {
    public static List<WebActionDetails> createActionDetails(EExtensionAction action, String name, List<Action> customActions) {
        WebActionDetails webActionDetails = new WebActionDetails();
        List<WebActionDetails> retList = new ArrayList<>();
        switch (action) {
            case INSTALL:
                webActionDetails.setButtonStyle("btn-primary");
                webActionDetails.setActionUrl("/administration/extension/install/" + name + ".html");
                webActionDetails.setIconName("glyphicon-floppy-saved");
                webActionDetails.setTooltip("invoice.tooltip.createrefvat");
                retList.add(webActionDetails);
                break;
            case UPDATE:
                webActionDetails.setButtonStyle("btn-primary");
                webActionDetails.setActionUrl("/administration/extension/update/" + name + ".html");
                webActionDetails.setIconName("glyphicon-floppy-open");
                webActionDetails.setTooltip("invoice.tooltip.edit");
                retList.add(webActionDetails);
                break;
            case CUSTOM:
                for (Action customAction : customActions) {
                    webActionDetails = new WebActionDetails();
                    webActionDetails.setButtonStyle("btn-primary");
                    webActionDetails.setActionUrl(customAction.getHref());
                    webActionDetails.setIconName(customAction.getIcon());
                    webActionDetails.setTooltip(customAction.getTitle());
                    retList.add(webActionDetails);
                }

                break;
            default:
                break;
        }
        return retList;
    }
}
