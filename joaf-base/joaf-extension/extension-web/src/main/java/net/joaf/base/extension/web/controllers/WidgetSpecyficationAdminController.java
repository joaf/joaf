/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.extension.web.controllers;

import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.cqrs.PageredResultExtended;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.base.core.web.model.WrapperWDTO;
import net.joaf.base.extension.ExtensionExtension;
import net.joaf.base.extension.commands.*;
import net.joaf.base.extension.commands.handlers.CancelWidgetConfigurationCommandHandler;
import net.joaf.base.extension.commands.handlers.SaveWidgetConfigurationCommandHandler;
import net.joaf.base.extension.commands.handlers.TrashWidgetConfigurationCommandHandler;
import net.joaf.base.extension.commands.handlers.UndoTrashWidgetConfigurationCommandHandler;
import net.joaf.base.extension.model.WidgetConfiguration;
import net.joaf.base.extension.queries.WidgetQuery;
import net.joaf.base.extension.web.helpers.WidgetConfigurationActionHelper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Collections;
import java.util.Optional;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@Controller
@RequestMapping(ExtensionExtension.WIDGET_ADMIN_BASE_PATH)
public class WidgetSpecyficationAdminController {

    private static final Logger log = LoggerFactory.getLogger(WidgetSpecyficationAdminController.class);

    @Autowired
    private WidgetQuery query;

    @Autowired
    private CommandGateway commandGateway;

    @RequestMapping
    public String list(HttpSession session, Model model) {
        try {
            PageredRequest pageredRequest = new PageredRequest();
            PageredResult<WidgetConfiguration> all = query.findAll(pageredRequest);
            PageredResultExtended<WidgetConfiguration> allExt = new PageredResultExtended<>(all, this::createActions);
            model.addAttribute("elements", allExt.getElements());
        } catch (Exception e) {
            model.addAttribute("elements", Collections.emptyList());
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            log.debug("error", e);
        }
        return getViewTemplate("/list");
    }

    @RequestMapping("/create")
    public String create(HttpSession session, RedirectAttributes redirectAttributes, Principal principal, Model model) {
        try {
            WidgetConfiguration command = (WidgetConfiguration) commandGateway
                    .send(new CreateWidgetConfigurationCommand(principal.getName()), SaveWidgetConfigurationCommandHandler.class);
            return getRedirect("/edit/" + command.getUid());
        } catch (Exception e) {
            log.debug("error", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return getRedirect("");
        }
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable("id") String id, HttpSession session, RedirectAttributes redirectAttributes, Model model) {
        try {
            WidgetConfiguration command = query.findOne(id);
            model.addAttribute("command", command);
            return getViewTemplate("/edit");
        } catch (Exception e) {
            log.debug("error", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return getRedirect("");
        }
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String saveItem(@Valid @ModelAttribute("command") WidgetConfiguration command, BindingResult bindingResult, @PathVariable("id") String id,
                           HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) {
        try {
            SaveWidgetConfigurationCommand commandSave = new SaveWidgetConfigurationCommand(command, principal.getName());
            commandGateway.validate(bindingResult, commandSave);
            if (bindingResult.hasErrors()) {
                return getViewTemplate("/edit");
            }
            commandGateway.send(commandSave, SaveWidgetConfigurationCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successsave");
            return getRedirect("");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return getViewTemplate("/edit");
        }
    }

    @RequestMapping("/trash/{uid}")
    public String trash(@PathVariable("uid") String uid, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes,
                        Principal principal, Model model) {
        try {
            commandGateway.send(new TrashWidgetConfigurationCommand(uid, principal.getName()), TrashWidgetConfigurationCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successtrash",
                    httpRequest.getContextPath() + ExtensionExtension.WIDGET_ADMIN_BASE_PATH + "/undotrash/" + uid + ".html");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    @RequestMapping("/undotrash/{id}")
    public String undotrash(@PathVariable("id") String id, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes,
                            Principal principal, Model model) {
        try {
            commandGateway.send(new UndoTrashWidgetConfigurationCommand(id, principal.getName()), UndoTrashWidgetConfigurationCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successundotrash", "1");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    @RequestMapping(value = "/cancel/{uid}", method = RequestMethod.POST)
    public String cancel(@PathVariable("uid") String uid, HttpSession session, Principal principal, Model model) {
        try {
            commandGateway.send(new CancelWidgetConfigurationCommand(uid, principal.getName()), CancelWidgetConfigurationCommandHandler.class);
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    private void createActions(WrapperWDTO<WidgetConfiguration> element) {
        element.getData().getActions().forEach(action -> element.getWebActions().add(WidgetConfigurationActionHelper.createActionDetails(action, element.getData().getUid())));
    }

    private String getRedirect(String subPath) {
        return "redirect:" + ExtensionExtension.WIDGET_ADMIN_BASE_PATH + "" + subPath + ".html";
    }

    private String getViewTemplate(String subPath) {
        return "freemarker" + ExtensionExtension.WIDGET_ADMIN_BASE_PATH + StringUtils.trimToEmpty(subPath);
    }

}
