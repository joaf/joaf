<#import "/templates/base.ftl" as page />
<@page.layout "net.joaf.main.extension.widget.modulename">


<div class="awidget full-width">
    <div class="panel">
        <div class="btn-group">
            <a href="${rc.contextPath}/administration/widgetspecyfication/create.html" class="btn btn-primary">Create</a>
        </div>
        <div class="btn-group">
        ${action_buttons!}
        </div>
        <div class="pull-right">
            <a href="${rc.contextPath}/administration.html" class="btn btn-primary"><i
                    class="fa fa-gear fa-fw"></i> <@spring.message 'joaf.back' /></a>
        </div>
        <table class="table table-bordered admin-media ">
            <thead>
            <tr>
                <th>Bean</th>
                <th>Scope</th>
                <th>Uid</th>
                <th>Control</th>
            </tr>
            </thead>
            <tbody>

                <#list elements as element>
                <tr>
                    <td>${element.data.widgetBeanName}</td>
                    <td>${element.data.scope}</td>
                    <td>${element.data.uid}</td>
                    <td>
                        <#list element.webActions as action>
                            <a href="${rc.contextPath}${action.actionUrl}" class="btn btn-xs ${action.buttonStyle}"
                               data-toggle="tooltip" data-placement="bottom"
                               title="<@spring.message action.tooltip />"><span aria-hidden="true" class="glyphicon ${action.iconName}"></span></a>
                        </#list>
                    </td>
                </tr>
                </#list>
            </tbody>
        </table>
    </div>

</div>
</@page.layout>