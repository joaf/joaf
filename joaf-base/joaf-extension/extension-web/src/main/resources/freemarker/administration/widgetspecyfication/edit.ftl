<#import "/templates/base.ftl" as page />
<@page.layout "net.joaf.mainextensions.menu.modulename">
<div class="awidget full-width">
    <div class="awidget-head">
        <div class="btn-group">
        <#--<a href="/remoteconnections/edit/0.html" class="btn btn-primary">Create</a>-->
            <#--<a href="/invoice/syncInfakt.html" class="btn btn-info">Infakt sync</a>-->
        <#--<button class="btn btn-success">Center</button>-->
        <#--<button class="btn btn-primary">Right</button>-->
        </div>
    </div>
    <div class="awidget-body">
        <form class="form-horizontal" role="form" method="post"
              action="${rc.contextPath}/administration/widgetspecyfication/edit/${command.uid}.html">
            <div class="form-group">
                <label class="col-lg-2 control-label">Bean name</label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.widgetBeanName' 'placeholder="Bean name" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">Template position</label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.templatePosition' 'placeholder="Template position" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">Ordering</label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.ordering' 'placeholder="Ordering" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>

            <@spring.formHiddenInput 'command.uid' '' />
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="button"
                    onclick="$(this.form).attr('action','${rc.contextPath}/administration/widgetspecyfication/cancel/${command.uid}.html');$(this.form).submit();"
                    class="btn btn-default">Cancel
            </button>
        </form>
    </div>

</div>
</@page.layout>