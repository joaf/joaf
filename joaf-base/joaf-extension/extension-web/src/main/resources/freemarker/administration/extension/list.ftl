<#import "/templates/base.ftl" as page />
<@page.layout "net.joaf.main.extension.modulename">


<div class="awidget full-width">
    <div class="panel">
        <div class="">
            <div class="btn-group">
            ${action_buttons!}
            </div>
            <div class="pull-right"><a href="${rc.contextPath}/administration.html"
                                       class="btn btn-primary"><i
                    class="fa fa-gear fa-fw"></i> <@spring.message 'joaf.back' /></a></div>
        </div>
        <table class="table table-bordered admin-media ">
            <thead>
            <tr>
                <th>Name</th>
                <th>Version</th>
                <th>CodeVersion</th>
                <th>Control</th>
            </tr>
            </thead>
            <tbody>

                <#list elements as element>
                <tr>
                    <td>${element.data.name}</td>
                    <td>${element.data.version}</td>
                    <td>${element.data.codeVersion}</td>
                    <td>
                        <div class="hidden-table-cell">
                            <#list element.webActions as action>
                                <a href="${rc.contextPath}${action.actionUrl}" class="btn btn-xs ${action.buttonStyle}"
                                   data-toggle="tooltip" data-placement="bottom"
                                   title=""><span class="glyphicon ${action.iconName}" aria-hidden="true"> </span></a>
                            </#list>
                        </div>
                    </td>
                </tr>
                </#list>
            </tbody>
        </table>
    </div>

</div>
</@page.layout>