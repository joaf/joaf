/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.extension.repository.cassandra;

import com.datastax.driver.core.*;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.cassandra.AbstractCrudRepositoryCassandra;
import net.joaf.base.db.cassandra.RepositoryCassandra;
import net.joaf.base.extension.model.Extension;
import net.joaf.base.extension.repository.api.ExtensionRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@Repository
public class ExtensionRepositoryCassandra extends AbstractCrudRepositoryCassandra<Extension> implements ExtensionRepository, RepositoryCassandra {

    @Override
    public Integer findCountExtensionByName(String name) {
        Session session = sessionProvider.connect();
        ResultSet rs = session.execute("SELECT * FROM extension WHERE name = ?;", name);
        return rs.all().size();
    }

    @Override
    public void insertByValue(String name, String version) {
        Session session = sessionProvider.connect();
        PreparedStatement statement = session.prepare("INSERT INTO extension (uid, name, version) VALUES (?, ?, ?);");
        BoundStatement boundStatement = new BoundStatement(statement);
        session.execute(boundStatement.bind(UUID.randomUUID(), name, version));
    }

    @Override
    public List<Extension> findAll() throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        ResultSet rs = session.execute("SELECT * FROM extension;");
        List<Row> all = rs.all();
        return all.stream().map(this::mapRow).collect(Collectors.toList());
    }

    @Override
    public String collectionName() {
        return "extension";
    }

    @Override
    public Class<Extension> getEntityClass() {
        return Extension.class;
    }

    @Override
    public void store(Extension element) throws JoafDatabaseException {
        throw new IllegalStateException("Not implemented");
    }

    @Override
    public void insert(Extension element) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        PreparedStatement statement = session.prepare("INSERT INTO extension (uid, name, version) "
                + "VALUES (?, ?, ?);");
        BoundStatement boundStatement = new BoundStatement(statement);
        session.execute(boundStatement.bind(UUID.fromString(element.getUid()), element.getName(), element.getVersion()));
    }

    @Override
    protected Extension bindRow(Row row) {
        throw new IllegalStateException("Not implemented");
    }

    @Override
    public Extension findByName(String extensionName) throws JoafDatabaseException {
        try {
            Session session = sessionProvider.connect();
            ResultSet rs = session.execute("SELECT * FROM extension WHERE name = ?;", extensionName);
            // return first found element
            return rs.all().stream().map(this::mapRow).reduce((paramLeft, paramRight) -> paramLeft).orElse(null);
        } catch (Exception e) {
            throw new JoafDatabaseException("database error", e);
        }
    }

    @Override
    public boolean tableExists() {
        ResultSet resultSet = sessionProvider.connect()
                .execute("SELECT columnfamily_name FROM system.schema_columnfamilies WHERE columnfamily_name='extension' allow filtering;");
        return resultSet.iterator().hasNext();
    }

    private boolean schemaExists() {
        ResultSet describeKeyspace = sessionProvider.connect().execute("SELECT * from system.schema_keyspaces where keyspace_name = 'joaf';");
        return describeKeyspace.iterator().hasNext();
    }

    private Extension mapRow(Row row) {
        Extension extension = new Extension();
        extension.setUid(row.getUUID("uid").toString());
        extension.setName(row.getString("name"));
        extension.setVersion(row.getString("version"));
        return extension;
    }

    @Override
    public void runCommands(List<String> commands, String type) {
        if ("cassandra".equals(type)) {
            Session session = sessionProvider.connect();
            for (String command : commands) {
                try {
                    session.execute(command);
                } catch (Exception e) {
                    System.out.println("Błąd wywołania: [" + command + "]");
                    System.out.println(e.getLocalizedMessage());
                }
            }
        }
    }

    @Override
    public void updateVersion(String name, String number) throws JoafDatabaseException {
        Session session = sessionProvider.connect();
        Extension byName = this.findByName(name);
        if (byName == null) {
            Extension extension = new Extension();
            extension.setUid(UUID.randomUUID().toString());
            extension.setName(name);
            extension.setVersion(number);
            this.insert(extension);
        } else {
            String query = "UPDATE extension SET version = ? WHERE uid = ?";
            session.execute(query, number, UUID.fromString(byName.getUid()));
        }
    }

    @Override
    public boolean isInstalled() {
        return this.schemaExists() && this.tableExists();
    }
}
