/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.extension.helpers;

import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.extension.Widget;
import net.joaf.base.extension.model.WidgetConfiguration;
import net.joaf.base.extension.repository.api.WidgetRepository;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@Component
public class WidgetHelper implements ApplicationContextAware {

    ApplicationContext applicationContext;
    @Autowired
    private WidgetRepository repository;

    public List<Pair<Widget, WidgetConfiguration>> findForUserAndSubject(String username, String subjectId) throws JoafDatabaseException {
        List<WidgetConfiguration> all = repository.findAll().stream().filter(x -> EObjectState.ACCEPTED.equals(x.getObjectState()))
                .collect(Collectors.toList());
        List<Pair<Widget, WidgetConfiguration>> retList = new ArrayList<>();
        for (WidgetConfiguration widgetConfiguration : all) {
            Widget widget = this.applicationContext.getBean(widgetConfiguration.getWidgetBeanName(), Widget.class);
            if (widget != null && widget.availableForUserAndSubject(username, subjectId)) {
                retList.add(new ImmutablePair<>(widget, widgetConfiguration));
            }
        }
        return retList;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
