/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.extension.interceptors;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.session.SessionContext;
import net.joaf.base.extension.Widget;
import net.joaf.base.extension.helpers.WidgetHelper;
import net.joaf.base.extension.model.WidgetConfiguration;
import net.joaf.base.extension.model.WidgetMAV;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;
import org.springframework.web.servlet.view.AbstractTemplateView;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.security.Principal;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@Component
public class WidgetsInterceptor implements HandlerInterceptor, ApplicationContextAware {

    @Autowired
    private WidgetHelper helper;

    @Autowired
    private FreeMarkerConfigurer freeMarkerConfigurer;

    private ApplicationContext applicationContext;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        if (request.getPathInfo().contains(".css") || request.getPathInfo().contains(".js") || request.getPathInfo().contains(".png")) {
            return;
        }
        if (modelAndView == null || modelAndView.getViewName() == null) {
            return;
        }
        if (modelAndView.getViewName().startsWith("redirect:")) {
            return;
        }
        SessionContext sessionContext = SessionContext.loadFromSession(request.getSession());
        String subjectId = null;
        if (sessionContext != null) {
            subjectId = sessionContext.getSubjectCardDataId();
        }
        Principal userPrincipal = request.getUserPrincipal();
        String username = null;
        if (userPrincipal != null) {
            username = userPrincipal.getName();
        }
        //org.springframework.web.servlet.view.AbstractTemplateView
        WebApplicationContext webApplicationContext;
        if (this.applicationContext instanceof WebApplicationContext) {
            webApplicationContext = (WebApplicationContext) applicationContext;
        } else {
            throw new IllegalArgumentException("applicationContext should be webApplicationContext");
        }
        try {
            List<Pair<Widget, WidgetConfiguration>> forUserAndSubject = helper.findForUserAndSubject(username, subjectId);
            Map<String, Object> model = modelAndView.getModel();
            for (Pair<Widget, WidgetConfiguration> widget : forUserAndSubject) {
                WidgetMAV mav = widget.getLeft().process(model, request, response);
                if (mav.getView() == null) {
                    //skip rendering
                    continue;
                }
                //populate with original model
                Map<String, Object> dataModel = mav.getModel();
                dataModel.putAll(model);
                RequestContext requestContext = new RequestContext(request, response, webApplicationContext.getServletContext(), mav.getModel());
                this.renderMavToMainModel(widget.getRight().getTemplatePosition(), mav, model, requestContext);
            }
        } catch (JoafDatabaseException e) {
            e.printStackTrace();
        }
    }

    private void renderMavToMainModel(String templatePosition, WidgetMAV mav, Map<String, Object> mainModel, RequestContext requestContext) {
        try {
            Configuration configuration = freeMarkerConfigurer.getConfiguration();
            Template template = configuration.getTemplate(mav.getView());
            // Expose RequestContext instance for Spring macros, and rc variable
            Map<String, Object> dataModel = mav.getModel();
            dataModel.put(AbstractTemplateView.SPRING_MACRO_REQUEST_CONTEXT_ATTRIBUTE, requestContext);
            dataModel.put("rc", requestContext);

            CharArrayWriter writer = new CharArrayWriter();
            template.process(dataModel, writer);
            String data = new String(writer.toCharArray());
            List<String> topWidgets = (List<String>) mainModel.get(templatePosition);
            if (topWidgets == null) {
                topWidgets = new LinkedList<>();
                mainModel.put(templatePosition, topWidgets);
            }
            topWidgets.add(data);
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
