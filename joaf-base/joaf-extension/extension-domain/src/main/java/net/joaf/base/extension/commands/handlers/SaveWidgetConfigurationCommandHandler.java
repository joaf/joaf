/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.extension.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.base.extension.Widget;
import net.joaf.base.extension.commands.SaveWidgetConfigurationCommand;
import net.joaf.base.extension.model.WidgetConfiguration;
import net.joaf.base.extension.repository.api.WidgetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.time.LocalDateTime;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class SaveWidgetConfigurationCommandHandler implements CommandHandler<SaveWidgetConfigurationCommand, Object> {
    @Autowired
    private WidgetRepository repository;

    @Autowired
    private ApplicationContext context;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, SaveWidgetConfigurationCommand command) throws JoafException {
        if (command.getElement().getWidgetBeanName() == null || command.getElement().getWidgetBeanName().trim().equals("")) {
            originalBindingResult.addError(new FieldError("command", "widgetBeanName", "Incorrect bean " + command.getElement().getWidgetBeanName()));
        } else if (!context.containsBean(command.getElement().getWidgetBeanName())) {
            originalBindingResult.addError(new FieldError("command", "widgetBeanName", "Incorrect bean " + command.getElement().getWidgetBeanName()));
        } else if (!(context.getBean(command.getElement().getWidgetBeanName()) instanceof Widget)) {
            originalBindingResult.addError(new FieldError("command", "widgetBeanName", "Incorrect bean " + command.getElement().getWidgetBeanName()));
        }
        return originalBindingResult;
    }

    @Override
    public Object execute(SaveWidgetConfigurationCommand command) throws JoafException {
        WidgetConfiguration dbObject = repository.findOne(command.getElement().getUid());
        if (EObjectState.NEW.equals(dbObject.getObjectState())) {
            repository.updateObjectState(command.getElement().getUid(), EObjectState.ACCEPTED);
        }
        command.getElement().setUpdated(LocalDateTime.now());
        repository.store(command.getElement());
        return null;
    }
}
