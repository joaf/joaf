/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.extension.interceptors;

import net.joaf.base.core.session.SessionContext;
import net.joaf.base.extension.helpers.WidgetHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@Component
public class TemplatesInterceptor implements HandlerInterceptor, ApplicationContextAware {

    @Autowired
    private WidgetHelper helper;

    @Autowired
    private FreeMarkerConfigurer freeMarkerConfigurer;

    private ApplicationContext applicationContext;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        if (request.getPathInfo().contains(".css") || request.getPathInfo().contains(".js") || request.getPathInfo().contains(".png") || request.getPathInfo()
                .contains("assets")) {
            return;
        }
        if (modelAndView == null || modelAndView.getViewName() == null) {
            return;
        }
        if (modelAndView.getViewName().startsWith("redirect:")) {
            return;
        }
        SessionContext sessionContext = SessionContext.loadFromSession(request.getSession());
        String templateName = "template";
        if (sessionContext != null && StringUtils.trimToNull(sessionContext.getTemplateName()) != null) {
            templateName = sessionContext.getTemplateName();
        }
        if (!modelAndView.getModel().containsKey("templateName")) {
            modelAndView.getModel().put("templateName", templateName);
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
