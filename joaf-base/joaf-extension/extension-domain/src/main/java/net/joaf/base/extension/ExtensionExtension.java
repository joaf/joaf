/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.extension;

import net.joaf.base.core.error.JoafDatabaseException;
import org.springframework.stereotype.Component;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@Component
public class ExtensionExtension extends AbstractExtension implements JoafExtension {
    public static final String EXTENSION_BASE_PATH = "/";
    public static final String EXTENSION_ADMIN_BASE_PATH = "/administration/extension";
    public static final String WIDGET_ADMIN_BASE_PATH = "/administration/widgetspecyfication";

    @Override
    public String getExtensionMetadataFile() {
        return "/extension-extension.xml";
    }

    public void fullInstall() throws JoafDatabaseException {
        installAgainstVersion(null);
    }
}
