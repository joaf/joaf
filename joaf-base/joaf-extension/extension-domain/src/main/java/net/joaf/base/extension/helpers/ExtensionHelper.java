/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.extension.helpers;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.extension.model.Extension;
import net.joaf.base.extension.model.extensioninfo.ExtensionMetadata;
import net.joaf.base.extension.model.extensioninfo.Version;
import net.joaf.base.extension.repository.api.ExtensionRepository;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@Component
public class ExtensionHelper {

    @Autowired
    private ExtensionRepository repository;

    public int compareVersions(String version1, String version2) {
        DefaultArtifactVersion v1 = new DefaultArtifactVersion(version1);
        DefaultArtifactVersion v2 = new DefaultArtifactVersion(version2);
        return v1.compareTo(v2);
    }

    public Extension findExtension(String name) throws JoafDatabaseException {
        return repository.findByName(name);
    }

    public void setRepository(ExtensionRepository repository) {
        this.repository = repository;
    }

    public ExtensionMetadata readExtensionMetadata(InputStream stream) throws IOException, JAXBException, XMLStreamException {
        XMLInputFactory xif = XMLInputFactory.newFactory();
        StreamSource xml = new StreamSource(stream);
        XMLStreamReader xsr = xif.createXMLStreamReader(xml);

        JAXBContext jaxbContext = JAXBContext.newInstance(ExtensionMetadata.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        return unmarshaller.unmarshal(xsr, ExtensionMetadata.class).getValue();
    }

    public void executeUpdate(List<String> commands, String type) {
        repository.runCommands(commands, type);
    }

    public void updateVersion(String name, String number) throws JoafDatabaseException {
        repository.updateVersion(name, number);
    }

    public Optional<Version> findMaxVersion(ExtensionMetadata extensionMetadata) {
        if (extensionMetadata == null || extensionMetadata.getVersions() == null) {
            return Optional.empty();
        }
        return extensionMetadata.getVersions().getVersion().stream().max((o1, o2) -> compareVersions(o1.getNumber(), o2.getNumber()));
    }
}
