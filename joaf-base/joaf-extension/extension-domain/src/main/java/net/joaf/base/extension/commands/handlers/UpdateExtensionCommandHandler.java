/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.extension.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.error.JoafException;
import net.joaf.base.extension.commands.UpdateExtensionCommand;
import net.joaf.base.extension.services.ExtensionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class UpdateExtensionCommandHandler implements CommandHandler<UpdateExtensionCommand, Object> {

    @Autowired
    private ExtensionService extensionService;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, UpdateExtensionCommand command) throws JoafException {
        if (StringUtils.isEmpty(command.getName())) {
            originalBindingResult.addError(new FieldError("command", "", ""));
        }
        return originalBindingResult;
    }

    @Override
    public Object execute(UpdateExtensionCommand command) throws JoafException {
        extensionService.install(command.getName());
        return null;
    }
}
