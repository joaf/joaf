/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.extension.dataproviders;

import net.joaf.base.extension.model.Extension;
import net.joaf.base.extension.repository.api.ExtensionRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @author Cyprian.Sniegota
 * @since 1.1
 */
public class ExtensionDataProviderTest {

    @Mock
    private ExtensionRepository repository;

    @InjectMocks
    private ExtensionDataProvider objectUnderTest = new ExtensionDataProvider();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindAll() throws Exception {
        //given
        when(repository.findAll()).thenReturn(createDbResponse());
        //when
        List<Extension> all = objectUnderTest.findAll();
        //then
        assertEquals(all.size(), 2);
    }

    private List<Extension> createDbResponse() {
        List<Extension> retList = new ArrayList<>();
        Extension extension = new Extension();
        extension.setUid("");
        extension.setName("x1");
        extension.setVersion("1.0.0");
        retList.add(extension);
        extension = new Extension();
        extension.setUid("");
        extension.setName("x2");
        extension.setVersion("1.0.1");
        retList.add(extension);
        return retList;
    }
}