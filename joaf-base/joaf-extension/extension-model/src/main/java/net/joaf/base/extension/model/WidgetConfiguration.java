/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.extension.model;

import com.google.common.collect.Maps;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.db.model.StringUidEntity;
import net.joaf.base.core.model.enums.EStandardAction;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
public class WidgetConfiguration implements Serializable, StringUidEntity {
    private static final long serialVersionUID = 4434781021234528132L;
    private String uid;
    private String widgetBeanName;
    private EWidgetScope scope;
    private String widgetTargetFile;
    private String widgetTargetUrlRegexp;
    private String templatePosition;
    private Integer ordering;
    private LocalDateTime created;
    private LocalDateTime updated;
    private Map<String, String> config = Maps.newHashMap();
    private EObjectState objectState;
    private List<EStandardAction> actions = new ArrayList<>();

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getWidgetBeanName() {
        return widgetBeanName;
    }

    public void setWidgetBeanName(String widgetBeanName) {
        this.widgetBeanName = widgetBeanName;
    }

    public EWidgetScope getScope() {
        return scope;
    }

    public void setScope(EWidgetScope scope) {
        this.scope = scope;
    }

    public String getWidgetTargetFile() {
        return widgetTargetFile;
    }

    public void setWidgetTargetFile(String widgetTargetFile) {
        this.widgetTargetFile = widgetTargetFile;
    }

    public String getWidgetTargetUrlRegexp() {
        return widgetTargetUrlRegexp;
    }

    public void setWidgetTargetUrlRegexp(String widgetTargetUrlRegexp) {
        this.widgetTargetUrlRegexp = widgetTargetUrlRegexp;
    }

    public Integer getOrdering() {
        return ordering;
    }

    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }

    public String getTemplatePosition() {
        return templatePosition;
    }

    public void setTemplatePosition(String templatePosition) {
        this.templatePosition = templatePosition;
    }

    public List<EStandardAction> getActions() {
        return actions;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    public Map<String, String> getConfig() {
        return config;
    }

    public void setConfig(Map<String, String> config) {
        this.config = config;
    }

    public EObjectState getObjectState() {
        return objectState;
    }

    public void setObjectState(EObjectState objectState) {
        this.objectState = objectState;
    }

    public enum EWidgetScope {
        TEMPLATE, EXTENSION
    }
}
