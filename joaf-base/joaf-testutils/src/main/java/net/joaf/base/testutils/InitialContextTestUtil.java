package net.joaf.base.testutils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;
import javax.naming.spi.InitialContextFactoryBuilder;
import javax.naming.spi.NamingManager;
import java.util.Hashtable;

/**
 * Initial Context test util. Helps
 *
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class InitialContextTestUtil {
    private static final InitialContextTestUtil INSTANCE = new InitialContextTestUtil();

    private final TestInitialContext initialContext;

    public InitialContextTestUtil() {
        TestInitialContext initialContextLocal = null;
        try {
            initialContextLocal = new TestInitialContext();
        } catch (NamingException e) {
            e.printStackTrace();
        }
        initialContext = initialContextLocal;
        if (!NamingManager.hasInitialContextFactoryBuilder()) {
            try {
                TestInitialContextFactory testInitialContextFactory = new TestInitialContextFactory(initialContext);
                NamingManager.setInitialContextFactoryBuilder(new TestInitialContextFactoryBuilder(testInitialContextFactory));
            } catch (NamingException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("[WARNING] Not initialized properly");
        }
    }

    public static InitialContextTestUtil getInstance() {
        return INSTANCE;
    }

    public TestInitialContext getInitialContext() {
        return initialContext;
    }

    public static class TestInitialContextFactoryBuilder implements InitialContextFactoryBuilder {

        private final TestInitialContextFactory initialContextFactory;

        public TestInitialContextFactoryBuilder(TestInitialContextFactory testInitialContextFactory) {
            this.initialContextFactory = testInitialContextFactory;
        }

        @Override
        public InitialContextFactory createInitialContextFactory(Hashtable<?, ?> environment) throws NamingException {
            return initialContextFactory;
        }
    }

    public static class TestInitialContextFactory implements InitialContextFactory {

        private final Context context;

        public TestInitialContextFactory(TestInitialContext initialContext) {
            this.context = initialContext;
        }

        @Override
        public synchronized Context getInitialContext(Hashtable<?, ?> environment) throws NamingException {
            return context;
        }
    }

    public static class TestInitialContext extends InitialContext {
        private Context mockContext;

        public TestInitialContext() throws NamingException {}

        @Override
        public Object lookup(String name) throws NamingException {
            return mockContext.lookup(name);
        }

        public Context getMockContext() {
            return mockContext;
        }

        public void setMockContext(Context mockContext) {
            this.mockContext = mockContext;
        }
    }
}
