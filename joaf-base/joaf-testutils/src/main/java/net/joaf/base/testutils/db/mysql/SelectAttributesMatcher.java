package net.joaf.base.testutils.db.mysql;

import org.hamcrest.Description;
import org.mockito.ArgumentMatcher;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.internal.matchers.VarargMatcher;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import static org.mockito.Matchers.argThat;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class SelectAttributesMatcher {

    private static final String WHERE_KEYWORD = "where";

    public static void validSelectQuery(JdbcTemplate jdbcTemplateMock) {
        SqlArgumentsCheckData sqlArgumentsCheckData = new SqlArgumentsCheckData();
        Mockito.verify(jdbcTemplateMock).query(argThat(new QuerySqlArgumentMatcher<String>(sqlArgumentsCheckData)), (Object[]) argThat(new QueryArgsArgumentMatcher<>(sqlArgumentsCheckData)), Matchers.<RowMapper<Object>>anyObject());
    }

    public static void validSelectQueryForObject(JdbcTemplate jdbcTemplateMock) {
        SqlArgumentsCheckData sqlArgumentsCheckData = new SqlArgumentsCheckData(false);
        Mockito.verify(jdbcTemplateMock).queryForObject(argThat(new QuerySqlArgumentMatcher<String>(sqlArgumentsCheckData)), (Object[]) argThat(new QueryArgsArgumentMatcher<>(sqlArgumentsCheckData)), Matchers.<RowMapper<Object>>anyObject());
    }

    public static void validSelectNoParamsQuery(JdbcTemplate jdbcTemplateMock) {
        SqlArgumentsCheckData sqlArgumentsCheckData = new SqlArgumentsCheckData(false);
        Mockito.verify(jdbcTemplateMock).query(argThat(new QuerySqlArgumentMatcher<String>(sqlArgumentsCheckData)), (Object[]) argThat(new QueryArgsArgumentMatcher<>(sqlArgumentsCheckData)), Matchers.<RowMapper<Object>>anyObject());
    }

    private static class QuerySqlArgumentMatcher<T> extends ArgumentMatcher<T> {

        private SqlArgumentsCheckData sqlArgumentsCheckData;
        private String errorMessage = "";

        public QuerySqlArgumentMatcher(SqlArgumentsCheckData sqlArgumentsCheckData) {
            this.sqlArgumentsCheckData = sqlArgumentsCheckData;
        }

        @Override
        public boolean matches(Object argument) {
            sqlArgumentsCheckData.clear();
            sqlArgumentsCheckData.countQmarkBefore((String) argument, WHERE_KEYWORD);
            sqlArgumentsCheckData.countCommaBefore((String) argument, WHERE_KEYWORD);
            sqlArgumentsCheckData.countQmark((String) argument);
            sqlArgumentsCheckData.countComma((String) argument);
            boolean qmarkCountTest;
            if (sqlArgumentsCheckData.isFailIfNotFound()) {
                qmarkCountTest = sqlArgumentsCheckData.getQmarkCount() > 0;
            } else {
                qmarkCountTest = sqlArgumentsCheckData.getQmarkCount() >= 0;
            }
            boolean whereTest = sqlArgumentsCheckData.getQmarkCount() == 0 && !sqlArgumentsCheckData.isFailIfNotFound() || sqlArgumentsCheckData.stringExists((String) argument, WHERE_KEYWORD);

            if (!qmarkCountTest) {
                this.errorMessage = " No question marks in query!";
            } else if (!whereTest) {
                this.errorMessage = " Update statement should contain WHERE keyword";
            }
            return qmarkCountTest && whereTest;
        }

        @Override
        public void describeTo(Description description) {
            super.describeTo(description);
            description.appendText(this.errorMessage);
        }
    }

    private static class QueryArgsArgumentMatcher<T> extends ArgumentMatcher<T> implements VarargMatcher {

        private SqlArgumentsCheckData sqlArgumentsCheckData;
        private String errorMessage = "";

        public QueryArgsArgumentMatcher(SqlArgumentsCheckData sqlArgumentsCheckData) {
            this.sqlArgumentsCheckData = sqlArgumentsCheckData;
        }

        @Override
        public boolean matches(Object argument) {
            Object[] objs = (Object[]) argument;
            boolean numberOfArgsTest = objs.length == sqlArgumentsCheckData.getQmarkCount();
            if (!numberOfArgsTest) {
                this.errorMessage = " Number of arguments doesn't match number of question marks in query";
            }
            return numberOfArgsTest;
        }

        @Override
        public void describeTo(Description description) {
            super.describeTo(description);
            description.appendText(this.errorMessage);
        }
    }
}
