package net.joaf.base.testutils.db.mysql;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Shared data for Attribute Machers
 *
 * @author Cyprian.Sniegota
 * @since 1.2
 */
final class SqlArgumentsCheckData {
    private final boolean failIfNotFound;
    private int qmarkCount = 0;
    private int commaCount = 0;
    private int qmarkBeforeCount = 0;
    private int commaBeforeCount = 0;
    private Pattern patternQmark = Pattern.compile("(\\?)");
    private Pattern patternComma = Pattern.compile("(\\,)");

    public SqlArgumentsCheckData() {
        this(true);
    }

    public SqlArgumentsCheckData(boolean failIfNotFound) {
        this.failIfNotFound = failIfNotFound;
    }

    private void incQmarkCount() {
        qmarkCount++;
    }

    private void incCommaCount() {
        commaCount++;
    }

    private void incQmarkBeforeCount() {
        qmarkBeforeCount++;
    }

    private void incCommaBeforeCount() {
        commaBeforeCount++;
    }

    public void countQmark(String argument) {
        Matcher matcher = patternQmark.matcher(argument);
        while (matcher.find()) {
            this.incQmarkCount();
        }
    }

    public void countQmarkBefore(String argument, String keyword) {
        Matcher matcher = patternQmark.matcher(cutString(argument, keyword));
        while (matcher.find()) {
            this.incQmarkBeforeCount();
        }
    }

    public void countComma(String argument) {
        Matcher matcher = patternComma.matcher(argument);
        while (matcher.find()) {
            this.incCommaCount();
        }
    }

    public void countCommaBefore(String argument, String keyword) {
        Matcher matcher = patternComma.matcher(cutString(argument, keyword));
        while (matcher.find()) {
            this.incCommaBeforeCount();
        }
    }


    private String cutString(String argument, String keyword) {
        int endIndex = argument.toLowerCase().indexOf(keyword.toLowerCase());
        if (endIndex >= 0) {
            return argument.substring(0, endIndex);
        } else {
            return argument;
        }
    }

    public boolean stringExists(String argument, String search) {
        return argument.toLowerCase().contains(search.toLowerCase());
    }

    public int getCommaCount() {
        return commaCount;
    }

    public int getQmarkCount() {
        return qmarkCount;
    }

    public int getQmarkBeforeCount() {
        return qmarkBeforeCount;
    }

    public int getCommaBeforeCount() {
        return commaBeforeCount;
    }

    public boolean isFailIfNotFound() {
        return failIfNotFound;
    }

    public void clear() {
        this.qmarkCount = 0;
        this.commaCount = 0;
        this.commaBeforeCount = 0;
        this.qmarkBeforeCount = 0;
    }
}
