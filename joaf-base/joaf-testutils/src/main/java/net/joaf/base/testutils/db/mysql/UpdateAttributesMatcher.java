package net.joaf.base.testutils.db.mysql;

import org.hamcrest.Description;
import org.mockito.ArgumentMatcher;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.internal.matchers.VarargMatcher;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Validates jdbc insert query: number of question marks, commas and update attributes
 *
 * @author Cyprian Śniegota
 * @since 1.2
 */
public class UpdateAttributesMatcher {

    private static final String WHERE_KEYWORD = "where";

    public static void validUpdateQuery(JdbcTemplate jdbcTemplateMock) {
        SqlArgumentsCheckData sqlArgumentsCheckData = new SqlArgumentsCheckData();
        Mockito.verify(jdbcTemplateMock).update(Matchers.argThat(new UpdateSqlArgumentMatcher<String>(sqlArgumentsCheckData)), (Object[]) Matchers.argThat(new UpdateArgsArgumentMatcher(sqlArgumentsCheckData)));
    }

    private static class UpdateSqlArgumentMatcher<T> extends ArgumentMatcher<T> {

        private SqlArgumentsCheckData sqlArgumentsCheckData;
        private String errorMessage = "";

        public UpdateSqlArgumentMatcher(SqlArgumentsCheckData sqlArgumentsCheckData) {
            this.sqlArgumentsCheckData = sqlArgumentsCheckData;
        }

        @Override
        public boolean matches(Object argument) {
            sqlArgumentsCheckData.clear();
            sqlArgumentsCheckData.countQmarkBefore((String) argument, WHERE_KEYWORD);
            sqlArgumentsCheckData.countCommaBefore((String) argument, WHERE_KEYWORD);
            sqlArgumentsCheckData.countQmark((String) argument);
            sqlArgumentsCheckData.countComma((String) argument);
            boolean qmarkCountTest = sqlArgumentsCheckData.getQmarkCount() > 0;
            boolean qmarkCommaTest = sqlArgumentsCheckData.getCommaBeforeCount() == (sqlArgumentsCheckData.getQmarkBeforeCount() - 1);
            boolean whereTest = sqlArgumentsCheckData.stringExists((String) argument, WHERE_KEYWORD);
            if (!qmarkCountTest) {
                this.errorMessage = " No question marks in query!";
            } else if (!qmarkCommaTest) {
                this.errorMessage = " Incorrect number of Question Marks vs Commas";
            } else if (!whereTest) {
                this.errorMessage = " Update statement should contain WHERE keyword";
            }
            return qmarkCountTest && qmarkCommaTest && whereTest;
        }

        @Override
        public void describeTo(Description description) {
            super.describeTo(description);
            description.appendText(this.errorMessage);
        }
    }

    private static class UpdateArgsArgumentMatcher<T> extends ArgumentMatcher<T> implements VarargMatcher {

        private SqlArgumentsCheckData sqlArgumentsCheckData;
        private String errorMessage;

        public UpdateArgsArgumentMatcher(SqlArgumentsCheckData sqlArgumentsCheckData) {
            this.sqlArgumentsCheckData = sqlArgumentsCheckData;
        }

        @Override
        public boolean matches(Object argument) {
            Object[] objs = (Object[]) argument;
            boolean numberOfArgsTest = objs.length == sqlArgumentsCheckData.getQmarkCount();
            if (!numberOfArgsTest) {
                this.errorMessage = " Number of arguments doesn't match number of question marks in query";
            }
            return numberOfArgsTest;
        }

        @Override
        public void describeTo(Description description) {
            super.describeTo(description);
            description.appendText(this.errorMessage);
        }
    }
}
