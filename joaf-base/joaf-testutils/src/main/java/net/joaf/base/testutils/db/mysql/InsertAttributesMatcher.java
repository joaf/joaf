package net.joaf.base.testutils.db.mysql;

import org.hamcrest.Description;
import org.mockito.ArgumentMatcher;
import org.mockito.internal.matchers.VarargMatcher;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;

/**
 * Validates jdbc insert query: number of question marks, commas and update attributes
 *
 * @author Cyprian Śniegota
 * @since 1.2
 */
public class InsertAttributesMatcher {

    private InsertSqlArgumentMatcher<String> insertSqlArgumentMatcher;
    private InsertArgsArgumentMatcher<Object[]> insertArgsArgumentMatcher;

    public static void validInsertQuery(JdbcTemplate jdbcTemplateMock) {
        SqlArgumentsCheckData sqlArgumentsCheckData = new SqlArgumentsCheckData();
        verify(jdbcTemplateMock).update(argThat(new InsertSqlArgumentMatcher<>(sqlArgumentsCheckData)), argThat(new InsertArgsArgumentMatcher<Object[]>(sqlArgumentsCheckData)));
    }

    private static class InsertSqlArgumentMatcher<T> extends ArgumentMatcher<T> {

        private SqlArgumentsCheckData sqlArgumentsCheckData;

        private String errorMessage = "";

        public InsertSqlArgumentMatcher(SqlArgumentsCheckData sqlArgumentsCheckData) {
            this.sqlArgumentsCheckData = sqlArgumentsCheckData;
        }

        @Override
        public boolean matches(Object argument) {
            sqlArgumentsCheckData.clear();
            sqlArgumentsCheckData.countQmark((String) argument);
            sqlArgumentsCheckData.countComma((String) argument);

            boolean qmarkCountTest = sqlArgumentsCheckData.getQmarkCount() > 0;
            boolean qmarkCommaTest = sqlArgumentsCheckData.getCommaCount() == (sqlArgumentsCheckData.getQmarkCount() - 1) * 2;
            if (!qmarkCountTest) {
                this.errorMessage = " No question marks in query!";
            } else if (!qmarkCommaTest) {
                this.errorMessage = " Incorrect number of Question Marks vs Commas";
            }
            return qmarkCountTest && qmarkCommaTest;
        }

        @Override
        public void describeTo(Description description) {
            super.describeTo(description);
            description.appendText(this.errorMessage);
        }
    }

    private static class InsertArgsArgumentMatcher<T> extends ArgumentMatcher<T> implements VarargMatcher {

        private SqlArgumentsCheckData sqlArgumentsCheckData;
        private String errorMessage;

        public InsertArgsArgumentMatcher(SqlArgumentsCheckData sqlArgumentsCheckData) {
            this.sqlArgumentsCheckData = sqlArgumentsCheckData;
        }

        @Override
        public boolean matches(Object argument) {
            Object[] objs = (Object[]) argument;
            boolean numberOfArgsTest = objs.length == sqlArgumentsCheckData.getQmarkCount();
            if (!numberOfArgsTest) {
                this.errorMessage = " Number of arguments doesn't match number of question marks in query";
            }
            return numberOfArgsTest;
        }

        @Override
        public void describeTo(Description description) {
            super.describeTo(description);
            description.appendText(this.errorMessage);
        }
    }
}
