package net.joaf.base.testutils.db.mysql;

import org.hamcrest.Description;
import org.mockito.ArgumentMatcher;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.internal.matchers.VarargMatcher;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class DeleteAttributesMatcher {
    private static final String WHERE_KEYWORD = "where";

    public static void validDeleteQuery(JdbcTemplate jdbcTemplateMock) {
        SqlArgumentsCheckData sqlArgumentsCheckData = new SqlArgumentsCheckData();
        Mockito.verify(jdbcTemplateMock).update(Matchers.argThat(new DeleteSqlArgumentMatcher<String>(sqlArgumentsCheckData)), (Object[]) Matchers.argThat(new DeleteArgsArgumentMatcher(sqlArgumentsCheckData)));
    }

    private static class DeleteSqlArgumentMatcher<T> extends ArgumentMatcher<T> {

        private SqlArgumentsCheckData sqlArgumentsCheckData;
        private String errorMessage = "";

        public DeleteSqlArgumentMatcher(SqlArgumentsCheckData sqlArgumentsCheckData) {
            this.sqlArgumentsCheckData = sqlArgumentsCheckData;
        }

        @Override
        public boolean matches(Object argument) {
            sqlArgumentsCheckData.clear();
            sqlArgumentsCheckData.countQmarkBefore((String) argument, WHERE_KEYWORD);
            sqlArgumentsCheckData.countCommaBefore((String) argument, WHERE_KEYWORD);
            sqlArgumentsCheckData.countQmark((String) argument);
            sqlArgumentsCheckData.countComma((String) argument);
            boolean qmarkCountTest = sqlArgumentsCheckData.getQmarkCount() > 0;
            boolean qmarkCommaTest = (sqlArgumentsCheckData.getCommaBeforeCount() == 0) && (sqlArgumentsCheckData.getQmarkBeforeCount() == 0);
            boolean whereTest = sqlArgumentsCheckData.stringExists((String) argument, WHERE_KEYWORD);
            if (!qmarkCountTest) {
                this.errorMessage = " No question marks in query!";
            } else if (!qmarkCommaTest) {
                this.errorMessage = " Incorrect number of Question Marks vs Commas";
            } else if (!whereTest) {
                this.errorMessage = " Update statement should contain WHERE keyword";
            }
            return qmarkCountTest && qmarkCommaTest && whereTest;
        }

        @Override
        public void describeTo(Description description) {
            super.describeTo(description);
            description.appendText(this.errorMessage);
        }
    }

    private static class DeleteArgsArgumentMatcher<T> extends ArgumentMatcher<T> implements VarargMatcher {

        private SqlArgumentsCheckData sqlArgumentsCheckData;
        private String errorMessage;

        public DeleteArgsArgumentMatcher(SqlArgumentsCheckData sqlArgumentsCheckData) {
            this.sqlArgumentsCheckData = sqlArgumentsCheckData;
        }

        @Override
        public boolean matches(Object argument) {
            boolean numberOfArgsTest = sqlArgumentsCheckData.getQmarkCount() == 1;
            if (!numberOfArgsTest) {
                this.errorMessage = " Number of arguments doesn't match number of question marks in query (expected 1)";
            }
            return numberOfArgsTest;
        }

        @Override
        public void describeTo(Description description) {
            super.describeTo(description);
            description.appendText(this.errorMessage);
        }
    }
}
