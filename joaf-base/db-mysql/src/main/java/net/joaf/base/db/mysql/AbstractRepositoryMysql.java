/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.db.mysql;

import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.db.CrudRepository;
import net.joaf.base.core.db.ERepositoryType;
import net.joaf.base.core.db.JoafRepository;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.db.model.StringUidEntity;
import net.joaf.base.core.error.JoafDatabaseException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Abstract generic repository for Mysql
 *
 * @author cyprian
 * @since 1.1
 */
public abstract class AbstractRepositoryMysql<T extends StringUidEntity & Serializable> implements JoafRepository, CrudRepository<T> {

    protected static final String TABLE_NAME = "{tableName}";
    protected static final String COLUMN_NAME = "{columnName}";
    private static final String FIND_ALL = "SELECT * FROM {tableName}";
    private static final String FIND_ONE = "SELECT * FROM {tableName} WHERE uid = ?";
    private static final String FIND_BY_FIELD = "SELECT * FROM {tableName} WHERE {columnName} = ?";
    private static final String REMOVE_ONE = "DELETE FROM {tableName} WHERE uid = ?";
    private static final String LIMIT_1 = " LIMIT 1";
    private static final String LIMIT_X = " LIMIT %d";
    private static final String OFFSET_X = " OFFSET %d";
    private static final String UPDATE_OBJECTSTATE = "UPDATE {tableName} SET objectState = ? where uid = ?";

    protected abstract String getTableName();

    protected abstract JdbcTemplate getJdbcTemplate();

    protected abstract T mapRow(ResultSet rs, int rowNum) throws SQLException;

    protected String tableQuery(String query) {
        return query.replace(TABLE_NAME, getTableName());
    }

    private String fieldQuery(String query, String fieldName) {
        return query.replace(COLUMN_NAME, fieldName);
    }

    public List<T> findAll() throws JoafDatabaseException {
        try {
            return getJdbcTemplate().query(tableQuery(FIND_ALL), new Object[]{}, this::mapRow);
        } catch (Exception e) {
            throw createDatabaseException(e);
        }
    }

    @Override
    public PageredResult<T> findAll(PageredRequest request) throws JoafDatabaseException {
        try {
            StringBuilder query = new StringBuilder(FIND_ALL);
            Map<String, String> andWhere = new HashMap<>();
            if (request.getState().isPresent()) {
                andWhere.put("objectState", request.getState().get().toString());
            }
            if (request.getSubjectUid().isPresent()) {
                andWhere.put("subjectUid", request.getSubjectUid().get());
            }
            query.append(" WHERE TRUE ");
            andWhere.forEach((vala, valb) -> query.append(" AND ").append(vala).append("=? "));
            if (request.getLimit().isPresent()) {
                query.append(String.format(LIMIT_X, request.getLimit().get()));
            }
            if (request.getOffset().isPresent()) {
                query.append(String.format(OFFSET_X, request.getOffset().get()));
            }
            return new PageredResult<>(getJdbcTemplate().query(tableQuery(query.toString()), andWhere.values().toArray(), this::mapRow), Optional.empty(),
                    request.getLimit(), request.getOffset());
        } catch (Exception e) {
            throw createDatabaseException(e);
        }
    }

    public T findOne(String uid) throws JoafDatabaseException {
        try {
            return getJdbcTemplate().queryForObject(tableQuery(FIND_ONE), new Object[]{uid}, this::mapRow);
        } catch (Exception e) {
            throw createDatabaseException(e);
        }
    }

    public Optional<T> findOneOptional(String uid) throws JoafDatabaseException {
        try {
            return getJdbcTemplate().query(tableQuery(FIND_ONE + LIMIT_1), new Object[]{uid}, this::mapRow).stream().findFirst();
        } catch (Exception e) {
            throw createDatabaseException(e);
        }
    }

    public void remove(String uid) throws JoafDatabaseException {
        try {
            getJdbcTemplate().update(tableQuery(REMOVE_ONE), uid);
        } catch (Exception e) {
            throw createDatabaseException(e);
        }

    }

    protected Optional<T> findOneByFieldOptional(String fieldName, String fieldValue) throws JoafDatabaseException {
        try {
            return findListByField(fieldName, fieldValue).stream().findFirst();
        } catch (Exception e) {
            throw createDatabaseException(e);
        }
    }

    protected List<T> findListByField(String fieldName, String fieldValue) throws JoafDatabaseException {
        try {
            return getJdbcTemplate().query(fieldQuery(tableQuery(FIND_BY_FIELD), fieldName), new Object[]{fieldValue}, this::mapRow);
        } catch (Exception e) {
            throw createDatabaseException(e);
        }
    }

    @Override
    public void updateObjectState(String uid, EObjectState newState) throws JoafDatabaseException {
        try {
            getJdbcTemplate().update(tableQuery(UPDATE_OBJECTSTATE), newState.toString(), uid);
        } catch (Exception e) {
            throw createDatabaseException(e);
        }
    }

    protected JoafDatabaseException createDatabaseException(Throwable throwable) {
        return new JoafDatabaseException("Database error", throwable);
    }

    @Override
    public ERepositoryType getType() {
        return ERepositoryType.MYSQL;
    }

    public String prepareId() {
        return UUID.randomUUID().toString();
    }

    public String collectionName() {
        return getTableName();
    }
}
