package net.joaf.base.db.mysql;

import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.db.ERepositoryType;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.db.model.StringUidEntity;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.testutils.db.mysql.DeleteAttributesMatcher;
import net.joaf.base.testutils.db.mysql.SelectAttributesMatcher;
import net.joaf.base.testutils.db.mysql.UpdateAttributesMatcher;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.init.UncategorizedScriptException;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class AbstractRepositoryMysqlTest {

    @InjectMocks
    private TestAbstractRepositoryMysql objectUnderTest = new TestAbstractRepositoryMysql();
    @Mock
    private JdbcTemplate jdbcTemplateMock;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindAll() throws Exception {
        //given
        given(jdbcTemplateMock.query(anyString(), Matchers.<Object[]>anyObject(), Matchers.<RowMapper<Object>>anyObject())).willReturn(Collections.singletonList(new TestEntity()));
        //when
        List<TestEntity> all = objectUnderTest.findAll();
        //then
        assertEquals(1, all.size());
        SelectAttributesMatcher.validSelectNoParamsQuery(jdbcTemplateMock);
    }

    @Test(expected = JoafDatabaseException.class)
    public void testFindAllException() throws Exception {
        //given
        given(jdbcTemplateMock.query(anyString(), Matchers.<Object[]>anyObject(), Matchers.<RowMapper<Object>>anyObject())).willThrow(new UncategorizedScriptException("test exception"));
        //when
        objectUnderTest.findAll();
        //then exception
    }

    @Test
    public void testFindAllPagered() throws Exception {
        //given
        given(jdbcTemplateMock.query(anyString(), Matchers.<Object[]>anyObject(), Matchers.<RowMapper<Object>>anyObject())).willReturn(Collections.singletonList(new TestEntity()));
        //when
        PageredResult<TestEntity> result = objectUnderTest.findAll(new PageredRequest());
        //then
        assertNotNull(result);
        assertEquals(1, result.getResult().size());
        SelectAttributesMatcher.validSelectNoParamsQuery(jdbcTemplateMock);
    }

    @Test(expected = JoafDatabaseException.class)
    public void testFindAllPageredException() throws Exception {
        //given
        given(jdbcTemplateMock.query(anyString(), Matchers.<Object[]>anyObject(), Matchers.<RowMapper<Object>>anyObject())).willThrow(new UncategorizedScriptException("test exception"));
        //when
        objectUnderTest.findAll(new PageredRequest());
        //then exception
    }

    @Test
    public void testFindAllPageredStateFilter() throws Exception {
        //given
        ArgumentCaptor<String> queryCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Object[]> paramsCaptor = ArgumentCaptor.forClass(Object[].class);
        given(jdbcTemplateMock.query(queryCaptor.capture(), paramsCaptor.capture(), Matchers.<RowMapper<Object>>anyObject())).willReturn(Collections.singletonList(new TestEntity()));
        //when
        PageredResult<TestEntity> result = objectUnderTest.findAll(new PageredRequest(EObjectState.ACCEPTED));
        //then
        assertNotNull(result);
        assertEquals(1, result.getResult().size());
        assertTrue(queryCaptor.getValue().contains("objectState=?"));
        List<Object> params = Arrays.asList(paramsCaptor.getValue());
        assertTrue(params.contains(EObjectState.ACCEPTED.name()));
        SelectAttributesMatcher.validSelectQuery(jdbcTemplateMock);
    }

    @Test
    public void testFindAllPageredSubjectFilter() throws Exception {
        //given
        ArgumentCaptor<String> queryCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Object[]> paramsCaptor = ArgumentCaptor.forClass(Object[].class);
        given(jdbcTemplateMock.query(queryCaptor.capture(), paramsCaptor.capture(), Matchers.<RowMapper<Object>>anyObject())).willReturn(Collections.singletonList(new TestEntity()));
        //when
        PageredResult<TestEntity> result = objectUnderTest.findAll(new PageredRequest(null, null, EObjectState.ACCEPTED, "subUID"));
        //then
        assertNotNull(result);
        assertEquals(1, result.getResult().size());
        assertTrue(queryCaptor.getValue().contains("subjectUid=?"));
        List<Object> params = Arrays.asList(paramsCaptor.getValue());
        assertTrue(params.contains("subUID"));
        SelectAttributesMatcher.validSelectQuery(jdbcTemplateMock);
    }

    @Test
    public void testFindAllPageredLimitAndOffset() throws Exception {
        //given
        ArgumentCaptor<String> queryCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Object[]> paramsCaptor = ArgumentCaptor.forClass(Object[].class);
        given(jdbcTemplateMock.query(queryCaptor.capture(), paramsCaptor.capture(), Matchers.<RowMapper<Object>>anyObject())).willReturn(Collections.singletonList(new TestEntity()));
        //when
        PageredResult<TestEntity> result = objectUnderTest.findAll(new PageredRequest(1, 0, null, "subUID"));
        //then
        assertNotNull(result);
        assertEquals(1, result.getResult().size());
        System.out.printf(queryCaptor.getValue());
        assertTrue(queryCaptor.getValue().contains(" LIMIT 1 OFFSET 0"));
        List<Object> params = Arrays.asList(paramsCaptor.getValue());
        SelectAttributesMatcher.validSelectQuery(jdbcTemplateMock);
    }

    @Test
    public void testFindOne() throws Exception {
        //given
        UUID uuid = UUID.randomUUID();
        String uid = uuid.toString();
        ArgumentCaptor<String> queryCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Object[]> paramsCaptor = ArgumentCaptor.forClass(Object[].class);
        given(jdbcTemplateMock.queryForObject(queryCaptor.capture(), paramsCaptor.capture(), Matchers.<RowMapper<Object>>anyObject())).willReturn(new TestEntity());
        //when
        TestEntity one = objectUnderTest.findOne(uid);
        //then
        assertNotNull(one);
        assertEquals("SELECT * FROM test WHERE uid = ?", queryCaptor.getValue());
        assertEquals(Arrays.asList(new Object[]{uid}), Arrays.asList(paramsCaptor.getValue()));
        SelectAttributesMatcher.validSelectQueryForObject(jdbcTemplateMock);
    }

    @Test(expected = JoafDatabaseException.class)
    public void testFindOneException() throws Exception {
        //given
        UUID uuid = UUID.randomUUID();
        String uid = uuid.toString();
        ArgumentCaptor<String> queryCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Object[]> paramsCaptor = ArgumentCaptor.forClass(Object[].class);
        given(jdbcTemplateMock.queryForObject(queryCaptor.capture(), paramsCaptor.capture(), Matchers.<RowMapper<Object>>anyObject())).willThrow(new UncategorizedScriptException("test exception"));
        //when
        objectUnderTest.findOne(uid);
        //then exception
    }

    @Test
    public void testFindOneOptional() throws Exception {
        //given
        UUID uuid = UUID.randomUUID();
        String uid = uuid.toString();
        ArgumentCaptor<String> queryCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Object[]> paramsCaptor = ArgumentCaptor.forClass(Object[].class);
        given(jdbcTemplateMock.query(queryCaptor.capture(), paramsCaptor.capture(), Matchers.<RowMapper<Object>>anyObject())).willReturn(Collections.singletonList(new TestEntity()));
        //when
        Optional<TestEntity> result = objectUnderTest.findOneOptional(uid);
        //then
        assertNotNull(result);
        assertTrue(result.isPresent());
        assertEquals("SELECT * FROM test WHERE uid = ? LIMIT 1", queryCaptor.getValue());
        assertEquals(Arrays.asList(new Object[]{uid}), Arrays.asList(paramsCaptor.getValue()));
        SelectAttributesMatcher.validSelectQuery(jdbcTemplateMock);
    }

    @Test(expected = JoafDatabaseException.class)
    public void testFindOneOptionalException() throws Exception {
        //given
        UUID uuid = UUID.randomUUID();
        String uid = uuid.toString();
        ArgumentCaptor<String> queryCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Object[]> paramsCaptor = ArgumentCaptor.forClass(Object[].class);
        given(jdbcTemplateMock.query(queryCaptor.capture(), paramsCaptor.capture(), Matchers.<RowMapper<Object>>anyObject())).willThrow(new UncategorizedScriptException("test exception"));
        //when
        objectUnderTest.findOneOptional(uid);
        //then exception
    }

    @Test
    public void testRemove() throws Exception {
        //given
        UUID uuid = UUID.randomUUID();
        String uid = uuid.toString();
        //when
        objectUnderTest.remove(uid);
        //then
        DeleteAttributesMatcher.validDeleteQuery(jdbcTemplateMock);
    }

    @Test(expected = JoafDatabaseException.class)
    public void testRemoveException() throws Exception {
        //given
        UUID uuid = UUID.randomUUID();
        String uid = uuid.toString();
        given(jdbcTemplateMock.update(anyString(), Matchers.<Object>anyVararg())).willThrow(new UncategorizedScriptException("test exception"));
        //when
        objectUnderTest.remove(uid);
        //then exception
    }

    @Test
    public void testUpdateObjectState() throws Exception {
        //given
        UUID uuid = UUID.randomUUID();
        String uid = uuid.toString();
        //when
        objectUnderTest.updateObjectState(uid, EObjectState.ACCEPTED);
        //then
        UpdateAttributesMatcher.validUpdateQuery(jdbcTemplateMock);
    }

    @Test(expected = JoafDatabaseException.class)
    public void testUpdateObjectStateException() throws Exception {
        //given
        UUID uuid = UUID.randomUUID();
        String uid = uuid.toString();
        given(jdbcTemplateMock.update(anyString(), Matchers.<Object>anyVararg())).willThrow(new UncategorizedScriptException("test exception"));
        //when
        objectUnderTest.updateObjectState(uid, EObjectState.ACCEPTED);
        //then exception
    }

    @Test
    public void testFindOneByFieldTest() throws JoafDatabaseException {
        //given
        UUID uuid = UUID.randomUUID();
        String uid = uuid.toString();
        ArgumentCaptor<String> queryCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Object[]> paramsCaptor = ArgumentCaptor.forClass(Object[].class);
        given(jdbcTemplateMock.query(queryCaptor.capture(), paramsCaptor.capture(), Matchers.<RowMapper<Object>>anyObject())).willReturn(Collections.singletonList(new TestEntity()));
        //when
        Optional<TestEntity> result = objectUnderTest.findOneOptionalByUidFieldForTest(uid);
        //then
        assertNotNull(result);
        assertTrue(result.isPresent());
        assertEquals("SELECT * FROM test WHERE uid = ?", queryCaptor.getValue());
        assertEquals(Arrays.asList(new Object[]{uid}), Arrays.asList(paramsCaptor.getValue()));
        SelectAttributesMatcher.validSelectQuery(jdbcTemplateMock);
    }

    @Test(expected = JoafDatabaseException.class)
    public void testFindOneByFieldTestException() throws Exception {
        //given
        UUID uuid = UUID.randomUUID();
        String uid = uuid.toString();
        ArgumentCaptor<String> queryCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Object[]> paramsCaptor = ArgumentCaptor.forClass(Object[].class);
        given(jdbcTemplateMock.query(queryCaptor.capture(), paramsCaptor.capture(), Matchers.<RowMapper<Object>>anyObject())).willThrow(new UncategorizedScriptException("test exception"));
        //when
        objectUnderTest.findOneOptionalByUidFieldForTest(uid);
        //then exception
    }


    @Test
    public void testGetType() throws Exception {
        //given
        //when
        ERepositoryType result = objectUnderTest.getType();
        //then
        assertEquals(ERepositoryType.MYSQL, result);
    }

    @Test
    public void testCollectionName() throws Exception {
        //given
        //when
        String result = objectUnderTest.collectionName();
        //then
        assertEquals("test", result);
    }

    @Test
    public void testPrepareId() {
        //given
        //when
        String result = objectUnderTest.prepareId();
        //then
        assertNotNull(result);
        assertEquals(36, result.length());
    }

    private static class TestEntity implements StringUidEntity, Serializable {

        @Override
        public String getUid() {
            return null;
        }
    }

    private class TestAbstractRepositoryMysql extends AbstractRepositoryMysql<TestEntity> {

        @Override
        protected String getTableName() {
            return "test";
        }

        @Override
        protected JdbcTemplate getJdbcTemplate() {
            return jdbcTemplateMock;
        }

        @Override
        protected TestEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new TestEntity();
        }

        @Override
        public void insert(TestEntity element) throws JoafDatabaseException {

        }

        @Override
        public void store(TestEntity element) throws JoafDatabaseException {

        }

        @Override
        public Class<TestEntity> getEntityClass() {
            return null;
        }

        public Optional<TestEntity> findOneOptionalByUidFieldForTest(String value) throws JoafDatabaseException {
            return this.findOneByFieldOptional("uid", value);
        }

        public List<TestEntity> findListByUidFieldForTest(String value) throws JoafDatabaseException {
            return this.findListByField("uid", value);
        }
    }
}
