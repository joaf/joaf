/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.language.repository.mysql;

import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.utils.DateUtils;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.base.language.model.LanguageDefinition;
import net.joaf.base.language.repository.api.LanguageDefinitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@Repository
public class LanguageDefinitionRepositoryMysql extends AbstractRepositoryMysql<LanguageDefinition> implements LanguageDefinitionRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    protected String getTableName() {
        return "language";
    }

    @Override
    protected JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    protected LanguageDefinition mapRow(ResultSet rs, int rowNum) throws SQLException {
        LanguageDefinition element = new LanguageDefinition();
        element.setUid(rs.getString("uid"));
        element.setName(rs.getString("name"));
        element.setOriginalName(rs.getString("originalName"));
        element.setCode(rs.getString("code"));
        element.setCreated(DateUtils.dateToLocalDateTime(rs.getDate("created")));
        element.setUpdated(DateUtils.dateToLocalDateTime(rs.getDate("updated")));
        element.setObjectState(EObjectState.valueOf(rs.getString("objectState")));
        return element;
    }

    @Override
    public void insert(LanguageDefinition element) throws JoafDatabaseException {
        try {
            String sql = "INSERT INTO " + getTableName() + " (uid, name, originalName, code, created, updated, objectState) VALUES (?,?,?,?,?,?,?)";
            jdbcTemplate.update(sql, element.getUid(), element.getName(), element.getOriginalName(), element.getCode(),
                    DateUtils.localDateTimeToDate(element.getCreated()),
                    DateUtils.localDateTimeToDate(element.getUpdated()), element.getObjectState().toString());
        } catch (Exception e) {
            throw new JoafDatabaseException("Repository error", e);
        }
    }

    @Override
    public void store(LanguageDefinition element) throws JoafDatabaseException {
        try {
            String sql = "UPDATE " + getTableName() + " SET name=?, originalName=?, code=?, updated=? where uid=?";
            jdbcTemplate.update(sql, element.getName(), element.getOriginalName(), element.getCode(), DateUtils.localDateTimeToDate(element.getUpdated()),
                    element.getUid());
        } catch (Exception e) {
            throw new JoafDatabaseException("Repository error", e);
        }
    }

    public void updateState(String uid, EObjectState newState) throws JoafDatabaseException {
        try {
            String sql = "UPDATE " + getTableName() + " SET objectState=? where uid=?";
            jdbcTemplate.update(sql, newState.toString(), uid);
        } catch (Exception e) {
            throw new JoafDatabaseException("Repository error", e);
        }
    }

    @Override
    public String collectionName() {
        return getTableName();
    }

    @Override
    public Class<LanguageDefinition> getEntityClass() {
        return LanguageDefinition.class;
    }

    @Override
    public String prepareId() {
        return UUID.randomUUID().toString();
    }
}
