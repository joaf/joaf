/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.language.model;

import net.joaf.base.core.db.model.JoafEntity;
import net.joaf.base.core.db.model.StringUidEntity;
import net.joaf.base.core.model.enums.EStandardAction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
public class LanguageDefinition extends JoafEntity implements Serializable, StringUidEntity {

    private String name;
    private String originalName;
    private String code;
    private List<EStandardAction> actions = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<EStandardAction> getActions() {
        return actions;
    }

}
