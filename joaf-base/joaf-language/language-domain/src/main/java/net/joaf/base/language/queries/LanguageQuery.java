/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.language.queries;

import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.model.enums.EStandardAction;
import net.joaf.base.language.model.LanguageDefinition;
import net.joaf.base.language.repository.api.LanguageDefinitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@Service
public class LanguageQuery {

    @Autowired
    private LanguageDefinitionRepository repository;

    public PageredResult<LanguageDefinition> findAll(PageredRequest request) throws JoafDatabaseException {
        PageredResult<LanguageDefinition> all = repository.findAll(request);
        all.getResult().forEach(this::appendActions);
        return all;
    }

    public Map<String, String> findNameMap() throws JoafDatabaseException {
        PageredResult<LanguageDefinition> all = repository.findAll(new PageredRequest(EObjectState.ACCEPTED));
        return all.getResult().stream().collect(Collectors.toMap(LanguageDefinition::getCode, LanguageDefinition::getName));
    }

    public LanguageDefinition findOne(String uid) throws JoafDatabaseException {
        return repository.findOne(uid);
    }

    private void appendActions(LanguageDefinition item) {
        item.getActions().add(EStandardAction.EDIT);
        item.getActions().add(EStandardAction.TRASH);
    }
}
