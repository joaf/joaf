/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.language.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.base.language.commands.CreateLanguageCommand;
import net.joaf.base.language.model.LanguageDefinition;
import net.joaf.base.language.repository.api.LanguageDefinitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import java.time.LocalDateTime;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@CommandHandlerComponent
public class CreateLanguageCommandHandler implements CommandHandler<CreateLanguageCommand, Object> {

    @Autowired
    private LanguageDefinitionRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, CreateLanguageCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(CreateLanguageCommand command) throws JoafException {
        LanguageDefinition language = new LanguageDefinition();
        language.setUid(repository.prepareId());
        language.setObjectState(EObjectState.NEW);
        LocalDateTime now = LocalDateTime.now();
        language.setCreated(now);
        language.setUpdated(now);
        language.setCode("");
        language.setName("");
        language.setOriginalName("");
        repository.insert(language);
        return language;
    }
}
