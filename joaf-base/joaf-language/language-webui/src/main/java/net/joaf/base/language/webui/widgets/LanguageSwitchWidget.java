/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package net.joaf.base.language.webui.widgets;

import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.db.model.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.extension.Widget;
import net.joaf.base.extension.exceptions.WidgetException;
import net.joaf.base.extension.model.WidgetMAV;
import net.joaf.base.language.model.LanguageDefinition;
import net.joaf.base.language.queries.LanguageQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author Cyprian Śniegota
 * @since 1.1
 */
@Component
public class LanguageSwitchWidget implements Widget {

    private final Logger logger = LoggerFactory.getLogger(LanguageSwitchWidget.class);
    private static final String form = "/forms/mainmenuwidget/mainmenuwidget.xml";

    @Autowired
    private LanguageQuery query;

    @Override
    public boolean availableForUserAndSubject(String username, String subjectId) {
        return true;
    }

    @Override
    public WidgetMAV process(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws WidgetException {
        String fileName = "freemarker/widget/widgetSwitch.ftl";
        WidgetMAV widgetMav = new WidgetMAV(fileName);
        Map<String, Object> dataModel = widgetMav.getModel();

        List<LanguageDefinition> menuElements = Collections.emptyList();
        try {
            menuElements = this.getMenuElements(request);
        } catch (JoafDatabaseException e) {
            e.printStackTrace();
        }
        dataModel.put("joaf_languages", menuElements);

        return widgetMav;
    }

    @Override
    public String getForm() {
        return form;
    }

    private List<LanguageDefinition> getMenuElements(HttpServletRequest request) throws JoafDatabaseException {
        return query.findAll(new PageredRequest(EObjectState.ACCEPTED)).getResult();
    }

}
