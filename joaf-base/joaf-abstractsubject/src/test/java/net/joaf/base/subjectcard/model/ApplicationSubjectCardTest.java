package net.joaf.base.subjectcard.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Cyprian Sniegota
 * @since 1.4
 */
public class ApplicationSubjectCardTest {

    @Test
    public void testAppName() {
        //when
        String appUid = ApplicationSubjectCard.APP_UID;
        //then
        assertEquals("app", appUid);
    }

}