package net.joaf.base.subjectcard.model;

/**
 * Application subject card
 *
 * @author Cyprian Śniegota
 * @since 1.2
 */
public final class ApplicationSubjectCard {

    /**
     * Default uid for application subject card (use this instead of creating subject card by administrator to use extension in application scope).
     */
    public static final String APP_UID = "app";

    private ApplicationSubjectCard() {
    }
}
